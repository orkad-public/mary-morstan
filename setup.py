#!/usr/bin/env python

import pathlib
import pkg_resources
import setuptools

with pathlib.Path('requirements.txt').open() as requirements_txt:
    requirements = [
        str(requirement)
        for requirement
        in pkg_resources.parse_requirements(requirements_txt)
    ]


setuptools.setup(
    name='mary-morstan',
    version='0.20.0',
    author='Parmentier Laurent',
    author_email='laurent.parmentier@corp.ovh.com',
    packages=setuptools.find_packages(),
    scripts=['bin/mary-morstan'],
    url='https://gitlab.parmentier.io/thesis/mary-morstan',
    description='An adaptive MO-AutoML',
    install_requires=requirements,
    setup_requires=['flake8'],
    include_package_data=True,
    package_data={'marymorstan': ['search_space.yml', 'ea_space.yml'],
                  'marymorstan.evolutionary_algorithms': ['simple_ea_space.yml']}
)
