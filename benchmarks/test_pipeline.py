# copyright: mary-morstan developers (see Authors.txt file), GPL v3 License (see LICENSE file)

import pytest
import logging

from requests.exceptions import ChunkedEncodingError
from numpy import random
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from openml import config
import importlib

from marymorstan.problems import PROBLEM_TYPE
from marymorstan.search_space import SearchSpace
from marymorstan.marymorstan import MaryMorstan
from marymorstan.pipeline import compile_population, generate_population
from marymorstan.successive_halving import SuccessiveHalving


def test_compile_population_large_search_space(benchmark):
    def run():
        population = generate_population(number_of_individuals=1000)
        pipelines = compile_population(population)

        X, y = make_classification(15, 5)
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        try:
            for pipeline, raw_pipeline in zip(pipelines, population):
                if raw_pipeline.components[0].name == 'KNeighborsClassifier':
                    X, y = make_classification(150, 5)
                    X_train, X_test, y_train, y_test = train_test_split(X, y)

                    model = pipeline.fit(X_train, y_train)
                    assert model.predict(X_test)[0] in [0, 1]
                else:
                    model = pipeline.fit(X_train, y_train)
                    assert model.predict(X_test)[0] in [0, 1]
        except Exception as e:
            print("Impossible to fit the pipeline %s" % pipeline)
            pytest.fail(e)

    benchmark.pedantic(run, iterations=1, rounds=3)

def test_compile_population_large_search_space_tsc(benchmark):
    def run():
        # NOTE these libraries makes too much noise
        logging.getLogger('numba').setLevel(logging.INFO)
        logging.getLogger('matplotlib').setLevel(logging.INFO)

        population = generate_population(search_space=SearchSpace('./misc/search_spaces/time_series_classification_space.yml'), number_of_individuals=25)
        pipelines = compile_population(population)

        dataset_preprocessing_module = importlib.import_module("datasets.ucr_dataset_preprocessing")
        dataset = dataset_preprocessing_module.MyDataSetPreprocessing("BeetleFly")
        X_train, X_test, y_train, y_test = train_test_split(dataset.get_X(), dataset.get_y(),
                                                            random_state=42, test_size=0.25)

        for pipeline, raw_pipeline in zip(pipelines, population):
            logging.debug('fit %s', str(raw_pipeline))
            try:
                model = pipeline.fit(X_train, y_train)
                logging.debug("Success to fit the pipeline %s", str(pipeline))
                assert model.predict(X_test)[0] in [1, 2] or model.predict(X_test)[0] in ['1', '2']
            except Exception as e:
                logging.debug("Impossible to fit the pipeline %s", str(pipeline))
                pytest.fail(str(e))

    benchmark.pedantic(run, iterations=1, rounds=3)


class TestOptimization():
    def test_optimize(self, benchmark):
        dataset_preprocessing_module = importlib.import_module("datasets.iris_dataset_preprocessing")
        dataset = dataset_preprocessing_module.MyDataSetPreprocessing("iris")
        X_train, _, y_train, _ = train_test_split(dataset.get_X(), dataset.get_y(), test_size=.25, random_state=42)
        random.seed(42)

        def run():
            # NOTE keep RandomState instead of just a seed. It allows to have a better diversity during the variation process.
            mm = MaryMorstan(generations=10,
                            population_size=100,
                            random_state=random.RandomState(42))

            pipelines = mm.optimize(X_train=dataset.get_X(), y_train=dataset.get_y(), random_state=42)

            for pipeline in pipelines:
                print(pipeline.fitness.values)

        benchmark.pedantic(run, iterations=1, rounds=3)

    def test_optimize_with_statistics(self, benchmark):
        dataset_preprocessing_module = importlib.import_module("datasets.iris_dataset_preprocessing")
        dataset = dataset_preprocessing_module.MyDataSetPreprocessing("iris")
        random.seed(42)

        def run():
            # NOTE keep RandomState instead of just a seed. It allows to have a better diversity during the variation process.
            mm = MaryMorstan(generations=10,
                            population_size=100,
                            random_state=random.RandomState(42),
                            enable_statistics=True)

            pipelines = mm.optimize(X_train=dataset.get_X(), y_train=dataset.get_y(), random_state=42)

            for pipeline in pipelines:
                print(pipeline.fitness.values)

        benchmark.pedantic(run, iterations=1, rounds=3)

class TestSuccessiveHalving():
    def setup_method(self):
        dataset_preprocessing_module = importlib.import_module("datasets.ics_uci_dataset_preprocessing")
        self.dataset = dataset_preprocessing_module.MyDataSetPreprocessing("spambase")
        self.X_train, _, self.y_train, _ = train_test_split(self.dataset.get_X(), self.dataset.get_y(), test_size=.1, random_state=42)
        random.seed(42)

        self.generations = 6
        self.population_size = 10

    def test_optimize_without_successive_halving(self, benchmark):
        def run():
            # NOTE keep RandomState instead of just a seed. It allows to have a better diversity during the variation process.
            mm = MaryMorstan(generations=self.generations,
                            population_size=self.population_size,
                            random_state=random.RandomState(42),
                            successive_halving=None)

            pipelines = mm.optimize(X_train=self.dataset.get_X(), y_train=self.dataset.get_y(), random_state=42)

            for pipeline in pipelines:
                print(pipeline.fitness.values)

        benchmark.pedantic(run, iterations=1, rounds=3)

    def test_optimize_with_successive_halving(self, benchmark):
        def run():
            successive_halving = SuccessiveHalving(number_of_generations=self.generations,
                                                   population_size=self.population_size,
                                                   minimum_population_size=2,
                                                   initial_budget=int(.1 * len(self.X_train)),
                                                   maximum_budget=int(.2 * len(self.X_train)))

            # NOTE keep RandomState instead of just a seed. It allows to have a better diversity during the variation process.
            mm = MaryMorstan(generations=self.generations,
                            population_size=self.population_size,
                            random_state=random.RandomState(42),
                            successive_halving=successive_halving)

            pipelines = mm.optimize(X_train=self.dataset.get_X(), y_train=self.dataset.get_y(), random_state=42)

            for pipeline in pipelines:
                print(pipeline.fitness.values)

        benchmark.pedantic(run, iterations=1, rounds=3)


@pytest.fixture()
def dataset(pytestconfig):
    return pytestconfig.getoption("dataset")

class TestLotOfDifferentDatasetsClassification():
    def setup_method(self):
        self.datasets = ['Australian', 'blood-transfusion-service-center', 'car', 'christine', 'cnae-9', 'credit-g', 'dilbert',
                         'fabert', 'jasmine', 'kc1', 'kr-vs-kp', 'mfeat-factors', 'phoneme', 'segment', 'sylvine',
                         'vehicle', 'adult', 'Amazon_employee_access',  'bank-marketing', 'connect-4',
                         'Fashion-MNIST', 'guillermo', 'Helena', 'higgs', 'Jannis',
                         'jungle_chess_2pcs_raw_endgame_complete', 'MiniBooNE', 'nomao',
                         'numerai28.6', 'riccardo', 'Robert', 'Shuttle', 'Volkert', 'Airlines',
                         'Covertype', 'Dionis']
        # not used datasets :  'APSFailure', 'KDDCup09_appetency','Albert' : Nan Values
        random.seed(42)
        self.dataset_preprocessing_module = importlib.import_module("datasets.openml_dataset_preprocessing")
        self.generations = 2
        self.population_size = 2

    # In order to try faster, we are using SH method
    def test_optimize(self, dataset):

        def run(dataset_name):

            dataset = self.dataset_preprocessing_module.MyDataSetPreprocessing(dataset_name,
                                                                          encode_features_to_int=True,
                                                                          encode_targets_to_int=True,
                                                                          drop_columns_with_unique_value=True)

            X_train, _, y_train, _ = train_test_split(dataset.get_X(), dataset.get_y(), test_size=.1, random_state=44)

            search_space_file = './misc/search_spaces/tests/search_space_tpot_light_no_logistic.yml'

            successive_halving = None

            # For large datasets, use SH technique
            if len(X_train) > 2000:
                initial_budget = 200
                maximum_budget = 800
                successive_halving = SuccessiveHalving(number_of_generations=self.generations,
                                                       population_size=self.population_size,
                                                       minimum_population_size=2,
                                                       initial_budget=initial_budget,
                                                       maximum_budget=maximum_budget)

            # NOTE keep RandomState instead of just a seed. It allows to have a better diversity during the variation process.
            mm = MaryMorstan(generations=self.generations,
                            population_size=self.population_size,
                            search_space_file=search_space_file,
                            random_state=random.RandomState(44),
                            successive_halving=successive_halving)

            pipelines = mm.optimize(X_train=dataset.get_X(), y_train=dataset.get_y(), random_state=44)

            for pipeline in pipelines:
                print(pipeline.fitness.values)

        for dataset in self.datasets:
            print(f"Try the dataset {dataset}")
            run(dataset)



class TestLotOfDifferentDatasetsTSc():
    def setup_method(self):
        self.datasets = ['Adiac', 'ArrowHead', 'Beef', 'BeetleFly', 'BirdChicken', 'Car', 'CBF'
                         , 'ChlorineConcentration',
                         'CinCECGTorso', 'Coffee', 'Computers', 'CricketX', 'CricketY', 'CricketZ',
                         'DiatomSizeReduction', 'DistalPhalanxOutlineAgeGroup', 'DistalPhalanxOutlineCorrect',
                         'DistalPhalanxTW','Earthquakes','ECG200', 'ECG5000', 'ECGFiveDays', 'ElectricDevices',
                         'FaceAll', 'FaceFour', 'FacesUCR', 'FiftyWords', 'Fish', 'FordA', 'FordB', 'GunPoint', 'Ham',
                         'HandOutlines', 'Haptics', 'Herring', 'InlineSkate', 'InsectWingbeatSound', 'ItalyPowerDemand',
                         'LargeKitchenAppliances', 'Lightning2', 'Lightning7', 'Mallat', 'Meat', 'MedicalImages',
                         'MiddlePhalanxOutlineAgeGroup', 'MiddlePhalanxOutlineCorrect', 'MiddlePhalanxTW', 'MoteStrain',
                         'NonInvasiveFetalECGThorax1', 'NonInvasiveFetalECGThorax2', 'OliveOil', 'OSULeaf',
                         'PhalangesOutlinesCorrect', 'Phoneme', 'Plane', 'ProximalPhalanxOutlineAgeGroup',
                         'ProximalPhalanxOutlineCorrect', 'ProximalPhalanxTW', 'RefrigerationDevices', 'ScreenType',
                         'ShapeletSim', 'ShapesAll', 'SmallKitchenAppliances', 'SonyAIBORobotSurface1',
                         'SonyAIBORobotSurface2', 'StarLightCurves', 'Strawberry', 'SwedishLeaf', 'Symbols',
                         'SyntheticControl', 'ToeSegmentation1', 'ToeSegmentation2', 'Trace', 'TwoLeadECG',
                         'TwoPatterns', 'UWaveGestureLibraryAll', 'UWaveGestureLibraryX', 'UWaveGestureLibraryY',
                         'UWaveGestureLibraryZ', 'Wafer', 'Wine', 'WordSynonyms', 'Worms', 'WormsTwoClass', 'Yoga',
                         'ACSF1', 'AllGestureWiimoteX', 'AllGestureWiimoteY', 'AllGestureWiimoteZ', 'BME', 'Chinatown',
                         'Crop', 'DodgerLoopDay', 'DodgerLoopGame', 'DodgerLoopWeekend', 'EOGHorizontalSignal',
                         'EOGVerticalSignal', 'EthanolLevel', 'FreezerRegularTrain', 'FreezerSmallTrain', 'Fungi',
                         'GestureMidAirD1', 'GestureMidAirD2', 'GestureMidAirD3', 'GesturePebbleZ1', 'GesturePebbleZ2',
                         'GunPointAgeSpan', 'GunPointMaleVersusFemale', 'GunPointOldVersusYoung', 'HouseTwenty',
                         'InsectEPGRegularTrain', 'InsectEPGSmallTrain', 'MelbournePedestrian',
                         'MixedShapesRegularTrain', 'MixedShapesSmallTrain', 'PickupGestureWiimoteZ',
                         'PigAirwayPressure', 'PigArtPressure', 'PigCVP', 'PLAID', 'PowerCons', 'Rock',
                         'SemgHandGenderCh2', 'SemgHandMovementCh2', 'SemgHandSubjectCh2', 'ShakeGestureWiimoteZ',
                         'SmoothSubspace', 'UMD']
        random.seed(42)

        self.generations = 2
        self.population_size = 2
        self.dataset_preprocessing_module = importlib.import_module("datasets.ucr_dataset_preprocessing")

    def test_optimize(self, dataset):

        def run(dataset_name):

            dataset = self.dataset_preprocessing_module.MyDataSetPreprocessing(dataset_name)
            X_train, _, y_train, _ = train_test_split(dataset.get_X(), dataset.get_y(), test_size=.1,
                                                      random_state=44, shuffle=False)

            search_space_file = './misc/search_spaces/time_series_light_classification_space.yml'

            # NOTE keep RandomState instead of just a seed. It allows to have a better diversity during the variation process.
            mm = MaryMorstan(generations=self.generations,
                            population_size=self.population_size,
                            search_space_file=search_space_file,
                            random_state=random.RandomState(44),
                            problem_type=PROBLEM_TYPE.TIMESERIES_SUPERVISED_CLASSIFICATION)

            pipelines = mm.optimize(X_train=X_train, y_train=y_train, random_state=44)

            for pipeline in pipelines:
                print(pipeline.fitness.values)


        for dataset in self.datasets:
            if dataset not in ['CinCECGTorso', 'ElectricDevices', 'FordA', 'FordB', 'HandOutlines', 'InlineSkate',
                               'Mallat', 'NonInvasiveFetalECGThorax1', 'UWaveGestureLibraryX', 'UWaveGestureLibraryY',
                               'UWaveGestureLibraryZ','NonInvasiveFetalECGThorax2', 'Phoneme', 'Yoga', 'EthanolLevel',
                               'MixedShapesRegularTrain', 'MixedShapesSmallTrain', 'SemgHandGenderCh2',
                               'StarLightCurves', 'UWaveGestureLibraryAll', 'Wafer', 'FreezerRegularTrain',
                               'FreezerSmallTrain', 'AllGestureWiimoteX', 'AllGestureWiimoteY', 'AllGestureWiimoteZ',
                               'GestureMidAirD1', 'GestureMidAirD2', 'GestureMidAirD3', 'GesturePebbleZ1',
                               'GesturePebbleZ2', 'MelbournePedestrian', 'PickupGestureWiimoteZ', 'PLAID',
                               'ShakeGestureWiimoteZ']:
                print("Try the dataset {}".format(dataset))
                run(dataset)



