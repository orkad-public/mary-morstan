# -*- coding: utf-8 -*-
# copyright: mary-morstan developers (see Authors.txt file), GPL v3 License (see LICENSE file)

import openml
from marymorstan.preprocessing.dataset_preprocessing import DatasetPreprocessing, UnknownDataset


class MyDataSetPreprocessing(DatasetPreprocessing):
    """This class allow to download a dataset from openml library"""
    def load_dataset(self, dataset_location):
        """ specific to the dataset origin, retrieve and return the dataset as a panda Dataframe"""
        try:
            openml_dataset = openml.datasets.get_dataset(dataset_location, download_data=True)
            X, y, _, _ = openml_dataset.get_data(dataset_format="dataframe")
            X["target"] = y
            return X
        except Exception:
            raise UnknownDataset(dataset_location)
