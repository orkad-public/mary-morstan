# -*- coding: utf-8 -*-
# copyright: mary-morstan developers (see Authors.txt file), GPL v3 License (see LICENSE file)

from marymorstan.preprocessing.dataset_preprocessing import DatasetPreprocessing, UnknownDataset
from ucimlrepo import fetch_ucirepo


class MyDataSetPreprocessing(DatasetPreprocessing):
    """This class allow to download a dataset from archive.ics.uci.edu"""
    def load_dataset(self, dataset_location):
        """ specific to the dataset origin, retrieve and return the dataset as a panda Dataframe"""
        try:
            uci_dataset = fetch_ucirepo(name=dataset_location)
            dataset = uci_dataset.data.original
            return dataset
        except Exception:
            raise UnknownDataset(dataset_location)

