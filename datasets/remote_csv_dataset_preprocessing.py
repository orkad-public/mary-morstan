# -*- coding: utf-8 -*-
# copyright: mary-morstan developers (see Authors.txt file), GPL v3 License (see LICENSE file)

import tempfile
import os
import pandas as pd
from marymorstan.preprocessing.dataset_preprocessing import DatasetPreprocessing, UnknownDataset
import requests


class MyDataSetPreprocessing(DatasetPreprocessing):
    """This class allow to load a dataset from a CSV file"""
    def load_dataset(self, dataset_location):
        """ specific to the dataset origin, retrieve and return the dataset as a panda Dataframe"""
        try:
            r = requests.get(dataset_location, allow_redirects=True)
            if r.status_code != 200:
                raise Exception(f"Trouble to download {dataset_location}, got response {r.status_code}")
            output_directory = tempfile.mkdtemp()
            with open(f'{output_directory}/{os.path.basename(dataset_location)}', 'wb') as f:
                f.write(r.content)
            dataset = pd.read_csv(f"{output_directory}/{os.path.basename(dataset_location)}")
            return dataset
        except Exception:
            raise UnknownDataset(dataset_location)
