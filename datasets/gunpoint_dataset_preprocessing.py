# -*- coding: utf-8 -*-
# copyright: mary-morstan developers (see Authors.txt file), GPL v3 License (see LICENSE file)

from sktime.datasets import load_gunpoint
from marymorstan.preprocessing.dataset_preprocessing import DatasetPreprocessing


class MyDataSetPreprocessing(DatasetPreprocessing):
    """This class allow to download the gunpoint dataset from sktime library"""

    def load_dataset(self, dataset_location):
        """ specific to the dataset origin, retrieve and return the dataset as a panda Dataframe"""
        X, y = load_gunpoint(return_X_y=True)
        dataset = X
        # Add the target variable to the DataFrame
        dataset['target'] = y
        return dataset

