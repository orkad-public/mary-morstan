# -*- coding: utf-8 -*-
# copyright: mary-morstan developers (see Authors.txt file), GPL v3 License (see LICENSE file)

from sktime.datasets import load_longley
from marymorstan.preprocessing.dataset_preprocessing import DatasetPreprocessing


class MyDataSetPreprocessing(DatasetPreprocessing):
    """This class allow to download the longley dataset from sktime library"""

    def load_dataset(self, dataset_location):
        """ specific to the dataset origin, retrieve and return the dataset as a panda Dataframe"""
        y, X = load_longley()
        dataset = X
        # Add the target variable to the DataFrame
        dataset['target'] = y
        return dataset

