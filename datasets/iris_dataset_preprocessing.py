# -*- coding: utf-8 -*-
# copyright: mary-morstan developers (see Authors.txt file), GPL v3 License (see LICENSE file)

import numpy as np
from marymorstan.preprocessing.dataset_preprocessing import DatasetPreprocessing
from sklearn.datasets import load_iris
import pandas as pd


class MyDataSetPreprocessing(DatasetPreprocessing):
    """This class allow to download the iris dataset from sklearn library"""

    def load_dataset(self, dataset_location):
        """ specific to the dataset origin, retrieve and return the dataset as a panda Dataframe"""
        iris = load_iris()
        dataset = pd.DataFrame(np.concatenate((iris.data, np.array([iris.target]).T), axis=1),
                               columns=iris.feature_names + ['target'])
        return dataset
