# -*- coding: utf-8 -*-
# copyright: mary-morstan developers (see Authors.txt file), GPL v3 License (see LICENSE file)

from marymorstan.preprocessing.dataset_preprocessing import DatasetPreprocessing, UnknownDataset
from aeon.datasets import load_classification
from aeon.transformations.collection import PaddingTransformer
import pandas as pd
import logging

class MyDataSetPreprocessing(DatasetPreprocessing):
    """This class allow to download a dataset from aeon library"""
    def __init__(self, dataset_location,
                 encode_features_to_int=False,
                 encode_targets_to_int=False,
                 fill_nan_values=None,
                 drop_nan_values=False,
                 drop_columns_with_unique_value=False):
        """load a dataset according to the dataset_location (csv, url, ...),
            the dataset will be store as a panda dataframe,
            do some preprocessing tasks according to given parameters"""
        self.dataset = self.load_dataset(dataset_location)

        if fill_nan_values is not None or drop_nan_values or drop_columns_with_unique_value:
            message = ("fill_nan_value/drop_nan_values/drop_columns_with_unique_value options "
                       "are not supported currently")
            logging.error(message)
            raise Exception(message)
        self.column = self.dataset.columns[-1]  # assume that the last column is the prediction

        # if encode_features_to_int:
        #     self.X = self._encode_datas_to_numeric(self.X)
        # if encode_targets_to_int:
        #     self.y = self._encode_datas_to_numeric(self.y)

    def load_dataset(self, dataset_location):
        """ specific to the dataset origin, retrieve and return the dataset as a panda Dataframe"""
        try:
            self.X, self.y = load_classification(name=dataset_location)
            if isinstance(self.X, list):  # case of unequal length classification problems
                pt = PaddingTransformer()
                self.X = pt.fit_transform(self.X)
            self.X = self.X.reshape(self.X.shape[0], self.X.shape[1] * self.X.shape[2])
            # self.X = from_2d_array_to_nested(self.X)
            dataset = pd.DataFrame(self.X)
            # Add the target variable to the DataFrame
            dataset['target'] = self.y
            return dataset
        except Exception:
            raise UnknownDataset(dataset_location)


# def from_2d_array_to_nested(
#     X, index=None, columns=None, time_index=None, cells_as_numpy=False
# ):
#     """Convert tabular pandas DataFrame with only primitives in cells into
#     nested pandas DataFrame with a single column.
#
#     Parameters
#     ----------
#     X : pd.DataFrame
#
#     cells_as_numpy : bool, default = False
#         If True, then nested cells contain NumPy array
#         If False, then nested cells contain pandas Series
#
#     index : array-like, shape=[n_samples], optional (default = None)
#         Sample (row) index of transformed DataFrame
#
#     time_index : array-like, shape=[n_obs], optional (default = None)
#         Time series index of transformed DataFrame
#
#     Returns
#     -------
#     Xt : pd.DataFrame
#         Transformed DataFrame in nested format
#     """
#
#     if (time_index is not None) and cells_as_numpy:
#         raise ValueError(
#             "`Time_index` cannot be specified when `return_arrays` is True, "
#             "time index can only be set to "
#             "pandas Series"
#         )
#     if isinstance(X, pd.DataFrame):
#         X = X.to_numpy()
#
#     container = np.array if cells_as_numpy else pd.Series
#
#     # for 2d numpy array, rows represent instances, columns represent time points
#     n_instances, n_timepoints = X.shape
#
#     if time_index is None:
#         time_index = np.arange(n_timepoints)
#     kwargs = {"index": time_index}
#
#     Xt = pd.DataFrame(
#         pd.Series([container(X[i, :], **kwargs) for i in range(n_instances)])
#     )
#     if index is not None:
#         Xt.index = index
#     if columns is not None:
#         Xt.columns = columns
#     return Xt