from marymorstan.successive_halving import SuccessiveHalving


class TestSuccessiveHalving():
    def setup_method(self):
        self.successive_halving = SuccessiveHalving(initial_budget=5,
                                                    maximum_budget=50,
                                                    number_of_generations=100,
                                                    minimum_population_size=10,
                                                    population_size=100)

    def test_successive_halving_budget(self):
        expectations = [{'budget': 5, 'iteration': 1},
                        {'budget': 5, 'iteration': 25},
                        {'budget': 10, 'iteration': 26},
                        {'budget': 10, 'iteration': 50},
                        {'budget': 20, 'iteration': 51},
                        {'budget': 20, 'iteration': 75},
                        {'budget': 40, 'iteration': 76},
                        {'budget': 40, 'iteration': 100}]

        for expectation in expectations:
            assert expectation['budget'] == self.successive_halving.budget(current_iteration=expectation['iteration'])

    def test_successive_halving_population(self):
        expectations = [{'population': 100, 'iteration': 1},
                        {'population': 100, 'iteration': 25},
                        {'population': 50, 'iteration': 26},
                        {'population': 50, 'iteration': 50},
                        {'population': 25, 'iteration': 51},
                        {'population': 25, 'iteration': 75},
                        {'population': 12, 'iteration': 76},
                        {'population': 12, 'iteration': 100}]

        for expectation in expectations:
            assert expectation['population'] == self.successive_halving.population(current_iteration=expectation['iteration'])
