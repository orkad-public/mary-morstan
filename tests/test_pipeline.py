import pytest
import re
import logging
import copy

import numpy
from numpy import random

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import FunctionTransformer
from sklearn.feature_selection import RFE

from marymorstan.search_space import SearchSpace, SEARCH_SPACE_KEY
from marymorstan.initialization import RandomInitialization, LHSInitialization
from marymorstan.pipeline import MLPipeline, MLPipelineComponent, MLPipelineComponentType, compile_population, generate_population, InvalidHyperparameters

from . import simple_search_space, simple_ml_pipeline, pipeline_with_selector, \
pipeline_with_combine_dfs_at_beginning, pipeline_with_combine_dfs_multiple_times, pipeline_with_stacking, \
pipeline_with_combine_two_previouses_dfs_at_beginning, pipeline_with_combine_two_previouses_dfs_at_second_step, \
pipeline_with_combine_two_previouses_dfs_successive, pipeline_with_combine_two_previouses_dfs_after_second_step  # noqa: F401


class TestMLPipeline():
    def test_generate_pipeline(self, simple_ml_pipeline):
        assert simple_ml_pipeline[0].name == "GaussianNB"
        assert simple_ml_pipeline[0].type == MLPipelineComponentType.ESTIMATOR

        assert simple_ml_pipeline[1].name == "Normalizer"
        assert simple_ml_pipeline[1].type == MLPipelineComponentType.PREPROCESSOR

    def test_generate_forbidden_with_previous_clause(self):
        simple_forbidden_search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_search_space.yml")
        pipeline = MLPipeline.generate_pipeline(search_space=simple_forbidden_search_space, random_state=42)
        logging.debug("pipeline: %s", pipeline)
        assert pipeline[0].name == "MultinomialNB"
        assert pipeline[0].type == MLPipelineComponentType.ESTIMATOR

        assert pipeline[1].name == "MinMaxScaler"
        assert pipeline[1].type == MLPipelineComponentType.PREPROCESSOR

        simple_forbidden_search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_groups_search_space.yml")
        pipeline = MLPipeline.generate_pipeline(search_space=simple_forbidden_search_space, random_state=42)
        logging.debug("pipeline: %s", pipeline)
        assert pipeline[0].name == "MultinomialNB"
        assert pipeline[0].type == MLPipelineComponentType.ESTIMATOR

        assert pipeline[1].name == "MinMaxScaler"
        assert pipeline[1].type == MLPipelineComponentType.PREPROCESSOR

    def test_generate_forbidden_with_previous_for_all_previous_clause(self):
        simple_forbidden_search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_for_all_previous_search_space.yml")
        pipeline = MLPipeline.generate_pipeline(search_space=simple_forbidden_search_space)

        assert len(pipeline) == 1

    def test_generate_forbidden_with_previouses_clause(self):
        simple_forbidden_search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previouses_search_space.yml")
        pipeline = MLPipeline.generate_pipeline(search_space=simple_forbidden_search_space, initialization=RandomInitialization(4, 4), random_state=42)
        logging.debug("pipeline: %s", pipeline)
        assert pipeline[0].name == "MultinomialNB"
        assert pipeline[0].type == MLPipelineComponentType.ESTIMATOR

        for i in range(1, 4):
            assert pipeline[i].name == "MinMaxScaler"

        # Test with groups
        simple_forbidden_search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previouses_groups_search_space.yml")
        pipeline = MLPipeline.generate_pipeline(search_space=simple_forbidden_search_space, initialization=RandomInitialization(4, 4), random_state=42)
        logging.debug("pipeline: %s", pipeline)
        assert pipeline[0].name == "MultinomialNB"
        assert pipeline[0].type == MLPipelineComponentType.ESTIMATOR

        for i in range(1, 4):
            assert pipeline[i].name == "MinMaxScaler"

    def test_generate_pipeline_with_lhs(self, caplog):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_groups_search_space.yml")

        population_size = 40
        initialization = LHSInitialization(min_size=1, max_size=3, population_size=population_size, search_space=search_space, random_state=42)

        pipelines = []

        # save pipeline_sizes for later use (because they are consumed when pipeline are generated)
        pipeline_sizes = copy.deepcopy(initialization.pipeline_sizes)

        for _ in range(population_size):
            pipelines.append(MLPipeline.generate_pipeline(search_space=search_space, initialization=initialization, random_state=42))

        assert not "degraded" in caplog.text

        # ensure pop_pipeline_size() has been used
        assert len(initialization.pipeline_sizes) == 0

        # ensure that each generated pipeline match the sizes initiate by LHS
        for pipeline in pipelines:
            assert pipeline_sizes.pop(0) == len(pipeline)

    def test_generate_pipeline_with_lhs_degraded(self, caplog):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_groups_search_space.yml")

        population_size = 10
        initialization = LHSInitialization(min_size=1, max_size=3, population_size=population_size, search_space=search_space, random_state=42)

        for _ in range(population_size):
            pipeline = MLPipeline.generate_pipeline(search_space=search_space, initialization=initialization, random_state=42)

        assert "degraded" in caplog.text

    def test_str(self, simple_ml_pipeline, pipeline_with_selector):
        assert str(simple_ml_pipeline) == str(['GaussianNB({})', "Normalizer({'norm': 'l1'})", "Normalizer({'norm': 'max'})"])
        # NOTE allow to check that subcomponent information are present in the string
        assert str(pipeline_with_selector[0]) == str(["DecisionTreeClassifier({'criterion': 'gini', 'max_depth': 4, 'min_samples_split': 14, 'min_samples_leaf': 3})", \
                                                      "SelectFwe({'alpha': 0.05, 'subcomponent:subcomponent_callable:score_func': {}})", \
                                                      "RFE({'step': 0.7000000000000001, 'subcomponent:subcomponent_estimator:estimator': {'n_estimators': 100, 'criterion': 'gini', 'max_features': 0.2}})"])

    def test_generate_forbidden_hyperparameters(self, mocker):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_hyperparameters_search_space.yml")

        for seed in range(50):
            logging.debug("seed %d", seed)
            try:
                pipeline = MLPipeline.generate_pipeline(search_space=search_space, random_state=seed)
            except Exception as e:
                pytest.fail("Unexpected failure %s", e)

        mocker.patch("marymorstan.pipeline.MLPipelineComponent.LIMIT_NUMBER_OF_TRY", 2)
        with pytest.raises(InvalidHyperparameters):
            pipeline = MLPipeline.generate_pipeline(search_space=search_space, random_state=5)

    def test_generate_forbidden_large_preprocessors_space(self):
        pass

    def test_compile_component(self):  # TODO write test
        pass

    def test_compile_pipeline(self, simple_search_space):
        X, y = make_classification(100, 100)
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        pipeline = MLPipeline.generate_pipeline(search_space=simple_search_space, random_state=numpy.random.RandomState(41))
        pipeline = MLPipeline.compile_pipeline(pipeline, search_space=simple_search_space)

        estimator_step_name = list(pipeline.named_steps.keys())[2]

        assert re.search("^GaussianNB.*", estimator_step_name)
        assert isinstance(pipeline, Pipeline)

        estimator_step = list(pipeline.named_steps.values())[1]
        assert isinstance(estimator_step, BaseEstimator)

        preprocessor_step = list(pipeline.named_steps.values())[0]
        assert isinstance(preprocessor_step, TransformerMixin)

        model = pipeline.fit(X_train, y_train)
        assert model.predict(X_test)[0] in [0, 1]

    def test_compile_pipeline_with_selectors(self, pipeline_with_selector):
        X, y = make_classification(100, 100)
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        pipeline, _ = pipeline_with_selector
        compiled_pipeline = pipeline.compile()

        estimator_step = list(compiled_pipeline.named_steps.values())[1]
        assert isinstance(estimator_step, BaseEstimator)

        preprocessor_step = list(compiled_pipeline.named_steps.values())[0]
        assert isinstance(preprocessor_step, TransformerMixin)

        model = compiled_pipeline.fit(X_train, y_train)
        assert model.predict(X_test)[0] in [0, 1]

    def test_compile_pipeline_with_combine_dfs(self, pipeline_with_combine_dfs_at_beginning, pipeline_with_combine_dfs_multiple_times):
        X, y = make_classification(100, 100)
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        pipeline, _ = pipeline_with_combine_dfs_at_beginning
        compiled_pipeline = pipeline.compile()

        assert pipeline[1].name == 'CombineDFs'

        preprocessor_step = list(compiled_pipeline.named_steps.values())[0]
        assert isinstance(preprocessor_step, FeatureUnion)

        model = compiled_pipeline.fit(X_train, y_train)
        assert model.predict(X_test)[0] in [0, 1]

        # Mutltiple combineDFs
        pipeline, _ = pipeline_with_combine_dfs_multiple_times
        compiled_pipeline = pipeline.compile()

        logging.debug(compiled_pipeline.steps[0])
        logging.debug(compiled_pipeline.steps[1])
        logging.debug(compiled_pipeline.steps[2])

        assert pipeline[2].name == 'CombineDFs'
        assert pipeline[4].name == 'CombineDFs'

        preprocessor_step = list(compiled_pipeline.named_steps.values())[0]
        assert isinstance(preprocessor_step, FeatureUnion)
        assert isinstance(preprocessor_step.transformer_list[0][1], FunctionTransformer)
        assert isinstance(preprocessor_step.transformer_list[1][1], FunctionTransformer)

        preprocessor_step = list(compiled_pipeline.named_steps.values())[1]
        assert isinstance(preprocessor_step, FeatureUnion)
        assert isinstance(preprocessor_step.transformer_list[0][1], FunctionTransformer)
        assert isinstance(preprocessor_step.transformer_list[1][1], RFE)

        estimator_step = list(compiled_pipeline.named_steps.values())[2]
        assert isinstance(estimator_step, BaseEstimator)
        assert not isinstance(estimator_step, FeatureUnion)

        model = compiled_pipeline.fit(X_train, y_train)
        assert model.predict(X_test)[0] in [0, 1]

    def test_compile_pipeline_with_stacking(self, pipeline_with_stacking):
        X, y = make_classification(100, 100)
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        pipeline, _ = pipeline_with_stacking
        compiled_pipeline = pipeline.compile()

        logging.debug(compiled_pipeline.steps[0])
        logging.debug(compiled_pipeline.steps[1])
        logging.debug(compiled_pipeline.steps[2])

        named_steps = list(compiled_pipeline.named_steps.keys())
        assert 'MaxAbsScaler_' in named_steps[0]
        assert 'StackingEstimator_BernoulliNB_' in named_steps[1]
        assert 'GaussianNB_' in named_steps[2]

        estimator_step = list(compiled_pipeline.named_steps.values())[2]
        assert isinstance(estimator_step, BaseEstimator)

        preprocessor_step = list(compiled_pipeline.named_steps.values())[1]
        assert isinstance(preprocessor_step, TransformerMixin)

        preprocessor_step = list(compiled_pipeline.named_steps.values())[0]
        assert isinstance(preprocessor_step, TransformerMixin)

        model = compiled_pipeline.fit(X_train, y_train)
        assert model.predict(X_test)[0] in [0, 1]

    def test_compile_pipeline_with_combine_two_previouses_dfs(self, pipeline_with_combine_two_previouses_dfs_at_beginning, \
                                                              pipeline_with_combine_two_previouses_dfs_at_second_step, \
                                                              pipeline_with_combine_two_previouses_dfs_successive, \
                                                              pipeline_with_combine_two_previouses_dfs_after_second_step):
        X, y = make_classification(100, 100)
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        # CombineTwoPreviousesDFs at the beginning
        pipeline, _ = pipeline_with_combine_two_previouses_dfs_at_beginning
        compiled_pipeline = pipeline.compile()

        assert pipeline[1].name == 'CombineTwoPreviousesDFs'

        preprocessor_step = list(compiled_pipeline.named_steps.values())[0]
        assert isinstance(preprocessor_step, FeatureUnion)

        model = compiled_pipeline.fit(X_train, y_train)
        assert model.predict(X_test)[0] in [0, 1]

        # CombineTwoPreviousesDFs at second step
        pipeline, _ = pipeline_with_combine_two_previouses_dfs_at_second_step
        compiled_pipeline = pipeline.compile()

        assert pipeline[1].name == 'CombineTwoPreviousesDFs'

        preprocessor_steps = list(compiled_pipeline.named_steps.values())

        assert isinstance(preprocessor_steps[0], FeatureUnion)

        assert isinstance(preprocessor_steps[0].transformer_list[0][1], FunctionTransformer)
        assert isinstance(preprocessor_steps[0].transformer_list[1][1], RFE)

        # NOTE this test is important, it highligh an issue where one of the twoPrevious component was include twice (once in the union, and another time after which is wrong)
        assert not isinstance(preprocessor_steps[1], FeatureUnion)

        model = compiled_pipeline.fit(X_train, y_train)
        assert model.predict(X_test)[0] in [0, 1]

        # CombineTwoPreviousesDFs in succession
        pipeline, _ = pipeline_with_combine_two_previouses_dfs_successive

        compiled_pipeline = pipeline.compile()

        assert pipeline[3].name == 'CombineTwoPreviousesDFs'
        assert pipeline[2].name == 'CombineTwoPreviousesDFs'

        preprocessor_steps = list(compiled_pipeline.named_steps.values())
        assert isinstance(preprocessor_steps[0], FeatureUnion)

        assert isinstance(preprocessor_steps[0].transformer_list[0][1], FunctionTransformer)
        assert isinstance(preprocessor_steps[0].transformer_list[1][1], FeatureUnion)

        assert isinstance(preprocessor_steps[0].transformer_list[1][1].transformer_list[0][1], FunctionTransformer)
        assert isinstance(preprocessor_steps[0].transformer_list[1][1].transformer_list[1][1], FunctionTransformer)

        model = compiled_pipeline.fit(X_train, y_train)
        assert model.predict(X_test)[0] in [0, 1]

        # CombineTwoPreviousesDFs after the second step
        pipeline, _ = pipeline_with_combine_two_previouses_dfs_after_second_step
        compiled_pipeline = pipeline.compile()

        logging.debug(str(pipeline))

        logging.debug(compiled_pipeline.named_steps.values())

        assert pipeline[1].name == 'CombineTwoPreviousesDFs'
        assert pipeline[2].name != 'CombineTwoPreviousesDFs'
        assert pipeline[3].name != 'CombineTwoPreviousesDFs'

        preprocessor_steps = list(compiled_pipeline.named_steps.values())

        assert len(preprocessor_steps) == 2

        assert isinstance(preprocessor_steps[0], FeatureUnion)

        # NOTE for sure, it should not be a copy of the dataframe, but the combination of the two previous components
        assert not isinstance(preprocessor_steps[0].transformer_list[0][1], FunctionTransformer)
        assert not isinstance(preprocessor_steps[0].transformer_list[1][1], FunctionTransformer)

        assert isinstance(preprocessor_steps[0].transformer_list[0][1], RFE)
        assert isinstance(preprocessor_steps[0].transformer_list[1][1], RFE)

        assert 'DecisionTreeClassifier_' in list(compiled_pipeline.named_steps.keys())[1]

    def test_compile_pipeline_with_combine_two_previouses_dfs_and_two_stacking(self):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/search_space_tpot.yml")

        random_state = random.RandomState(10)

        pipeline = MLPipeline(search_space=search_space, generate=False)

        component = MLPipelineComponent(name="ExtraTreesClassifier",
                                        component_type=MLPipelineComponentType.ESTIMATOR,
                                        hyperparameters={'n_estimators': 100, 'criterion': 'gini', 'max_features': 1.0, 'min_samples_split': 2, 'min_samples_leaf': 8, 'bootstrap': False},
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['ExtraTreesClassifier'],
                                        subcomponents={})
        pipeline.append(component)

        component = MLPipelineComponent(name="CombineTwoPreviousesDFs",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters={},
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['CombineTwoPreviousesDFs'],
                                        subcomponents={})
        pipeline.append(component)

        component = MLPipelineComponent(name="StandardScaler",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters={},
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['StandardScaler'],
                                        subcomponents={})
        pipeline.append(component)

        component = MLPipelineComponent(name="KNeighborsClassifier",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters={},
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['KNeighborsClassifier'],
                                        subcomponents={})
        pipeline.append(component)

        MLPipeline.compile_pipeline(pipeline, search_space=search_space)

    def test_compile_pipeline_with_combine_successive_two_previouses_and_stacking(self):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/search_space_tpot.yml")

        random_state = random.RandomState(10)

        pipeline = MLPipeline(search_space=search_space, generate=False)

        component = MLPipelineComponent(name="ExtraTreesClassifier",
                                        component_type=MLPipelineComponentType.ESTIMATOR,
                                        hyperparameters={'n_estimators': 100, 'criterion': 'gini', 'max_features': 1.0, 'min_samples_split': 2, 'min_samples_leaf': 8, 'bootstrap': False},
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['ExtraTreesClassifier'],
                                        subcomponents={})
        pipeline.append(component)

        component = MLPipelineComponent(name="CombineTwoPreviousesDFs",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters={},
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['CombineTwoPreviousesDFs'],
                                        subcomponents={})
        pipeline.append(component)

        component = MLPipelineComponent(name="CombineTwoPreviousesDFs",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters={},
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['CombineTwoPreviousesDFs'],
                                        subcomponents={})
        pipeline.append(component)

        component = MLPipelineComponent(name="MultinomialNB",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters={},
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['MultinomialNB'],
                                        subcomponents={})
        pipeline.append(component)

        compiled_pipeline = MLPipeline.compile_pipeline(pipeline, search_space=search_space)

        preprocessor_steps = list(compiled_pipeline.named_steps.values())
        assert isinstance(preprocessor_steps[0], FeatureUnion)

        assert isinstance(preprocessor_steps[0].transformer_list[0][1], FeatureUnion)
        assert isinstance(preprocessor_steps[0].transformer_list[0][1].transformer_list[0][1], FunctionTransformer)
        assert isinstance(preprocessor_steps[0].transformer_list[0][1].transformer_list[1][1], FunctionTransformer)
        assert 'StackingEstimator_MultinomialNB_' in preprocessor_steps[0].transformer_list[1][0]


    def test_hash(self, simple_ml_pipeline, simple_search_space):
        assert simple_ml_pipeline.hash() == "d5055b451e3207576a96e2c5e77357feba4910ab"

        another_simple_ml_pipeline = MLPipeline(search_space=simple_search_space, random_state=4224)

        assert another_simple_ml_pipeline.hash() != simple_ml_pipeline.hash()

    def test_ohe_preprocessing(self):
        pytest.skip("skip test for now, should be enable once fixed when input data for OHE is nd.array instead of pandas.df")
        search_space = SearchSpace(search_space_file="./misc/search_spaces/search_space_tpot_no_xgb.yml")
        pipeline = MLPipeline(generate=False, search_space=search_space)

        component = MLPipelineComponent(name='LinearSVC',
                                        component_type=MLPipelineComponentType.ESTIMATOR,
                                        hyperparameters={'penalty': 'l2', 'loss': 'squared_hinge', 'dual': False, 'tol': 0.0001, 'C': 25.0},
                                        search_space_component_node=search_space.estimators['LinearSVC'],
                                        subcomponents={})
        pipeline.append(component)

        component = MLPipelineComponent(name='OneHotEncoder',
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters={'minimum_fraction': 0.2, 'sparse': False, 'threshold': 10},
                                        search_space_component_node=search_space.preprocessors['OneHotEncoder'],
                                        subcomponents={})
        pipeline.append(component)

        assert MLPipeline.is_valid_pipeline(pipeline)

        import importlib
        dataset_preprocessing_module = importlib.import_module("datasets.openml_dataset_preprocessing")
        ds = dataset_preprocessing_module.MyDataSetPreprocessing("credit-g", encode_features_to_int=True,
                                                                 encode_targets_to_int=True)
        X, y = ds.get_X(), ds.get_y()
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        pipeline = MLPipeline.compile_pipeline(pipeline, search_space=search_space)

        model = pipeline.fit(X_train, y_train)
        assert model.predict(X_test)[0] in [0, 1]


    def test_is_valid_pipeline(self, simple_ml_pipeline):
        assert MLPipeline.is_valid_pipeline(simple_ml_pipeline)

        simple_forbidden_search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_search_space.yml")
        pipeline = MLPipeline(generate=False, search_space=simple_forbidden_search_space)

        # Create an invalid pipeline (forbidden previous):
        component = MLPipelineComponent(name='MultinomialNB',
                                        component_type=MLPipelineComponentType.ESTIMATOR,
                                        hyperparameters={},
                                        search_space_component_node=simple_forbidden_search_space.estimators['MultinomialNB'],
                                        subcomponents={})
        pipeline.append(component)

        # 1. valid for now
        assert MLPipeline.is_valid_pipeline(pipeline)

        component = MLPipelineComponent(name='Normalizer',
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters={},
                                        search_space_component_node=simple_forbidden_search_space.preprocessors['Normalizer'],
                                        subcomponents={})
        pipeline.append(component)

        # 2. invalid now
        assert not MLPipeline.is_valid_pipeline(pipeline)

        simple_forbidden_search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_hyperparameters_search_space.yml")

        # Create an invalid pipeline (forbidden hyperparameters):
        pipeline = MLPipeline(initialization=RandomInitialization(1), search_space=simple_forbidden_search_space)
        pipeline.components[0].hyperparameters['C'] = -1

        pipeline.append(component)

        assert not MLPipeline.is_valid_pipeline(pipeline)

        # Create an invalid pipeline (forbidden previouses):
        tpot_search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previouses_search_space.yml")
        pipeline = MLPipeline(generate=False, search_space=tpot_search_space)

        component = MLPipelineComponent(name='MultinomialNB',
                                        component_type=MLPipelineComponentType.ESTIMATOR,
                                        hyperparameters={},
                                        search_space_component_node=tpot_search_space.estimators['MultinomialNB'],
                                        subcomponents={})
        pipeline.append(component)

        component = MLPipelineComponent(name='Normalizer',
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters={},
                                        search_space_component_node=tpot_search_space.preprocessors['Normalizer'],
                                        subcomponents={})
        pipeline.append(component)
        pipeline.append(component)

        assert not MLPipeline.is_valid_pipeline(pipeline)

        # Test with fit: not valid due to negative values in the features
        X, y = make_classification(100, 100)
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previouses_search_space.yml")
        pipeline = MLPipeline(generate=False,
                              allow_fit_to_valid_pipeline=True,
                              budget_per_fit_to_valid_pipeline_seconds=1,
                              fit_pretest_X=X,
                              fit_pretest_y=y,
                              search_space=search_space)

        component = MLPipelineComponent(name='MultinomialNB',
                                        component_type=MLPipelineComponentType.ESTIMATOR,
                                        hyperparameters={},
                                        search_space_component_node=search_space.estimators['MultinomialNB'],
                                        subcomponents={})
        pipeline.append(component)

        assert not MLPipeline.is_valid_pipeline(pipeline)

        # Test timeout
        # FIXME re-enable after experiments to compare with TPOT
        pytest.skip("skip timeout part for now, only valid when swallow_exc=False, which is not to respect TPOT v0.11.5 bias")
        X, y = make_classification(10000, 100)
        search_space = SearchSpace(search_space_file="./misc/search_spaces/search_space_tpot_no_xgb.yml")
        pipeline = MLPipeline(generate=False,
                              allow_fit_to_valid_pipeline=True,
                              budget_per_fit_to_valid_pipeline_seconds=1,
                              fit_pretest_X=X,
                              fit_pretest_y=y,
                              search_space=search_space)

        component = MLPipelineComponent(name='DecisionTreeClassifier',
                                        component_type=MLPipelineComponentType.ESTIMATOR,
                                        hyperparameters={},
                                        search_space_component_node=search_space.estimators['DecisionTreeClassifier'],
                                        subcomponents={})
        pipeline.append(component)

        assert not MLPipeline.is_valid_pipeline(pipeline)

        # Create an invalid pipeline (requirement not respected):
        tpot_search_space = SearchSpace(search_space_file="./misc/search_spaces/simple_requirement_previous_groups_search_space.yml")
        pipeline = MLPipeline(generate=False, search_space=tpot_search_space)

        component = MLPipelineComponent(name='MultinomialNB',
                                        component_type=MLPipelineComponentType.ESTIMATOR,
                                        hyperparameters={},
                                        search_space_component_node=tpot_search_space.estimators['MultinomialNB'],
                                        subcomponents={})
        pipeline.append(component)
        assert not MLPipeline.is_valid_pipeline(pipeline)

        component = MLPipelineComponent(name='MinMaxScaler',
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters={},
                                        search_space_component_node=tpot_search_space.preprocessors['Normalizer'],
                                        subcomponents={})

        pipeline.append(component)
        assert not MLPipeline.is_valid_pipeline(pipeline)


    def test_complexity(self):
        tpot_search_space = SearchSpace("misc/search_spaces/search_space_tpot_light.yml")

        pipeline = MLPipeline.generate_pipeline(search_space=tpot_search_space, initialization=RandomInitialization(10), random_state=numpy.random.RandomState(41))

        assert pipeline.complexity() == 9


class TestMLPipelineComponent():
    def test_generate_hyperparameters(self, simple_search_space):
        bernoulli = simple_search_space.estimators['BernoulliNB']

        hyperparameters = MLPipelineComponent.generate_hyperparameters(bernoulli, use_default=True)
        assert hyperparameters['alpha'] == 1.

        hyperparameters = MLPipelineComponent.generate_hyperparameters(bernoulli, use_default=False, random_state=43)
        assert hyperparameters['alpha'] == 10.

    def test_generate_subcomponents(self):
        ss = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space_with_selectors.yml")

        fwe = ss.algorithms['SelectFwe']
        subcomponents = MLPipelineComponent.generate_subcomponents(fwe, random_state=None)
        assert 'score_func' in subcomponents
        assert subcomponents['score_func'].type == MLPipelineComponentType.SUBCOMPONENT_CALLABLE

        rfe = ss.algorithms['RFE']
        subcomponents = MLPipelineComponent.generate_subcomponents(rfe, random_state=None)
        assert 'estimator' in subcomponents
        assert isinstance(subcomponents['estimator'], MLPipelineComponent)
        assert subcomponents['estimator'].type == MLPipelineComponentType.SUBCOMPONENT_ESTIMATOR


def test_generate_population(simple_search_space):
    population = generate_population(search_space=simple_search_space, number_of_individuals=10)

    for individual in population:
        assert isinstance(individual, MLPipeline)


def test_compile_population(simple_search_space):
    population = generate_population(search_space=simple_search_space, number_of_individuals=10)
    pipelines = compile_population(population)

    X, y = make_classification(15, 5)
    X_train, X_test, y_train, y_test = train_test_split(X, y)

    for pipeline in pipelines:
        model = pipeline.fit(X_train, y_train)
        assert model.predict(X_test)[0] in [0, 1]


def test_compile_population_large_search_space():
    population = generate_population(number_of_individuals=10)
    pipelines = compile_population(population)

    X, y = make_classification(15, 5)
    X_train, X_test, y_train, y_test = train_test_split(X, y)

    try:
        for pipeline, raw_pipeline in zip(pipelines, population):
            if raw_pipeline.components[0].name == 'KNeighborsClassifier':
                X, y = make_classification(150, 5)
                X_train, X_test, y_train, y_test = train_test_split(X, y)

                model = pipeline.fit(X_train, y_train)
                assert model.predict(X_test)[0] in [0, 1]
            else:
                model = pipeline.fit(X_train, y_train)
                assert model.predict(X_test)[0] in [0, 1]
    except Exception as e:
        print("Impossible to fit the pipeline %s" % pipeline)
        pytest.fail(e)
