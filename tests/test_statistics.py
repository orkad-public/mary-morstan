from numpy import random
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split

from marymorstan.marymorstan import MaryMorstan
from marymorstan.objectives import OBJECTIVE

from marymorstan import statistics


class TestStatistics():
    def setup_method(self):
        self.statistics = statistics.Statistics()

    def test__output_dataframe(self):
        random.seed(42)

        mm = MaryMorstan(generations=1, population_size=2,
                         objectives=[OBJECTIVE.PRECISION, OBJECTIVE.RECALL], random_state=42,
                         enable_statistics=True)

        # pipeline_with_stats = MLPipeline(enable_statistics=True)
        features, target = make_classification(144, 10, random_state=42)
        X_train, _, y_train, _ = train_test_split(features, target, test_size=.25, random_state=42)

        mm.optimize(X_train, y_train, random_state=42)

        df = mm.statistics._output_dataframe()

        columns = df.columns.tolist()

        assert 'failing_pipelines' in columns
        assert 'diversity_ratio' in columns

        assert 'fitnesses_per_candidate_selected' in columns
        assert isinstance(df['fitnesses_per_candidate_selected'].iloc[0], list)
        assert len(df['fitnesses_per_candidate_selected'].iloc[1]) > 0

        assert 'test_fitness_best_individual' in columns
