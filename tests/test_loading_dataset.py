import pytest


import importlib
import pandas as pd
from marymorstan.preprocessing.dataset_preprocessing import UnknownDataset


class TestLoadingDataset():
    def setup_method(self):
        self.dataset_preprocessing_module = importlib.import_module("datasets.ucr_dataset_preprocessing")
    def test_bad_location(self):

        with pytest.raises(UnknownDataset) as e:
            dataset = self.dataset_preprocessing_module.MyDataSetPreprocessing("badName")
            assert "not an available dataset" in str(e.value)

    def test_test_good_location(self):

        dataset = self.dataset_preprocessing_module.MyDataSetPreprocessing("GunPoint")
        print(type(dataset.get_dataset))
        assert isinstance(dataset.get_dataset(), pd.DataFrame)

