import copy

import pytest
from numpy import random

from sklearn.datasets import make_classification, make_regression
from sklearn.model_selection import train_test_split

from marymorstan.problems import PROBLEM_TYPE
from marymorstan.objectives import OBJECTIVE
from marymorstan.pipeline import generate_population
from marymorstan.marymorstan import MaryMorstan


class TestMaryMorstan():
    # NOTE https://docs.pytest.org/en/latest/logging.html
    def test_evaluation_timeout_exception(self, caplog):
        random.seed(42)

        mm = MaryMorstan(generations=0, population_size=1, random_state=random.RandomState(42),
                         budget_per_candidate_seconds=.1)

        features, target = make_classification(150000, 50, random_state=42)
        X_train, _, y_train, _ = train_test_split(features, target, test_size=.25, random_state=42)

        mm.optimize(X_train, y_train, random_state=42)

        assert "It took too much time to evaluate the pipeline" in caplog.text
        assert "skip evaluation" in caplog.text

    def test_optimize_classification(self):
        random.seed(42)

        mm = MaryMorstan(generations=1,
                         population_size=2,
                         random_state=random.RandomState(42),
                         budget_per_candidate_seconds=.1)

        features, target = make_classification(100, 25, random_state=42)
        X_train, _, y_train, _ = train_test_split(features, target, test_size=.25, random_state=42)

        pipelines = mm.optimize(X_train, y_train, random_state=42)

        assert len(pipelines) == 2

    def test_optimize_regression(self):
        random.seed(42)

        mm = MaryMorstan(generations=1, population_size=2,
                         search_space_file='./misc/search_spaces/search_space_regression_light.yml',
                         objectives=[OBJECTIVE.RMSE, OBJECTIVE.MAE],
                         random_state=random.RandomState(42), budget_per_candidate_seconds=.1)

        features, target = make_regression(400, 25, random_state=42)
        X_train, _, y_train, _ = train_test_split(features, target, test_size=.25, random_state=42)

        pipelines = mm.optimize(X_train, y_train, random_state=42)

        assert len(pipelines) == 2
        # To minimize the RMSE, we need to put weight to -1 in order to have a negative values (weighted_values)
        #  passed to the selection which maximize the objectives
        assert pipelines[0].fitness.weights == [-1., -1.]

        assert pytest.approx(MaryMorstan.best(pipelines).fitness.values, rel=1e-2) == (40.67, 25.63)
        assert pytest.approx(MaryMorstan.best(pipelines).fitness.weighted_values, rel=1e-2) == (-40.67, -25.63)

    def test_optimize_timeseries_classification(self):
        random.seed(42)

        mm = MaryMorstan(generations=1, population_size=2,
                         min_pipeline_size=1,
                         max_pipeline_size=3,
                         wall_time_optimization_seconds=30,
                         problem_type=PROBLEM_TYPE.TIMESERIES_SUPERVISED_CLASSIFICATION,
                         allow_fit_to_valid_pipeline=True,
                         budget_per_fit_to_valid_pipeline_seconds=.1,
                         ea_space_file='./misc/evolutionary_algorithms_spaces/ea_space_tsc.yml',
                         search_space_file='./misc/search_spaces/time_series_light_classification_space.yml',
                         random_state=random.RandomState(42), budget_per_candidate_seconds=.5)



        import importlib

        dataset_preprocessing_module = importlib.import_module("datasets.gunpoint_dataset_preprocessing")
        dataset = dataset_preprocessing_module.MyDataSetPreprocessing("gunpoint")
        X_train, _, y_train, _ = train_test_split(dataset.get_X(), dataset.get_y(), test_size=.25, random_state=42,
                                                  shuffle=False)

        pipelines = mm.optimize(X_train, y_train, random_state=random.RandomState(43))

    def test_best(self):
        random_state = random.RandomState(42)
        pipelines = generate_population(number_of_individuals=3,
                                        random_state=random_state)

        for index, p in enumerate(pipelines):
            p.fitness.values = (index,)

        # simulate pipeline 0 as the best on fitness (not on test)
        pipelines[0].fitness.values = (3,)

        assert MaryMorstan.best(copy.deepcopy(pipelines)).hash() == pipelines[0].hash()
