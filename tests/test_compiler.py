import logging
import pytest

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import FeatureUnion
from sklearn.preprocessing import FunctionTransformer
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split

from marymorstan.pipeline import MLPipeline
from marymorstan.compiler import MLPipelineCompiler
from marymorstan.search_space import SearchSpace

from . import simple_search_space, simple_ml_pipeline, pipeline_with_selector, \
pipeline_with_combine_dfs_at_beginning, pipeline_with_combine_dfs_multiple_times, pipeline_with_stacking, \
pipeline_with_combine_two_previouses_dfs_at_beginning, pipeline_with_combine_two_previouses_dfs_at_second_step, \
pipeline_with_combine_two_previouses_dfs_successive, pipeline_with_combine_two_previouses_dfs_after_second_step  # noqa: F401


class TestMLPipelineCompiler():
    def setup_method(self):
        self.X , self.y = make_classification(100, 100)
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.X, self.y)

    def test_compile(self, simple_search_space, simple_ml_pipeline):
        pipeline_compiler = MLPipelineCompiler(simple_search_space)
        compiled_pipeline = pipeline_compiler.compile(str(simple_ml_pipeline))

        preprocessing_step = list(compiled_pipeline.named_steps.values())[0]
        assert isinstance(preprocessing_step, TransformerMixin)

        estimator_step = list(compiled_pipeline.named_steps.values())[2]
        assert isinstance(estimator_step, BaseEstimator)

    def test_compile_with_selector(self, pipeline_with_selector):
        pipeline, search_space_with_selector = pipeline_with_selector
        pipeline_compiler = MLPipelineCompiler(search_space_with_selector)
        compiled_pipeline = pipeline_compiler.compile(str(pipeline))

        logging.debug(compiled_pipeline.steps[0])
        logging.debug(compiled_pipeline.steps[1])
        logging.debug(compiled_pipeline.steps[2])

        named_steps = list(compiled_pipeline.named_steps.keys())

        logging.debug(named_steps)

        assert 'RFE_' in named_steps[0]
        assert 'SelectFwe_' in named_steps[1]
        assert 'DecisionTreeClassifier_' in named_steps[2]

        model = compiled_pipeline.fit(self.X_train, self.y_train)
        assert model.predict(self.X_test)[0] in [0, 1]

    def test_compile_with_combine_dfs(self, pipeline_with_combine_dfs_at_beginning, pipeline_with_combine_dfs_multiple_times):
        pipeline, search_space_with_selectors = pipeline_with_combine_dfs_at_beginning
        pipeline_compiler = MLPipelineCompiler(search_space_with_selectors)
        compiled_pipeline = pipeline_compiler.compile(str(pipeline))

        assert pipeline[1].name == 'CombineDFs'

        preprocessor_step = list(compiled_pipeline.named_steps.values())[0]
        assert isinstance(preprocessor_step, FeatureUnion)

        pipeline, search_space_with_combine_dfs = pipeline_with_combine_dfs_multiple_times
        pipeline_compiler = MLPipelineCompiler(search_space_with_combine_dfs)
        compiled_pipeline = pipeline_compiler.compile(str(pipeline))

        named_steps = list(compiled_pipeline.named_steps.keys())

        logging.debug(named_steps)

        assert 'FeatureUnion_' in named_steps[0]
        assert 'FunctionTransformer_copy_' in list(compiled_pipeline.named_steps.values())[0].transformer_list[0][0]
        assert 'FunctionTransformer_copy_' in list(compiled_pipeline.named_steps.values())[0].transformer_list[1][0]

        assert 'FeatureUnion_' in named_steps[1]
        assert 'FunctionTransformer_copy_' in list(compiled_pipeline.named_steps.values())[1].transformer_list[0][0]
        assert 'RFE_' in list(compiled_pipeline.named_steps.values())[1].transformer_list[1][0]

        assert 'SelectFwe_' in named_steps[2]

        assert 'DecisionTreeClassifier_' in named_steps[3]

        model = compiled_pipeline.fit(self.X_train, self.y_train)
        assert model.predict(self.X_test)[0] in [0, 1]

    def test_compile_with_stacking(self, pipeline_with_stacking):
        pipeline, stacking_search_space = pipeline_with_stacking
        pipeline_compiler = MLPipelineCompiler(stacking_search_space)
        compiled_pipeline = pipeline_compiler.compile(str(pipeline))

        named_steps = list(compiled_pipeline.named_steps.keys())

        assert 'MaxAbsScaler_' in named_steps[0]
        assert 'StackingEstimator_BernoulliNB_' in named_steps[1]
        assert 'GaussianNB_' in named_steps[2]

        model = compiled_pipeline.fit(self.X_train, self.y_train)
        assert model.predict(self.X_test)[0] in [0, 1]

    def test_compile_with_combine_two_previouses_dfs(self, pipeline_with_combine_two_previouses_dfs_at_beginning, \
                                                     pipeline_with_combine_two_previouses_dfs_at_second_step, \
                                                     pipeline_with_combine_two_previouses_dfs_successive, \
                                                     pipeline_with_combine_two_previouses_dfs_after_second_step):
        # CombineTwoPreviousesDFs at the beginning
        pipeline, search_space_with_two_combine_dfs = pipeline_with_combine_two_previouses_dfs_at_beginning
        pipeline_compiler = MLPipelineCompiler(search_space_with_two_combine_dfs)
        compiled_pipeline = pipeline_compiler.compile(str(pipeline))

        named_steps = list(compiled_pipeline.named_steps.keys())

        logging.debug(named_steps)

        assert 'FeatureUnion_' in named_steps[0]
        assert 'DecisionTreeClassifier_' in named_steps[1]

        model = compiled_pipeline.fit(self.X_train, self.y_train)
        assert model.predict(self.X_test)[0] in [0, 1]

        # CombineTwoPreviousesDFs at second step
        pipeline, search_space_with_two_combine_dfs = pipeline_with_combine_two_previouses_dfs_at_second_step
        pipeline_compiler = MLPipelineCompiler(search_space_with_two_combine_dfs)
        compiled_pipeline = pipeline_compiler.compile(str(pipeline))

        logging.debug(str(pipeline))

        named_steps = list(compiled_pipeline.named_steps.keys())

        logging.debug(named_steps)

        assert 'FeatureUnion_' in named_steps[0]
        assert 'DecisionTreeClassifier_' in named_steps[1]

        model = compiled_pipeline.fit(self.X_train, self.y_train)
        assert model.predict(self.X_test)[0] in [0, 1]

        # CombineTwoPreviousesDFs in succession
        pipeline, search_space_with_two_combine_dfs = pipeline_with_combine_two_previouses_dfs_successive
        pipeline_compiler = MLPipelineCompiler(search_space_with_two_combine_dfs)
        compiled_pipeline = pipeline_compiler.compile(str(pipeline))

        logging.debug(str(pipeline))

        preprocessor_steps = list(compiled_pipeline.named_steps.values())
        named_steps = list(compiled_pipeline.named_steps.keys())

        logging.debug(named_steps)

        assert 'FeatureUnion_' in named_steps[0]

        assert isinstance(preprocessor_steps[0].transformer_list[0][1], FunctionTransformer)
        assert isinstance(preprocessor_steps[0].transformer_list[1][1], FeatureUnion)

        assert isinstance(preprocessor_steps[0].transformer_list[1][1].transformer_list[0][1], FunctionTransformer)
        assert isinstance(preprocessor_steps[0].transformer_list[1][1].transformer_list[1][1], FunctionTransformer)

        assert 'SelectFwe_' in named_steps[1]
        assert 'DecisionTreeClassifier_' in named_steps[2]

        model = compiled_pipeline.fit(self.X_train, self.y_train)
        assert model.predict(self.X_test)[0] in [0, 1]

        # CombineTwoPreviousesDFs after the second step
        pipeline, search_space_with_two_combine_dfs = pipeline_with_combine_two_previouses_dfs_after_second_step
        pipeline_compiler = MLPipelineCompiler(search_space_with_two_combine_dfs)
        compiled_pipeline = pipeline_compiler.compile(str(pipeline))

        logging.debug(str(pipeline))

        named_steps = list(compiled_pipeline.named_steps.keys())

        logging.debug(named_steps)

        assert 'FeatureUnion_' in named_steps[0]
        assert 'RFE_' in list(compiled_pipeline.named_steps.values())[0].transformer_list[0][0]
        assert 'RFE_' in list(compiled_pipeline.named_steps.values())[0].transformer_list[1][0]

        assert 'DecisionTreeClassifier_' in named_steps[1]

        model = compiled_pipeline.fit(self.X_train, self.y_train)
        assert model.predict(self.X_test)[0] in [0, 1]


    def test_timeout_complex_pipeline(self):
        # FIXME re-enable after experiments to compare with TPOT
        pytest.skip("skip timeout part for now, only valid when swallow_exc=False, which is not to respect TPOT v0.11.5 bias")
        search_space = SearchSpace(search_space_file="./misc/search_spaces/search_space_tpot_no_xgb.yml")
        pipeline_compiler = MLPipelineCompiler(search_space)
        pipeline = pipeline_compiler.from_raw('''["BernoulliNB({'alpha': 1.0})", "OneHotEncoder({'minimum_fraction': 0.1, 'sparse': False, 'threshold': 10})", "PolynomialFeatures({'degree': 2, 'include_bias': False, 'interaction_only': False})", 'CombineDFs({})']''')
        pipeline.allow_fit_to_valid_pipeline = True
        pipeline.budget_per_fit_to_valid_pipeline_seconds = .1
        pipeline.max_number_of_fits = 2

        # FIXME jasmine dataset is little bit too much
        import importlib

        dataset_preprocessing_module = importlib.import_module("datasets.openml_dataset_preprocessing")
        ds = dataset_preprocessing_module.MyDataSetPreprocessing("jasmine", encode_features_to_int=True,
                                                                 encode_targets_to_int=True)

        pipeline.fit_pretest_X = ds.get_X()
        pipeline.fit_pretest_y = ds.get_y()
        assert not MLPipeline.is_valid_pipeline(pipeline)
