import pytest
import numpy

from marymorstan.initialization import RandomInitialization
from marymorstan.search_space import SearchSpace

from marymorstan.pipeline import MLPipeline


@pytest.fixture()
def simple_search_space():
    return SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space.yml")


@pytest.fixture()
def simple_ml_pipeline(simple_search_space):
    return MLPipeline(search_space=simple_search_space, random_state=42)


@pytest.fixture()
def pipeline_with_selector():
    search_space_with_selectors = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space_with_selectors.yml")
    pipeline = MLPipeline.generate_pipeline(search_space=search_space_with_selectors, random_state=numpy.random.RandomState(41))
    return pipeline, search_space_with_selectors


@pytest.fixture()
def pipeline_with_combine_dfs_at_beginning():
    search_space_with_combine_dfs = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space_with_combine_dfs.yml")
    pipeline = MLPipeline.generate_pipeline(search_space=search_space_with_combine_dfs, initialization=RandomInitialization(2, 2), random_state=numpy.random.RandomState(46))
    return pipeline, search_space_with_combine_dfs


@pytest.fixture()
def pipeline_with_combine_dfs_multiple_times():
    search_space_with_combine_dfs = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space_with_combine_dfs.yml")
    pipeline = MLPipeline.generate_pipeline(search_space=search_space_with_combine_dfs, initialization=RandomInitialization(5, 5), random_state=numpy.random.RandomState(42))
    return pipeline, search_space_with_combine_dfs


@pytest.fixture()
def pipeline_with_stacking():
    stacking_search_space = SearchSpace("misc/search_spaces/tests/search_space_stacking.yml")
    # pipeline with 2 estimators (one used as synthetic feature) and one preprocessor
    pipeline = MLPipeline.generate_pipeline(search_space=stacking_search_space, initialization=RandomInitialization(3, 3), random_state=numpy.random.RandomState(48))
    return pipeline, stacking_search_space


@pytest.fixture()
def pipeline_with_combine_two_previouses_dfs_at_beginning():
    search_space_with_two_combine_dfs = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space_with_combine_two_previouses_dfs.yml")
    pipeline = MLPipeline.generate_pipeline(search_space=search_space_with_two_combine_dfs, initialization=RandomInitialization(2, 2), random_state=numpy.random.RandomState(46))
    return pipeline, search_space_with_two_combine_dfs


@pytest.fixture()
def pipeline_with_combine_two_previouses_dfs_at_second_step():
    search_space_with_two_combine_dfs = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space_with_combine_two_previouses_dfs.yml")
    pipeline = MLPipeline.generate_pipeline(search_space=search_space_with_two_combine_dfs, initialization=RandomInitialization(3, 3), random_state=numpy.random.RandomState(46))
    return pipeline, search_space_with_two_combine_dfs


@pytest.fixture()
def pipeline_with_combine_two_previouses_dfs_successive():
    search_space_with_two_combine_dfs = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space_with_combine_two_previouses_dfs.yml")
    pipeline = MLPipeline.generate_pipeline(search_space=search_space_with_two_combine_dfs, initialization=RandomInitialization(4, 4), random_state=numpy.random.RandomState(48))
    return pipeline, search_space_with_two_combine_dfs


@pytest.fixture()
def pipeline_with_combine_two_previouses_dfs_after_second_step():
    search_space_with_two_combine_dfs = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space_with_combine_two_previouses_dfs.yml")
    pipeline = MLPipeline.generate_pipeline(search_space=search_space_with_two_combine_dfs, initialization=RandomInitialization(4, 4), random_state=numpy.random.RandomState(62))
    return pipeline, search_space_with_two_combine_dfs
