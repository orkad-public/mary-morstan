import pytest

from marymorstan.problems import PROBLEM_TYPE
from marymorstan.objectives import Objectives, OBJECTIVE, InvalidObjectives


class TestOBJECTIVE():
    def test_str(self):
        assert str(OBJECTIVE.NEGATIVE_ROOT_MEAN_SQUARE) == 'negative_root_mean_square'
        assert str(OBJECTIVE.R2) == 'r2'

    def test_argparse(self):
        accuracy_objective_name = str(OBJECTIVE.ACCURACY)
        assert OBJECTIVE.argparse(accuracy_objective_name) == OBJECTIVE.ACCURACY
        assert OBJECTIVE.argparse('innexistantObjective') == 'innexistantObjective'

    def test_is_custom_objective(self):
        assert OBJECTIVE.is_custom_objective(OBJECTIVE.MIN_PIPELINE_SIZE)
        assert not OBJECTIVE.is_custom_objective(OBJECTIVE.R2)

    def test_is_minimize(self):
        assert OBJECTIVE.is_minimizer(OBJECTIVE.MIN_PIPELINE_SIZE)
        assert OBJECTIVE.is_minimizer(OBJECTIVE.R2)
        assert OBJECTIVE.is_minimizer(OBJECTIVE.RMSE)
        assert not OBJECTIVE.is_minimizer(OBJECTIVE.BALANCED_ACCURACY)


class TestObjectives():
    def test_problem_type_not_handled(self):
        with pytest.raises(InvalidObjectives) as e:
            Objectives(problem_type="unknown")

        assert "cannot define" in str(e.value)

        with pytest.raises(InvalidObjectives) as e:
            Objectives(objectives=[type('obj', (object,), {'name' : 'innexistantObjective', 'value': 'noValue'})])

        assert "impossible to deduce" in str(e.value)

    def test_scorers_with_regression_objective_callable(self):
        objectives = Objectives(objectives=[OBJECTIVE.R2])
        assert callable(objectives.scorers[OBJECTIVE.R2.name])

    def test_scorers_with_default_regression_objective_metrics_sklearn(self):
        objectives = Objectives(problem_type=PROBLEM_TYPE.SUPERVISED_REGRESSION)
        assert callable(objectives.scorers[OBJECTIVE.RMSE.name])

    def test_scorers_with_regression_objective_own_mapping(self):
        objectives = Objectives(objectives=[OBJECTIVE.MAE])
        assert callable(objectives.scorers[OBJECTIVE.MAE.name])

    def test_scorers_with_one_classification_objective(self):
        objectives = Objectives(objectives=[OBJECTIVE.F1])
        assert callable(objectives.scorers[OBJECTIVE.F1.name])

    def test_scorers_with_two_regression_objectives(self):
        objectives = Objectives(objectives=[OBJECTIVE.NEGATIVE_ROOT_MEAN_SQUARE, OBJECTIVE.RMSE])
        assert callable(objectives.scorers[OBJECTIVE.NEGATIVE_ROOT_MEAN_SQUARE.name])
        assert callable(objectives.scorers[OBJECTIVE.RMSE.name])

    def test_scorers_with_custom_objective(self):
        objectives = Objectives(objectives=[OBJECTIVE.MAE, OBJECTIVE.MIN_PIPELINE_SIZE, OBJECTIVE.NEGATIVE_ROOT_MEAN_SQUARE, OBJECTIVE.TPOT_BALANCED_ACCURACY])
        assert len(objectives.scorers) == 3
        assert list(objectives.scorers.keys()) == [OBJECTIVE.MAE.name, OBJECTIVE.NEGATIVE_ROOT_MEAN_SQUARE.name, OBJECTIVE.TPOT_BALANCED_ACCURACY.name]

    def test_ordered_weights(self):
        objectives = Objectives(objectives=[OBJECTIVE.MAE, OBJECTIVE.MIN_PIPELINE_SIZE, OBJECTIVE.NEGATIVE_ROOT_MEAN_SQUARE])
        assert objectives.ordered_weights == [-1., -1., 1.]

    def test_min_pipeline_size_present(self):
        objectives = Objectives(objectives=[OBJECTIVE.MIN_PIPELINE_SIZE, OBJECTIVE.RMSE])
        assert objectives.min_pipeline_size_present
        objectives = Objectives(objectives=[OBJECTIVE.RMSE])
        assert not objectives.min_pipeline_size_present

    def test_index(self):
        objectives = Objectives(objectives=[OBJECTIVE.MIN_PIPELINE_SIZE, OBJECTIVE.RMSE])
        assert objectives.index(OBJECTIVE.RMSE) == 1
        assert objectives.index(OBJECTIVE.MIN_PIPELINE_SIZE) == 0

        with pytest.raises(ValueError):
            objectives.index('inexistant')

    def test_default_values(self):
        objectives = Objectives(objectives=[OBJECTIVE.MAE, OBJECTIVE.MIN_PIPELINE_SIZE, OBJECTIVE.NEGATIVE_ROOT_MEAN_SQUARE])
        assert objectives.default_values == (float('inf'), float('inf'), -float('inf'))

    def test_scorer_default_values(self):
        objectives = Objectives(objectives=[OBJECTIVE.MAE, OBJECTIVE.MIN_PIPELINE_SIZE, OBJECTIVE.NEGATIVE_ROOT_MEAN_SQUARE])
        assert objectives.scorer_default_values == (float('inf'), -float('inf'))

    def test_ordered_values_for_objectives(self):
        objectives = Objectives(objectives=[OBJECTIVE.MAE, OBJECTIVE.MIN_PIPELINE_SIZE, OBJECTIVE.NEGATIVE_ROOT_MEAN_SQUARE])
        values = objectives.ordered_values_for_objectives({OBJECTIVE.MIN_PIPELINE_SIZE: 2, OBJECTIVE.NEGATIVE_ROOT_MEAN_SQUARE: 1, OBJECTIVE.MAE: 5})
        assert values == (5, 2, 1)
