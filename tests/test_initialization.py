import pytest

from marymorstan.search_space import SearchSpace
from marymorstan.initialization import LHSInitialization


class TestLHSInitialization():
    def setup_method(self):
        self.search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_groups_search_space.yml")
        self.initialization = LHSInitialization(min_size=1, max_size=3, population_size=2, search_space=self.search_space, random_state=42)

    def test_pop_pipeline_size(self):
        assert self.initialization.pop_pipeline_size() != 0
        assert self.initialization.pop_pipeline_size() != 0

        with pytest.raises(Exception) as e:
            self.initialization.pop_pipeline_size()
            assert "No more pipeline size availables" in str(e.value)

    def test_get_estimator(self):
        initialization = LHSInitialization(min_size=1, population_size=4, \
                                           search_space=self.search_space, random_state=42)

        assert initialization.get_estimator() == 'GaussianNB'
        assert initialization.get_estimator() == 'BernoulliNB'
        assert initialization.get_estimator() == 'MultinomialNB'
        # one cycle, go to the beginning
        assert initialization.get_estimator() == 'GaussianNB'

    def test_get_preprocessor(self):
        # degraded mode (random preprocessor selected)
        assert self.initialization.get_preprocessor(1) != 'Normalizer'

        # normal situation
        initialization = LHSInitialization(min_size=1, max_size=3, population_size=100, \
                                           search_space=self.search_space, random_state=42)

        assert initialization.get_preprocessor(1) == 'Normalizer'
        assert initialization.get_preprocessor(2) == 'Normalizer'

        for _ in range(len(self.search_space.list_of_estimators) + 1):
            estimator = initialization.get_estimator()

        assert estimator == 'GaussianNB'
        assert initialization.get_preprocessor(1) == 'StandardScaler'
        assert initialization.get_preprocessor(2) == 'Normalizer'

        for _ in range(len(self.search_space.list_of_estimators) + 1):
            for _ in range(len(self.search_space.list_of_preprocessors)):
                initialization.get_estimator()

        assert initialization.get_preprocessor(1) == 'MaxAbsScaler'
        assert initialization.get_preprocessor(2) == 'StandardScaler'


    def test__degraded_index(self):
        assert self.initialization._degraded_index == 0

        initialization = LHSInitialization(min_size=1, population_size=10, \
                                           search_space=self.search_space, random_state=42)

        assert initialization._degraded_index == None

        initialization = LHSInitialization(min_size=1, max_size=3, population_size=10, \
                                           search_space=self.search_space, random_state=42)

        assert initialization._degraded_index == 2
