import pytest

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split

from marymorstan.objectives import Objectives, OBJECTIVE
from marymorstan.evaluation import Evaluation, EVALUATION_STRATEGY, InvalidStrategy

from . import simple_search_space, simple_ml_pipeline  # noqa: F401


class TestEvaluation():
    def setup_method(self):
        X, y = make_classification(15, 5, random_state=42)
        self.X_train, _, self.y_train, _ = train_test_split(X, y, random_state=42)
        self.evaluator = Evaluation(X_train=self.X_train, y_train=self.y_train,
                              objectives=Objectives(),
                              test_size=.1,
                              time_limit_seconds=.2,
                              number_of_jobs=1,
                              random_state=42)

    def test_innexistant_strategy(self, simple_ml_pipeline):
        with pytest.raises(InvalidStrategy) as e:
            self.evaluator.evaluate(pipelines=[simple_ml_pipeline], evaluation_strategy="InnexistantStrategy")

    def test_evaluate_strategy_holdout(self, simple_ml_pipeline):
        assert not simple_ml_pipeline.fitness.values
        self.evaluator.evaluate(pipelines=[simple_ml_pipeline], evaluation_strategy=EVALUATION_STRATEGY.HOLDOUT)
        assert simple_ml_pipeline.fitness.values != None

    def test_evaluate_strategy_k_fold(self, simple_ml_pipeline):
        assert not simple_ml_pipeline.fitness.values
        self.evaluator.evaluate(pipelines=[simple_ml_pipeline], evaluation_strategy=EVALUATION_STRATEGY.K_FOLD)
        assert simple_ml_pipeline.fitness.values[0] == pytest.approx(0.66, abs=0.1)

    def test_evaluate_strategy_repeat_k_fold(self, simple_ml_pipeline):
        evaluator = Evaluation(X_train=self.X_train, y_train=self.y_train,
                              objectives=Objectives(objectives=[OBJECTIVE.ACCURACY, OBJECTIVE.PRECISION]),
                              test_size=.1,
                              time_limit_seconds=.2,
                              number_of_jobs=1,
                              random_state=42)

        assert not simple_ml_pipeline.fitness.values
        evaluator.evaluate(pipelines=[simple_ml_pipeline], evaluation_strategy=EVALUATION_STRATEGY.REPEAT_K_FOLD)
        # NOTE should be better than k_fold
        assert simple_ml_pipeline.fitness.values[0] == pytest.approx(0.8, abs=0.1)
        assert simple_ml_pipeline.fitness.values[1] == pytest.approx(0.7, abs=0.1)
        del simple_ml_pipeline.fitness.values

    def test_evaluate_strategy_shuffle_split(self, simple_ml_pipeline):
        assert not simple_ml_pipeline.fitness.values
        self.evaluator.evaluate(pipelines=[simple_ml_pipeline], evaluation_strategy=EVALUATION_STRATEGY.SHUFFLE_SPLIT)
        assert simple_ml_pipeline.fitness.values == (0.8,)

    def test_evaluate_strategy_stratified_shuffle_split(self, simple_ml_pipeline):
        assert not simple_ml_pipeline.fitness.values
        self.evaluator.evaluate(pipelines=[simple_ml_pipeline], evaluation_strategy=EVALUATION_STRATEGY.STRATIFIED_SHUFFLE_SPLIT)
        assert simple_ml_pipeline.fitness.values == (0.9,)

    def test_evaluate_strategy_stratified_k_fold(self, simple_ml_pipeline):
        assert not simple_ml_pipeline.fitness.values
        self.evaluator.evaluate(pipelines=[simple_ml_pipeline], evaluation_strategy=EVALUATION_STRATEGY.STRATIFIED_K_FOLD)
        assert simple_ml_pipeline.fitness.values == (0.9,)

    def test_evaluate_strategy_timeseries_k_split(self, simple_ml_pipeline):
        assert not simple_ml_pipeline.fitness.values
        self.evaluator.evaluate(pipelines=[simple_ml_pipeline], evaluation_strategy=EVALUATION_STRATEGY.TIME_SERIES_K_SPLIT)
        assert simple_ml_pipeline.fitness.values == (0.6,)

    def test_evaluate_strategy_stratified_k_fold_with_objectives(self, simple_ml_pipeline):
        evaluator = Evaluation(X_train=self.X_train, y_train=self.y_train,
                              objectives=Objectives(objectives=[OBJECTIVE.ACCURACY, OBJECTIVE.F1]),
                              time_limit_seconds=.2,
                              number_of_jobs=1,
                              random_state=42)

        assert not simple_ml_pipeline.fitness.values
        evaluator.evaluate(pipelines=[simple_ml_pipeline], evaluation_strategy=EVALUATION_STRATEGY.STRATIFIED_K_FOLD)
        assert simple_ml_pipeline.fitness.values == (0.9, 0.8)
