import pytest
import copy

from marymorstan.initialization import RandomInitialization
from marymorstan.search_space import SearchSpace, SEARCH_SPACE_KEY
from marymorstan.pipeline import MLPipeline, MLPipelineComponent, MLPipelineComponentType
from marymorstan import variation

from . import simple_search_space  # noqa: F401


class TestMutationGaussian():
    def test_call(self, caplog):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_hyperparameters_search_space.yml")
        pipeline = MLPipeline(search_space=search_space, random_state=39)

        mutated_individual = variation.MutationGaussian(sigma_ratio=.1)(individual=copy.deepcopy(pipeline), random_state=36)

        assert mutated_individual.components[0].hyperparameters['tol'] >= 0
        assert pipeline.components[0].hyperparameters['tol'] != mutated_individual.components[0].hyperparameters['tol']
        assert mutated_individual.components[0].hyperparameters['tol'] == pytest.approx(pipeline.components[0].hyperparameters['tol'] + 0.005375, abs=0.000001)

        mutated_individual = variation.MutationGaussian()(individual=copy.deepcopy(pipeline), random_state=42)

        assert mutated_individual.components[0].hyperparameters['tol'] >= 0
        assert pipeline.components[0].hyperparameters['tol'] == mutated_individual.components[0].hyperparameters['tol']

        search_space = SearchSpace(search_space_file="./misc/search_spaces/search_space_tpot.yml")
        pipeline = MLPipeline(search_space=search_space, initialization=RandomInitialization(1), random_state=40)

        mutated_individual = variation.MutationGaussian(sigma_ratio=.1)(individual=copy.deepcopy(pipeline), random_state=36)

        assert mutated_individual.components[0].hyperparameters['min_samples_split'] != pipeline.components[0].hyperparameters['min_samples_split']

        mutated_individual = variation.MutationGaussian(sigma_ratio=.9)(individual=copy.deepcopy(pipeline), random_state=34)
        assert "New value 31 for the hyperparameter min_samples_leaf violates the boundary define in search space" in caplog.text
        assert mutated_individual.components[0].hyperparameters['min_samples_leaf'] == 21


class TestMutationNGaussian():
    def test_call(self, caplog):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_hyperparameters_search_space.yml")
        pipeline = MLPipeline(search_space=search_space, random_state=39)

        mutated_individual = variation.MutationNGaussian(n_ratio=.72, sigma_ratio=.1)(individual=copy.deepcopy(pipeline), random_state=36)

        # remains unchanged with n_ratio <= .72
        assert mutated_individual.components[0].hyperparameters['tol'] >= 0
        assert pipeline.components[0].hyperparameters['tol'] == mutated_individual.components[0].hyperparameters['tol']


class TestMutationNUniformInteger():
    def test_call(self):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_hyperparameters_search_space.yml")
        pipeline = MLPipeline(search_space=search_space, random_state=40, enable_statistics=True)

        # ratio too low => no change on the individual
        mutated_individual = variation.MutationNUniformInteger(n_ratio=.1)(individual=copy.deepcopy(pipeline), random_state=42)

        assert mutated_individual.hash() == pipeline.hash()


class TestMutationUniformInteger():
    def test_call(self):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_hyperparameters_search_space.yml")
        pipeline = MLPipeline(search_space=search_space, random_state=40, enable_statistics=True)

        mutated_individual = variation.MutationUniformInteger()(individual=copy.deepcopy(pipeline), random_state=42)

        assert mutated_individual.components[0].hyperparameters['C'] >= 0
        assert pipeline.components[0].hyperparameters['C'] != mutated_individual.components[0].hyperparameters['C']

        assert len(mutated_individual.statistics.variations[0]) == 1
        assert 'LinearSVC' == mutated_individual.statistics.variations[0][0]['components'][0]
        assert 10 == mutated_individual.statistics.variations[0][0]['parameters']['upper_bounds'][0]

        # NOTE tol cant be changed because the difference between the bounds are <= 1
        assert mutated_individual.components[0].hyperparameters['tol'] >= 0
        assert pipeline.components[0].hyperparameters['tol'] == mutated_individual.components[0].hyperparameters['tol']

        mutated_individual = variation.MutationUniformInteger(lower_bound=0., upper_bound=2.)(individual=copy.deepcopy(pipeline), random_state=37)

        assert mutated_individual.components[0].hyperparameters['C'] >= 0
        assert pipeline.components[0].hyperparameters['C'] == mutated_individual.components[0].hyperparameters['C']


class TestMutationOneRandom():
    def test_call(self):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_hyperparameters_search_space.yml")

        # Test first individual
        pipeline = MLPipeline(search_space=search_space, random_state=39)
        assert pipeline.components[2].hyperparameters['norm'] == 'l1'

        mutated_individual = variation.MutationOneRandom()(individual=copy.deepcopy(pipeline), random_state=42)
        assert mutated_individual.components[2].hyperparameters['norm'] == 'l1'

        # Test another individual with the same mutation
        pipeline = MLPipeline(search_space=search_space, random_state=38)
        assert pipeline.components[2].hyperparameters['norm'] == 'l2'

        mutated_individual = variation.MutationOneRandom()(individual=copy.deepcopy(pipeline), random_state=42)
        assert mutated_individual.components[2].hyperparameters['norm'] == 'l1'


class TestMutationInsert():
    def test_call(self):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_hyperparameters_search_space.yml")

        pipeline = MLPipeline(search_space=search_space, random_state=39)
        assert pipeline.components[2].hyperparameters['norm'] == 'l1'
        assert len(pipeline) == 3

        # insert mutation that works
        mutated_individual = variation.MutationInsert()(individual=copy.deepcopy(pipeline), random_state=44)
        assert mutated_individual.components[3].hyperparameters['norm'] == 'l1'
        assert len(mutated_individual) == 4

        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/impossible_requirement_search_space.yml", check_search_space_file=False)

        # HACK allow_fit_to_valid_pipeline=True has been set to autorize the creation of an invalid pipeline
        pipeline = MLPipeline(search_space=search_space, initialization=RandomInitialization(2), allow_fit_to_valid_pipeline=True, random_state=39)
        assert len(pipeline) == 1

        mutated_individual = variation.MutationInsert()(individual=copy.deepcopy(pipeline), random_state=44)
        assert len(mutated_individual) == 1


class TestMutationDelete():
    def test_call(self):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_search_space.yml")

        pipeline = MLPipeline(search_space=search_space, random_state=42)
        assert pipeline.components[0].name == 'MultinomialNB'
        assert len(pipeline) == 3

        # deletion that makes invalid pipeline
        mutated_individual = variation.MutationDelete()(individual=copy.deepcopy(pipeline), random_state=44)
        assert len(mutated_individual) == 3

        # deletes that work until it cannot delete anymore
        mutated_individual = variation.MutationDelete()(individual=mutated_individual, random_state=39)
        assert len(mutated_individual) == 2

        mutated_individual = variation.MutationDelete()(individual=mutated_individual, random_state=39)
        assert len(mutated_individual) == 1

        mutated_individual = variation.MutationDelete()(individual=mutated_individual, random_state=39)
        assert len(mutated_individual) == 1


class TestMutationReplace():
    def test_call(self, simple_search_space):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_search_space.yml")

        pipeline = MLPipeline(search_space=search_space, random_state=42)
        assert len(pipeline) == 3
        assert pipeline.components[0].name == 'MultinomialNB'

        # change estimator
        mutated_individual = variation.MutationReplace()(individual=copy.deepcopy(pipeline), random_state=44)
        assert len(mutated_individual) == 3
        assert mutated_individual.components[0].name != pipeline.components[0].name
        assert mutated_individual.components[1].name == pipeline.components[1].name
        assert mutated_individual.components[2].name == pipeline.components[2].name

        # change preprocessor
        mutated_individual = variation.MutationReplace()(individual=copy.deepcopy(pipeline), random_state=40)
        assert mutated_individual.components[0].name == pipeline.components[0].name
        assert mutated_individual.components[1].name == pipeline.components[1].name
        assert mutated_individual.components[2].name != pipeline.components[2].name

        # invalid pipeline => mutation not happening
        mutated_individual = variation.MutationReplace()(individual=copy.deepcopy(pipeline), random_state=32)
        assert mutated_individual.hash() == pipeline.hash()

        # change hyperparameter
        pipeline = MLPipeline(search_space=simple_search_space, initialization=RandomInitialization(2), random_state=42)
        assert pipeline.components[0].name == 'GaussianNB'
        assert pipeline.components[1].hyperparameters['norm'] == 'l1'

        mutated_individual = variation.MutationReplace()(individual=copy.deepcopy(pipeline), random_state=52)
        assert mutated_individual.components[0].name == pipeline.components[0].name
        assert mutated_individual.components[1].name == pipeline.components[1].name
        assert mutated_individual.components[1].hyperparameters['norm'] == 'l2'


class TestMutationReplaceNodeOnly():
    def test_call(self, simple_search_space):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_search_space.yml")

        pipeline = MLPipeline(search_space=search_space, random_state=42)
        assert len(pipeline) == 3
        assert pipeline.components[0].name == 'MultinomialNB'

        # change estimator
        mutated_individual = variation.MutationReplaceNodeOnly()(individual=copy.deepcopy(pipeline), random_state=44)
        assert len(mutated_individual) == 3
        assert mutated_individual.components[0].name != pipeline.components[0].name
        assert mutated_individual.components[1].name == pipeline.components[1].name
        assert mutated_individual.components[2].name == pipeline.components[2].name

        # change preprocessor
        mutated_individual = variation.MutationReplaceNodeOnly()(individual=copy.deepcopy(pipeline), random_state=40)
        assert mutated_individual.components[0].name == pipeline.components[0].name
        assert mutated_individual.components[1].name == pipeline.components[1].name
        assert mutated_individual.components[2].name != pipeline.components[2].name

        # invalid pipeline => mutation not happening
        mutated_individual = variation.MutationReplace()(individual=copy.deepcopy(pipeline), random_state=32)
        assert mutated_individual.hash() == pipeline.hash()



class TestCrossoverOnePointExactly():
    def test_call(self):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_search_space.yml")

        pipeline_1 = MLPipeline(search_space=search_space, initialization=RandomInitialization(2), random_state=39)
        assert len(pipeline_1) == 2
        assert pipeline_1.components[0].name == 'BernoulliNB'
        assert pipeline_1.components[1].name == 'StandardScaler'

        pipeline_2 = MLPipeline(search_space=search_space, initialization=RandomInitialization(2), random_state=40)
        assert len(pipeline_2) == 2
        assert pipeline_2.components[0].name == 'MultinomialNB'
        assert pipeline_2.components[1].name == 'MinMaxScaler'

        cx_pipeline_1 = copy.deepcopy(pipeline_1)
        cx_pipeline_2 = copy.deepcopy(pipeline_2)

        variation.CrossoverOnePointExactly()(individual_1=cx_pipeline_1, individual_2=cx_pipeline_2, random_state=44)

        # cxpoint on estimator node
        assert cx_pipeline_1.hash() == pipeline_1.hash()
        assert cx_pipeline_2.hash() != pipeline_2.hash()
        assert cx_pipeline_2.components[0].name == 'BernoulliNB'
        assert cx_pipeline_2.components[1].name == 'MinMaxScaler'


class TestCrossoverOnePoint():
    def test_call(self, caplog):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_search_space.yml")

        pipeline_1 = MLPipeline(search_space=search_space, random_state=39)
        assert len(pipeline_1) == 3
        assert pipeline_1.components[0].name == 'BernoulliNB'
        assert pipeline_1.components[1].name == 'StandardScaler'
        assert pipeline_1.components[2].name == 'Normalizer'

        pipeline_2 = MLPipeline(search_space=search_space, random_state=42)
        assert len(pipeline_2) == 3
        assert pipeline_2.components[0].name == 'MultinomialNB'
        assert pipeline_2.components[1].name == 'MinMaxScaler'
        assert pipeline_2.components[2].name == 'MaxAbsScaler'

        cx_pipeline_1 = copy.deepcopy(pipeline_1)
        cx_pipeline_2 = copy.deepcopy(pipeline_2)
        hash_before_cx = cx_pipeline_1.hash()

        variation.CrossoverOnePoint()(individual_1=cx_pipeline_1, individual_2=cx_pipeline_2, random_state=44)

        # cx works for candidate 1
        assert cx_pipeline_1.components[0].name == 'BernoulliNB'
        assert cx_pipeline_1.components[1].name == 'MinMaxScaler'
        assert cx_pipeline_1.components[2].name == 'MaxAbsScaler'
        assert cx_pipeline_1.hash() != hash_before_cx

        # cx does not work for candidate 2 (not valid with forbiddens previous)
        assert cx_pipeline_2.hash() == pipeline_2.hash()
        assert "makes an invalid pipeline for individual_2" in caplog.text

        # check it works when pipeline size is 1
        pipeline_1 = MLPipeline(search_space=search_space, initialization=RandomInitialization(1), random_state=39)
        pipeline_2 = MLPipeline(search_space=search_space, initialization=RandomInitialization(2), random_state=42)
        cx_pipeline_1, cx_pipeline_2 = variation.CrossoverOnePoint()(individual_1=copy.deepcopy(pipeline_1), individual_2=copy.deepcopy(pipeline_2), random_state=43)
        assert cx_pipeline_1.hash() != pipeline_1.hash()
        assert cx_pipeline_2.hash() != pipeline_2.hash()


class TestCrossoverOnePointAverage():
    def test_call(self, caplog):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_search_space.yml")

        pipeline_1 = MLPipeline(search_space=search_space, random_state=39)
        assert len(pipeline_1) == 3
        assert pipeline_1.components[0].name == 'BernoulliNB'
        assert pipeline_1.components[1].name == 'StandardScaler'
        assert pipeline_1.components[2].name == 'Normalizer'

        pipeline_2 = MLPipeline(search_space=search_space, random_state=42)
        assert len(pipeline_2) == 3
        assert pipeline_2.components[0].name == 'MultinomialNB'
        assert pipeline_2.components[1].name == 'MinMaxScaler'
        assert pipeline_2.components[2].name == 'MaxAbsScaler'

        cx_pipeline_1 = copy.deepcopy(pipeline_1)
        cx_pipeline_2 = copy.deepcopy(pipeline_2)
        variation.CrossoverOnePointAverage()(individual_1=cx_pipeline_1, individual_2=cx_pipeline_2, random_state=44)

        # no common components => no change
        assert cx_pipeline_1.hash() == pipeline_1.hash()
        assert cx_pipeline_2.hash() == pipeline_2.hash()
        assert "no common components" in caplog.text

        pipeline_3 = MLPipeline(search_space=search_space, random_state=45)
        assert len(pipeline_3) == 3
        assert pipeline_3.components[0].name == 'MultinomialNB'
        assert pipeline_3.components[1].name == 'MinMaxScaler'
        assert pipeline_3.components[2].name == 'StandardScaler'

        cx_pipeline_2 = copy.deepcopy(pipeline_2)
        cx_pipeline_3 = copy.deepcopy(pipeline_3)

        variation.CrossoverOnePointAverage()(individual_1=cx_pipeline_2, individual_2=cx_pipeline_3, random_state=44)

        # no hyperparameters for the selected component => no change
        assert cx_pipeline_2.hash() == pipeline_2.hash()
        assert cx_pipeline_3.hash() == pipeline_3.hash()
        assert "no hyperparameters for component" in caplog.text

        pipeline_4 = MLPipeline(search_space=search_space, initialization=RandomInitialization(2), random_state=50)
        assert len(pipeline_4) == 2
        assert pipeline_4.components[0].name == 'GaussianNB'
        assert pipeline_4.components[1].name == 'Normalizer'

        cx_pipeline_1 = copy.deepcopy(pipeline_1)
        cx_pipeline_4 = copy.deepcopy(pipeline_4)

        variation.CrossoverOnePointAverage()(individual_1=cx_pipeline_1, individual_2=cx_pipeline_4, random_state=44)
        assert "no hyperparameters of type number for component" in caplog.text

        pipeline_5 = MLPipeline(search_space=search_space, random_state=46)
        assert len(pipeline_5) == 3
        assert pipeline_5.components[0].name == 'BernoulliNB'
        assert pipeline_5.components[1].name == 'Normalizer'
        assert pipeline_5.components[2].name == 'MaxAbsScaler'

        cx_pipeline_1 = copy.deepcopy(pipeline_1)
        cx_pipeline_5 = copy.deepcopy(pipeline_5)

        # alpha parameter changed for one candidate
        variation.CrossoverOnePointAverage()(individual_1=cx_pipeline_1, individual_2=cx_pipeline_5, random_state=44)
        assert cx_pipeline_1.components[0].hyperparameters['alpha'] == 50.005
        assert cx_pipeline_5.components[0].hyperparameters['alpha'] == 100


class TestCrossoverTwoPoint():
    def test_call(self, caplog):
        search_space = SearchSpace(search_space_file="./misc/search_spaces/search_space_tpot.yml")

        # build first pipeline
        pipeline_1 = MLPipeline(search_space=search_space, generate=False)

        component = MLPipelineComponent(name="DecisionTreeClassifier",
                                        component_type=MLPipelineComponentType.ESTIMATOR,
                                        hyperparameters=MLPipelineComponent.generate_hyperparameters(search_space_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['DecisionTreeClassifier'], random_state=42),
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['DecisionTreeClassifier'],
                                        subcomponents={})
        pipeline_1.append(component)

        component = MLPipelineComponent(name="CombineTwoPreviousesDFs",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters={},
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['CombineTwoPreviousesDFs'],
                                        subcomponents={})
        pipeline_1.append(component)

        component = MLPipelineComponent(name="RandomForestClassifier",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters=MLPipelineComponent.generate_hyperparameters(search_space_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['RandomForestClassifier'], random_state=42),
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['RandomForestClassifier'],
                                        subcomponents={})
        pipeline_1.append(component)


        component = MLPipelineComponent(name="StandardScaler",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters=MLPipelineComponent.generate_hyperparameters(search_space_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['StandardScaler'], random_state=42),
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['StandardScaler'],
                                        subcomponents={})
        pipeline_1.append(component)

        component = MLPipelineComponent(name="PCA",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters=MLPipelineComponent.generate_hyperparameters(search_space_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['PCA'], random_state=42),
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['PCA'],
                                        subcomponents={})
        pipeline_1.append(component)

        component = MLPipelineComponent(name="Binarizer",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters=MLPipelineComponent.generate_hyperparameters(search_space_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['Binarizer'], random_state=42),
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['Binarizer'],
                                        subcomponents={})
        pipeline_1.append(component)

        # build second pipeline
        pipeline_2 = MLPipeline(search_space=search_space, generate=False)

        component = MLPipelineComponent(name="LinearSVC",
                                        component_type=MLPipelineComponentType.ESTIMATOR,
                                        hyperparameters=MLPipelineComponent.generate_hyperparameters(search_space_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['LinearSVC'], random_state=42),
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['LinearSVC'],
                                        subcomponents={})

        pipeline_2.append(component)

        component = MLPipelineComponent(name="Normalizer",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters=MLPipelineComponent.generate_hyperparameters(search_space_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['Normalizer'], random_state=42),
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['Normalizer'],
                                        subcomponents={})

        pipeline_2.append(component)

        cx_pipeline_1 = copy.deepcopy(pipeline_1)
        cx_pipeline_2 = copy.deepcopy(pipeline_2)

        variation.CrossoverTwoPoint()(individual_1=cx_pipeline_1, individual_2=cx_pipeline_2, random_state=44)
        assert cx_pipeline_1.hash() == pipeline_1.hash()
        assert cx_pipeline_2.hash() == pipeline_2.hash()
        assert "has a size less than 3" in caplog.text

        # add more components to the pipeline_2
        component = MLPipelineComponent(name="PolynomialFeatures",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters=MLPipelineComponent.generate_hyperparameters(search_space_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['PolynomialFeatures'], random_state=42),
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['PolynomialFeatures'],
                                        subcomponents={})

        pipeline_2.append(component)

        component = MLPipelineComponent(name="RobustScaler",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters=MLPipelineComponent.generate_hyperparameters(search_space_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['RobustScaler'], random_state=42),
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['RobustScaler'],
                                        subcomponents={})

        pipeline_2.append(component)

        component = MLPipelineComponent(name="SelectFwe",
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters=MLPipelineComponent.generate_hyperparameters(search_space_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['SelectFwe'], random_state=42),
                                        search_space_component_node=search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]['SelectFwe'],
                                        subcomponents={})

        pipeline_2.append(component)

        cx_pipeline_2 = copy.deepcopy(pipeline_2)

        variation.CrossoverTwoPoint()(individual_1=cx_pipeline_1, individual_2=cx_pipeline_2, random_state=44) # 44-> 1,3; 43 -> 1,2

        assert cx_pipeline_1.hash() != pipeline_1.hash()
        assert cx_pipeline_2.hash() != pipeline_2.hash()

        # first candidate
        assert cx_pipeline_1.components[0].name == 'DecisionTreeClassifier'
        # first point
        assert cx_pipeline_1.components[1].name == 'Normalizer'
        assert cx_pipeline_1.components[2].name == 'PolynomialFeatures'
        # second point
        assert cx_pipeline_1.components[3].name == 'StandardScaler'
        assert cx_pipeline_1.components[4].name == 'PCA'

        # second candidate
        assert cx_pipeline_2.components[0].name == 'LinearSVC'
        # first point
        assert cx_pipeline_2.components[1].name == 'CombineTwoPreviousesDFs'
        assert cx_pipeline_2.components[2].name == 'RandomForestClassifier'
        assert cx_pipeline_2.components[3].name == 'RobustScaler'
        assert cx_pipeline_2.components[4].name == 'SelectFwe'
