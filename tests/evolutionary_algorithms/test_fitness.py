from marymorstan.evolutionary_algorithms.fitness import Fitness


class TestFitness():
    def setup_method(self):
        self.fitness = Fitness()

    def test_add_values(self):
        self.fitness.values = [1, 2, 3]
        assert self.fitness.values == [1, 2, 3]

    def test_delete_values(self):
        self.fitness.values = [1, 2, 3]
        del self.fitness.values
        assert self.fitness.values == None

    def test_add_weights(self):
        self.fitness.weights = [1, 2, 3]
        assert self.fitness.weights == [1, 2, 3]

    def test_delete_weights(self):
        self.fitness.values = [1, 1]

        self.fitness.weights = [1, 2]
        del self.fitness.weights

        assert self.fitness.weights == [1, 1]
        assert self.fitness.values == [1, 1]

    def test_default_weighted_values(self):
        self.fitness.values = (1, 5)
        assert self.fitness.weighted_values == (1, 5)

    def test_weighted_values(self):
        self.fitness.weights = (0.5, 0.5)
        self.fitness.values = (1, 1.5)

        assert self.fitness.weighted_values == (.5, 1.5*.5)

        # No values, we can't compute weighted_values
        del self.fitness.values
        assert self.fitness.weighted_values == None

        # No weights implies uniform weights
        self.fitness.values = (1, 2, 3)
        del self.fitness.weights

        assert self.fitness.weighted_values == (1, 2 ,3)

    def test_valid(self):
        assert self.fitness.valid is False
        self.fitness.values = [1, 2]
        assert self.fitness.valid is True

    def test_comparison(self):
        fitness1 = Fitness(weights=(1., 2.), values=(5, 4))
        fitness2 = Fitness(weights=(2, 2), values=(1, 1))
        fitness3 = Fitness(weights=(1, 1), values=(2, 2))

        assert fitness1 > fitness2
        assert fitness1 >= fitness2
        assert not fitness1 < fitness2
        assert not fitness1 <= fitness2
        assert fitness1 != fitness2
        assert fitness2 == fitness3

    def test_str(self):
        assert str(self.fitness) == str(())

    def test_dominates(self):
        weights = (1.0, 2.0)
        fitness1 = Fitness(weights=weights, values=(5, 5))
        fitness2 = Fitness(weights=weights, values=(6, 5))
        fitness3 = Fitness(weights=weights, values=(6, 5))
        fitness4 = Fitness(weights=(3.0, 1.0), values=(4, 3))

        assert fitness2.dominates(fitness1)
        assert fitness2.dominates(fitness3) is False
        assert fitness4.dominates(fitness1) is False


    def test_has_one_better_objective(self):
        weights = (1.0, 1.0)
        fitness1 = Fitness(weights=weights, values=(5, 7))
        fitness2 = Fitness(weights=weights, values=(6, 5))
        fitness3 = Fitness(weights=weights, values=(6, 4))

        assert not fitness1.has_one_better_objective(fitness1)
        assert fitness1.has_one_better_objective(fitness2)
        assert fitness2.has_one_better_objective(fitness1)
        assert fitness2.has_one_better_objective(fitness3)
        assert not fitness3.has_one_better_objective(fitness2)
