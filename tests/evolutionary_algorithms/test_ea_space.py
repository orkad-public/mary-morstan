import pytest

from marymorstan.evolutionary_algorithms.mutation import MutationGaussian
from marymorstan.evolutionary_algorithms.utils import get_name
from marymorstan.evolutionary_algorithms import ea_space as eas


class TestEASpace():
    def test_read_search_space(self):
        with pytest.raises(FileNotFoundError):
            eas.EASpace(evolutionary_algorithms_space_file="innexistant_file.yml")

    def test_check_ea_space(self):
        with pytest.raises(eas.InvalidEASpace) as e:
            ea_space = eas.EASpace(evolutionary_algorithms_space_file="./misc/evolutionary_algorithms_spaces/invalid_portion_of_population_missing.yml")
        assert "crossover_portion_of_population is missing" in str(e.value)

        with pytest.raises(eas.InvalidEASpace) as e:
            ea_space = eas.EASpace(evolutionary_algorithms_space_file="./misc/evolutionary_algorithms_spaces/invalid_no_crossovers_and_mutations_search_space.yml")
        assert "please define at least" in str(e.value)

        with pytest.raises(eas.InvalidEASpace) as e:
            ea_space = eas.EASpace(evolutionary_algorithms_space_file="./misc/evolutionary_algorithms_spaces/invalid_selection_missing.yml")
        assert "is not defined" in str(e.value)


    def test_one_mutation_and_select_with_parameters(self):
        ea_space = eas.EASpace(evolutionary_algorithms_space_file="./misc/evolutionary_algorithms_spaces/test_one_mutation_and_select_with_parameters.yml")

        crossovers = ea_space.crossover_instances()
        assert len(crossovers) == 0

        mutations = ea_space.mutation_instances()
        assert len(mutations) == 1
        assert isinstance(mutations[0], MutationGaussian)

        selection_method, parameters = ea_space.selection_instance()

        assert get_name(selection_method()) == "SelectKBest"
        assert parameters == {'k': 1}

        with pytest.raises(eas.InvalidEASpace):
            ea_space = eas.EASpace(evolutionary_algorithms_space_file="./misc/evolutionary_algorithms_spaces/invalid_one_mutation_and_select_with_parameters.yml")
            ea_space.mutation_instances()

    def test_mutation_from_parameters(self):
        ea_space = eas.EASpace(evolutionary_algorithms_space_file="./misc/evolutionary_algorithms_spaces/test_one_mutation_and_select_with_parameters.yml")
        ea_space.set_parameters({'mutations': ['marymorstan.evolutionary_algorithms.mutation.MutationGaussian', 'marymorstan.evolutionary_algorithms.mutation.MutationGaussian(mu=5)']})

        mutations = ea_space.mutation_instances()
        assert len(mutations) == 2
        assert isinstance(mutations[0], MutationGaussian)
        assert mutations[0].mu == 0
        assert isinstance(mutations[1], MutationGaussian)
        assert mutations[1].mu == 5

    def test_initialization(self):
        # str format
        ea_space = eas.EASpace(evolutionary_algorithms_space_file="./misc/evolutionary_algorithms_spaces/ea_space_tpot.yml")
        initialization_method, parameters = ea_space.initialization_instance()
        assert str(initialization_method) == "<class 'marymorstan.initialization.RandomInitialization'>"
        assert parameters['min_size'] == 1
        assert parameters['max_size'] == 3

        # dict format
        ea_space = eas.EASpace(evolutionary_algorithms_space_file="./misc/evolutionary_algorithms_spaces/ea_space_tpot_with_lhs.yml")
        initialization_method, parameters = ea_space.initialization_instance()
        assert str(initialization_method) == "<class 'marymorstan.initialization.LHSInitialization'>"
        assert parameters['min_size'] == 1
        assert parameters['max_size'] == 3
