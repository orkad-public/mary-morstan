from marymorstan.utils import read_yaml
from marymorstan.evolutionary_algorithms.utils import *


def test_random_state_parser(mocker):
    mocker.patch("numpy.random.RandomState", return_value=1.)
    rsp = random_state_parser()

    assert rsp == 1.

    asserted_random_state = random.RandomState()
    rsp = random_state_parser(asserted_random_state)

    assert rsp == asserted_random_state

    asserted_random_state = random.RandomState(42)
    rsp = random_state_parser(42)

    assert rsp == asserted_random_state


def test_get_name():
    class C():
        def __str__(self):
            return self.__class__.__name__

    c = C()

    assert get_name(c) == 'C'

    def func():
        pass

    assert get_name(func) == 'func'


def test_copy_population():
    assert len(copy_population([1,2,3])) == 3
    assert len(copy_population([1,2,3,4], n=2)) == 2
    assert len(copy_population([1,2,3,4], n=5)) == 5
    # check it does not go through random when exact n is specified
    assert copy_population([1,2,3,4,5], n=5) == [1,2,3,4,5]
    # check that we handle array multidimensional
    assert len(copy_population([[1,[2,3]],[1,[2,3]]], n=5)) == 5


def test_balance_probabilities():
    probabilities = [1/3, 1/3, 1/3]
    assert balance_probabilities(probabilities) == probabilities

    # create a disproportion
    assert balance_probabilities(probabilities[:2]) == [1/2, 1/2]


def test_extract_fractions():
    f = read_yaml('./misc/evolutionary_algorithms_spaces/ea_space_with_nsga_2.yml')
    probabilities = f['mutation_probabilities']
    fractions = extract_fractions(probabilities)
    assert fractions[0] == 1/3
