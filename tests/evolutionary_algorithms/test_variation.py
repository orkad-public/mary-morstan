import pytest
from unittest.mock import MagicMock

from marymorstan.evolutionary_algorithms.meta_data import EAMetaData
from marymorstan.evolutionary_algorithms.individual import SimpleIndividual
from marymorstan.evolutionary_algorithms.mutation import *
from marymorstan.evolutionary_algorithms.crossover import *
from marymorstan.evolutionary_algorithms.variation import *

from . import simple_population  # noqa: F401


@pytest.fixture()
def mock_variation():
    mock_variation = MagicMock()

    mock_variation.crossover_onepoint = MagicMock(side_effect=CrossoverOnePoint())
    mock_variation.crossover_onepoint.__name__ = 'CrossoverOnePoint'

    mock_variation.mutation_uniform_integer = MagicMock(side_effect=MutationUniformInteger(lower_bound=1., upper_bound=2.)) # NOTE always return 1
    mock_variation.mutation_uniform_integer.__name__ = 'MutationUniformInteger'

    return mock_variation


class TestVariation():
    def test_variation_and(self, simple_population, mock_variation, caplog):
        mutation = CustomizableMutations(portion_of_population=.5, random_state=42)
        mutation.append(mutation=mock_variation.mutation_uniform_integer)

        crossover = CustomizableCrossovers(portion_of_population=.5, random_state=42)
        crossover.append(crossover=mock_variation.crossover_onepoint, random_state=42)

        variation = Variation(ea_meta_data=EAMetaData(), mutation=mutation, crossover=crossover, random_state=43)
        offspring = variation(population=simple_population, one_variation_per_individual=False)

        # ensure each variation is called once
        assert mock_variation.crossover_onepoint.call_count >= 1
        assert mock_variation.mutation_uniform_integer.call_count >= 1

        # ensure that a candidate had two different variations
        #assert "only one variation per individual" not in caplog.text

        # NOTE check that a same candidate go through a mutation and a crossover
        asserted_offspring = [SimpleIndividual(attributes=[2, 5, 6]), SimpleIndividual(attributes=[4, 3, 4])]

        assert offspring == asserted_offspring

    def test_variation_or(self, simple_population, mock_variation):
        mutation = CustomizableMutations(portion_of_population=.5, random_state=42)
        mutation.append(mutation=mock_variation.mutation_uniform_integer)

        crossover = CustomizableCrossovers(portion_of_population=.25, random_state=42)
        crossover.append(crossover=mock_variation.crossover_onepoint, random_state=42)

        variation = Variation(ea_meta_data=EAMetaData(), mutation=mutation, crossover=crossover, random_state=43)
        offspring = variation(population=simple_population, one_variation_per_individual=True)

        # ensure each variation is called once
        assert mock_variation.crossover_onepoint.call_count >= 1
        assert mock_variation.mutation_uniform_integer.call_count >= 1

        # ensure that at least one candidate did not have two different variations
        #assert "only one variation per individual" in caplog.text

        asserted_offspring = [SimpleIndividual(attributes=[2, 3, 4]), SimpleIndividual(attributes=[1, 4, 5])]

        assert offspring == asserted_offspring

    # TODO test get_selected_variations()
