from unittest.mock import MagicMock

import pytest
import copy

from marymorstan.evolutionary_algorithms.meta_data import EAMetaData
from marymorstan.evolutionary_algorithms.variation import Variation
from marymorstan.evolutionary_algorithms.crossover import *
from marymorstan.evolutionary_algorithms.individual import SimpleIndividual

from . import simple_population  # noqa: F401


@pytest.fixture()
def mock_crossover():
    mock_crossover = MagicMock()

    mock_crossover.crossover_onepoint = MagicMock(side_effect=CrossoverOnePoint())
    mock_crossover.crossover_onepoint.__name__ = CrossoverOnePoint.__name__

    return mock_crossover


def test_crossover_unimplemented_methods(simple_population):
    with pytest.raises(NotImplementedError):
        Crossovers(portion_of_population=1.).crossovers


class TestCustomizableCrossovers():
    def test_no_crossover(self, simple_population):
        crossover = Variation(ea_meta_data=EAMetaData(), crossover=CustomizableCrossovers())

        with pytest.raises(Exception):
            crossovered_population = crossover(population=copy.deepcopy(simple_population))

    def test_custom_crossover(self, simple_population):
        seed = 44

        crossover = CustomizableCrossovers(portion_of_population=.5)
        crossover.append(crossover=CrossoverOnePoint(), random_state=seed)

        variation = Variation(ea_meta_data=EAMetaData(), crossover=crossover, random_state=seed)
        offspring = variation(population=copy.deepcopy(simple_population))

        crossovered_individual_1 = offspring[0]

        asserted_individual_1 = simple_population[0]
        asserted_individual_1[0] = 1
        asserted_individual_1[1] = 4
        asserted_individual_1[2] = 5
        epsilon = 0.01

        assert crossovered_individual_1.fitness != asserted_individual_1.fitness
        assert asserted_individual_1[0] - epsilon <= crossovered_individual_1[0] <= asserted_individual_1[0] + epsilon
        assert asserted_individual_1[1] - epsilon <= crossovered_individual_1[1] <= asserted_individual_1[1] + epsilon
        assert asserted_individual_1[2] - epsilon <= crossovered_individual_1[2] <= asserted_individual_1[2] + epsilon

        crossovered_individual_2 = offspring[1]

        asserted_individual_2 = simple_population[1]
        asserted_individual_2[0] = 3
        asserted_individual_2[1] = 2
        asserted_individual_2[2] = 3
        epsilon = 0.01

        assert crossovered_individual_2.fitness != asserted_individual_2.fitness
        assert asserted_individual_2[0] - epsilon <= crossovered_individual_2[0] <= asserted_individual_2[0] + epsilon
        assert asserted_individual_2[1] - epsilon <= crossovered_individual_2[1] <= asserted_individual_2[1] + epsilon
        assert asserted_individual_2[2] - epsilon <= crossovered_individual_2[2] <= asserted_individual_2[2] + epsilon

    def test_custom_mutation_with_no_crossover(self, simple_population, mock_crossover):
        seed = 42

        crossover = CustomizableCrossovers(portion_of_population=0.)
        crossover.append(crossover=mock_crossover.crossover_onepoint, probability_of_crossover=1., random_state=seed)

        variation = Variation(ea_meta_data=EAMetaData(), crossover=crossover, random_state=seed)

        offspring = variation(population=copy.deepcopy(simple_population))

        assert mock_crossover.crossover_onepoint.call_count == 0

    def test_custom_mutation_with_one_crossover_per_individual(self, simple_population):
        # TODO
        pass


# TODO add tests
class TestSimpleCrossovers():
    pass


def test_crossover_onepoint(simple_population):
    asserted_individual_1 = copy.deepcopy(simple_population[0])
    asserted_individual_2 = copy.deepcopy(simple_population[1])

    asserted_individual_1[0] = 1
    asserted_individual_1[1] = 4
    asserted_individual_1[2] = 5

    asserted_individual_2[0] = 3
    asserted_individual_2[1] = 2
    asserted_individual_2[2] = 3

    epsilon = 0.01

    crossover = CrossoverOnePoint()
    crossovered_individual_1, crossovered_individual_2 = crossover(individual_1=simple_population[0], individual_2=simple_population[1], random_state=42)

    assert asserted_individual_1.fitness == crossovered_individual_1.fitness
    assert asserted_individual_1[0] - epsilon <= crossovered_individual_1[0] <= asserted_individual_1[0] + epsilon
    assert asserted_individual_1[1] - epsilon <= crossovered_individual_1[1] <= asserted_individual_1[1] + epsilon
    assert asserted_individual_1[2] - epsilon <= crossovered_individual_1[2] <= asserted_individual_1[2] + epsilon

    assert asserted_individual_2[0] - epsilon <= crossovered_individual_2[0] <= asserted_individual_2[0] + epsilon
    assert asserted_individual_2[1] - epsilon <= crossovered_individual_2[1] <= asserted_individual_2[1] + epsilon
    assert asserted_individual_2[2] - epsilon <= crossovered_individual_2[2] <= asserted_individual_2[2] + epsilon

    assert str(crossover) == 'CrossoverOnePoint'


def test_crossover_onepoint_with_statistics():
    individual_1 = SimpleIndividual(attributes=[1, 2], enable_statistics=True)
    individual_2 = SimpleIndividual(attributes=[3, 4], enable_statistics=True)

    crossover = CrossoverOnePoint()
    crossovered_individual_1, crossovered_individual_2 = crossover(individual_1=individual_1, individual_2=individual_2, random_state=42)

    crossovered_individual_1_variations = crossovered_individual_1.statistics.variations[0]
    assert 'CrossoverOnePoint' == crossovered_individual_1_variations[0]['name']
