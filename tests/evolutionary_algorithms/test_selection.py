import pytest

from marymorstan.evolutionary_algorithms.individual import SimpleIndividual
from marymorstan.evolutionary_algorithms.selection import *


@pytest.fixture()
def individuals():
    weights=(1.,)

    best = SimpleIndividual()
    best.fitness.weights = weights
    best.fitness.values = (5.,)

    good = SimpleIndividual()
    good.fitness.weights = weights
    good.fitness.values = (4.,)

    bad = SimpleIndividual()
    bad.fitness.weights = weights
    bad.fitness.values = (2.,)

    similar_bad = SimpleIndividual()
    similar_bad.fitness.weights = weights
    similar_bad.fitness.values = (2.,)

    return [best, bad, good, similar_bad]


@pytest.fixture()
def moo_individuals():
    weights=(1, 1)

    individuals = []
    list_of_values = [(4,1), (3,3), (2,4), (1,4), (3,2), (2,3), (3,1), (1,1)]
    k = 6

    for values in list_of_values:
        individual = SimpleIndividual()
        individual.fitness.weights = weights
        individual.fitness.values = values
        individuals.append(individual)

    return individuals, list_of_values, weights


def test_select_best(individuals):
    assert SelectBest()(population=individuals, current_iteration=0) == [individuals[0]]


def test_select_k_best(individuals):
    assert SelectKBest()(population=individuals, current_iteration=0, k=2) == [individuals[0], individuals[2]]
    # check callable is working
    assert SelectKBest()(population=individuals,
                         k = lambda current_iteration: 1 if current_iteration == 3 else 2,
                         current_iteration=3) == [individuals[0]]


def test_select_random(individuals):
    assert SelectRandom()(population=individuals, current_iteration=0)[0] in individuals


def test_select_k_random(individuals):
    assert len(SelectKRandom()(population=individuals, current_iteration=0, k=3)) == 3
    # check callable is working
    assert len(SelectKRandom()(population=individuals,
                               k = lambda current_iteration: 1 if current_iteration == 3 else 2,
                               current_iteration=3)) == 1


def test_select_random_with_exclude(individuals):
    assert select_random_with_exclude([], exclude=0) == []
    assert select_random_with_exclude(individuals[:1], exclude=0) == []

    selected_individuals = select_random_with_exclude(individuals[:2], exclude=0)
    assert isinstance(selected_individuals, SimpleIndividual)


def test_select_tournament(individuals):
    assert select_tournament([], k=0, tournsize=0) == []
    assert select_tournament(individuals, k=0, tournsize=2) == []
    assert len(select_tournament(individuals, k=1, tournsize=2)) == 1
    assert len(select_tournament(individuals, k=3, tournsize=100)) == 3

    assert len(SelectTournament()(population=individuals, k=50, current_iteration=1)) == 50


def test_select_nsga_2(moo_individuals):
    assert select_nsga_2([], k=0) == []

    k = 6
    individuals, list_of_values, _ = moo_individuals
    expected_list_of_values = list_of_values[:k]

    selected_individuals = SelectNSGA2()(individuals, current_iteration=0, k=k)

    for i in range(k):
        assert selected_individuals[i].fitness.values in expected_list_of_values

    # check callable is working
    assert len(SelectNSGA2()(individuals, current_iteration=0, k=lambda _: 2)) == 2


def test_select_nsga_3(moo_individuals):
    assert select_nsga_3([], k=0) == []

    k = 6
    individuals, list_of_values, _ = moo_individuals
    expected_list_of_values = list_of_values[:k]

    selected_individuals = SelectNSGA3()(individuals, current_iteration=0, k=k)

    for i in range(k):
        assert selected_individuals[i].fitness.values in expected_list_of_values

    # check callable is working
    assert len(SelectNSGA3()(individuals, current_iteration=0, k=lambda _: 2)) == 2


def test_select_spea_2(moo_individuals):
    assert select_spea_2([], k=0) == []

    k = 6
    individuals, list_of_values, _ = moo_individuals
    expected_list_of_values = list_of_values[:k]

    selected_individuals = SelectSPEA2()(individuals, current_iteration=0, k=k)

    for i in range(k):
        assert selected_individuals[i].fitness.values in expected_list_of_values

    # check callable is working
    assert len(SelectSPEA2()(individuals, current_iteration=0, k=lambda _: 2)) == 2
