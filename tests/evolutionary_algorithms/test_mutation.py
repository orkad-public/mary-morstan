from unittest.mock import MagicMock

import pytest
import copy

from marymorstan.evolutionary_algorithms.meta_data import EAMetaData
from marymorstan.evolutionary_algorithms.individual import SimpleIndividual
from marymorstan.evolutionary_algorithms.operator_strategy import StaticDistribution
from marymorstan.evolutionary_algorithms.variation import Variation
from marymorstan.evolutionary_algorithms.mutation import *

from . import simple_population  # noqa: F401


@pytest.fixture()
def mock_mutation():
    mock_mutation = MagicMock()

    mock_mutation.mutation_gaussian = MagicMock(side_effect=MutationGaussian(mu=[0, 0, 0], sigma=[1, 1, 1]))
    mock_mutation.mutation_gaussian.__name__ = 'MutationGaussian'

    mock_mutation.mutation_uniform_integer = MagicMock(side_effect=MutationUniformInteger(lower_bound=1., upper_bound=2.)) # NOTE always return 1
    mock_mutation.mutation_uniform_integer.__name__ = 'MutationUniformInteger'

    return mock_mutation


def test_mutation_unimplemented_methods():
    with pytest.raises(NotImplementedError):
        mutation = Mutations(portion_of_population=1.).mutations


class TestCustomizableMutations():
    def test_no_mutation(self, simple_population):
        mutation = Variation(ea_meta_data=EAMetaData(), mutation=CustomizableMutations())

        with pytest.raises(Exception):
            mutated_population = mutation(population=copy.deepcopy(simple_population))

    def test_custom_mutation(self, simple_population):
        from numpy import random
        mutation = CustomizableMutations(portion_of_population=1.)
        variate = Variation(ea_meta_data=EAMetaData(), mutation=mutation, random_state=42)
        mutation.append(MutationGaussian(), random_state=random.RandomState(42))
        offspring = variate(population=copy.deepcopy(simple_population))

        mutated_individual_1 = offspring[0]

        asserted_individual_1 = simple_population[0]
        asserted_individual_1[0] = -.11
        asserted_individual_1[1] = 2.31
        asserted_individual_1[2] = 3.27
        epsilon = 0.01

        assert mutated_individual_1.fitness != asserted_individual_1.fitness
        assert asserted_individual_1[0] - epsilon <= mutated_individual_1[0] <= asserted_individual_1[0] + epsilon
        assert asserted_individual_1[1] - epsilon <= mutated_individual_1[1] <= asserted_individual_1[1] + epsilon
        assert asserted_individual_1[2] - epsilon <= mutated_individual_1[2] <= asserted_individual_1[2] + epsilon

        mutated_individual_2 = offspring[1]

        asserted_individual_2 = simple_population[1]
        asserted_individual_2[0] = 4.01
        asserted_individual_2[1] = 3.41
        asserted_individual_2[2] = 4.47
        epsilon = 0.01

        assert mutated_individual_2.fitness != asserted_individual_2.fitness
        assert asserted_individual_2[0] - epsilon <= mutated_individual_2[0] <= asserted_individual_2[0] + epsilon
        assert asserted_individual_2[1] - epsilon <= mutated_individual_2[1] <= asserted_individual_2[1] + epsilon
        assert asserted_individual_2[2] - epsilon <= mutated_individual_2[2] <= asserted_individual_2[2] + epsilon

    def test_custom_mutation_with_two_mutations(self, simple_population, mock_mutation):
        mutate = CustomizableMutations(portion_of_population=1.)
        mutate.append(mock_mutation.mutation_gaussian, random_state=random.RandomState(42))
        mutate.append(mock_mutation.mutation_uniform_integer, random_state=42)

        variate = Variation(ea_meta_data=EAMetaData(), mutation=mutate, random_state=43)

        offspring = variate(population=simple_population)

        # each mutation is call at least once for each individual
        # assumption: default parameters for variation (one_variation_per_individual and one_mutation_per_individual are false)
        assert mock_mutation.mutation_gaussian.call_count == 2
        assert mock_mutation.mutation_uniform_integer.call_count == 2

        mutated_individual_1 = offspring[0]

        # if values change, something goes wrong (or the chaining call of random_state changed due to a new call in between)
        asserted_individual_1 = copy.deepcopy(simple_population[0])
        asserted_individual_1[0] = .89
        asserted_individual_1[1] = 1.73
        asserted_individual_1[2] = 2.75

        assert mutated_individual_1.fitness != asserted_individual_1.fitness
        assert mutated_individual_1.similar(asserted_individual_1, absolute_tolerance=1e-2)

        mutated_individual_2 = offspring[1]

        asserted_individual_2 = copy.deepcopy(simple_population[1])
        asserted_individual_2[0] = 5
        asserted_individual_2[1] = 6
        asserted_individual_2[2] = 7

        assert mutated_individual_2.fitness != asserted_individual_2.fitness
        assert mutated_individual_2.similar(asserted_individual_2, absolute_tolerance=1e-2)


    def test_custom_mutation_with_probability(self, simple_population, mock_mutation):
        mutate = CustomizableMutations(portion_of_population=1., distribution_strategy=StaticDistribution(probabilities=[1., 0.]))

        mutate.append(mock_mutation.mutation_gaussian, random_state=random.RandomState(42))
        mutate.append(mock_mutation.mutation_uniform_integer)

        variate = Variation(ea_meta_data=EAMetaData(), mutation=mutate, random_state=41)

        offspring = variate(population=copy.deepcopy(simple_population))

        assert mock_mutation.mutation_gaussian.call_count == 2
        assert mock_mutation.mutation_uniform_integer.call_count == 0

        mutated_individual_1 = offspring[0]

        asserted_individual_1 = simple_population[0]
        asserted_individual_1[0] = -.111
        asserted_individual_1[1] = 2.31
        asserted_individual_1[2] = 3.27
        epsilon = 0.01

        assert mutated_individual_1.fitness != asserted_individual_1.fitness
        assert mutated_individual_1.similar(asserted_individual_1, absolute_tolerance=1e-2)

        mutated_individual_2 = offspring[1]

        asserted_individual_2 = simple_population[1]
        asserted_individual_2[0] = 4.01
        asserted_individual_2[1] = 3.41
        asserted_individual_2[2] = 4.47
        epsilon = 0.01

        assert mutated_individual_2.fitness != asserted_individual_2.fitness
        assert mutated_individual_2.similar(asserted_individual_2, absolute_tolerance=1e-2)

    def test_custom_mutation_with_no_mutation(self, simple_population, mock_mutation):
        mutate = CustomizableMutations(portion_of_population=0.)

        mutate.append(mock_mutation.mutation_gaussian)
        mutate.append(mock_mutation.mutation_uniform_integer)

        variate = Variation(ea_meta_data=EAMetaData(), mutation=mutate)

        offspring = variate(population=copy.deepcopy(simple_population))

        assert mock_mutation.mutation_gaussian.call_count == 0
        assert mock_mutation.mutation_uniform_integer.call_count == 0

    def test_custom_mutation_with_one_mutation_per_individual(self, simple_population, mock_mutation):
        mutate = CustomizableMutations(portion_of_population=1.)

        mutate.append(mock_mutation.mutation_gaussian, random_state=42)
        mutate.append(mock_mutation.mutation_uniform_integer, random_state=42)

        variate = Variation(ea_meta_data=EAMetaData(), mutation=mutate, random_state=45)

        offspring = variate(population=simple_population, one_mutation_per_individual=True)

        assert mock_mutation.mutation_gaussian.call_count == 1
        assert mock_mutation.mutation_uniform_integer.call_count == 1

        mutated_individual_1 = offspring[0]

        asserted_individual_1 = simple_population[0]
        asserted_individual_1[0] = 4
        asserted_individual_1[1] = 5
        asserted_individual_1[2] = 6
        epsilon = 0.01

        assert mutated_individual_1.fitness != asserted_individual_1.fitness
        assert mutated_individual_1.similar(asserted_individual_1, absolute_tolerance=1e-2)

        mutated_individual_2 = offspring[1]

        asserted_individual_2 = simple_population[1]
        asserted_individual_2[0] = 1.88
        asserted_individual_2[1] = 4.31
        asserted_individual_2[2] = 5.27
        epsilon = 0.01

        assert mutated_individual_2.fitness != asserted_individual_2.fitness
        assert mutated_individual_2.similar(asserted_individual_2, absolute_tolerance=1e-2)


class TestSimpleMutations():

    def test_simple_mutation(self, simple_population, mocker):

        mutation_gaussian = MagicMock(MutationGaussian)
        mutation_gaussian.__call__ = MagicMock(side_effect=simple_population)  # NOTE side_effect allow unit tests to work and check it is never called. In case it is called, this return has nonsense

        mocker.patch('marymorstan.evolutionary_algorithms.mutation.MutationGaussian', side_effect=mutation_gaussian)

        random_state = random.RandomState(56)
        mutate = SimpleMutations(portion_of_population=1., random_state=random_state)
        variate = Variation(ea_meta_data=EAMetaData(), mutation=mutate, random_state=random_state)

        offspring = variate(population=simple_population, one_mutation_per_individual=True)
        assert mutation_gaussian.__call__.call_count == 0  # NOTE based on the seed, mutation_gaussian should never been called

        mutated_individual_1 = offspring[0]

        asserted_individual_1 = simple_population[0]
        asserted_individual_1[0] = 3
        asserted_individual_1[1] = 5
        asserted_individual_1[2] = 6

        assert mutated_individual_1.fitness != asserted_individual_1.fitness
        assert mutated_individual_1 == asserted_individual_1

        mutated_individual_2 = offspring[1]
        asserted_individual_2 = simple_population[1]

        assert mutated_individual_2.fitness != asserted_individual_2.fitness
        assert mutated_individual_2 != asserted_individual_2


def test_mutation_gaussian(simple_population):
    asserted_individual = copy.deepcopy(simple_population[0])
    asserted_individual[0] = -.11
    asserted_individual[1] = 2.31
    asserted_individual[2] = 3.27
    epsilon = 0.01

    mutation = MutationGaussian()
    mutated_individual = mutation(individual=simple_population[0], random_state=42)

    assert mutated_individual.fitness == asserted_individual.fitness
    assert asserted_individual[0] - epsilon <= mutated_individual[0] <= asserted_individual[0] + epsilon
    assert asserted_individual[1] - epsilon <= mutated_individual[1] <= asserted_individual[1] + epsilon
    assert asserted_individual[2] - epsilon <= mutated_individual[2] <= asserted_individual[2] + epsilon

    with pytest.raises(IndexError):
        MutationGaussian(mu = [0])(individual=simple_population[0])

    with pytest.raises(IndexError):
        MutationGaussian(sigma = [0])(individual=simple_population[0])

    assert not mutated_individual.statistics


def test_mutation_gaussian_with_statistics():
    individual = SimpleIndividual(attributes=[1, 2, 3], enable_statistics=True)

    mutation = MutationGaussian(sigma=5)
    mutated_individual = mutation(individual=individual, random_state=42)

    mutated_individual_stats_generation_0 = mutated_individual.statistics.variations[0]

    assert 'MutationGaussian__mu__0.0__sigma__5' == mutated_individual_stats_generation_0[0]['name']
    assert 5. == mutated_individual_stats_generation_0[0]['parameters']['sigma']

    assert 3 == mutated_individual.statistics.number_of_variations()

    assert str(mutation) == 'MutationGaussian__mu__0.0__sigma__5'


def test_mutation_uniform_integer(simple_population):
    asserted_individual = copy.deepcopy(simple_population[0])
    asserted_individual[0] = 2
    asserted_individual[1] = 4
    asserted_individual[2] = 4
    epsilon = 0.01

    mutation = MutationUniformInteger(lower_bound=1.0, upper_bound=3.0)
    mutated_individual = mutation(individual=simple_population[0], random_state=42)

    assert mutated_individual.fitness == asserted_individual.fitness
    assert asserted_individual[0] - epsilon <= mutated_individual[0] <= asserted_individual[0] + epsilon
    assert asserted_individual[1] - epsilon <= mutated_individual[1] <= asserted_individual[1] + epsilon
    assert asserted_individual[2] - epsilon <= mutated_individual[2] <= asserted_individual[2] + epsilon

    with pytest.raises(IndexError):
        MutationUniformInteger(lower_bound = [0])(individual=simple_population[0])

    with pytest.raises(IndexError):
        MutationUniformInteger(upper_bound = [0])(individual=simple_population[0])


def test_mutation_uniform_integer_with_statistics():
    individual = SimpleIndividual(attributes=[1, 2], enable_statistics=True)

    mutation = MutationUniformInteger(lower_bound=1.0, upper_bound=3.0)
    mutated_individual = mutation(individual=individual, random_state=42)

    mutated_individual_stats_generation_0 = mutated_individual.statistics.variations[0]

    assert 'MutationUniformInteger__lower_bound__1.0__upper_bound__3.0' == mutated_individual_stats_generation_0[0]['name']
    assert 1. == mutated_individual_stats_generation_0[0]['parameters']['lower_bound']

    assert 2 == mutated_individual.statistics.number_of_variations()
