import pytest

from marymorstan.evolutionary_algorithms import statistics
from marymorstan.evolutionary_algorithms.base import SimpleEA

from . import simple_population_with_statistics  # noqa: F401


class TestStatistics():
    def setup_method(self):
        self.statistics = statistics.Statistics()

    def test_number_of_generations(self, simple_population_with_statistics):
        assert self.statistics.number_of_generations == 0

        self.statistics.set_individuals(individuals=simple_population_with_statistics, generation=0)
        assert self.statistics.number_of_generations == 1

        self.statistics.set_individuals(individuals=simple_population_with_statistics, generation=1)
        assert self.statistics.number_of_generations == 2


    def test__output_dataframe(self, simple_population_with_statistics):
        dumb_evaluation_method = lambda pop, iter: [setattr(ind.fitness, 'values', [ind.attributes[0]]) for ind in pop]

        ea = SimpleEA(generations = 2, evaluation_method = dumb_evaluation_method, random_state = 61, statistics = self.statistics)

        ea.optimize(simple_population_with_statistics, k=1)
        df = self.statistics._output_dataframe()

        assert len(df['generation'].tolist()) == 3
        assert df['generation'].tolist() == [0, 1, 2]

        assert len(df['elapsed_time'].tolist()) == 3
        assert (df['elapsed_time'] > 0).all()

        assert len(df['best_individual'].tolist()) == 3
        assert df[df.generation == 0]['best_individual'].iloc[0] is not None
        assert list(df[df.generation == 2]['best_individual'].iloc[0].values())[0][0] == pytest.approx(5.93, rel=1e-2)
