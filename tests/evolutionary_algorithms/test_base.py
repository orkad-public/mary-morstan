import pytest
import copy

from marymorstan import EA, SimpleEA, CustomizableEA
from marymorstan.evolutionary_algorithms.algorithms import EA_ALGORITHM, EAAlgorithmMuPlusLambdaParameters, EAAlgorithmMuCommaLambdaParameters
from marymorstan.evolutionary_algorithms import EA_METHODS
from marymorstan.evolutionary_algorithms.individual import SimpleIndividual
from marymorstan.evolutionary_algorithms.ea_space import EASpace
from marymorstan.evolutionary_algorithms.statistics import Statistics

from . import simple_population, simple_population_with_statistics  # noqa: F401


class TestEA():

    def test_unimplemented_methods(self):
        ea = EA()
        with pytest.raises(NotImplementedError):
            ea.variate(None, None, None)

        with pytest.raises(NotImplementedError):
            ea.evaluate(None, None)

        with pytest.raises(NotImplementedError):
            ea.select(None, None)

        with pytest.raises(NotImplementedError):
            ea.stop_criterion(None)


class TestDumbEA():
    def test_optimize(self):
        variate = lambda self, pop, off_size, iter, elapsed, total_time, global_stop: [pop[1], pop[0]]  # FIXME find a way to apply a simple variation
        evaluate = lambda self, pop, iter: None
        select = lambda self, pop, current_iteration: [pop[0], pop[1]]
        stop_criterion = lambda self, current_iteration: current_iteration == 3

        DumbEA = type('DumbEA', (EA,),{
                'variate': variate,
                'evaluate': evaluate,
                'select': select,
                'stop_criterion': stop_criterion,
            })

        dumb_ea = DumbEA()

        population = [SimpleIndividual([1]), SimpleIndividual([10])]

        best_population = dumb_ea.optimize(population=population)

        assert best_population == [population[0], population[1]]


# TODO test dumb ea with complex individual
class TestDumbEAComplexIndividual():
    pass

# TODO test copy.deepcopy (ensure pop is modified)


@pytest.fixture()
def asserted_individual_for_simple_ea():
    return [SimpleIndividual(attributes=[4.80, 5.69, 4.25]),
            SimpleIndividual(attributes=[4.80, 6.69, 4.25])]


class TestSimpleEA():
    def test_optimize(self, simple_population, mocker, asserted_individual_for_simple_ea):
        dumb_evaluation_method = lambda pop, iter: [setattr(ind.fitness, 'values', [ind.attributes[0]]) for ind in pop]

        ea = SimpleEA(generations = 2,
                      evaluation_method=dumb_evaluation_method,
                      crossover_probability=.1,
                      mutation_probability=.9,
                      random_state=37)

        mocker.spy(ea, name='_variation')

        ea.optimize(population=simple_population, k=2)

        assert ea._variation.call_count == 2

        # FIXME check that crossover and mutation are called at least one each

        assert simple_population[0].similar(asserted_individual_for_simple_ea[0], absolute_tolerance=0.01)
        assert simple_population[1].similar(asserted_individual_for_simple_ea[1], absolute_tolerance=0.01)

    def test_optimize_with_statistics(self, simple_population_with_statistics):
        dumb_evaluation_method = lambda pop, iter: [setattr(ind.fitness, 'values', [ind.attributes[0]]) for ind in pop]

        generations = 4

        ea = SimpleEA(generations = generations,
                      evaluation_method=dumb_evaluation_method,
                      crossover_probability=.2,
                      mutation_probability=.8,
                      random_state=2,
                      statistics=Statistics())

        best_population = ea.optimize(population=copy.deepcopy(simple_population_with_statistics), k=2)


class TestCustomizableEA():
    def test_optimize(self, simple_population):
        dumb_variation = lambda pop, offspring_size, iteration, elapsed, total_time, global_stop: [pop[1], pop[0]]  # FIXME find a way to apply a simple variation
        dumb_evaluation = lambda pop, iter: None
        dumb_selection = lambda pop, current_iteration: [pop[0], pop[1]]
        stop_criterion = lambda current_iteration: current_iteration == 10

        ea = CustomizableEA(generations = 10)
        ea.register(EA_METHODS.SELECTION, dumb_selection)
        ea.register(EA_METHODS.EVALUATION, dumb_evaluation)
        ea.register(EA_METHODS.VARIATION, dumb_variation)
        ea.register(EA_METHODS.TERMINATION, stop_criterion)

        best_population = ea.optimize(population=simple_population)

        assert best_population == simple_population


    def test_optimize_mu_plus_lambda(self, simple_population):
        dumb_evaluation = lambda pop, iter: None

        # test mu and lambda parameters
        mu = 5
        lambda_ = 10

        ea = CustomizableEA(generations = 2, force_unique_individuals=False,  algorithm=EA_ALGORITHM.MU_PLUS_LAMBDA, algorithm_parameters=EAAlgorithmMuPlusLambdaParameters(mu=mu, lambda_=lambda_))
        dumb_selection = lambda pop, current_iteration, k: [pop[0] for i in range(k)]
        stop_criterion = lambda current_iteration: current_iteration == 2

        def dumb_variation(pop, offspring_size, iteration, elapsed, total_time, global_stop):
            # should generate lambda_ offspring
            assert offspring_size == lambda_
            return [pop[1], pop[0]]

        ea.register(EA_METHODS.SELECTION, dumb_selection)
        ea.register(EA_METHODS.EVALUATION, dumb_evaluation)
        ea.register(EA_METHODS.VARIATION, dumb_variation)
        ea.register(EA_METHODS.TERMINATION, stop_criterion)

        # k should be avoided because overwrite by mu
        best_population = ea.optimize(population=simple_population, k=1400)

        # ensure mu is called
        assert len(best_population) == mu

    def test_optimize_mu_comma_lambda(self, simple_population):
        dumb_evaluation = lambda pop, iter: None

        # test mu and lambda parameters
        mu = 10
        lambda_ = 2

        ea = CustomizableEA(generations = 1, force_unique_individuals=False, algorithm=EA_ALGORITHM.MU_COMMA_LAMBDA, algorithm_parameters=EAAlgorithmMuCommaLambdaParameters(mu=mu, lambda_=lambda_))
        dumb_selection = lambda pop, current_iteration, k: [pop[0] for i in range(k)]
        stop_criterion = lambda current_iteration: current_iteration == 1

        def dumb_variation(pop, offspring_size, iteration, elapsed, total_time, global_stop):
            # should generate lambda_ offspring
            assert offspring_size == lambda_
            return [pop[1], pop[0]]

        ea.register(EA_METHODS.SELECTION, dumb_selection)
        ea.register(EA_METHODS.EVALUATION, dumb_evaluation)
        ea.register(EA_METHODS.VARIATION, dumb_variation)
        ea.register(EA_METHODS.TERMINATION, stop_criterion)

        # k should be avoided because overwrite by mu
        best_population = ea.optimize(population=simple_population, k=1400)

        # ensure mu is called
        assert len(best_population) == mu

    def test_optimize_with_simple_algorithm(self, simple_population):
        dumb_variation = lambda pop, offspring_size, iteration, elapsed, total_time, global_stop: [pop[1], pop[0]]  # FIXME find a way to apply a simple variation
        dumb_evaluation = lambda pop, iter: None
        dumb_selection = lambda pop, current_iteration: [pop[0], pop[1]]
        stop_criterion = lambda current_iteration: current_iteration == 1

        ea = CustomizableEA(algorithm=EA_ALGORITHM.SIMPLE, generations = 1)
        ea.register(EA_METHODS.SELECTION, dumb_selection)
        ea.register(EA_METHODS.EVALUATION, dumb_evaluation)
        ea.register(EA_METHODS.VARIATION, dumb_variation)
        ea.register(EA_METHODS.TERMINATION, stop_criterion)

        best_population = ea.optimize(population=copy.deepcopy(simple_population))

        assert best_population != simple_population

    def test_optimize_load_from_ea_space(self, simple_population, asserted_individual_for_simple_ea):
        dumb_evaluation_method = lambda pop, iter: [setattr(ind.fitness, 'values', [ind.attributes[0]]) for ind in pop]

        ea = CustomizableEA(generations = 2, random_state=37)
        ea.register(EA_METHODS.EVALUATION, dumb_evaluation_method)
        ea.register(EA_METHODS.TERMINATION, lambda current_iteration: current_iteration >= 2)
        ea.load_from_ea_space(ea_space=EASpace())

        best_population = ea.optimize(population=simple_population, k=2)

        # NOTE results should be same as SimpleEA (because the default ea space file imitate the code of SimpleEA)
        assert best_population[0].similar(asserted_individual_for_simple_ea[0], absolute_tolerance=0.01)
        assert best_population[1].similar(asserted_individual_for_simple_ea[1], absolute_tolerance=0.01)

        # test mu and lambda parameters
        mu = 13
        lambda_ = 11

        ea = CustomizableEA(generations = 2, force_unique_individuals=False)
        dumb_selection = lambda pop, current_iteration, k: [pop[0] for i in range(k)]
        stop_criterion = lambda current_iteration: current_iteration == 2

        def dumb_variation(pop, offspring_size, iteration, elapsed, total_time, global_stop):
            # should generate lambda_ offspring
            assert len(pop) == lambda_
            return [pop[1], pop[0]]

        ea.register(EA_METHODS.EVALUATION, dumb_evaluation_method)
        ea.register(EA_METHODS.VARIATION, dumb_variation)
        ea.register(EA_METHODS.TERMINATION, stop_criterion)

        ea_space = EASpace(evolutionary_algorithms_space_file='./misc/evolutionary_algorithms_spaces/test_ea_space_mu_plus_lambda_parameters.yml')
        ea.load_from_ea_space(ea_space=ea_space)

        # k should be overwrite by mu
        best_population = ea.optimize(population=simple_population, k=1400)

        # ensure mu is called
        assert len(best_population) == mu


    def test_optimize_unimplemented(self):
        dumb_ea = CustomizableEA(generations = 10)
        initial_population = []

        with pytest.raises(NotImplementedError):
            dumb_ea.optimize(population=initial_population)

        dumb_ea.register(EA_METHODS.EVALUATION, lambda pop, iter: None)

        with pytest.raises(NotImplementedError):
            dumb_ea.optimize(population=initial_population)

        dumb_ea.register(EA_METHODS.TERMINATION, lambda **kwargs: None)

        with pytest.raises(NotImplementedError):
            dumb_ea.optimize(population=initial_population)

        dumb_ea.register(EA_METHODS.VARIATION, lambda pop, off_size, current_iteration, elapsed, total_time, global_stop: [])

        with pytest.raises(NotImplementedError):
            dumb_ea.optimize(population=initial_population)
