import pytest

from marymorstan.evolutionary_algorithms.operator_strategy import InvalidDistribution, TimeBasedDistribution


class TestTimeBasedDistribution():
    def setup_method(self):
        probabilities = []
        probabilities.append({.25: [.1, .9]})
        probabilities.append({.75: [.5, .5]})
        self.tbd = TimeBasedDistribution(probabilities=probabilities, len_operators=2)

    def test_invalid_encoding(self):
        with pytest.raises(InvalidDistribution) as e:
            TimeBasedDistribution(probabilities=[{.25: [.1, .9]}], len_operators=2)
        assert "sum of the elapsed time ratios should be equal to 1" in str(e.value)

        with pytest.raises(InvalidDistribution) as e:
            TimeBasedDistribution(probabilities=[{.25: [.1, .9]}, {.75: [.1, .3]}], len_operators=3)
        assert "number of operators (3) differs to the number of probabilities (2)" in str(e.value)

    def test_get_probabilities(self):
        assert self.tbd.get_probabilities(elapsed_time=0, total_time=10) == [.1, .9]
        assert self.tbd.get_probabilities(elapsed_time=5, total_time=10) == [.5, .5]
        assert self.tbd.get_probabilities(elapsed_time=10, total_time=10) == [.5, .5]
