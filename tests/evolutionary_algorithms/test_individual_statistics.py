from marymorstan.evolutionary_algorithms.individual_statistics import IndividualStatistics
from marymorstan.evolutionary_algorithms.individual import SimpleIndividual


class TestIndividualStatistics():
    def setup_method(self):
        self.individual = SimpleIndividual(attributes=[1, 2, 3], enable_statistics=False)
        self.statistics = IndividualStatistics(self.individual)

    def test_append_variation(self):
        self.statistics.append_variation('MutationGaussian', {'mu': 10, 'sigma': 1})
        assert len(self.statistics.variations) == 1
        assert self.statistics.variations[0][0]['name'] == 'MutationGaussian'

        self.statistics.append_variation('MutationGaussian', {'mu': 20, 'sigma': 1})
        assert len(self.statistics.variations[0]) == 2

        assert len(self.statistics.variations[1]) == 0
        self.individual.meta_data.current_iteration = 1
        self.statistics.append_variation('MutationGaussian', {'mu': 10, 'sigma': 3}, attribute=1)
        assert len(self.statistics.variations[1]) == 1
        assert self.statistics.variations[1][0]['attribute'] == 1

    def test_number_of_variations(self):
        for i in range(3):
            self.statistics.append_variation('MutationGaussian', {'mu': 10, 'sigma': 1})

        self.individual.meta_data.current_iteration = 1

        for i in range(2):
            self.statistics.append_variation('MutationGaussian', {'mu': 10, 'sigma': 1})

        assert self.statistics.number_of_variations() == 5
        assert self.statistics.number_of_variations(1) == 2

    def test_to_pandas(self):
        self.individual.meta_data.current_iteration = 0
        self.statistics.append_variation('MutationGaussian', {'mu': 10, 'sigma': 1})
        self.statistics.append_variation('MutationGaussian', {'mu': 10, 'sigma': 1})

        self.individual.meta_data.current_iteration = 1
        self.statistics.append_variation('MutationGaussian', {'mu': 50, 'sigma': 1})

        df = self.statistics.to_pandas()
        assert len(df[df.variation_occurs_at_generation == 0]) == 2
        assert len(df[df.variation_occurs_at_generation == 1]) == 1
        assert df.variation_occurs_at_generation.tolist() == [0, 0, 1]
        assert df.last_variation_at_generation.tolist() == [1, 1, 1]
        assert df[df.variation_occurs_at_generation == 0].parameters.apply(lambda x : x['mu']).mean() == 10
