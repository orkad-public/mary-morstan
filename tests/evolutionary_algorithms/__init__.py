import pytest

from marymorstan.evolutionary_algorithms.individual import SimpleIndividual


@pytest.fixture()
def simple_population():
    weights=(1.,)

    best = SimpleIndividual(attributes=[1, 2, 3])
    best.fitness.weights = weights
    best.fitness.values = (5.,)

    bad = SimpleIndividual(attributes=[3, 4, 5])
    bad.fitness.weights = weights
    bad.fitness.values = (2.,)

    return [best, bad]


@pytest.fixture()
def simple_population_with_statistics():
    weights=(1.,)

    best = SimpleIndividual(attributes=[1, 2, 3], enable_statistics=True)
    best.fitness.weights = weights
    best.fitness.values = (5.,)

    bad = SimpleIndividual(attributes=[3, 4, 5], enable_statistics=True)
    bad.fitness.weights = weights
    bad.fitness.values = (2.,)

    return [best, bad]
