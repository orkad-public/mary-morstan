import pytest

from marymorstan.evolutionary_algorithms.individual import Individual, SimpleIndividual


class TestIndividual():
    def test_basic_individual(self):
        BasicIndividual = type('BasicIndividual', (Individual,),{
                '__len__': lambda self: 42,
                '__next__': lambda self: 41,
                '__iter__': lambda self: (yield 43),
        })

        individual = BasicIndividual()

        assert len(individual) == 42
        assert next(individual) == 41
        assert next(iter(individual)) == 43

    def test_individual_unimplemented_methods(self):
        individual = Individual()

        with pytest.raises(NotImplementedError):
            individual.attributes

        with pytest.raises(NotImplementedError):
            len(individual)

        with pytest.raises(NotImplementedError):
            next(individual)

        with pytest.raises(NotImplementedError):
            iter(individual)

        with pytest.raises(NotImplementedError):
            individual[0]

        with pytest.raises(NotImplementedError):
            individual[0] = 1

        with pytest.raises(NotImplementedError):
            del individual[0]

        with pytest.raises(NotImplementedError):
            individual.insert(0, 1)

        with pytest.raises(NotImplementedError):
            individual == individual

        with pytest.raises(NotImplementedError):
            individual != individual

        with pytest.raises(NotImplementedError):
            str(individual)

        with pytest.raises(NotImplementedError):
            individual.similar(individual)

        with pytest.raises(NotImplementedError):
            individual.hash()


class TestSimpleIndividual():
    def setup_method(self):
        self.individual = SimpleIndividual(attributes=[1, 2, 3])

    def test_attributes(self):
        self.individual.attributes == [1, 2, 3]

    def test_len(self):
        assert len(self.individual) == 3

    def test_next(self):
        assert next(iter(self.individual)) == 1

    def test_get(self):
        assert self.individual[0] == 1

    def test_set(self):
        self.individual[0] = 42
        assert self.individual[0] == 42

    def test_insert(self):
        self.individual.insert(3, 4)
        assert self.individual[3] == 4
        assert list(self.individual) == [1, 2, 3, 4]

    def test_delete(self):
        del self.individual[0]
        assert self.individual[0] == 2

    def test_str(self):
        assert str(self.individual) == "[1, 2, 3]"

    def test_rpr(self):
        assert repr(self.individual) == "[1, 2, 3]"

    def test_similar(self):
        other = SimpleIndividual(attributes=[1, 2, 3.0000000001])
        assert self.individual.similar(other)

        other = SimpleIndividual(attributes=[1, 2, 3.1])
        assert not self.individual.similar(other)

    def test_hash(self):
        ind1 = SimpleIndividual(attributes=[1, 2, 3.1])
        ind2 = SimpleIndividual(attributes=[1, 2, 3.1])

        # ensure that two different instances have the same "id"
        assert ind1.hash() != None
        assert ind1.hash() == ind2.hash()
