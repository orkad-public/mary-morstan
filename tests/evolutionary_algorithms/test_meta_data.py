from marymorstan.evolutionary_algorithms.meta_data import EAMetaData


class TestEAMetaData():
    def test_hash_known_individuals(self):
        ea_meta_data = EAMetaData()
        assert len(ea_meta_data.hash_known_individuals) == 0
        ea_meta_data.hash_known_individuals.add('ind_hash_1')
        assert len(ea_meta_data.hash_known_individuals) == 1
        ea_meta_data.hash_known_individuals.add('ind_hash_1')
        assert len(ea_meta_data.hash_known_individuals) == 1
