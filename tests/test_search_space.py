import pytest

from marymorstan import search_space as ss

from . import simple_search_space  # noqa: F401


class TestSearchSpace():
    # TODO test pre_forbidden
    # TODO test forbidden hyperparameters

    def test_read_search_space(self):
        with pytest.raises(FileNotFoundError):
            ss.SearchSpace(search_space_file="innexistant_file.yml")

        with pytest.raises(Exception):
            ss.SearchSpace(search_space_file="./misc/search_spaces/tests/malformed_search_space.yml")

        #with pytest.raises(InvalidSearchSpace):
        #    SearchSpace(search_space_file="./misc/search_spaces/tests/malformed_search_space.yml")

        search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space.yml")
        assert "version" in search_space
        assert search_space['version'] == 2
        assert "groups" in search_space
        assert "algorithms" in search_space
        assert "preprocessors" in search_space['groups']
        assert "estimators" in search_space['groups']

    def test_set_search_space(self):
        search_space = ss.SearchSpace()
        search_space.set_search_space(search_space_file="./misc/search_spaces/tests/simple_search_space.yml")
        assert "LinearSVC" not in search_space['groups']['estimators']

    def test_len(self):
        search_space = ss.SearchSpace()
        assert len(search_space) > 0

    def test_algorithms_combinatorial_size(self):
        search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_previous_groups_search_space.yml")
        assert search_space.algorithms_combinatorial_size(pipeline_size=0) == 0
        assert search_space.algorithms_combinatorial_size(pipeline_size=1) == 3
        assert search_space.algorithms_combinatorial_size(pipeline_size=2) == (3 * 4)
        assert search_space.algorithms_combinatorial_size(pipeline_size=3) == (3 * 4 * 4)
        assert search_space.algorithms_combinatorial_size(pipeline_size=4) == (3 * 4**3)

        with pytest.raises(NotImplementedError):
            search_space.algorithms_combinatorial_size(pipeline_size=2, include_hyperparameters=True)

        assert search_space.algorithms_combinatorial_size(pipeline_size=1, include_hyperparameters=True) == (1 + 6 + 1)

        search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/tests/search_space_tpot_light_no_logistic_with_arange.yml")
        assert search_space.algorithms_combinatorial_size(pipeline_size=1, include_hyperparameters=True) == (1 + 200 + 6*2 + 2*10*19*20 + 100*2*2)

    def test_list_of_algorithms(self, simple_search_space):
        algorithms = simple_search_space.list_of_algorithms
        assert 'GaussianNB' in algorithms
        assert 'BernoulliNB' in algorithms
        assert 'InnexistantAlgor' not in algorithms

    def test_check_search_space_groups_in_accordance_with_algorithms(self):
        search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/tests/inconsistent_groups_with_algorithms_2.yml", check_search_space_file=False)

        with pytest.raises(ss.InvalidSearchSpace) as iss:
            search_space.check_search_space_groups_in_accordance_with_algorithms()

        assert "algorithm" in str(iss.value)
        assert "specified in group does not exist" in str(iss.value)

    def test_check_search_space_forbidden_part(self):
        search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/tests/invalid_forbidden_hyperparameters_search_space.yml", check_search_space_file=False)

        with pytest.raises(ss.InvalidSearchSpace) as iss:
            search_space.check_search_space_forbidden_part(mapper=search_space.search_space)

        assert "invalid clause" in str(iss.value)

    def test_check_search_space_requirement_part(self):
        search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/tests/impossible_requirement_search_space.yml", check_search_space_file=False)

        with pytest.raises(ss.InvalidSearchSpace):
            search_space.check_search_space_requirement_part(mapper=search_space.search_space)

    def test_check_search_space_has_requirement_or_forbidden(self):
        search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/tests/impossible_requirement_and_forbidden_search_space.yml", check_search_space_file=False)

        with pytest.raises(ss.InvalidSearchSpace):
            ss.SearchSpace.check_search_space_has_requirement_or_forbidden(mapper=search_space.search_space)


def test_get_hyperparameter_type():
    search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space.yml", check_search_space_file=True)

    component = search_space.estimators['BernoulliNB']
    assert ss.get_hyperparameter_type(component=component, hyperparameter='alpha') == 'categorical'

    with pytest.raises(KeyError):
        ss.get_hyperparameter_type(component=component, hyperparameter='whatever') == 'categorical'

    with pytest.raises(KeyError):
        component = search_space[ss.SEARCH_SPACE_KEY.ESTIMATOR.value]['GaussianNB']
        ss.get_hyperparameter_type(component=component, hyperparameter='GaussianNB has no hyperparameter') == 'categorical'


def test_hyperparameter_lower_upper_bound():
    search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/tests/simple_search_space_with_mixed_hyperparameters.yml")

    component_node = search_space.estimators['ExtraTreesClassifier']

    with pytest.raises(KeyError):
        ss.hyperparameter_lower_upper_bound(component_node=component_node, hyperparameter_name="innexistant_hyperparameter")

    with pytest.raises(TypeError):
        ss.hyperparameter_lower_upper_bound(component_node=component_node, hyperparameter_name='criterion')

    # NOTE test choices
    lower_bound, upper_bound = ss.hyperparameter_lower_upper_bound(component_node=component_node, hyperparameter_name='n_estimators')
    assert lower_bound == pytest.approx(1, abs=0.1)
    assert upper_bound == pytest.approx(100, abs=0.1)

    # NOTE test range
    lower_bound, upper_bound = ss.hyperparameter_lower_upper_bound(component_node=component_node, hyperparameter_name='min_samples_split')
    assert lower_bound == pytest.approx(2, abs=0.1)
    assert upper_bound == pytest.approx(21, abs=0.1)

    # NOTE test arange
    lower_bound, upper_bound = ss.hyperparameter_lower_upper_bound(component_node=component_node, hyperparameter_name='max_features')
    assert lower_bound == pytest.approx(.05, abs=0.01)
    assert upper_bound == pytest.approx(1.01, abs=0.01)

    component_node = search_space.estimators['LinearSVC']

    lower_bound, upper_bound = ss.hyperparameter_lower_upper_bound(component_node=component_node, hyperparameter_name='C')
    assert lower_bound == pytest.approx(1e-4)
    assert upper_bound == pytest.approx(25., abs=0.01)


def test_hyperparameter_value_violate_boundary():
    search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/search_space_tpot.yml", check_search_space_file=True)
    component = search_space.estimators['DecisionTreeClassifier']

    assert not ss.hyperparameter_value_violate_boundary(hyperparameter_name='max_depth', hyperparameter_value=5, component_node=component)
    assert ss.hyperparameter_value_violate_boundary(hyperparameter_name='max_depth', hyperparameter_value=12, component_node=component)
    assert ss.hyperparameter_value_violate_boundary(hyperparameter_name='max_depth', hyperparameter_value=-1, component_node=component)


def test_extract_variables_from_clause():
    clause = "penalty == 'l1' and loss == 'logistic_regression' and dual == True"

    variables = ss.extract_variables_from_clause(clause=clause)

    assert variables[0] == "penalty"
    assert variables[1] == "loss"
    assert variables[2] == "dual"

    clause = "alpha <= 6 or beta > 5"

    variables = ss.extract_variables_from_clause(clause=clause)

    assert variables[0] == "alpha"
    assert variables[1] == "beta"

    clause = "5.4342 <= 6 or beta > 5"

    variables = ss.extract_variables_from_clause(clause=clause)

    assert len(variables) == 1
    assert variables[0] == "beta"

    clause = "5.4342 <= alpha or 3 > 5"

    variables = ss.extract_variables_from_clause(clause=clause)

    assert len(variables) == 1
    assert variables[0] == "alpha"

    clause = "True <= alpha"

    variables = ss.extract_variables_from_clause(clause=clause)

    assert len(variables) == 1
    assert variables[0] == "alpha"

    clause = "myVar == 'l1' and (124.41 == anotherVar and var1 == 42 or var2 == 5) or (penalty == 'l1' and loss == 'logistic_regression' and dual == True or \"5\" > var) or 6 > value and var_hello == None"

    variables = ss.extract_variables_from_clause(clause=clause)

    assert len(variables) == 10
    assert variables == ["myVar", "anotherVar", "var1", "var2", "penalty", "loss", "dual", "var", "value", "var_hello"]


def test_append_prefix_to_variables_in_clause():
    clause = "dual==True and penalty=='l1' and loss=='squared_hinge'"
    expected_result = "node[\"hyperparameters\"][\"dual\"]==True and node[\"hyperparameters\"][\"penalty\"]=='l1' and node[\"hyperparameters\"][\"loss\"]=='squared_hinge'"
    assert ss.append_prefix_to_variables_in_clause(prefix="node[\"hyperparameters\"]", clause=clause) == expected_result

    clause = "penalty == 'l2' and loss == 'hinge'"
    expected_result = "node[\"hyperparameters\"][\"penalty\"] == 'l2' and node[\"hyperparameters\"][\"loss\"] == 'hinge'"
    assert ss.append_prefix_to_variables_in_clause(prefix="node[\"hyperparameters\"]", clause=clause) == expected_result

    clause = "max_features > 1. or max_features < 0"
    expected_result = "node[\"hyperparameters\"][\"max_features\"] > 1. or node[\"hyperparameters\"][\"max_features\"] < 0"
    assert ss.append_prefix_to_variables_in_clause(prefix="node[\"hyperparameters\"]", clause=clause) == expected_result

    clause = "5.4342 > 1. or max_features < 0"
    expected_result = "5.4342 > 1. or node[\"hyperparameters\"][\"max_features\"] < 0"
    assert ss.append_prefix_to_variables_in_clause(prefix="node[\"hyperparameters\"]", clause=clause) == expected_result


def test_is_valid_clause():
    search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/tests/invalid_forbidden_hyperparameters_search_space.yml", check_search_space_file=False)
    component = search_space.estimators['LinearSVC']
    clause = component[ss.SEARCH_SPACE_KEY.FORBIDDEN.value][ss.SEARCH_SPACE_KEY.FORBIDDEN_HYPERPARAMETERS.value][0]

    with pytest.raises(ss.InvalidClause):
        ss.is_valid_clause(component=component, clause=clause)


def test_hyperparameter_value_violiate_forbidden_clauses(simple_search_space):
    assert not ss.hyperparameter_value_violiate_forbidden_clauses(hyperparameter_name="test", hyperparameter_value="test", forbiddens=None)
    assert not ss.hyperparameter_value_violiate_forbidden_clauses(hyperparameter_name="test", hyperparameter_value="test", forbiddens={})

    search_space = ss.SearchSpace(search_space_file="./misc/search_spaces/tests/simple_forbidden_hyperparameters_search_space.yml")
    hyperparameters = {'penalty': 'l2', 'loss': 'hinge', 'dual': True, 'tol': 1e-3, 'C': 25.}
    assert ss.hyperparameter_value_violiate_forbidden_clauses(hyperparameter_name="C", hyperparameter_value=-1, forbiddens=search_space.estimators['LinearSVC'][ss.SEARCH_SPACE_KEY.FORBIDDEN.value], **hyperparameters)
    assert ss.hyperparameter_value_violiate_forbidden_clauses(hyperparameter_name="C", hyperparameter_value=-0.1, forbiddens=search_space.estimators['LinearSVC'][ss.SEARCH_SPACE_KEY.FORBIDDEN.value], **hyperparameters)
    assert not ss.hyperparameter_value_violiate_forbidden_clauses(hyperparameter_name="C", hyperparameter_value=0, forbiddens=search_space.estimators['LinearSVC'][ss.SEARCH_SPACE_KEY.FORBIDDEN.value], **hyperparameters)


def test_evaluate_clause():
    assert ss.evaluate_clause("1 == 1")
    assert not ss.evaluate_clause("1 != 1")

    assert ss.evaluate_clause("kwargs['myVar'] == False", myVar=False)

    with pytest.raises(NameError):
        ss.evaluate_clause("innexistante_variable == False")
