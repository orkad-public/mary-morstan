import pytest

import numpy as np

from marymorstan.utils import *


def test_keys_exists():
    elements = {"a": 1, "b": 2, "c": {"d": 3}, "e": None}

    assert keys_exists(elements, "a")
    assert keys_exists(elements, "c", "d")
    assert not keys_exists(elements, "d")
    assert keys_exists(elements, "e")

    with pytest.raises(AttributeError):
        keys_exists(elements)


def test_read_yaml():
    with pytest.raises(Exception):
        f = read_yaml("innExistant_file.yml")

    yaml_dict = read_yaml("./misc/search_spaces/tests/simple_search_space.yml")

    assert 'version' in yaml_dict


def test_is_number():
    assert is_number(1)
    assert is_number(1.5)
    assert not is_number("5")
    assert is_number(np.uint32)
    obj = type('obj', (object,), {'propertyName' : 'propertyValue'})
    assert not is_number(obj)


def test_is_integer():
    assert not is_integer(True)
    assert is_integer(1)
    assert not is_integer(1.5)
    assert not is_integer("5")
    assert is_integer(np.uint32)
    obj = type('obj', (object,), {'propertyName' : 'propertyValue'})
    assert not is_integer(obj)


def test_is_float():
    assert not is_float(1)
    assert is_float(1.5)
    assert not is_float("5")
    assert not is_float(np.uint32)
    obj = type('obj', (object,), {'propertyName' : 'propertyValue'})
    assert not is_float(obj)
