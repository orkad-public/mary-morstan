#!/usr/bin/env python3

from sklearn.model_selection import train_test_split
from sklearn.model_selection._validation import _score

import importlib
import logging

from marymorstan.objectives import OBJECTIVE, Objectives
from marymorstan.marymorstan import MaryMorstan

log_level = getattr(logging, 'ERROR')

dataset_preprocessing_module = importlib.import_module("datasets.iris_dataset_preprocessing")
dataset = dataset_preprocessing_module.MyDataSetPreprocessing("iris")

X_train, X_test, y_train, y_test = train_test_split(dataset.get_X(), dataset.get_y(), test_size=.25, random_state=42)
objs = [OBJECTIVE.argparse('balanced_accuracy'), OBJECTIVE.argparse('accuracy')]

mm = MaryMorstan(generations=4, population_size=5, objectives=objs)
pipelines = mm.optimize(X_train=X_train, y_train=y_train, random_state=42)

best_pipeline = MaryMorstan.best(pipelines)

print("best pipeline found:", str(best_pipeline))
print("Objectives:", str(mm.objectives))

print(f'current validation_scores: {str(list(best_pipeline.fitness.weighted_values))}')

best_pipeline_compiled = best_pipeline.compile()
best_pipeline_compiled.fit(X_train, y_train)

print(type(best_pipeline_compiled))

score = best_pipeline_compiled.score(X_train, y_train)
print("train score 1(accuracy): ", score)
train_scores = _score(best_pipeline_compiled, X_train, y_train, mm.objectives.scorers).values()
test_scores = _score(best_pipeline_compiled, X_test, y_test, mm.objectives.scorers).values()

print("train scores :", str(list(train_scores)))
print("test scores :", str(list(test_scores)))

import sklearn
pred_y_test = best_pipeline_compiled.predict(X_test)
print("test score balanced_accuracy: ", sklearn.metrics.balanced_accuracy_score(y_test, pred_y_test))
print("test score accuracy: ", sklearn.metrics.accuracy_score(y_test, pred_y_test))

pred_y_train = best_pipeline_compiled.predict(X_train)
print("train score balanced_accuracy: ", sklearn.metrics.balanced_accuracy_score(y_train, pred_y_train))
print("train score accuracy: ", sklearn.metrics.accuracy_score(y_train, pred_y_train))
