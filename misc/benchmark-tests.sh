#!/bin/bash

pytest_debug="$1"
specific_benchmark="$2"

python3 -m pytest \
${pytest_debug} \
-p no:cacheprovider \
${specific_benchmark}
