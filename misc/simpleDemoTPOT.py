#!/usr/bin/env python3

from sklearn.model_selection import train_test_split
from sklearn.model_selection._validation import _score, cross_val_score

import importlib
import logging
import numpy as np

from marymorstan.marymorstan import MaryMorstan
from marymorstan.objectives import OBJECTIVE
from marymorstan.evaluation import EVALUATION_STRATEGY

log_level = getattr(logging, 'ERROR')
logging.basicConfig(format='%(asctime)s [%(filename)s:%(lineno)d] %(levelname)s %(message)s', level=log_level)

dataset_preprocessing_module = importlib.import_module("datasets.iris_dataset_preprocessing")
dataset = dataset_preprocessing_module.MyDataSetPreprocessing("iris")

X_train, X_test, y_train, y_test = train_test_split(dataset.get_X(), dataset.get_y(), test_size=.25, random_state=42)

classifier_config_dict = {
    'sklearn.ensemble.ExtraTreesClassifier': {
        'n_estimators': [100],
        'criterion': ["gini", "entropy"],
        'max_features': np.arange(0.05, 1.01, 0.05),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21),
        'bootstrap': [True, False]
    },

    # 'xgboost.XGBClassifier': {
    #     'n_estimators': [100],
    #     'max_depth': range(1, 11),
    #     'learning_rate': [1e-3, 1e-2, 1e-1, 0.5, 1.],
    #     'subsample': np.arange(0.05, 1.01, 0.05),
    #     'min_child_weight': range(1, 21),
    #     'verbosity': [0]
    # },
    #
    # 'sklearn.linear_model.SGDClassifier': {
    #     'loss': ['log', 'hinge', 'modified_huber', 'squared_hinge', 'perceptron'],
    #     'penalty': ["l2", "l1", "elasticnet", "none"],
    #     'alpha': [0.0, 0.01, 0.001],
    #     'learning_rate': ["constant", "optimal", "invscaling", "adaptive"],
    #     'fit_intercept': [True, False],
    #     'l1_ratio': [0.25, 0.0, 1.0, 0.75, 0.5],
    #     'eta0': [0.1, 1.0, 0.01],
    #     'power_t': [0.5, 0.0, 1.0, 0.1, 100.0, 10.0, 50.0]
    # },

    'sklearn.ensemble.RandomForestClassifier': {
        'n_estimators': [100],
        'criterion': ["gini", "entropy"],
        'max_features': np.arange(0.05, 1.01, 0.05),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21),
        'bootstrap': [True, False]
    },

    'sklearn.linear_model.RidgeClassifier': {
        "solver": ["sparse_cg", "lsqr", "saga", "cholesky", "svd", "auto", "sag", "lbfgs"],
        "tol": [0.001, 0.1],
        "alpha": [0.000001, 0.001, 1, 10],
        "max_iter": [10000]
    },

    'sklearn.linear_model.Perceptron': {
        "penalty": ["l2", "l1", "elasticnet"],
        "alpha": [0.000001, 0.001, 1],
        "tol": [0.0001, 0.001, 0.1],
        "max_iter": [10000],
        "fit_intercept": [True, False]
    },

    'sklearn.linear_model.PassiveAggressiveClassifier': {
        "C": [1e-4, 1e-3, 1e-2, 1e-1, 0.5, 1., 5., 10., 15., 20., 25.],
        "tol": [0.0001, 0.001, 0.1],
        "fit_intercept": [True, False],
        "max_iter": [10000]
    },

    'sklearn.linear_model.LogisticRegression': {
        "penalty": ["l2", "l1"],
        'C': [1e-4, 1e-3, 1e-2, 1e-1, 0.5, 1., 5., 10., 15., 20., 25.],
        'dual': [True, False]
    },

    'sklearn.ensemble.GradientBoostingClassifier': {
        'n_estimators': [100],
        'learning_rate': [1e-3, 1e-2, 1e-1, 0.5, 1.],
        'max_depth': range(1, 11),
        'min_samples_split': range(2, 21),
        'min_samples_leaf': range(1, 21),
        'subsample': np.arange(0.05, 1.01, 0.05),
        'max_features': np.arange(0.05, 1.01, 0.05)
    },

    'sklearn.tree.DecisionTreeClassifier': {
        'criterion': ["gini", "entropy"],
        'max_depth': range(1, 11),
        "splitter": ["best", "random"],
        'min_samples_split': range(2, 21),
        "max_features": ["auto", "sqrt", "log2"],
        'min_samples_leaf': range(1, 21)
    },

    # Preprocesssors
    'sklearn.preprocessing.Binarizer': {
        'threshold': np.arange(0.0, 1.01, 0.05)
    },

    'sklearn.decomposition.FastICA': {
        'tol': np.arange(0.0, 1.01, 0.05)
    },

    'sklearn.preprocessing.MaxAbsScaler': {
    },

    'sklearn.preprocessing.MinMaxScaler': {
    },

    'sklearn.preprocessing.Normalizer': {
        'norm': ['l1', 'l2', 'max']
    },

    'sklearn.preprocessing.RobustScaler': {
    },

    'sklearn.preprocessing.StandardScaler': {
    },

    'tpot.builtins.OneHotEncoder': {
        'minimum_fraction': [0.05, 0.1, 0.15, 0.2, 0.25],
        'sparse': [False],
        'threshold': [10]
    },

    # Selectors
    'sklearn.feature_selection.SelectFwe': {
        'alpha': np.arange(0, 0.05, 0.001),
        'score_func': {
            'sklearn.feature_selection.f_classif': None
        }
    },

    'sklearn.feature_selection.SelectPercentile': {
        'percentile': range(1, 100),
        'score_func': {
            'sklearn.feature_selection.f_classif': None
        }
    },

    'sklearn.feature_selection.VarianceThreshold': {
        'threshold': [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.2]
    },

    'sklearn.feature_selection.RFE': {
        'step': np.arange(0.05, 1.01, 0.05),
        'estimator': {
            'sklearn.ensemble.ExtraTreesClassifier': {
                'n_estimators': [100],
                'criterion': ['gini', 'entropy'],
                'max_features': np.arange(0.05, 1.01, 0.05)
            }
        }
    },

    'sklearn.feature_selection.SelectFromModel': {
        'threshold': np.arange(0, 1.01, 0.05),
        'estimator': {
            'sklearn.ensemble.ExtraTreesClassifier': {
                'n_estimators': [100],
                'criterion': ['gini', 'entropy'],
                'max_features': np.arange(0.05, 1.01, 0.05)
            }
        }
    }
}
from tpot import TPOTClassifier

tpot = TPOTClassifier(  # scoring="f1",  # métrique de scoring qu'on cherche à optimiser
    scoring="accuracy",
    template="Transformer-Selector-Classifier",
    random_state=42,  # random_seed pour la reproductibilité
    generations=4,  # Number of iterations to the run pipeline optimization process
    population_size=5,
    config_dict=classifier_config_dict,
    cv=5  # cross validation
)

tpot.fit(X_train, y_train)
best = tpot.fitted_pipeline_
best.fit(X_train, y_train)
print(str(best))
validation_score = np.mean(cross_val_score(estimator=best, X=X_train, y=y_train, cv=5))
print("validation score:", validation_score)
score_train = best.score(X_train, y_train)
score_test = best.score(X_test, y_test)
y_pred = best.predict(X_train)
# f1_train_score = f1_score(y_train, y_pred)
# y_pred = best.predict(X_test)
# f1_test_score = f1_score(y_test, y_pred)
print(f"Score train best: {tpot.score(X_train, y_train)}")
print(f"Score test best: {tpot.score(X_test, y_test)}\n")
print(f"Score train : {tpot.score(X_train, y_train)}")
print(f"Score test: {tpot.score(X_test, y_test)}\n")
# print(f"Best classifier: {bestClassifier}")
# print(f"Best selector: {bestSelector}")
# print(f"Best transformer: {bestTransformer}")

mm = MaryMorstan(
    objectives=[OBJECTIVE.ACCURACY],
    generations=4,
    population_size=5,
    search_space_file="./misc/search_spaces/search_space_demo.yml",
    evaluation_strategy=EVALUATION_STRATEGY.STRATIFIED_K_FOLD
    )

pipelines = mm.optimize(
    X_train=X_train, y_train=y_train, random_state=42)

best_pipeline = MaryMorstan.best(pipelines)


print("best pipeline found:", str(best_pipeline))
print("Objectives:", str(mm.objectives))

best_pipeline_compiled = best_pipeline.compile()
best_pipeline_compiled.fit(X_train, y_train)

print(f'current validation_scores: {str(list(best_pipeline.fitness.weighted_values))}')

validation_score = np.mean(cross_val_score(estimator=best_pipeline_compiled, X=X_train, y=y_train, cv=5))
print("validation score ???? MM : ", validation_score)


train_scores = _score(best_pipeline_compiled, X_train, y_train, mm.objectives.scorers).values()
test_scores = _score(best_pipeline_compiled, X_test, y_test, mm.objectives.scorers).values()



print("train scores :", str(list(train_scores)))
print("test scores :", str(list(test_scores)))

