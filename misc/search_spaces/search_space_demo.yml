version: 2

include_estimators_in_preprocessors: False # Synthetic features

groups:
  estimators: ['ExtraTreesClassifier', 'XGBClassifier', 'SGDClassifier',
               'RandomForestClassifier', 'RidgeClassifier', 'Perceptron',
               'PassiveAggressiveClassifier', 'LogisticRegression', 'GradientBoostingClassifier',
               'DecisionTreeClassifier']
  preprocessors: ["Binarizer", "FastICA", "MaxAbsScaler", "MinMaxScaler", "Normalizer", "RobustScaler",
                  "StandardScaler", "SelectFwe", "OneHotEncoder", "SelectPercentile", "VarianceThreshold",
                  "RFE", "SelectFromModel"]

algorithms:
  ExtraTreesClassifier:
    backend: "sklearn"
    class_identifier: "ensemble.ExtraTreesClassifier"
    forbiddens:
      hyperparameters:
        - max_features > 1. or max_features < 0
    hyperparameters:
      n_estimators:
        type: "categorical"
        choices: [100]
        default: 100
      criterion:
        type: "categorical"
        choices: ["gini", "entropy"]
        default: "gini"
      max_features:
        type: "categorical"
        arange: (0.05, 1.01, 0.05)
        default: "auto"
      min_samples_split:
        type: "categorical"
        range: (2, 21)
        default: 2
      min_samples_leaf:
        type: "categorical"
        range: (1, 21)
        default: 1
      bootstrap:
        type: "categorical"
        choices: [True, False]
        default: False
  XGBClassifier:
    backend: "xgboost"
    class_identifier: "XGBClassifier"
    forbiddens:
      hyperparameters:
        - learning_rate < 0
        - subsample <= 0 or subsample > 1
    hyperparameters:
      n_estimators:
        type: "categorical"
        choices: [100]
        default: 100
      learning_rate:
        type: "categorical"
        choices: [1e-3, 1e-2, 1e-1, 0.5, 1.]
        default: .1
      max_depth:
        type: "categorical"
        range: (1, 11)
        default: 3
      subsample:
        type: "categorical"
        arange: (0.05, 1.01, 0.05)
        default: 1.
      min_child_weight:
        type: "categorical"
        range: (1, 21)
      verbosity:
        type: "categorical"
        choices: [0]
        default: 0
  SGDClassifier:
    backend: "sklearn"
    class_identifier: "linear_model.SGDClassifier"
    hyperparameters:
      loss:
        type: "categorical"
        choices: [ "log", "hinge", "modified_huber", "squared_hinge", "perceptron"]
        default: "log"
      penalty:
        type: "categorical"
        choices: ["l2", "l1", "elasticnet"]
        default: "l2"
      alpha:
        type: "categorical"
        choices: [0.0, 0.01, 0.001]
        default: 0.01
      learning_rate:
        type: "categorical"
        choices: ["constant", "optimal", "invscaling", "adaptive"]
        default: "constant"
      fit_intercept:
        type: "categorical"
        choices: [True, False]
        default: True
      l1_ratio:
        type: "categorical"
        choices: [0.25, 0.0, 1.0, 0.75, 0.5]
        default: 1.0
      eta0:
        type: "categorical"
        choices: [0.1, 1.0, 0.01]
        default: 1.0
      power_t:
        type: "categorical"
        choices: [0.5, 0.0, 1.0, 0.1, 100.0, 10.0, 50.0]
        default: 0.1
  RandomForestClassifier:
    backend: "sklearn"
    class_identifier: "ensemble.RandomForestClassifier"
    forbiddens:
      hyperparameters:
        - max_features > 1. or max_features < 0
    hyperparameters:
      n_estimators:
        type: "categorical"
        choices: [100]
        default: 100
      criterion:
        type: "categorical"
        choices: ["gini", "entropy"]
        default: "gini"
      max_features:
        type: "categorical"
        arange: (0.05, 1.01, 0.05)
        default: "auto"
      min_samples_split:
        type: "categorical"
        range: (2, 21)
        default: 2
      min_samples_leaf:
        type: "categorical"
        range: (1, 21)
        default: 1
      bootstrap:
        type: "categorical"
        choices: [True, False]
        default: False
  RidgeClassifier:
    backend: "sklearn"
    class_identifier: "linear_model.RidgeClassifier"
    hyperparameters:
      solver:
        type: "categorical"
        choices: ["sparse_cg", "lsqr", "saga", "cholesky", "svd", "auto", "sag", "lbfgs"]
        default: "sparse_cg"
      tol:
        type: "categorical"
        choices: [0.001, 0.1]
        default: 0.001
      alpha:
        type: "categorical"
        choices: [0.000001, 0.001, 1, 10]
        default: 0.001
      max_iter:
        type: "categorical"
        choices: [10000]
        default: 10000
  Perceptron:
    backend: "sklearn"
    class_identifier: "linear_model.Perceptron"
    hyperparameters:
      penalty:
        type: "categorical"
        choices: ["l2", "l1", "elasticnet"]
        default: "l2"
      alpha:
        type: "categorical"
        choices: [0.000001, 0.001, 1]
        default: 0.001
      tol:
        type: "categorical"
        choices: [0.0001, 0.001, 0.1]
        default: 0.001
      max_iter:
        type: "categorical"
        choices: [10000]
        default: 10000       
      fit_intercept:     
        type: "categorical"
        choices: [True, False]
        default: True
  PassiveAggressiveClassifier:
    backend: "sklearn"
    class_identifier: "linear_model.PassiveAggressiveClassifier"
    hyperparameters:
      C:
        type: "categorical"
        choices: [1e-4, 1e-3, 1e-2, 1e-1, 0.5, 1., 5., 10., 15., 20., 25.]
        default: 0.5
      tol:
        type: "categorical"
        choices: [0.0001, 0.001, 0.1]
        default: 0.001
      fit_intercept:     
        type: "categorical"
        choices: [True, False]
        default: True
      max_iter:
        type: "categorical"
        choices: [10000]
        default: 10000
  LogisticRegression:
    backend: "sklearn"
    class_identifier: "linear_model.LogisticRegression"
    limits:
      number_of_try_hyperparameters: 50
    forbiddens:
      hyperparameters:
        - dual == True and penalty == 'l1'
        - C < 0
        - solver == 'lbfgs' and (penalty == 'l1' or dual == True)
    hyperparameters:
      penalty:
        type: "categorical"
        choices: ["l1", "l2"]
        default: "l2"
      C:
        type: "categorical"
        choices: [1e-4, 1e-3, 1e-2, 1e-1, 0.5, 1., 5., 10., 15., 20., 25.]
        default: 1.
      dual:
        type: "categorical"
        choices: [True, False]
        default: False
      solver:
        type: "categorical"
        choices: ['lbfgs']
        default: 'lbfgs'
  GradientBoostingClassifier:
    backend: "sklearn"
    class_identifier: "ensemble.GradientBoostingClassifier"
    forbiddens:
      hyperparameters:
        - learning_rate < 0
        - max_features > 1. or max_features < 0
        - subsample <= 0 or subsample > 1
    hyperparameters:
      n_estimators:
        type: "categorical"
        choices: [100]
        default: 100
      learning_rate:
        type: "categorical"
        choices: [1e-3, 1e-2, 1e-1, 0.5, 1.]
        default: 1e-1
      max_depth:
        type: "categorical"
        range: (1, 11)
        default: 3
      min_samples_split:
        type: "categorical"
        range: (2, 21)
        default: 2
      min_samples_leaf:
        type: "categorical"
        range: (1, 21)
        default: 1
      subsample:
        type: "categorical"
        arange: (0.05, 1.01, 0.05)
        default: 1.
      max_features:
        type: "categorical"
        arange: (0.05, 1.01, 0.05)
        default: None
  DecisionTreeClassifier:
    backend: "sklearn"
    class_identifier: "tree.DecisionTreeClassifier"
    hyperparameters:
      criterion:
        type: "categorical"
        choices: ["gini", "entropy"]
        default: "gini"
      max_depth:
        type: "categorical"
        range: (1, 11)
        default: None
      splitter:
        type: "categorical"
        choices: ["best", "random"]
        default: "best"
      min_samples_split:
        type: "categorical"
        range: (2, 21)
        default: 2
      min_samples_leaf:
        type: "categorical"
        range: (1, 21)
        default: 1
  Binarizer:
    backend: "sklearn"
    class_identifier: "preprocessing.Binarizer"
    hyperparameters:
      threshold:
        type: "categorical"
        arange: (0., 1.01, 0.05)
        default: 0.
  FastICA:
    backend: "sklearn"
    class_identifier: "decomposition.FastICA"
    forbiddens:
      previous:
        - Binarizer  # TODO not obvious case, found thanks to benchmark and manual try, could be found smartly
        - FastICA
    hyperparameters:
      tol:
        type: "categorical"
        arange: (0., 1.01, 0.05)
        default: .01
  Normalizer:
    backend: "sklearn"
    class_identifier: "preprocessing.Normalizer"
    hyperparameters:
      norm:
        type: "categorical"
        choices: ["l1", "l2", "max"]
        default: "l1"
  StandardScaler:
    backend: "sklearn"
    class_identifier: "preprocessing.StandardScaler"
  MaxAbsScaler:
    backend: "sklearn"
    class_identifier: "preprocessing.MaxAbsScaler"
  MinMaxScaler:
    backend: "sklearn"
    class_identifier: "preprocessing.MinMaxScaler"
  RobustScaler:
    backend: "sklearn"
    class_identifier: "preprocessing.RobustScaler"
  OneHotEncoder:
    backend: "marymorstan"
    class_identifier: "preprocessing.one_hot_encoder.OneHotEncoder"
    hyperparameters:
      minimum_fraction:
        type: "categorical"
        choices: [0.05, 0.1, 0.15, 0.2, 0.25]
      sparse:
        type: "categorical"
        choices: [False]
      threshold:
        type: "categorical"
        choices: [10]
  SelectFwe:
    backend: "sklearn"
    class_identifier: "feature_selection.SelectFwe"
    hyperparameters:
      alpha:
        type: "categorical"
        arange: (0., 0.05, 0.001)
      score_func:
        type: 'callable'
        class_identifier: feature_selection.f_classif
  SelectPercentile:
    backend: "sklearn"
    class_identifier: "feature_selection.SelectPercentile"
    hyperparameters:
      percentile:
        type: "categorical"
        range: (1, 100)
      score_func:
        type: 'callable'
        class_identifier: feature_selection.f_classif
    forbiddens:
      previous:
        - RFE
        - VarianceThreshold
        - SelectFromModel
        - SelectPercentile
  VarianceThreshold:
    backend: "sklearn"
    class_identifier: "feature_selection.VarianceThreshold"
    hyperparameters:
      threshold:
        type: "categorical"
        choices: [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1, 0.2]
    forbiddens:
      previous:
        - RFE
        - VarianceThreshold
        - SelectFromModel
        - SelectPercentile
  RFE:
    backend: "sklearn"
    class_identifier: "feature_selection.RFE"
    hyperparameters:
      step:
        type: "categorical"
        arange: (0.05, 1.01, 0.05)
      estimator:
        type: 'estimator'
        class_identifier: "ensemble.ExtraTreesClassifier"
        hyperparameters:
          n_estimators:
            type: "categorical"
            choices: [100]
          criterion:
            type: "categorical"
            choices: ['gini', 'entropy']
          max_features:
            type: "categorical"
            arange: (0.05, 1.01, 0.05)
    forbiddens:
      previous:
        - RFE
        - VarianceThreshold
        - SelectFromModel
        - SelectPercentile
  SelectFromModel:
    backend: "sklearn"
    class_identifier: "feature_selection.SelectFromModel"
    hyperparameters:
      threshold:
        type: "categorical"
        arange: (0, 1.01, 0.05)
      estimator:
        type: 'estimator'
        class_identifier: "ensemble.ExtraTreesClassifier"
        hyperparameters:
          n_estimators:
            type: "categorical"
            choices: [100]
          criterion:
            type: "categorical"
            choices: ['gini', 'entropy']
          max_features:
            type: "categorical"
            arange: (0.05, 1.01, 0.05)
    forbiddens:
      previous:
        - RFE
        - VarianceThreshold
        - SelectFromModel
        - SelectPercentile
                  
