#!/bin/bash

mary-morstan --generations 5 --population-size 5 --seed 40 --log-level=DEBUG \
--evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_tpot.yml' \
--successive-halving --successive-halving-minimum-population-size 2 --successive-halving-initial-budget 100 \
--successive-halving-maximum-budget 200 2>/dev/null | grep 'pipeline' | wc -l | grep '^2$' >/dev/null || exit 70
