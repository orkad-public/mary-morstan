#!/bin/bash

results=$(mary-morstan  \
  --dataset 'http://archive.ics.uci.edu/ml/machine-learning-databases/forest-fires/forestfires.csv' \
  --dataset-preprocessing 'datasets.remote_csv_dataset_preprocessing' \
  --search-space './misc/search_spaces/search_space_regression_light.yml' --objectives 'r2' \
  --evaluation-test-size .25 \
  --generations 2 --population-size 10 --seed 42 --log-level=INFO 2>&1 || exit 40)

exit_code="$?"

if [ "${exit_code}" != "0" ]
then
  echo ${results}
  exit ${exit_code}
fi

echo ${results} | grep 0.11792351206144569 >/dev/null || exit 41
echo ${results} | grep 0.7401839889682496 >/dev/null || exit 42
