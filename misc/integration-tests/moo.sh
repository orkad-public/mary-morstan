#!/usr/bin/env bash

results=$(mary-morstan --dataset 'iris' --dataset-preprocessing "datasets.iris_dataset_preprocessing" \
 --evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_with_nsga_2.yml' \
--objectives 'accuracy' 'precision_weighted' 'roc_auc_ovr' 'roc_auc_ovo' 'min_training_time' \
--generations 2 --population-size 10 --seed 42 --evaluation-test-size .25 --log-level=DEBUG 2>&1 || exit 50)

exit_code="$?"


if [ "${exit_code}" != "0" ]
then
  echo "${results}"
  exit ${exit_code}
fi

# NOTE the importance of this test is to show we have 4 different values (i.e., 5 objectives)
echo ${results} | grep -E "[0-9]\.[0-9]+, [0-9]\.[0-9]+, [0-9]\.[0-9]+, [0-9]\.[0-9]+, -[0-9]\.[0-9]+" >/dev/null || exit 51
#echo ${results} | grep -E "0\.9642857142857143, 0\.9672619047619048, 0\.9964349376114082, 0\.997245179063361, -[0-9]\.[0-9]+" >/dev/null || exit 52
