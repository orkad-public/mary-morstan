#!/bin/bash

mary-morstan --dataset 'iris' --dataset-preprocessing "datasets.iris_dataset_preprocessing" \
--generations 100 --population-size 100 --seed 42 --log-level=DEBUG --evaluation-strategy="holdout" --number-of-pipeline-failed-allowed 100 \
--enable-statistics --store-statistics /tmp/result.json >/dev/null 2>&1 &

pid=$!

sleep 2s

# simulate ctrl+c
kill -s 2 ${pid}

sleep 2s

if [ ! -f "/tmp/result.json" ]
then
  echo "Statistics and handling sigkill did not work"
  exit 21
else
  rm /tmp/result.json
fi

# signal with thread (--wall-time-in-seconds)

mary-morstan --dataset 'iris' --dataset-preprocessing "datasets.iris_dataset_preprocessing" \
--generations 100 --population-size 100 --seed 42 --log-level=DEBUG --evaluation-strategy="holdout" --number-of-pipeline-failed-allowed 100 \
--enable-statistics --store-statistics /tmp/result.json --wall-time-in-seconds 500 >/dev/null 2>&1 &

pid=$!

sleep 2s

# simulate ctrl+c
kill -s 2 ${pid}

sleep 2s

if [ ! -f "/tmp/result.json" ]
then
  echo "Statistics and handling sigkill did not work with wall time (threading)"
  exit 22
else
  rm /tmp/result.json
fi
