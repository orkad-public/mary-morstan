#!/bin/bash

# NOTE: known issue with (AttributeError: 'str' object has no attribute 'decode') in LogisticRegression.
# It has been fixed in later scikit-learn version, however we keep scikit-learn version during
# benchmarking against TPOT to have similar version. This explains why we wisely choose seed to avoid this issue.

results=$(mary-morstan --generations 2 --population-size 5 --seed 41 --log-level=DEBUG  --evaluation-test-size .1 \
--objectives 'balanced_accuracy' 'min_pipeline_size' \
--search-space './misc/search_spaces/search_space_tpot_light.yml' \
--evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_tpot.yml' 2>&1 || exit 60)

exit_code="$?"

if [ "${exit_code}" != "0" ]
then
  echo ${results}
  exit ${exit_code}
fi

# NOTE performance is not important, but pipeline size included in objectives is important to be present
echo ${results} | grep -E "\([0-9]\.[0-9]+, -2\.0\)" >/dev/null || exit 62
echo ${results} | grep "9b2e2bfa7c71bbb54cb7361a72a8919dabeedafa" >/dev/null || exit 63
echo ${results} | grep "1.0" >/dev/null || exit 64
