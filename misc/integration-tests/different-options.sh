#!/bin/bash

echo "--test-best-pipeline"
results=$(mary-morstan --dataset 'iris' --dataset-preprocessing "datasets.iris_dataset_preprocessing" \
--evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_with_nsga_2.yml' \
--objectives 'accuracy' 'precision_weighted' 'min_pipeline_size' 'roc_auc_ovo' --generations 2 --population-size 5 --seed 42 --log-level=ERROR \
--print-best-pipeline-only --evaluation-strategy="stratified_shuffle_split" --evaluation-test-size .2 --evaluation-n-splits 3 \
--test-best-pipeline 2>&1 || exit 90)

exit_code="$?"

if [ "${exit_code}" != "0" ]
then
  echo "${results}"
  exit ${exit_code}
fi
#
echo ${results} | grep 'obtaining the following new test_scores: \[1.0, 1.0, 1.0\]' >/dev/null || exit 91
echo ${results} | grep 'evaluation scores (0.9565217391304349, 0.9581320450885668, -3.0, 0.9722222222222223))' >/dev/null || exit 92

echo "--allow-fit-to-valid-pipeline"
results=$(mary-morstan --dataset 'iris' --dataset-preprocessing "datasets.iris_dataset_preprocessing" \
--evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_with_nsga_2.yml' \
--objectives 'accuracy' 'precision_weighted' 'min_pipeline_size' 'roc_auc_ovo' --generations 2 --population-size 5 --seed 42 --log-level=DEBUG \
--print-best-pipeline-only --evaluation-strategy="stratified_shuffle_split" --evaluation-test-size .2 --evaluation-n-splits 3 \
--allow-fit-to-valid-pipeline 2>&1 || exit 93)

exit_code="$?"

if [ "${exit_code}" != "0" ]
then
  echo "${results}"
  exit ${exit_code}
fi

echo ${results} | grep 'evaluation scores (0.9565217391304349, 0.9581320450885668, -3.0, 0.9730902777777777))' >/dev/null || exit 94
