#!/usr/bin/env bash

mary-morstan --dataset 'iris' --dataset-preprocessing "datasets.iris_dataset_preprocessing" \
                          --generations 2 --population-size 10 --seed 42 --log-level=DEBUG \
                          --evaluation-strategy="stratified_shuffle_split" --evaluation-summarize-method="median" \
                          --evaluation-test-size .1 --evaluation-n-splits 4 >/dev/null 2>&1 || exit 30
