#!/bin/bash

results=$(mary-morstan --generations 0 --population-size 5 --seed 2021236247 \
--log-level=DEBUG --objectives 'balanced_accuracy' 'min_pipeline_size' \
--evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_tpot.yml' \
--dataset car_evaluation --dataset-preprocessing 'datasets.ics_uci_dataset_preprocessing'  --test-size-ratio .25 \
--dataset-fill-nan 0 --number-of-pipeline-failed-allowed 100 --evaluation-strategy 'stratified_shuffle_split' \
--search-space './misc/search_spaces/tests/search_space_tpot_failing.yml' \
--evolutionary-algorithms-parameters '{"initialization": "marymorstan.initialization.RandomInitialization(min_size=3, max_size=3)"}' 2>&1 || exit 120)

exit_code="$?"

if [ "${exit_code}" != "0" ]
then
  echo ${results}
  exit ${exit_code}
fi

echo ${results} | grep '(-inf, -3.0)' >/dev/null || exit 121 # NOTE should have a -inf if it is a maximize objective
