#!/bin/bash

ea_space_file="./misc/evolutionary_algorithms_spaces/ea_space_with_nsga_2.yml"

# ensure the space file has specified probabilities
grep "mutation_probabilities: \[1/3, 1/3, 1/3\]" ${ea_space_file} >/dev/null || exit 100

# the previous probabilities should not match since we pass only one mutation through ea_parameters, but we also pass "mutation_probabilities" through ea_parameters.
# in this way, we ensure that ea_parameters are applied and dominate the parameters from the ea_space_file
echo "--evolutionary-algorithms-parameters"
results=$(mary-morstan --dataset 'iris' --dataset-preprocessing "datasets.iris_dataset_preprocessing" \
 --evolutionary-algorithms-space ${ea_space_file} \
--objectives 'accuracy' 'precision_weighted' 'min_pipeline_size' 'roc_auc_ovo' --generations 2 --population-size 5 --seed 45 --log-level=DEBUG \
--print-best-pipeline-only --evaluation-strategy="stratified_shuffle_split" --evaluation-test-size .3 --evaluation-n-splits 3 \
--evolutionary-algorithms-parameters '{"ea_algorithm":"mu,lambda", "ea_algorithm_parameters":{"mu": 11, "lambda_":13},"number_of_try_recall_variation": 1, "one_mutation_per_individual": true, "mutation_probabilities": "uniform", "mutations": ["marymorstan.variation.MutationGaussian(mu=5)"]}' 2>&1 || exit 101)

exit_code="$?"

if [ "${exit_code}" != "0" ]
then
  echo ${results}
  exit ${exit_code}
fi

echo ${results} | grep 'override parameter number_of_try_recall_variation to 1' >/dev/null || exit 102
echo ${results} | grep -v 'New unknown' >/dev/null || exit 103
echo ${results} | grep -v 'MutationUniformInteger' >/dev/null || exit 104
echo ${results} | grep -v 'MutationOneRandom' >/dev/null || exit 105
echo ${results} | grep 'Population size of selected individuals: 11' >/dev/null || exit 106
