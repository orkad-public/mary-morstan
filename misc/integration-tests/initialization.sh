#!/bin/bash

results=$(mary-morstan --generations 2 --population-size 10 --seed 43 --evaluation-test-size .1 \
--evolutionary-algorithms-space "./misc/evolutionary_algorithms_spaces/ea_space_tpot_with_lhs.yml" \
--log-level=DEBUG 2>&1 || exit 80)

exit_code="$?"

if [ "${exit_code}" != "0" ]
then
  echo ${results}
  exit ${exit_code}
fi

echo ${results} | grep -v "random estimator is selected" >/dev/null || exit 81
echo ${results} | grep "LHS works in degraded mode,random preprocessor is selected (degraded index: 1)" >/dev/null || exit 82
