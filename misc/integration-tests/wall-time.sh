echo "--wall-time-in-seconds"
results=$(timeout -k 5s 10s mary-morstan --dataset 'iris' --dataset-preprocessing "datasets.iris_dataset_preprocessing" \
 --evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_with_nsga_2.yml' \
--objectives 'accuracy' 'precision_weighted' 'min_pipeline_size' 'roc_auc_ovo' --generations -1 --population-size 4 --seed 42 --log-level=DEBUG \
--evaluation-strategy="stratified_shuffle_split" --evaluation-test-size .4 --evaluation-n-splits 2 --number-of-pipeline-failed-allowed -1 \
--wall-time-in-seconds 5 2>&1 || exit 110)

exit_code="$?"

if [ "${exit_code}" != "0" ]  # if it goes here, there are chance exit code 137 (signal kill due to timeout) has been reached (i.e., wall time don't do his job)
then
  echo ${results}
  exit ${exit_code}
fi

echo ${results} | grep 'pipeline:' >/dev/null || exit 111
echo ${results} | grep 'Wall time has been reached' >/dev/null || exit 112
echo ${results} | grep -vE 'ERROR Signal|Killed' >/dev/null || exit 113

# here there a few generation, threading should not continue until reaching the wall time (generations will)

results=$(timeout -k 5s 15s mary-morstan --dataset 'iris' --dataset-preprocessing "datasets.iris_dataset_preprocessing" \
 --evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_with_nsga_2.yml' \
--objectives 'accuracy' 'precision_weighted' 'min_pipeline_size' 'roc_auc_ovo' --generations 2 --population-size 2 --seed 42 --log-level=DEBUG \
--evaluation-strategy="stratified_shuffle_split" --evaluation-test-size .4 --evaluation-n-splits 2 --number-of-pipeline-failed-allowed -1 \
--wall-time-in-seconds 10 2>&1 || exit 114)

exit_code="$?"

if [ "${exit_code}" != "0" ]
then
  echo ${results}
  exit ${exit_code}
fi

# wall time reached should not be present !
echo ${results} | grep -v 'Wall time' | grep -v 'reached' >/dev/null || exit 115
