#!/bin/bash

results=$(mary-morstan --dataset 'GunPoint' --dataset-preprocessing "datasets.ucr_dataset_preprocessing" \
--problem-type 'timeseries_classification' \
--wall-time-in-seconds 60 --number-of-pipeline-failed-allowed 50 \
--search-space './misc/search_spaces/time_series_classification_space.yml' \
--evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_tsc.yml' \
--objectives 'accuracy' --init-pipeline-size-min 1 --init-pipeline-size-max 3 --allow-fit-to-valid-pipeline \
--budget-per-fit-to-valid-pipeline-seconds .5 \
--generations 1 --population-size 5 --seed 42 --log-level=DEBUG --budget-per-candidate-seconds 2 2>&1 || exit 140)

exit_code="$?"

if [ "${exit_code}" != "0" ]
then
  echo "${results}"
  exit ${exit_code}
fi

echo "${results}" | grep -v "Dataset is already split in two parts (train/test)" >/dev/null || exit 141
