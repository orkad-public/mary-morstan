#!/bin/bash

results=$(mary-morstan --generations 2 --population-size 10 --seed 42 \
--evaluation-test-size .25 --log-level=DEBUG 2>&1 || exit 1)

exit_code="$?"

if [ "${exit_code}" != "0" ]
then
  echo ${results}
  exit ${exit_code}
fi

echo ${results} | grep "Obtain mutated" >/dev/null || exit 2
# echo ${results} | grep "obtain crossovered" >/dev/null || exit 3 # TODO no crossover register yet
echo ${results} | grep 1.0 >/dev/null || exit 4
echo ${results} | grep 0.9642857142857143 >/dev/null || exit 5
echo ${results} | grep  "'GaussianNB({})', 'MaxAbsScaler({})', \"FastICA({'tol': 0.25})\"" >/dev/null || exit 6
echo ${results} | grep  "ab3823a56bc710440a7c5c5c83132a7b6e54a092" >/dev/null || exit 7
