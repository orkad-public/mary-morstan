#!/bin/bash

results=$(mary-morstan --dataset-preprocessing "datasets.longley_dataset_preprocessing" --dataset 'longley' \
--search-space './misc/search_spaces/time_series_forecasting_space.yml' \
--objectives 'mae' 'rmse' --init-pipeline-size-min=1 --init-pipeline-size-max=1 \
--evaluation-strategy='time_series_k_split' \
--generations 2 --population-size 10 --seed 42 --log-level=DEBUG 2>&1 || exit 150)


exit_code="$?"

if [ "${exit_code}" != "0" ]
then
  echo "${results}"
  exit ${exit_code}
fi

