#!/bin/bash

function run() {
  mary-morstan --dataset 'iris' --dataset-preprocessing "datasets.iris_dataset_preprocessing" \
   --generations -1 --seed 42 --log-level=DEBUG \
  --population-size 2 --evaluation-strategy="stratified_k_fold" --number-of-pipeline-failed-allowed -1 --wall-time-in-seconds 20 \
  --evolutionary-algorithms-parameters '{"mutation_strategy": "time_based_distribution", "mutation_probabilities":[{"0.5": [1.0, 0.0]}, {"0.5": [0.0, 1.0]}], "mutations":["marymorstan.variation.MutationGaussian", "marymorstan.variation.MutationUniformInteger"], "crossovers":[]}' >/tmp/result.log 2>&1 &
}

# NOTE check that MutationUniformInteger happen during the first 10 seconds, but MutationUniformInteger should not happen
run
pid=$!

sleep 8s

# simulate ctrl+c
kill -s 2 ${pid}

sleep 2s

if [ ! -f "/tmp/result.log" ]
then
  echo "ERROR: no logs, impossible to check"
  exit 130
fi

if ! grep "MutationGaussian" /tmp/result.log | grep -E "Apply|Try" >/dev/null
then
  cat /tmp/result.log
  echo "ERROR: No mutation occurs"
  exit 131
fi

if grep "MutationUniformInteger" /tmp/result.log | grep -E "Apply|Try" >/dev/null
then
  cat /tmp/result.log
  echo "ERROR: MutationUniformInteger SHOULD NOT happen during the first half part of the wall time"
  exit 132
fi

rm /tmp/result.log

# NOTE check that MutationUniformInteger happen
run
pid=$!

sleep 14s

# simulate ctrl+c
kill -s 2 ${pid}

sleep 2s

if [ ! -f "/tmp/result.log" ]
then
  echo "ERROR: no logs, impossible to check"
  exit 133
fi

if ! grep "MutationUniformInteger" /tmp/result.log | grep -E "Apply|Try" >/dev/null
then
  cat /tmp/result.log
  echo "ERROR: MutationUniformInteger SHOULD happen during the second half part of the wall time"
  exit 134
fi
