#!/usr/bin/env python3

from sklearn.model_selection import train_test_split
from sklearn.model_selection._validation import _score

import importlib
import logging
import numpy as np

from marymorstan.marymorstan import MaryMorstan

logger = logging.getLogger()
logger.setLevel(logging.INFO)

dataset_preprocessing_module = importlib.import_module("datasets.ucr_dataset_preprocessing")
gunpoint_dataset = dataset_preprocessing_module.MyDataSetPreprocessing("GunPoint")

print(gunpoint_dataset.get_X())
print("dataset size: ", len(gunpoint_dataset.get_X()))


X_train, X_test, y_train, y_test = train_test_split(gunpoint_dataset.get_X(), gunpoint_dataset.get_y(),
                                                    test_size=.25, random_state=42,shuffle=False)

mm = MaryMorstan(generations=2, population_size=10,
                 search_space_file='./misc/search_spaces/time_series_classification_space.yml',
                 min_pipeline_size=1, max_pipeline_size=2,  budget_per_candidate_seconds=5, random_state=42)

pipelines = mm.optimize(X_train=X_train, y_train=y_train, random_state=np.random.RandomState(42))

pipelines.sort(key=lambda p: p.fitness.values, reverse=True)

for pipeline in pipelines:
    print(pipeline)

# best_pipeline = MaryMorstan.best(pipelines)
#
# print("best pipeline found:", str(best_pipeline))
# print("Objectives:", str(mm.objectives))
#
# print(f'current validation_scores: {str(list(best_pipeline.fitness.weighted_values))}')
#
# best_pipeline_compiled = best_pipeline.compile()
# best_pipeline_compiled.fit(X_train, y_train)
#
#
# score = best_pipeline_compiled.score(X_train, y_train)
# print("train score 1: ", score)
# train_scores = _score(best_pipeline_compiled, X_train, y_train, mm.objectives.scorers).values()
# test_scores = _score(best_pipeline_compiled, X_test, y_test, mm.objectives.scorers).values()
#
# print("train scores :", str(list(train_scores)))
# print("test scores :", str(list(test_scores)))

