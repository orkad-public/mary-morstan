#!/bin/bash

pytest_debug="$1"
specific_test="$2"

pdb=""

if [ "$3" == "1" ]
then
  pdb="--trace"
fi

python3 -m pytest ${pdb} \
--cov-config=misc/coveragerc --cov=marymorstan --cov-report=term-missing \
--log-format='[%(pathname)s:%(lineno)d] %(levelname)s %(message)s' \
${pytest_debug} \
-p no:cacheprovider \
${specific_test}
