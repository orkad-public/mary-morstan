#!/usr/bin/env python3

from sklearn.model_selection import train_test_split
from sklearn.model_selection._validation import _score
import numpy as np

import importlib
import logging

from marymorstan.marymorstan import MaryMorstan

log_level = getattr(logging, 'ERROR')
logging.basicConfig(format='%(asctime)s [%(filename)s:%(lineno)d] %(levelname)s %(message)s', level=log_level)

dataset_preprocessing_module = importlib.import_module("datasets.iris_dataset_preprocessing")  # <1>
dataset = dataset_preprocessing_module.MyDataSetPreprocessing("iris")

X_train, X_test, y_train, y_test = train_test_split(dataset.get_X(), dataset.get_y(), test_size=.25, random_state=42)

mm = MaryMorstan(generations=4, population_size=5, random_state=np.random.RandomState(42))  # <2>
pipelines = mm.optimize(X_train=X_train, y_train=y_train,
                        random_state=np.random.RandomState(42))  # <3>

best_pipeline = MaryMorstan.best(pipelines)  # <4>
# then you can save it as a string and easily reimport later
best_pipeline_str = str(best_pipeline)

print("best pipeline found:", str(best_pipeline))
print("Objectives:", str(mm.objectives))

print("resulting scores")  # <5>

print(f'current validation_scores: {str(list(best_pipeline.fitness.weighted_values))}')

best_pipeline_compiled = best_pipeline.compile()
best_pipeline_compiled.fit(X_train, y_train)



train_scores = _score(best_pipeline_compiled, X_train, y_train, mm.objectives.scorers).values()
test_scores = _score(best_pipeline_compiled, X_test, y_test, mm.objectives.scorers).values()

print("train scores :", str(list(train_scores)))
print("test scores :", str(list(test_scores)))

