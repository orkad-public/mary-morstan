mary-morstan --generations 100 --population-size 100 --init-pipeline-size-min 1 --init-pipeline-size-max 3 \
--seed 4043602921 --test-size-ratio .25 --objectives 'tpot_balanced_accuracy' 'min_pipeline_size' \
--evaluation-strategy='k_fold' --evaluation-n-splits 5 \
--search-space './misc/search_spaces/search_space_tpot_no_xgb.yml' \
--evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_tpot.yml' \
 --dataset jasmine --dataset-preprocessing "datasets.openml_dataset_preprocessing" \
 --dataset-fill-nan 0 \
--log-level=DEBUG --enable-statistics --store-statistics /tmp/result.json \
--number-of-pipeline-failed-allowed -1 --wall-time-in-seconds 3600

exit 0

# TODO play with different test size ratio

mary-morstan --generations 0 --population-size 100 --init-pipeline-size-min 1 --init-pipeline-size-max 3 \
--seed 3147445070 --test-size-ratio .25 --objectives 'tpot_balanced_accuracy' 'min_pipeline_size' \
--evaluation-strategy='shuffle_split' --evaluation-n-splits 5 --evaluation-summarize-method "median" \
--search-space './misc/search_spaces/search_space_tpot_no_xgb.yml' \
--evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_tpot_with_lhs.yml' \
--dataset jasmine --dataset-preprocessing "datasets.openml_dataset_preprocessing"  --dataset-fill-nan 0 \
--log-level=DEBUG --enable-statistics --store-statistics /tmp/result.json \
--print-best-pipeline \
--number-of-pipeline-failed-allowed -1 --wall-time-in-seconds 1800


time mary-morstan --generations 0 --population-size 100 --init-pipeline-size-min 1 --init-pipeline-size-max 3 \
--seed 3265340589 --test-size-ratio .25 --objectives 'tpot_balanced_accuracy' 'min_pipeline_size' \
--evaluation-strategy='stratified_k_fold' --evaluation-n-splits 5 \
--search-space './misc/search_spaces/search_space_tpot_no_xgb.yml' \
--evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_tpot.yml' \
--dataset credit-g --dataset-preprocessing "datasets.openml_dataset_preprocessing" --dataset-fill-nan 0 \
--log-level=INFO --enable-statistics --store-statistics /tmp/result.json \
--number-of-pipeline-failed-allowed -1 --wall-time-in-seconds 1800 --allow-fit-to-valid-pipeline --max-number-of-fits 0

time mary-morstan --generations -1 --population-size 5 --init-pipeline-size-min 1 --init-pipeline-size-max 1 --seed 3265340589 --test-size-ratio .25 \
--objectives 'balanced_accuracy' --evaluation-strategy='stratified_k_fold' --evaluation-n-splits 5 --search-space './misc/search_spaces/time_series_classification_space.yml' \
--evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_tsc.yml' \
--dataset-mapper-file './misc/dataset_mapper_file/tsc-ucr-archive-2018.yaml' --dataset ArrowHead \
--dataset-fill-nan 0 --log-level=DEBUG --enable-statistics --store-statistics /tmp/result.json --number-of-pipeline-failed-allowed -1 --wall-time-in-seconds 1800 \
--budget-per-candidate-seconds 30 --problem-type timeseries_classification


mary-morstan --generations -1 --population-size 5 --seed 2882218836 --dataset-mapper-file ./misc/dataset_mapper_file/tsc-ucr-archive-2018.yaml \
--dataset EOGVerticalSignal --search-space ./misc/search_spaces/time_series_classification_space.yml --objectives balanced_accuracy \
--enable-statistics --store-statistics /tmp/result.json --number-of-pipeline-failed-allowed -1 --init-pipeline-size-max 1 \
--budget-per-candidate-seconds 300 --wall-time-in-seconds 1800 --evaluation-strategy "stratified_k_fold" --evaluation-test-size .25 \
--evaluation-n-splits 5 --test-best-pipeline --log-level=DEBUG --evolutionary-algorithms-space './misc/evolutionary_algorithms_spaces/ea_space_tsc.yml' --problem-type timeseries_classification
