#!/bin/bash

function handle_exit() {
  local exit_code="$1"
  if [ "${exit_code}" != "0" ]
  then
    exit ${exit_code}
  fi
}

echo "Try basic AutoML optimization"
./misc/integration-tests/basic-automl.sh
handle_exit "$?"

echo "Try basic optimization with statistics and signal handling"
./misc/integration-tests/handling-signal.sh
handle_exit "$?"

echo "Try AutoML optimization with another evaluation strategy"
./misc/integration-tests/evaluation-strategy.sh
handle_exit "$?"

echo "Try a regression search space"
./misc/integration-tests/regression.sh
handle_exit "$?"

# TODO handle issue for regression with seed 49
# TODO test with --evaluation-strategy="stratified_shuffle_split"

echo "Try a classification with multi objectives"
./misc/integration-tests/moo.sh
handle_exit "$?"

echo "Try optimization with TPOT variations and 2 objectives (min pipeline size)"
./misc/integration-tests/tpot-variations.sh
handle_exit "$?"

echo "Try optimization with SH (successive halving)"
./misc/integration-tests/successive-halving.sh
handle_exit "$?"

echo "Try initialization"
./misc/integration-tests/initialization.sh
handle_exit "$?"

echo "Try different options: "
./misc/integration-tests/different-options.sh
handle_exit "$?"

./misc/integration-tests/ea-parameters.sh
handle_exit "$?"

./misc/integration-tests/wall-time.sh
handle_exit "$?"

echo "Try an optimization with pipeline that fail"
./misc/integration-tests/failing-pipeline.sh
handle_exit "$?"

echo "Try an optimization with time_based_distribution"
./misc/integration-tests/time-based-strategy.sh
handle_exit "$?"

#****** Time series : work in progress ***********

echo "Try an univariate time series classification problem without preprocessing"
./misc/integration-tests/timeseries-classification.sh
handle_exit "$?"

echo "Try an multivariate time series forecasting problem without preprocessing"
./misc/integration-tests/multivariate-timeseries.sh
handle_exit "$?"
