# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from enum import Enum
from typing import Optional
from collections.abc import Sequence
import logging
import re
import math

from pkg_resources import resource_filename

import numpy as np
from numpy import random

from .evolutionary_algorithms.utils import random_state_parser

from .utils import keys_exists, read_yaml


class InvalidSearchSpace(Exception):
    pass


class InvalidClause(Exception):
    pass


class SEARCH_SPACE_KEY(Enum):
    GROUPS = "groups"
    ALGORITHMS = "algorithms"

    ESTIMATOR = "estimators"
    PREPROCESSOR = "preprocessors"

    COMPONENT_HYPERPARAMETERS = "hyperparameters"
    COMPONENT_HYPERPARAMETERS_TYPE = "type"

    FORBIDDEN = "forbiddens"
    FORBIDDEN_PREVIOUS = "previous"
    FORBIDDEN_PREVIOUSES = "previouses"
    FORBIDDEN_HYPERPARAMETERS = "hyperparameters"

    REQUIREMENT = "requirements"
    REQUIREMENT_PREVIOUS = "previous"


class SearchSpace(Sequence):
    def __init__(self, search_space_file=None, check_search_space_file=True):  # pylint: disable=super-init-not-called
        if not search_space_file:
            search_space_file = SearchSpace.get_default_search_space_file()

        self.search_space = read_yaml(search_space_file)

        if 'version' not in self.search_space or self.search_space['version'] != 2:
            raise InvalidSearchSpace(f"outdated version {self.search_space['version']} to represent the search space")

        if check_search_space_file:
            try:
                self.check_search_space(self.search_space)
            except InvalidSearchSpace as e:
                logging.error("Search space is not well formated: %s", e)
                raise e

        logging.debug("Search space defined from %s", search_space_file)

    def set_search_space(self, search_space_file, check_search_space_file=True):
        logging.debug("Set another search space %s", search_space_file)
        self.search_space = read_yaml(search_space_file)

        if check_search_space_file:
            try:
                self.check_search_space(self.search_space)
            except InvalidSearchSpace as e:
                logging.error("Search space is not well formated: %s", e)
                raise e

    @property
    def include_estimators_in_preprocessors(self):
        if 'include_estimators_in_preprocessors' in self.search_space:
            return self.search_space['include_estimators_in_preprocessors']
        return False

    @property
    def list_of_algorithms(self):
        return self.search_space[SEARCH_SPACE_KEY.ALGORITHMS.value].keys()

    @property
    def list_of_groups(self):
        return self.search_space[SEARCH_SPACE_KEY.GROUPS.value].keys()

    @property
    def list_of_estimators(self):
        return self.search_space[SEARCH_SPACE_KEY.GROUPS.value][SEARCH_SPACE_KEY.ESTIMATOR.value]

    @property
    def list_of_preprocessors(self):
        preprocessors = self.search_space[SEARCH_SPACE_KEY.GROUPS.value][SEARCH_SPACE_KEY.PREPROCESSOR.value]
        if preprocessors is None:
            preprocessors = []
        if self.include_estimators_in_preprocessors:
            preprocessors = preprocessors + self.list_of_estimators
        return preprocessors

    def algorithms_combinatorial_size(self, pipeline_size, include_hyperparameters=False):
        if pipeline_size <= 0:
            return 0

        if pipeline_size == 1:
            if include_hyperparameters is False:
                return len(self.list_of_estimators)

            count, estimators = 0, self.estimators

            for estimator, estimator_node in estimators.items():
                if SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS.value not in estimator_node:
                    count += 1
                    continue

                hyperparameters = estimator_node[SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS.value]

                tmp_count = []

                for _, hyperparameter_node in hyperparameters.items():
                    if hyperparameter_node['type'] != "categorical":
                        raise Exception(f"{__name__} do not handle the type {hyperparameter_node['type']}")

                    if 'choices' in hyperparameter_node:
                        tmp_count.append(len(hyperparameter_node['choices']))
                    elif 'range' in hyperparameter_node:
                        range_values = eval(hyperparameter_node['range'])  # pylint: disable=eval-used # TODO should avoid eval
                        tmp_count.append(len(range(range_values[0], range_values[1])))
                    elif 'arange' in hyperparameter_node:
                        arange_values = eval(hyperparameter_node['arange'])  # pylint: disable=eval-used # TODO should avoid eval
                        tmp_count.append(len(np.arange(arange_values[0], arange_values[1], arange_values[2])))
                    else:
                        raise KeyError(f"{__name__} is not able to "
                                       f"handle hyperparameter '{hyperparameter_node}' in {estimator}")

                count += np.prod(tmp_count)

            return count

        if include_hyperparameters is True:
            raise NotImplementedError("Do not handle the count of hyperparameters when the pipeline_size is greater than 1")

        # FIXME does not take the forbidden rules in consideration
        return len(self.list_of_estimators) * math.pow(len(self.list_of_preprocessors), pipeline_size - 1)

    @property
    def algorithms(self):
        return self.search_space[SEARCH_SPACE_KEY.ALGORITHMS.value]

    @property
    def estimators(self):
        estimator_nodes = {}
        for estimator_name in self.list_of_estimators:
            estimator_nodes[estimator_name] = self.search_space[SEARCH_SPACE_KEY.ALGORITHMS.value][estimator_name]
        return estimator_nodes

    @property
    def preprocessors(self):
        preprocessor_nodes = {}
        for preprocessor_name in self.list_of_preprocessors:
            preprocessor_nodes[preprocessor_name] = self.search_space[SEARCH_SPACE_KEY.ALGORITHMS.value][preprocessor_name]
        return preprocessor_nodes

    def __len__(self):
        return len(self.search_space)

    def __getitem__(self, index):
        return self.search_space[index]

    def __contains__(self, item):
        return item in self.search_space

    @staticmethod
    def get_default_search_space_file():
        return resource_filename(__name__, 'search_space.yml')

    def convert_groups_to_algorithms(self, elements):
        algorithms = elements.copy()
        for element in elements:
            if element not in self.list_of_groups:
                continue
            for algorithm in self.search_space[SEARCH_SPACE_KEY.GROUPS.value][element]:
                algorithms.add(algorithm)
            algorithms.remove(element)
        return algorithms

    def check_search_space(self, mapper: dict):
        if SEARCH_SPACE_KEY.GROUPS.value not in mapper:
            message = "groups are undefined"
            logging.error(message)
            raise InvalidSearchSpace(message)

        if SEARCH_SPACE_KEY.ALGORITHMS.value not in mapper:
            message = "algorithms are undefined"
            logging.error(message)
            raise InvalidSearchSpace(message)

        if SEARCH_SPACE_KEY.ESTIMATOR.value not in mapper[SEARCH_SPACE_KEY.GROUPS.value]:
            message = "estimators are not present in search space"
            logging.error(message)
            raise InvalidSearchSpace(message)

        if SEARCH_SPACE_KEY.PREPROCESSOR.value not in mapper[SEARCH_SPACE_KEY.GROUPS.value]:
            message = "preprocessors are not present in search space"
            logging.error(message)
            raise InvalidSearchSpace(message)

        self.check_search_space_groups_in_accordance_with_algorithms()
        self.check_search_space_forbidden_part(mapper=mapper)
        self.check_search_space_requirement_part(mapper=mapper)
        SearchSpace.check_search_space_has_requirement_or_forbidden(mapper=mapper)
        SearchSpace.check_search_space_type_in_hyperparameters(mapper=mapper)

        # FIXME check requirement part (cant have requirement_previous + forbidden_previous for a component)

    def check_search_space_groups_in_accordance_with_algorithms(self):
        algorithms = self.list_of_algorithms
        algorithms_in_groups = self.list_of_estimators + self.list_of_preprocessors

        for algorithm in algorithms:
            if algorithm not in algorithms_in_groups:
                logging.warning('algorithm %s is never used in estimators or preprocessors', algorithm)

            if algorithm in self.list_of_groups:
                raise InvalidSearchSpace(f'an algorithm ({algorithm}) has a name already defined in groups')

        for algorithm in algorithms_in_groups:
            if algorithm not in algorithms:
                raise InvalidSearchSpace(f'algorithm {algorithm} specified in group does not exist')

        for group in self.list_of_groups:
            if group in algorithms:
                raise InvalidSearchSpace(f'a group ({group}) has a name already defined in groups')

    def check_search_space_forbidden_part(self, mapper: dict):
        for component_name, component in mapper[SEARCH_SPACE_KEY.ALGORITHMS.value].items():
            if SEARCH_SPACE_KEY.FORBIDDEN.value in component:
                if SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUS.value in component[SEARCH_SPACE_KEY.FORBIDDEN.value]:
                    for pre_forbidden in component[SEARCH_SPACE_KEY.FORBIDDEN.value][SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUS.value]:
                        if pre_forbidden not in self.list_of_algorithms and pre_forbidden not in self.list_of_groups:
                            raise InvalidSearchSpace(f"(component/group: {component_name}) invalid previous forbidden,"
                                                     f" component or group {pre_forbidden} does not exist")

                if SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUSES.value in component[SEARCH_SPACE_KEY.FORBIDDEN.value]:
                    for pre_forbidden in component[SEARCH_SPACE_KEY.FORBIDDEN.value][SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUSES.value]:
                        if pre_forbidden not in self.list_of_algorithms and pre_forbidden not in self.list_of_groups:
                            raise InvalidSearchSpace(f"(component/group: {component_name}) invalid previous forbidden, component or group {pre_forbidden} does not exist")

                if SEARCH_SPACE_KEY.FORBIDDEN_HYPERPARAMETERS.value in component[SEARCH_SPACE_KEY.FORBIDDEN.value]:
                    for clause in component[SEARCH_SPACE_KEY.FORBIDDEN.value][SEARCH_SPACE_KEY.FORBIDDEN_HYPERPARAMETERS.value]:
                        try:
                            is_valid_clause(component=component, clause=clause)
                        except InvalidClause as e:
                            raise InvalidSearchSpace(f"(component: {component_name}) invalid clause {clause} : {e}") from e

    def check_search_space_requirement_part(self, mapper: dict):
        for component_name, component in mapper[SEARCH_SPACE_KEY.ALGORITHMS.value].items():
            if SEARCH_SPACE_KEY.REQUIREMENT.value in component:
                if SEARCH_SPACE_KEY.REQUIREMENT_PREVIOUS.value in component[SEARCH_SPACE_KEY.REQUIREMENT.value]:
                    for pre_requirement in component[SEARCH_SPACE_KEY.REQUIREMENT.value][SEARCH_SPACE_KEY.REQUIREMENT_PREVIOUS.value]:
                        if pre_requirement not in self.list_of_algorithms and pre_requirement not in self.list_of_groups:
                            raise InvalidSearchSpace(f"(component: {component_name}) invalid previous requirement, component {pre_requirement} does not exist")

        # TODO handle hyperparameter requirements

    @staticmethod
    def check_search_space_has_requirement_or_forbidden(mapper: dict):
        for component_name, component in mapper[SEARCH_SPACE_KEY.ALGORITHMS.value].items():
            if SEARCH_SPACE_KEY.REQUIREMENT.value in component and SEARCH_SPACE_KEY.FORBIDDEN.value in component:
                if SEARCH_SPACE_KEY.REQUIREMENT_PREVIOUS.value in component[SEARCH_SPACE_KEY.REQUIREMENT.value] and SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUS.value in component[SEARCH_SPACE_KEY.FORBIDDEN.value]:
                    raise InvalidSearchSpace(f"(component: {component_name}) cannot have both previous forbidden and previous requirement")

    @staticmethod
    def check_search_space_type_in_hyperparameters(mapper: dict):
        for component_name, component in mapper[SEARCH_SPACE_KEY.ALGORITHMS.value].items():
            if SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS.value not in component:
                continue

            for hyperparameter_name, hyperparameter in component[SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS.value].items():
                if SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS_TYPE.value not in hyperparameter:
                    raise InvalidSearchSpace(f"(component: {component_name}, hyperparameter: {hyperparameter_name}) does not have a type")
                if hyperparameter[SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS_TYPE.value] not in ['categorical', 'callable', 'estimator', 'static']:
                    raise InvalidSearchSpace(f"(component: {component_name}, hyperparameter: {hyperparameter_name}) invalid type")


def get_hyperparameter_type(component: dict, hyperparameter: str):
    return component[SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS.value][hyperparameter][SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS_TYPE.value]


def is_valid_clause(component: dict, clause: str):
    hyperparameters = component[SEARCH_SPACE_KEY.FORBIDDEN_HYPERPARAMETERS.value].keys()

    for variable in extract_variables_from_clause(clause):
        if variable not in hyperparameters:
            raise InvalidClause(f"variable '{variable}' is missing in hyperparameters")


# NOTE see https://regex101.com/r/tIrCgV/1/
# TODO handle ^ & and other special chars
def extract_variables_from_clause(clause: str) -> list:
    return re.findall(r'(?!\'[\w\s]*)\b(?!and|or|True|False|None)\b[a-zA-z]\w*(?![\w\s]*\')', clause)


def append_prefix_to_variables_in_clause(prefix: str, clause: str) -> str:
    # NOTE inspired from [Martijn Pieters] [so/q/20735384] [cc by-sa 3.0]
    return re.sub(r'(?!\'[\w\s]*)\b(?!and |or |True|False|None)\b([a-zA-z]\w*)(?![\w\s]*\')', r'{}["\1"]'.format(prefix), clause)


def hyperparameter_lower_upper_bound(component_node: dict, hyperparameter_name: str):
    if not keys_exists(component_node[SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS.value], hyperparameter_name, 'type'):
        raise KeyError(f"The hyperparameter '{hyperparameter_name}' does not exist in {component_node}")

    hyperparameter_node = component_node[SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS.value][hyperparameter_name]

    if 'choices' in hyperparameter_node:
        choices = hyperparameter_node['choices']
        return np.min(choices), np.max(choices)

    if 'range' in hyperparameter_node:
        range_values = eval(hyperparameter_node['range'])  # pylint: disable=eval-used # TODO should avoid eval
        return range_values[0], range_values[1]

    if 'arange' in hyperparameter_node:
        arange_values = eval(hyperparameter_node['arange'])  # pylint: disable=eval-used # TODO should avoid eval
        return arange_values[0], arange_values[1]

    raise KeyError(f"{__name__} is not able to handle hyperparameter '{hyperparameter_node}' in {component_node}")


def generate_hyperparameter_value(hyperparameter_node: dict, use_default: bool = False, random_state: Optional[random.RandomState] = None):  # pylint: disable=too-many-return-statements
    if hyperparameter_node[SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS_TYPE.value] == 'static':
        return hyperparameter_node['default']

    if hyperparameter_node[SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS_TYPE.value] != 'categorical':
        return None

    random_state = random_state_parser(random_state)

    if use_default and 'default' in hyperparameter_node:
        if hyperparameter_node['default'] == 'None':
            return None
        return hyperparameter_node['default']

    if 'range' in hyperparameter_node:
        range_values = eval(hyperparameter_node['range'])  # pylint: disable=eval-used # TODO should avoid eval
        return random_state.choice(range(range_values[0], range_values[1]))

    if 'arange' in hyperparameter_node:
        arange_values = eval(hyperparameter_node['arange'])  # pylint: disable=eval-used # TODO should avoid eval
        return random_state.choice(np.arange(arange_values[0], arange_values[1], arange_values[2]))  # TODO test and optimize (other random.choice method)
    v = random_state.choice(hyperparameter_node['choices'])

    # HACK/FIXME int64 makes trouble with some algorithms in sktime
    if isinstance(v, np.int64):
        return int(v)

    return v


def hyperparameter_value_violate_boundary(hyperparameter_name: str, hyperparameter_value, component_node: dict):
    lower_bound, upper_bound = hyperparameter_lower_upper_bound(component_node=component_node, hyperparameter_name=hyperparameter_name)

    if hyperparameter_value < lower_bound or hyperparameter_value > upper_bound:
        return True

    return False


def hyperparameter_value_violiate_forbidden_clauses(forbiddens: dict, hyperparameter_name=None, hyperparameter_value=None, **current_hyperparameters) -> bool:
    if not forbiddens or not keys_exists(forbiddens, SEARCH_SPACE_KEY.FORBIDDEN_HYPERPARAMETERS.value):
        return False

    kwargs = current_hyperparameters.copy()

    for clause in forbiddens[SEARCH_SPACE_KEY.FORBIDDEN_HYPERPARAMETERS.value]:
        if hyperparameter_name and hyperparameter_name in clause:
            kwargs[hyperparameter_name] = hyperparameter_value

        formated_clause = append_prefix_to_variables_in_clause(prefix="kwargs", clause=clause)

        if evaluate_clause(clause=formated_clause, **kwargs):
            return True

    return False


def evaluate_clause(clause: str, **kwargs) -> bool:  # pylint: disable=unused-argument
    try:
        if eval(clause):  # pylint: disable=eval-used # TODO should avoid eval
            return True
    except KeyError as e:
        logging.error("Variable %s does not exist in the forbidden clause %s", e, clause)
        raise e
    except NameError as e:
        logging.error("Variable is not found during the evaluation of the clause %s -> %s", clause, e)
        raise e

    return False
