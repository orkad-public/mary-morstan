# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

import math


# NOTE see https://www.researchgate.net/publication/339263193_TPOT-SH_A_Faster_Optimization_Algorithm_to_Solve_the_AutoML_Problem_on_Large_Datasets
class SuccessiveHalving():
    def __init__(self, number_of_generations, population_size, minimum_population_size, initial_budget, maximum_budget):
        if minimum_population_size > population_size:
            raise AttributeError('minimum_population_size should be inferior to population_size')

        if initial_budget > maximum_budget:
            raise AttributeError('initial_budget should be inferior to maximum_budget')

        self.number_of_generations = number_of_generations
        self.population_size = population_size
        self.initial_budget = initial_budget
        self.maximum_budget = maximum_budget
        self.minimum_population_size = minimum_population_size

    def budget(self, current_iteration):
        budget = self.maximum_budget / float(self.initial_budget)
        budget = math.floor(math.log(budget, 2) + 1)
        budget = current_iteration * budget
        budget = budget / float(self.number_of_generations + 1)
        budget = math.pow(2, math.floor(budget))
        budget = self.initial_budget * budget
        return int(budget)

    def population(self, current_iteration):
        population = self.population_size / float(self.minimum_population_size)
        population = math.floor(math.log(population, 2) + 1)
        population = population / float(self.number_of_generations + 1)
        population = math.floor(population * current_iteration)
        population = self.population_size / float(math.pow(2, population))
        return math.floor(population)
