# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from enum import Enum


class PROBLEM_TYPE(Enum):
    SUPERVISED_CLASSIFICATION_BINARY = "classification_binary"
    SUPERVISED_CLASSIFICATION_MULTICLASS = "classification_multiclass"
    SUPERVISED_REGRESSION = "regression"
    TIMESERIES_SUPERVISED_CLASSIFICATION = "timeseries_classification"
    TIMESERIES_SUPERVISED_REGRESSION = "timeseries_regression"

    def __str__(self):
        return str(self.value)
