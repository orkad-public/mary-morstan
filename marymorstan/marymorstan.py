# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)
"""Main class of the AutoML software"""
import logging
import threading
import time
import sys

from functools import partial

from pkg_resources import resource_filename

from .evolutionary_algorithms import CustomizableEA, EA_METHODS
from .evolutionary_algorithms.ea_space import EASpace
from .evolutionary_algorithms.utils import random_state_parser
from .evolutionary_algorithms.algorithms import EA_ALGORITHM
from .evolutionary_algorithms.operator_strategy import OPERATOR_DISTRIBUTION_STRATEGY

from .search_space import SearchSpace
from .initialization import RandomInitialization, INITIALIZATION_STRATEGY
from .pipeline import generate_population, MLPipeline
from .evaluation import Evaluation, EVALUATION_STRATEGY, SUMMARIZE_METHOD
from .objectives import Objectives, OBJECTIVE
from .problems import PROBLEM_TYPE
from .statistics import Statistics


class MaryMorstan():
    def __init__(self,  # pylint: disable=too-many-locals, too-many-statements
                 generations=100,
                 wall_time_optimization_seconds=None,
                 population_size=100,
                 min_pipeline_size=3,  # TODO change to 1
                 max_pipeline_size=3,
                 allow_fit_to_valid_pipeline=False,
                 max_number_of_fits=10,
                 budget_per_fit_to_valid_pipeline_seconds=2,
                 fit_pretest_X=None,
                 fit_pretest_y=None,
                 evaluation_strategy=EVALUATION_STRATEGY.HOLDOUT,  # TODO change to k-fold
                 evaluation_summarize_method=SUMMARIZE_METHOD.MEAN,
                 evaluation_test_size=.1,
                 evaluation_n_splits=5,
                 objectives=None,
                 problem_type=PROBLEM_TYPE.SUPERVISED_CLASSIFICATION_MULTICLASS,
                 search_space_file=None,
                 ea_space_file=None,
                 ea_parameters=None,
                 enable_statistics=False,
                 budget_per_candidate_seconds=300,
                 number_of_pipelines_failed_allowed=50,
                 successive_halving=None,
                 number_of_jobs=1,
                 random_state=None):
        self.search_space = SearchSpace(search_space_file=search_space_file)

        if max_number_of_fits <= 0:
            logging.warning("disable allow_fit_to_valid_pipeline because max_number_of_fits <= 0")
            allow_fit_to_valid_pipeline = False

        self.population_size = population_size
        self.wall_time_optimization_seconds = wall_time_optimization_seconds
        self.min_pipeline_size = min_pipeline_size
        self.max_pipeline_size = max_pipeline_size

        self.allow_fit_to_valid_pipeline = allow_fit_to_valid_pipeline
        self.max_number_of_fits = max_number_of_fits
        self.budget_per_fit_to_valid_pipeline_seconds = budget_per_fit_to_valid_pipeline_seconds
        self.fit_pretest_X = fit_pretest_X
        self.fit_pretest_y = fit_pretest_y

        self.evaluation_strategy = evaluation_strategy
        self.evaluation_summarize_method = evaluation_summarize_method
        self.evaluation_test_size = evaluation_test_size
        self.evaluation_n_splits = evaluation_n_splits

        self._objectives = Objectives(problem_type=problem_type, objectives=objectives)
        self.enable_statistics = enable_statistics
        self.number_of_pipelines_failed_allowed = number_of_pipelines_failed_allowed
        self.budget_per_candidate_seconds = budget_per_candidate_seconds

        self.successive_halving = successive_halving

        self.number_of_jobs = number_of_jobs

        self._statistics = None
        if enable_statistics:
            self._statistics = Statistics()
            first_objective = next(iter(self.objectives._objectives))
            self._statistics.first_objective_maximize = not OBJECTIVE.is_minimizer(first_objective)

        self.optimization_thread = None

        self.global_stop_optimization_event = None
        if self.wall_time_optimization_seconds is not None:
            self.global_stop_optimization_event = threading.Event()

        self.initialization = RandomInitialization(min_size=min_pipeline_size, max_size=max_pipeline_size)

        if ea_space_file is None:
            ea_space_file = resource_filename(__name__, 'ea_space.yml')

        ea_space = EASpace(ea_space_file)
        ea_space.set_parameters(ea_parameters)

        if (OPERATOR_DISTRIBUTION_STRATEGY.TIME_BASED_DISTRIBUTION in (ea_space.mutation_strategy,
                                                                       ea_space.crossover_strategy) and wall_time_optimization_seconds is None):
            raise Exception('Please specify wall_time_optimization_seconds parameters '
                            'if you plan to base your variation strategy on the time')

        if generations == -1:
            maxsize = sys.maxsize
            logging.debug('The number of generations set to %d', maxsize)
            generations = maxsize

        self.ea = CustomizableEA(generations=generations,
                                 force_revaluation=self.successive_halving is not None,
                                 wall_time=wall_time_optimization_seconds,
                                 statistics=self._statistics,
                                 random_state=random_state)
        self.ea.load_from_ea_space(ea_space=ea_space)

        if ea_space.initialization_instance() is not None:
            initialization_class, initialization_parameters = ea_space.initialization_instance()

            if initialization_class.strategy() == INITIALIZATION_STRATEGY.LHS:
                initialization_parameters.update({'population_size': self.population_size,
                                                  'search_space': self.search_space,
                                                  'random_state': random_state})

            self.initialization = initialization_class(**initialization_parameters)

        # FIXME find a more elegant way
        if ea_space.ea_algorithm == EA_ALGORITHM.MU_PLUS_LAMBDA_PLUS_KAPPA:
            self.ea.algorithm_parameters._ea_meta_data = self.ea.ea_meta_data
            self.ea.algorithm_parameters._generate_method = partial(MLPipeline.generate_pipeline,
                                                                    search_space=self.search_space,
                                                                    initialization=self.initialization,
                                                                    random_state=random_state,
                                                                    enable_statistics=enable_statistics)

        self.ea.register(EA_METHODS.TERMINATION, lambda current_iteration: current_iteration >= generations)

    @property
    def statistics(self):
        if self._statistics is None:
            logging.warning('There are no statistics, you need to enable them if you need it')
        return self._statistics

    @property
    def objectives(self):
        return self._objectives

    def stop_optimization(self):
        if self.global_stop_optimization_event is not None:
            self.global_stop_optimization_event.set()
        if self.optimization_thread:
            self.optimization_thread.join()

    def optimize(self, X_train, y_train, random_state=None):
        random_state = random_state_parser(random_state)

        optimized_pipelines = generate_population(search_space=self.search_space,
                                                  number_of_individuals=self.population_size,
                                                  initialization=self.initialization,
                                                  allow_fit_to_valid_pipeline=self.allow_fit_to_valid_pipeline,
                                                  max_number_of_fits=self.max_number_of_fits,
                                                  budget_per_fit_to_valid_pipeline_seconds=self.budget_per_fit_to_valid_pipeline_seconds,
                                                  fit_pretest_X=self.fit_pretest_X,
                                                  fit_pretest_y=self.fit_pretest_y,
                                                  random_state=random_state,
                                                  enable_statistics=self.enable_statistics,
                                                  fitness_weights=self.objectives.ordered_weights)

        evaluator = Evaluation(X_train=X_train, y_train=y_train,
                               evaluation_strategy=self.evaluation_strategy,
                               summarize_method=self.evaluation_summarize_method,
                               test_size=self.evaluation_test_size,
                               n_splits=self.evaluation_n_splits,
                               objectives=self.objectives,
                               time_limit_seconds=self.budget_per_candidate_seconds,
                               number_of_jobs=self.number_of_jobs,
                               number_of_failed_allowed=self.number_of_pipelines_failed_allowed,
                               successive_halving=self.successive_halving,
                               statistics=self._statistics,
                               random_state=random_state,
                               global_stop_optimization_event=self.global_stop_optimization_event)

        self.ea.register(EA_METHODS.EVALUATION, evaluator.evaluate)

        k = self.population_size

        if self.successive_halving is not None:
            k = self.successive_halving.population

        try:
            if self.wall_time_optimization_seconds is not None:
                self.optimization_thread = threading.Thread(target=self.ea.optimize, kwargs={'population': optimized_pipelines, 'k': k, 'global_stop_optimization_event': self.global_stop_optimization_event})
                self.optimization_thread.deamon = False
                self.optimization_thread.start()
                start_time = time.time()
                while True:
                    if (time.time() - start_time) > self.wall_time_optimization_seconds:
                        logging.warning('Wall time has been reached, the optimization process will end')
                        break
                    if self.global_stop_optimization_event.is_set():  # optimization is done (in term of generations/iterations)
                        break
                    if not self.optimization_thread.is_alive():
                        raise Exception("Thread found dead")
                    time.sleep(.1)  # avoid to keep the cpu
                self.stop_optimization()
            else:
                self.ea.optimize(population=optimized_pipelines, k=k)
        except Exception as e:
            logging.error("Trouble during optimization phase -> %s", e)
            raise e

        return optimized_pipelines

    @staticmethod
    def best(pipelines):
        completed_pipelines = list(filter(lambda obj: obj.fitness.weighted_values is not None, pipelines))
        completed_pipelines.sort(key=lambda p: p.fitness.weighted_values, reverse=True)
        return completed_pipelines[0]
