# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from collections import defaultdict

import pandas as pd

from .evolutionary_algorithms import statistics as eaStats


class Statistics(eaStats.Statistics):
    def __init__(self):
        super().__init__()
        self.failing_pipelines_per_generation = defaultdict(list)
        self.test_fitness_best_individual = {}

    def subtract_elapsed_time(self, elapsed_time, generation):
        self.elapsed_times[generation].append(-elapsed_time)

    def append_failing_pipeline(self, pipeline, generation):
        self.failing_pipelines_per_generation[generation].append(str(pipeline))

    def set_test_fitness_best_individual(self, generation, test_fitness):
        self.test_fitness_best_individual[generation] = test_fitness

    def _output_dataframe(self):
        dataframe = super()._output_dataframe()
        dataframe['failing_pipelines'] = self._extract_failing_pipelines()
        dataframe['test_fitness_best_individual'] = pd.Series(self.test_fitness_best_individual)
        return dataframe

    def _extract_failing_pipelines(self):
        failing_pipelines = []

        for current_generation in range(self.number_of_generations):
            failing_pipelines.append(self.failing_pipelines_per_generation[current_generation])

        return failing_pipelines
