# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from enum import Enum
import warnings
import importlib
import uuid
import logging
import hashlib
from copy import copy

from stopit import ThreadingTimeout, TimeoutException

from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import FunctionTransformer

from .evolutionary_algorithms.individual import SimpleIndividual, MetaData
from .evolutionary_algorithms.utils import random_state_parser

from .search_space import SearchSpace, SEARCH_SPACE_KEY, append_prefix_to_variables_in_clause, evaluate_clause, generate_hyperparameter_value
from .utils import keys_exists

from .initialization import INITIALIZATION_STRATEGY, RandomInitialization
from .preprocessing.stacking_estimator import StackingEstimator


class InvalidPipeline(Exception):
    pass


class InvalidHyperparameters(InvalidPipeline):
    pass


class MLPipeline(SimpleIndividual):
    LIMIT_NUMBER_OF_TRY_GENERATE = 10

    def __init__(self, search_space=None,
                 generate=True, initialization=None,
                 allow_fit_to_valid_pipeline=False, budget_per_fit_to_valid_pipeline_seconds=2, max_number_of_fits=10,
                 fit_pretest_X=None, fit_pretest_y=None,
                 random_state=None, enable_statistics=False, fitness_weights=None):
        super().__init__(enable_statistics=enable_statistics, fitness_weights=fitness_weights)

        if not search_space:
            search_space = SearchSpace()

        self.search_space = search_space
        self.random_state = random_state_parser(random_state)

        self.meta_data = MLPipelineMetaData()

        self.allow_fit_to_valid_pipeline = allow_fit_to_valid_pipeline
        self.max_number_of_fits = max_number_of_fits
        self.budget_per_fit_to_valid_pipeline_seconds = budget_per_fit_to_valid_pipeline_seconds

        self.fit_pretest_X = fit_pretest_X
        self.fit_pretest_y = fit_pretest_y

        if generate:
            self._generate_pipeline(initialization=initialization)

    def _generate_pipeline(self, initialization=None):  # pylint: disable=too-many-branches
        if initialization is None:
            initialization = RandomInitialization(min_size=3, max_size=3)

        if initialization.strategy() not in [INITIALIZATION_STRATEGY.RANDOM, INITIALIZATION_STRATEGY.LHS]:
            raise Exception(f"Impossible to generate pipeline, "
                            f"initialization strategy not handled: {str(initialization.strategy())}")

        if initialization.strategy() == INITIALIZATION_STRATEGY.RANDOM:
            pipeline_size = self.random_state.randint(low=initialization.min_size, high=initialization.max_size + 1)
        elif initialization.strategy() == INITIALIZATION_STRATEGY.LHS:
            pipeline_size = initialization.pop_pipeline_size()

        iteration = 0

        valid_pipeline = False

        while not valid_pipeline and iteration < MLPipeline.LIMIT_NUMBER_OF_TRY_GENERATE:
            logging.debug('Iteration %d to generate a pipeline', iteration)
            iteration += 1

            component = MLPipelineComponent.generate_component(search_space=self.search_space,
                                                               parent_components=None,
                                                               random_state=self.random_state,
                                                               selected_estimator=initialization.get_estimator(),
                                                               use_default_hyperparameters=initialization.default_hyperparameters)

            if keys_exists(component.search_space_component_node, SEARCH_SPACE_KEY.REQUIREMENT.value,
                           SEARCH_SPACE_KEY.REQUIREMENT_PREVIOUS.value) and pipeline_size == 1:
                message = (f'The generated candidate {component} has requirements '
                           f'that cannot be respected due to the pipeline size {pipeline_size} which is too small')
                logging.warning(message)
                self.clear()
                continue

            self.append(component)

            for component_index in range(1, pipeline_size):
                component = MLPipelineComponent.generate_component(search_space=self.search_space,
                                                                   parent_components=self.components,  # FIXME ideally change no by components only (allow to have more informations on candidate)
                                                                   random_state=self.random_state,
                                                                   selected_preprocessor=initialization.get_preprocessor(component_index=component_index),
                                                                   use_default_hyperparameters=initialization.default_hyperparameters)

                if component is None:
                    break

                self.append(component)

            if MLPipeline.is_valid_pipeline(self):
                valid_pipeline = True
                break

            # valid or not, LHS strategy has been designed with a specific order for each call.
            if initialization.strategy() == INITIALIZATION_STRATEGY.LHS:
                break

            # remove components if it is not the last try
            if iteration < MLPipeline.LIMIT_NUMBER_OF_TRY_GENERATE:
                self.clear()

        if not valid_pipeline:
            message = f'Generated pipeline is invalid : {str(self)}'
            logging.error(message)
            # NOTE it is not because a pipeline is not able to fit a sample, that it is not able to fit the real dataset.
            # For this reason, we authorize invalid generated pipeline if the option has been set
            if not self.allow_fit_to_valid_pipeline:
                raise InvalidPipeline(message)
        else:
            logging.debug("Candidate generated: %s (%s)", self, self.hash())

    @property
    def components(self):
        """
        In EA we use the term attribute to describe of what is composed an individual,
        but in ML it's better to use the term component.
        The pipeline (i.e., candidate) is composed of components
        (i.e., preprocessing methods and estimator)
        """
        return self.attributes

    def __str__(self):
        return str([str(component) for component in self.components])

    def compile(self):
        # TODO use by __repr__ ?
        return MLPipeline.compile_pipeline(self, self.search_space)

    def hash(self):
        if not self._inner_list:
            return None

        m = hashlib.sha1()
        for component in self.components:
            m.update(bytes(component.hash(), "utf-8"))

        return m.hexdigest()

    @staticmethod
    def generate_pipeline(search_space=None, initialization=None, random_state=None, enable_statistics=False):
        return MLPipeline(generate=True, search_space=search_space, initialization=initialization, random_state=random_state, enable_statistics=enable_statistics)

    @staticmethod
    def compile_component(component, search_space):
        component_name = component.name
        backend = search_space.algorithms[component_name]['backend']

        compiled_subcomponents = {}

        for subcomponent_name, subcomponent in component.subcomponents.items():
            subcomponent_backend = backend

            if 'backend' in subcomponent.search_space_component_node:
                subcomponent_backend = subcomponent.search_space_component_node['backend']

            class_identifier = subcomponent.search_space_component_node['class_identifier']
            if '.' in class_identifier:
                classifier_group = ".".join(class_identifier.split('.')[:-1])
                module_name = f"{subcomponent_backend}.{classifier_group}"
                module = importlib.import_module(module_name)
                class_name = class_identifier.split(".")[-1]
            else:
                module = importlib.import_module(subcomponent_backend)
                class_name = class_identifier

            if subcomponent.type == MLPipelineComponentType.SUBCOMPONENT_ESTIMATOR:
                compiled_subcomponents[subcomponent_name] = getattr(module, class_name)(**subcomponent.hyperparameters)
            else:
                compiled_subcomponents[subcomponent_name] = getattr(module, class_name)

        class_identifier = search_space.algorithms[component_name]['class_identifier']

        if backend == 'xgboost':
            module = importlib.import_module(backend)
            class_name = class_identifier
        else:
            classifier_group = ".".join(class_identifier.split('.')[:-1])
            module_name = f"{backend}.{classifier_group}"
            module = importlib.import_module(module_name)
            class_name = class_identifier.split(".")[-1]

        compiled_component = getattr(module, class_name)

        return compiled_component, compiled_subcomponents

    @staticmethod
    def _compile_pipeline_compile_component(component, search_space):
        name = f"{component.name}_{uuid.uuid1()}"
        compiled_component, compiled_subcomponents = MLPipeline.compile_component(component, search_space)
        kwargs = {}
        kwargs.update(component.hyperparameters)
        kwargs.update(compiled_subcomponents)

        estimator = compiled_component(**kwargs)

        return name, estimator

    @staticmethod
    def _compile_pipeline_two_copy_union():
        name = f"FeatureUnion_{uuid.uuid1()}".format(uuid.uuid1())
        union = FeatureUnion(transformer_list=[
            (f"FunctionTransformer_copy_{uuid.uuid1()}", FunctionTransformer(copy)),
            (f"FunctionTransformer_copy_{uuid.uuid1()}", FunctionTransformer(copy))])
        return name, union

    @staticmethod
    def _compile_pipeline_process_component(component, partial_pipeline, search_space):
        if component.name == 'CombineDFs':
            return MLPipeline._compile_pipeline_process_combine_dfs(partial_pipeline, search_space)

        if component.name == 'CombineTwoPreviousesDFs':
            return MLPipeline._compile_pipeline_process_two_combine_dfs(partial_pipeline, search_space)

        name, estimator = MLPipeline._compile_pipeline_compile_component(component, search_space)

        if component.name in search_space.list_of_estimators:
            name = f"StackingEstimator_{name}"
            estimator = StackingEstimator(estimator=estimator)

        return name, estimator

    @staticmethod
    def _compile_pipeline_process_combine_dfs(partial_pipeline, search_space):
        # case where CombineDFs has nothing previously
        if len(partial_pipeline) == 0:
            return MLPipeline._compile_pipeline_two_copy_union()

        next_component = partial_pipeline.pop()
        name, estimator = MLPipeline._compile_pipeline_process_component(next_component, partial_pipeline, search_space)

        union = FeatureUnion(transformer_list=[
            (f"FunctionTransformer_copy_{uuid.uuid1()}", FunctionTransformer(copy)),
            (name, estimator)])

        return f"FeatureUnion_{uuid.uuid1()}", union

    @staticmethod
    def _compile_pipeline_process_two_combine_dfs(partial_pipeline, search_space):
        if len(partial_pipeline) == 0:
            return MLPipeline._compile_pipeline_two_copy_union()

        if len(partial_pipeline) == 1:
            next_component = partial_pipeline.pop()
            name, estimator = MLPipeline._compile_pipeline_process_component(next_component, partial_pipeline, search_space)

            union = FeatureUnion(transformer_list=[
                (f"FunctionTransformer_copy_{uuid.uuid1()}", FunctionTransformer(copy)),
                (name, estimator)])

            return f"FeatureUnion_{uuid.uuid1()}", union

        next_component_1 = partial_pipeline.pop()
        next_component_2 = partial_pipeline.pop()

        next_component_1_name, next_estimator_1 = MLPipeline._compile_pipeline_process_component(next_component_1, partial_pipeline, search_space)
        next_component_2_name, next_estimator_2 = MLPipeline._compile_pipeline_process_component(next_component_2, partial_pipeline, search_space)

        union = FeatureUnion(transformer_list=[
            (next_component_1_name, next_estimator_1),
            (next_component_2_name, next_estimator_2)])

        return f"FeatureUnion_{uuid.uuid1()}", union

    @staticmethod
    def compile_pipeline(pipeline, search_space):
        steps = []

        logging.debug('Compile pipeline (hash: %s - id: %s)', pipeline.hash(), pipeline.identifier)

        # NOTE final estimator present at the beginning when created in this framework
        pipeline_reversed = pipeline.components[::-1]  # TODO change by pipeline.reverse()

        # add estimator
        name, estimator = MLPipeline._compile_pipeline_compile_component(pipeline_reversed.pop(), search_space)
        steps.append((name, estimator))

        # add preprocessors
        while len(pipeline_reversed) > 0:
            name, estimator = MLPipeline._compile_pipeline_process_component(pipeline_reversed.pop(), pipeline_reversed, search_space)
            steps.append((name, estimator))

        steps.reverse()
        return Pipeline(steps=steps, verbose=False)

    @staticmethod
    def _is_valid_pipeline_forbiddens(pipeline, index, component):
        # do not check if it is the tail component
        if (index + 1) >= len(pipeline):
            return True

        # check previouses component are respected
        if SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUSES.value in component.forbiddens:
            for pre_forbidden in component.forbiddens[SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUSES.value]:
                for i in range(index + 1, len(pipeline)):
                    if pre_forbidden == pipeline[i].name:
                        return False

        # check previous component is respected
        if SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUS.value in component.forbiddens:
            for pre_forbidden in component.forbiddens[SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUS.value]:
                if pre_forbidden == pipeline[index + 1].name:
                    return False

        return True

    @staticmethod
    def _is_valid_pipeline_requirements(pipeline, index, component):
        # if it is the last component with a requirement, it means that this component has invalid state
        if (index + 1) >= len(pipeline):
            return False

        # check previous component is present
        if SEARCH_SPACE_KEY.REQUIREMENT_PREVIOUS.value in component.requirements:
            at_least_one_requirement_present = False
            for pre_requirement in component.requirements[SEARCH_SPACE_KEY.REQUIREMENT_PREVIOUS.value]:
                if pre_requirement == pipeline[index + 1].name:
                    at_least_one_requirement_present = True
                    break
            if not at_least_one_requirement_present:
                return False

        return True

    @staticmethod
    def _is_valid_pipeline_fit_pipeline(pipeline):
        bad_pipeline = True
        count_fit = 0

        while bad_pipeline and count_fit < pipeline.max_number_of_fits:
            logging.debug('is_valid_pipeline(): try %d to fit the pipeline %s', count_fit, pipeline)
            try:
                compiled_pipeline = pipeline.compile()
                # FIXME should be swallow_exc=False, however to respect the bias with TPOT it is defined as True
                with ThreadingTimeout(pipeline.budget_per_fit_to_valid_pipeline_seconds, swallow_exc=True), warnings.catch_warnings():
                    warnings.filterwarnings("ignore")
                    compiled_pipeline.fit(pipeline.fit_pretest_X, pipeline.fit_pretest_y)
                bad_pipeline = False
            except TimeoutException as e:
                logging.debug('is_valid_pipeline(): timeout, pipeline is discarded, certainly complex, even on very small sample; message: %s', e)
            except BaseException as e:
                logging.debug('is_valid_pipeline(): not able to fit the pipeline; message: %s', e)
            finally:
                count_fit += 1

        if bad_pipeline:
            return False

        return True

    @staticmethod
    def is_valid_pipeline(pipeline):
        for index, component in enumerate(pipeline.components):
            if component is None or not MLPipelineComponent.is_valid_component(component):
                return False

            if component.forbiddens and not MLPipeline._is_valid_pipeline_forbiddens(pipeline, index, component):
                return False

            if component.requirements and not MLPipeline._is_valid_pipeline_requirements(pipeline, index, component):
                return False

        if pipeline.allow_fit_to_valid_pipeline and not MLPipeline._is_valid_pipeline_fit_pipeline(pipeline):  # NOTE inspired from TPOT
            return False

        return True

    def complexity(self):
        count = 0
        for component in self.components:
            if component.name in ('CombineDFs', 'CombineTwoPreviousesDFs'):
                continue
            count += 1
        return count


class MLPipelineComponentType(Enum):
    PREPROCESSOR = "preprocessors"
    ESTIMATOR = "estimators"
    SUBCOMPONENT_CALLABLE = "subcomponent_callable"
    SUBCOMPONENT_ESTIMATOR = "subcomponent_estimator"


class MLPipelineComponent():
    LIMIT_NUMBER_OF_TRY = 10

    def __init__(self, name: str,
                 component_type: MLPipelineComponentType,
                 hyperparameters: dict,
                 search_space_component_node: dict,
                 subcomponents: dict):
        self.name = name
        self.type = component_type
        self.hyperparameters = hyperparameters
        self.search_space_component_node = search_space_component_node
        self.subcomponents = subcomponents

    @property
    def forbiddens(self):
        if SEARCH_SPACE_KEY.FORBIDDEN.value not in self.search_space_component_node:
            return None
        return self.search_space_component_node[SEARCH_SPACE_KEY.FORBIDDEN.value]

    @property
    def requirements(self):
        if SEARCH_SPACE_KEY.REQUIREMENT.value not in self.search_space_component_node:
            return None
        return self.search_space_component_node[SEARCH_SPACE_KEY.REQUIREMENT.value]

    def hash(self):
        return hashlib.sha1(bytes(str(self), "utf-8")).hexdigest()

    def __str__(self):
        kwargs = {}
        kwargs.update(self.hyperparameters)
        for subcomponent_name, subcomponent in self.subcomponents.items():
            kwargs.update({f'subcomponent:{subcomponent.type.value}:{subcomponent_name}': subcomponent.hyperparameters})
        return f'{self.name}({kwargs})'

    @staticmethod
    def generate_component(search_space, parent_components, random_state, selected_estimator=None, selected_preprocessor=None, use_default_hyperparameters=False):
        if parent_components is None:
            if not selected_estimator:
                selected_estimator = random_state.choice(search_space.list_of_estimators)
            selected_estimator_node = search_space.estimators[selected_estimator]
            hyperparameters = MLPipelineComponent.generate_hyperparameters(selected_estimator_node, use_default=use_default_hyperparameters, random_state=random_state)
            subcomponents = MLPipelineComponent.generate_subcomponents(selected_estimator_node, random_state=random_state)
            component = MLPipelineComponent(name=selected_estimator,
                                            component_type=MLPipelineComponentType.ESTIMATOR,
                                            hyperparameters=hyperparameters,
                                            search_space_component_node=selected_estimator_node,
                                            subcomponents=subcomponents)

            return component

        if not selected_preprocessor:
            selected_preprocessor = random_state.choice(search_space.list_of_preprocessors)
        previous_component_node = parent_components[-1].search_space_component_node

        if keys_exists(previous_component_node, SEARCH_SPACE_KEY.REQUIREMENT.value, SEARCH_SPACE_KEY.REQUIREMENT_PREVIOUS.value):
            selected_preprocessor = MLPipelineComponent._select_preprocessor_with_requirement_previous_clause(search_space.list_of_preprocessors, previous_component_node, random_state=random_state)

            if selected_preprocessor is None:
                logging.warning("Impossible to select preprocessor in such a way that it respect requirements for the component %s", previous_component_node)
                return None

        if keys_exists(previous_component_node, SEARCH_SPACE_KEY.FORBIDDEN.value, SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUS.value) or MLPipelineComponent.previouses_forbidden_exist_in_parents(parent_components):
            selected_preprocessor = MLPipelineComponent._select_preprocessor_with_forbiden_previous_clauses(search_space, search_space.list_of_preprocessors, parent_components, random_state=random_state)

            if selected_preprocessor is None:
                logging.warning("Impossible to select preprocessor in such a way that it respect the forbiddens previous(es) for the parent components %s", str([str(c) for c in parent_components]))
                return None

        selected_preprocessor_node = search_space.preprocessors[selected_preprocessor]
        hyperparameters = MLPipelineComponent.generate_hyperparameters(selected_preprocessor_node, use_default=use_default_hyperparameters, random_state=random_state)
        subcomponents = MLPipelineComponent.generate_subcomponents(selected_preprocessor_node, random_state=random_state)
        component = MLPipelineComponent(name=selected_preprocessor,
                                        component_type=MLPipelineComponentType.PREPROCESSOR,
                                        hyperparameters=hyperparameters,
                                        search_space_component_node=selected_preprocessor_node,
                                        subcomponents=subcomponents)

        return component

    @staticmethod
    def generate_subcomponents(search_space_node, random_state):
        subcomponents = {}

        if 'hyperparameters' not in search_space_node:
            return subcomponents

        for name, node in search_space_node['hyperparameters'].items():
            hyperparameters = {}

            if node['type'] != 'estimator' and node['type'] != 'callable':
                continue

            component_type = MLPipelineComponentType.SUBCOMPONENT_CALLABLE

            if node['type'] == 'estimator':
                hyperparameters = MLPipelineComponent.generate_hyperparameters(node, random_state=random_state)  # FIXME use_default_hyperparameters
                component_type = MLPipelineComponentType.SUBCOMPONENT_ESTIMATOR

            subcomponents[name] = MLPipelineComponent(name=name,
                                                      component_type=component_type,
                                                      hyperparameters=hyperparameters,
                                                      search_space_component_node=node,
                                                      subcomponents={})
        return subcomponents

    @staticmethod
    def previouses_forbidden_exist_in_parents(parent_components):
        for component in parent_components:
            if keys_exists(component.search_space_component_node, SEARCH_SPACE_KEY.FORBIDDEN.value, SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUSES.value):
                return True
        return False

    @staticmethod
    def _select_preprocessor_with_requirement_previous_clause(preprocessors: list, previous_component_node, random_state):
        number_of_try = 0
        preprocessor_stack = preprocessors.copy()

        while number_of_try < MLPipelineComponent.LIMIT_NUMBER_OF_TRY:
            if len(preprocessor_stack) == 0:
                return None

            selected_preprocessor = random_state.choice(preprocessor_stack)
            preprocessor_stack.remove(selected_preprocessor)

            if selected_preprocessor in previous_component_node[SEARCH_SPACE_KEY.REQUIREMENT.value][SEARCH_SPACE_KEY.REQUIREMENT_PREVIOUS.value]:
                break

            number_of_try += 1

        if number_of_try >= MLPipelineComponent.LIMIT_NUMBER_OF_TRY:
            logging.warning("The number of try to find a valid previous component %s with the requirement clause pre has reach the limit", previous_component_node)

        return selected_preprocessor

    @staticmethod
    def _select_preprocessor_with_forbiden_previous_clauses(search_space: SearchSpace, preprocessors: list, parent_components, random_state):
        number_of_try = 0
        preprocessor_stack = preprocessors.copy()

        all_forbidden_previouses_components = set()

        for component in parent_components:
            if keys_exists(component.search_space_component_node, SEARCH_SPACE_KEY.FORBIDDEN.value, SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUSES.value):
                forbidden_previouses = component.search_space_component_node[SEARCH_SPACE_KEY.FORBIDDEN.value][SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUSES.value]
                for forbidden in forbidden_previouses:
                    all_forbidden_previouses_components.add(forbidden)

        if keys_exists(parent_components[-1].search_space_component_node, SEARCH_SPACE_KEY.FORBIDDEN.value, SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUS.value):
            forbiddens_previous = parent_components[-1].search_space_component_node[SEARCH_SPACE_KEY.FORBIDDEN.value][SEARCH_SPACE_KEY.FORBIDDEN_PREVIOUS.value]
            for forbidden in forbiddens_previous:
                all_forbidden_previouses_components.add(forbidden)

        all_forbidden_previouses_components = search_space.convert_groups_to_algorithms(all_forbidden_previouses_components)

        while number_of_try < MLPipelineComponent.LIMIT_NUMBER_OF_TRY:
            if len(preprocessor_stack) == 0:  # Impossible to find a compatible preprocessor with the parent_components
                return None

            selected_preprocessor = random_state.choice(preprocessor_stack)
            preprocessor_stack.remove(selected_preprocessor)

            if selected_preprocessor not in all_forbidden_previouses_components:
                break

            number_of_try += 1

        if number_of_try >= MLPipelineComponent.LIMIT_NUMBER_OF_TRY:
            logging.warning("The number of try to find a valid component with all previouses forbidden %s has reach the limit", all_forbidden_previouses_components)

        return selected_preprocessor

    @staticmethod
    def generate_hyperparameters(search_space_node, use_default=False, random_state=None):
        hyperparameters = {}

        if 'hyperparameters' not in search_space_node:
            return hyperparameters

        random_state = random_state_parser(random_state)

        number_of_try = 0
        limit_number_of_try = MLPipelineComponent.LIMIT_NUMBER_OF_TRY

        if keys_exists(search_space_node, 'limits', 'number_of_try_hyperparameters'):
            limit_number_of_try = search_space_node['limits']['number_of_try_hyperparameters']

        # NOTE repeat until found valid hyperparameters
        while number_of_try <= limit_number_of_try:

            for hyperparameter_name, hyperparameter_attributes in search_space_node['hyperparameters'].items():

                generated_value = generate_hyperparameter_value(hyperparameter_node=hyperparameter_attributes,
                                                                use_default=use_default, random_state=random_state)

                if generated_value is None:
                    continue

                hyperparameters[hyperparameter_name] = generated_value

                # FIXME handle continue values (upper/lower bound) based on a given law

                # TODO find a smart way to remove hyperparameters/values when a forbidden clause is raise

            if not MLPipelineComponent._forbidden_hyperparameters_exists(search_space_node=search_space_node,
                                                                         hyperparameters=hyperparameters):
                break

            if number_of_try >= limit_number_of_try:
                message = (f"Impossible to generate a valid combination of hyperparameters "
                           f"in search space {search_space_node} after {number_of_try} tentatives")
                logging.error(message)
                raise InvalidHyperparameters(message)

            number_of_try += 1

        return hyperparameters

    @staticmethod
    def _forbidden_hyperparameters_exists(search_space_node, hyperparameters):  # pylint: disable=unused-argument # NOTE hyperparameter used by eval # TODO move this outside of the class ?
        if keys_exists(search_space_node, SEARCH_SPACE_KEY.FORBIDDEN.value, 'hyperparameters'):
            for clause in search_space_node[SEARCH_SPACE_KEY.FORBIDDEN.value]['hyperparameters']:
                forbidden_clause = append_prefix_to_variables_in_clause(prefix="kwargs['hyperparameters']", clause=clause)  # TODO find a better solution

                if evaluate_clause(clause=forbidden_clause, hyperparameters=hyperparameters):
                    return True

        return False

    @staticmethod
    def is_valid_component(component):
        if MLPipelineComponent._forbidden_hyperparameters_exists(search_space_node=component.search_space_component_node, hyperparameters=component.hyperparameters):
            return False
        # TODO check also requirements
        return True


class MLPipelineMetaData(MetaData):
    def __init__(self):
        super().__init__()
        self._evaluation_values = {}

    @property
    def evaluation_values(self):
        return self._evaluation_values

    @evaluation_values.deleter
    def evaluation_values(self):
        self._evaluation_values = {}


def compile_population(population):
    pipelines = []

    for pipeline in population:
        pipelines.append(pipeline.compile())

    return pipelines


def generate_population(search_space=None,
                        number_of_individuals=100,
                        initialization=None,
                        allow_fit_to_valid_pipeline=False,
                        max_number_of_fits=10,
                        budget_per_fit_to_valid_pipeline_seconds=2,
                        fit_pretest_X=None,
                        fit_pretest_y=None,
                        random_state=None,
                        enable_statistics=False,
                        fitness_weights=None):
    population = []

    for _ in range(number_of_individuals):
        pipeline = MLPipeline(search_space=search_space,
                              initialization=initialization,
                              allow_fit_to_valid_pipeline=allow_fit_to_valid_pipeline,
                              max_number_of_fits=max_number_of_fits,
                              budget_per_fit_to_valid_pipeline_seconds=budget_per_fit_to_valid_pipeline_seconds,
                              fit_pretest_X=fit_pretest_X,
                              fit_pretest_y=fit_pretest_y,
                              random_state=random_state,
                              enable_statistics=enable_statistics,
                              fitness_weights=fitness_weights)
        population.append(pipeline)

    return population
