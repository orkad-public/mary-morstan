# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

import logging
from enum import Enum
from collections import OrderedDict

from sklearn import metrics

# from sklearn.metrics import SCORERS, make_scorer
from sklearn.metrics import make_scorer, get_scorer_names, get_scorer
# from sklearn.metrics import _scorer as sco

from .problems import PROBLEM_TYPE
from .metrics.tpot_balanced_accuracy import balanced_accuracy as tpot_balanced_accuracy


# NOTE https://stackoverflow.com/questions/43968006/support-for-enum-arguments-in-argparse
class OBJECTIVE(Enum):
    ACCURACY = "accuracy_score"
    BALANCED_ACCURACY = "balanced_accuracy_score"
    TPOT_BALANCED_ACCURACY = "tpot_balanced_accuracy"

    # binary
    ROC_AUC = "roc_auc_score"
    F1 = "f1_score"
    PRECISION = "precision"
    RECALL = "recall"

    # multiclass
    PRECISION_WEIGHTED = "precision_weighted"
    PRECISION_MACRO = "precision_macro"
    PRECISION_MICRO = "precision_micro"

    RECALL_WEIGHTED = "recall_weighted"
    RECALL_MACRO = "recall_macro"
    RECALL_MICRO = "recall_micro"

    F1_WEIGHTED = "f1_weighted"
    F1_MACRO = "f1_macro"
    F1_MICRO = "f1_micro"

    ROC_AUC_OVR = "roc_auc_ovr"
    ROC_AUC_OVO = "roc_auc_ovo"

    # regression
    NEGATIVE_ROOT_MEAN_SQUARE = "neg_root_mean_squared_error"
    RMSE = 'root_mean_squared_error'
    MAE = "mean_absolute_error"
    R2 = "r2"
    # custom objectives (please add in is_custom_objective(...))
    MIN_PIPELINE_SIZE = 'min_pipeline_size'
    MIN_TRAINING_TIME = 'min_training_time'

    def __str__(self):
        return self.name.lower()

    @property
    def name(self):  # pylint: disable=function-redefined,invalid-overridden-method
        return super().name.lower()

    @staticmethod
    def argparse(s):
        try:
            return OBJECTIVE[s.upper()]
        except KeyError:
            return s

    @staticmethod
    def is_custom_objective(objective):  # NOTE permit to distinguish from scorer in ML
        return objective in [OBJECTIVE.MIN_PIPELINE_SIZE, OBJECTIVE.MIN_TRAINING_TIME]

    @staticmethod
    def is_minimizer(objective):
        if objective == OBJECTIVE.RMSE:
            return True
        if objective == OBJECTIVE.MAE:
            return True
        if objective == OBJECTIVE.R2:
            return True
        return len(objective.name) > 4 and objective.name[:4] == 'min_'


class InvalidObjectives(Exception):
    pass


class Objectives():
    def __init__(self, problem_type=PROBLEM_TYPE.SUPERVISED_CLASSIFICATION_BINARY, objectives=None):
        self._objectives = OrderedDict()

        if objectives:
            for objective in objectives:
                key, value = Objectives._deduce_callable_objective(objective)
                self._objectives[key] = value
        else:
            logging.debug("objectives has not been specified, use the default ones")
            self._set_default_objectives(problem_type)

        self._indexes = list(self._objectives).index

        logging.debug("objectives settled as follow %s for the problem type '%s'", list(self._objectives.keys()), problem_type.value)

    def index(self, objective):
        return self._indexes(objective)

    @staticmethod
    def _deduce_callable_objective(objective: OBJECTIVE):
        if callable(objective.value):
            logging.debug("objective %s is callable", objective)
            return objective, objective.value

        try:
            if callable(getattr(metrics, objective.value)):
                logging.debug("objective %s (%s) is part of known function in sklearn.metrics", objective, objective.value)
                return objective, metrics.make_scorer(getattr(metrics, objective.value))
        except AttributeError:
            pass

        if isinstance(objective.value, str):
            logging.debug("try to find objective in our mapping")
            if objective.value == 'tpot_balanced_accuracy':
                return objective, make_scorer(tpot_balanced_accuracy)

            if objective.value == 'root_mean_squared_error':
                return objective, make_scorer(metrics.mean_squared_error, squared=False)

            if OBJECTIVE.is_custom_objective(objective):
                return objective, objective.value

            if objective.value in get_scorer_names():
                logging.debug("found objective %s as part of sklearn scorers", objective)
                return objective, get_scorer(objective.value)

        message = f"impossible to deduce the objective {objective}"
        logging.error(message)
        raise InvalidObjectives(message)

    @property
    def min_pipeline_size_present(self):
        return OBJECTIVE.MIN_PIPELINE_SIZE in self._objectives

    @property
    def min_training_time_present(self):
        return OBJECTIVE.MIN_TRAINING_TIME in self._objectives

    def _set_default_objectives(self, problem_type):
        classification_problems = [PROBLEM_TYPE.SUPERVISED_CLASSIFICATION_BINARY,
                                   PROBLEM_TYPE.SUPERVISED_CLASSIFICATION_MULTICLASS,
                                   PROBLEM_TYPE.TIMESERIES_SUPERVISED_CLASSIFICATION,
                                   PROBLEM_TYPE.TIMESERIES_SUPERVISED_REGRESSION]
        if problem_type in classification_problems:
            key, value = Objectives._deduce_callable_objective(OBJECTIVE.ACCURACY)
            self._objectives[key] = value
        elif problem_type == PROBLEM_TYPE.SUPERVISED_REGRESSION:
            key, value = Objectives._deduce_callable_objective(OBJECTIVE.RMSE)
            self._objectives[key] = value
        else:
            message = f"we cannot define the default objectives for the type of problem: {problem_type}"
            logging.error(message)
            raise InvalidObjectives(message)

    @property
    def scorers(self):  # TODO ensure the value is <= 1 (normalized value) ?
        scorers = {objective.name: objective_value for objective, objective_value in self._objectives.items()
                   if not OBJECTIVE.is_custom_objective(objective)}
        return scorers

    @property
    def scorer_keys(self):
        return [objective for objective, _ in self._objectives.items() if not OBJECTIVE.is_custom_objective(objective)]

    def __len__(self):
        return len(self._objectives)

    def __str__(self):
        return str([objective.name for objective in self._objectives.keys()])

    @property
    def ordered_weights(self):
        return list(map(lambda objective: -1. if OBJECTIVE.is_minimizer(objective) else 1., self._objectives))

    @property
    def default_values(self):
        return tuple(map(lambda objective: float('inf') if OBJECTIVE.is_minimizer(objective) else -float('inf'), self._objectives))

    @property
    def scorer_default_values(self):
        return tuple(map(lambda scorer: float('inf') if OBJECTIVE.is_minimizer(scorer) else -float('inf'), self.scorer_keys))

    def ordered_values_for_objectives(self, objectives_with_values):
        ordered_objectives = {}

        for objective, value in objectives_with_values.items():
            index = self.index(objective)
            ordered_objectives[index] = value

        ordered_objectives = OrderedDict(sorted(ordered_objectives.items())).values()

        return tuple(ordered_objectives)
