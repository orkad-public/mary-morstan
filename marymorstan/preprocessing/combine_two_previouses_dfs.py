# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

class CombineTwoPreviousesDFs():
    """Combine two DataFrames."""

    @property
    def __name__(self):
        """Instance name is the same as the class name."""
        return self.__class__.__name__
