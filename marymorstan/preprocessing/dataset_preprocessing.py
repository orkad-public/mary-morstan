# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from abc import ABC, abstractmethod
import pandas as pd
import logging


class UnknownDataset(Exception):
    """
    the dataset_location does not correspond to an available dataset
    """
    def __init__(self, dataset_location):
        Exception.__init__(self, dataset_location)
        self.dataset_location = dataset_location

    def __str__(self):
        return f"UnknownDataset Error, the {self.dataset_location} is not an available dataset name/location"


class DatasetPreprocessing(ABC):
    def __init__(self, dataset_location,
                 encode_features_to_int=False,
                 encode_targets_to_int=False,
                 fill_nan_values=None,
                 drop_nan_values=False,
                 drop_columns_with_unique_value=False):
        """load a dataset according to the dataset_location (csv, url, ...),
            the dataset will be stored as a panda dataframe,
            do some preprocessing tasks according to given parameters"""
        try:
            self.dataset = self.load_dataset(dataset_location)
            if drop_columns_with_unique_value:
                self._drop_columns_with_unique_value()
            if fill_nan_values is not None and drop_nan_values is True:
                message = "You can either specify a value fill_nan_values or set drop_nan_values to True. Not both."
                logging.error(message)
                raise Exception(message)
            self.dataset.fillna(value=fill_nan_values, method='ffill', inplace=True)
            if drop_nan_values:
                self.dataset = self.dataset.dropna()
            self.column = self.dataset.columns[-1]  # assume that the last column is the prediction
            self.X = self.dataset.drop([self.column], axis=1)
            self.y = self.dataset[self.column]

            if encode_features_to_int:
                self.X = self._encode_datas_to_numeric(self.X)
            if encode_targets_to_int:
                self.y = self._encode_datas_to_numeric(self.y)
        except UnknownDataset as error:
            print(error)
            raise error

    @abstractmethod
    def load_dataset(self, dataset_location):
        """ specific to the dataset origin, retrieve and return the dataset as a panda Dataframe"""

    def get_dataset(self):
        return self.dataset

    def get_y(self):
        """provide prediction of the dataset"""
        return self.y

    def get_X(self):
        """provide features of the dataset"""
        return self.X

    def _encode_datas_to_numeric(self, data):
        """encode given datas to numeric, return the transformed values"""
        if isinstance(data, pd.Series):
            return pd.Series(pd.factorize(data)[0])
        if isinstance(data, pd.DataFrame):
            obj_columns = data.dtypes.pipe(lambda x: x[(x == 'object') | (x == 'category')]).index
            for variable in obj_columns:
                data[variable] = pd.factorize(data[variable])[0]
            return data
        raise Exception(f'Impossible to encode data to numeric, do not handle {type(data)} type')

    def _drop_columns_with_unique_value(self):
        """update dataset variable instance by dropping columns with unique values"""
        count_unique = self.dataset.nunique()
        unique_columns = count_unique[count_unique == 1].index

        for variable in unique_columns:
            self.dataset = self.dataset.drop(variable, axis=1)
