# -*- coding: utf-8 -*-
# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)
# flake8: noqa
# pylint: skip-file


from sklearn.base import BaseEstimator, TransformerMixin


class DummyPreprocessor(BaseEstimator, TransformerMixin):
    """This is a dummy preprocessor :
    In a search space, if this preprocessor is the only one on offer,
    then the resulting pipelines are made up of only the available estimators."""

    def __init__(self, categorical_features='auto', dtype=float, # updated: before np.float
                 sparse=True, minimum_fraction=None, threshold=10):
        self.categorical_features = categorical_features
        self.dtype = dtype
        self.sparse = sparse
        self.minimum_fraction = minimum_fraction
        self.threshold = threshold

    def fit(self, X, y=None):
        """Dummy function to fit in with the sklearn API.
        Parameters
        ----------
        X : array-like, shape=(n_samples, n_feature)
            Input array of type int.

        Returns
        -------
        self
        """
        return self

    def transform(self, X):
        """Dummy function to fit in with the sklearn API.

        Parameters
        ----------
        X : array-like or sparse matrix, shape=(n_samples, n_features)
            Dense array or sparse matrix.

        Returns
        -------
        X
        """
        return X
