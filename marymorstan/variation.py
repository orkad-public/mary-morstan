# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from typing import Union, Optional
import logging
import copy
from collections import defaultdict

from numpy import random

from .search_space import hyperparameter_value_violiate_forbidden_clauses, hyperparameter_value_violate_boundary, hyperparameter_lower_upper_bound, SEARCH_SPACE_KEY, get_hyperparameter_type, generate_hyperparameter_value
from .evolutionary_algorithms.mutation import Mutation
from .evolutionary_algorithms.crossover import Crossover
from .evolutionary_algorithms.utils import random_state_parser

from .pipeline import MLPipeline, MLPipelineComponent
from .utils import keys_exists, is_number, is_float, is_integer


class MutationNGaussian(Mutation):
    def __init__(self,
                 n_ratio: float = 1.,
                 mu: Optional[float] = None,
                 sigma: Optional[float] = None,
                 sigma_ratio: float = .1):
        self.n_ratio = n_ratio
        self.mu = mu
        self.sigma = sigma
        self.sigma_ratio = sigma_ratio

        if self.n_ratio == 0 or self.n_ratio > 1.:
            raise ValueError('n_ratio of MutationNGaussian should be between 0 and 1')

    def __call__(self,  # pylint: disable=(too-many-branches
                 individual,
                 random_state: Optional[Union[random.RandomState, int]] = None):

        random_state = random_state_parser(random_state)

        cache_statistics = []

        for component in individual.components:
            for hyperparameter_name, hyperparameter_value in component.hyperparameters.items():
                if not is_number(hyperparameter_value):
                    logging.debug("Cannot apply %s on component %s - parameter %s is not a number", self.__class__.__name__, component.name, hyperparameter_name)
                    continue

                hyperparameter_type = get_hyperparameter_type(component=component.search_space_component_node, hyperparameter=hyperparameter_name)

                if hyperparameter_type == 'static':
                    logging.debug("Cannot apply MutationNGaussian for hyperparameter '%s' with the type '%s'", hyperparameter_name, hyperparameter_type)
                    continue

                if random_state.random() >= self.n_ratio:
                    continue

                determined_mu = self.mu if self.mu else 0
                determined_sigma = self.sigma

                if not self.sigma:
                    lower_bound, upper_bound = hyperparameter_lower_upper_bound(component_node=component.search_space_component_node, hyperparameter_name=hyperparameter_name)
                    determined_sigma = self.sigma_ratio * (upper_bound - lower_bound)

                noise = random_state.normal(determined_mu, determined_sigma)  # type: ignore[arg-type]

                if not is_float(hyperparameter_value):
                    noise = round(noise)

                if individual.statistics:
                    cache_statistics.append({'component_name': component.name,
                                             'mu': determined_mu,
                                             'sigma': determined_sigma})

                new_value = component.hyperparameters[hyperparameter_name] + noise

                if hyperparameter_value_violiate_forbidden_clauses(hyperparameter_name=hyperparameter_name, hyperparameter_value=new_value, forbiddens=component.forbiddens, **component.hyperparameters):
                    logging.warning("Change the hyperparameter %s with the value %f violate a forbidden clause on the component %s", hyperparameter_name, hyperparameter_value, component.name)
                    continue

                # takes the closest bound if new value violiates the boundary
                if hyperparameter_value_violate_boundary(hyperparameter_name=hyperparameter_name, hyperparameter_value=new_value, component_node=component.search_space_component_node):
                    logging.warning("New value %s for the hyperparameter %s violates the boundary define in search space, in consequence the value at the limit will be used", new_value, hyperparameter_name)
                    if abs(new_value - lower_bound) > abs(new_value - upper_bound):
                        new_value = upper_bound
                    else:
                        new_value = lower_bound

                component.hyperparameters[hyperparameter_name] = new_value

        if len(cache_statistics) > 0:
            self.notify_statistics(
                individual=individual,
                n_ratio=self.n_ratio,
                parameters={'mus': [s['mu'] for s in cache_statistics], 'sigmas': [s['sigma'] for s in cache_statistics]},
                components=[s['component_name'] for s in cache_statistics])

        return individual


class MutationGaussian(MutationNGaussian):
    def __init__(self,
                 mu: Optional[float] = None,
                 sigma: Optional[float] = None,
                 sigma_ratio: float = .1):
        self.mu = mu
        self.sigma = sigma
        self.sigma_ratio = sigma_ratio
        super().__init__(n_ratio=1., mu=mu, sigma=sigma, sigma_ratio=sigma_ratio)

    def __call__(self,
                 individual,
                 random_state: Optional[Union[random.RandomState, int]] = None):
        return super().__call__(individual=individual, random_state=random_state)


class MutationNUniformInteger(Mutation):
    def __init__(self,
                 n_ratio: float = 1.,
                 lower_bound: Optional[int] = None,
                 upper_bound: Optional[int] = None):
        self.n_ratio = n_ratio
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound

    def __call__(self,  # pylint: disable=too-many-branches
                 individual,
                 random_state: Optional[Union[random.RandomState, int]] = None):

        random_state = random_state_parser(random_state)

        cache_statistics = []

        for component in individual.components:
            for hyperparameter_name, hyperparameter_value in component.hyperparameters.items():
                if not is_number(hyperparameter_value):
                    logging.debug("Cannot apply %s on component %s - parameter %s is not a number", self.__class__.__name__, component.name, hyperparameter_name)
                    continue

                hyperparameter_type = get_hyperparameter_type(component=component.search_space_component_node, hyperparameter=hyperparameter_name)

                if hyperparameter_type == 'static':
                    logging.debug("Cannot apply MutationNUniformInteger for hyperparameter '%s' with the type '%s'", hyperparameter_name, hyperparameter_type)
                    continue

                if random_state.random() >= self.n_ratio:
                    continue

                if not self.lower_bound or not self.upper_bound:
                    # TODO allow another strategy than lower/upper ?
                    potential_lower_bound, potential_upper_bound = hyperparameter_lower_upper_bound(component_node=component.search_space_component_node, hyperparameter_name=hyperparameter_name)

                determined_lower_bound = 0
                determined_upper_bound = 0

                if not self.lower_bound:
                    determined_lower_bound = abs(component.hyperparameters[hyperparameter_name] - potential_lower_bound)
                else:
                    determined_lower_bound = self.lower_bound

                if not self.upper_bound:
                    determined_upper_bound = abs(potential_upper_bound - component.hyperparameters[hyperparameter_name])
                else:
                    determined_upper_bound = self.upper_bound

                high = min(determined_lower_bound, determined_upper_bound)

                if high < 2:
                    logging.debug("The high <=> min(low:%f, up:%f) for hyperparameter '%s' is lower than 2, %s cannot be applied", determined_upper_bound, determined_lower_bound, hyperparameter_name, self.__class__.__name__)
                    continue

                noise = random_state.randint(0, high)

                if individual.statistics:
                    cache_statistics.append({'component_name': component.name,
                                             'lower_bound': determined_lower_bound,
                                             'upper_bound': determined_upper_bound})

                new_value = component.hyperparameters[hyperparameter_name] + noise

                if hyperparameter_value_violiate_forbidden_clauses(hyperparameter_name=hyperparameter_name, hyperparameter_value=new_value, forbiddens=component.forbiddens, **component.hyperparameters):
                    logging.warning("Change the hyperparameter %s with the value %s violate a forbidden clause on the component %s", hyperparameter_name, hyperparameter_value, component.name)
                    continue

                component.hyperparameters[hyperparameter_name] = new_value

        if len(cache_statistics) > 0:
            self.notify_statistics(
                individual=individual,
                n_ratio=self.n_ratio,
                parameters={'lower_bounds': [s['lower_bound'] for s in cache_statistics], 'upper_bounds': [s['upper_bound'] for s in cache_statistics]},
                components=[s['component_name'] for s in cache_statistics])

        return individual


class MutationUniformInteger(MutationNUniformInteger):
    def __init__(self,
                 lower_bound: Optional[int] = None,
                 upper_bound: Optional[int] = None):
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
        super().__init__(n_ratio=1., lower_bound=lower_bound, upper_bound=upper_bound)

    def __call__(self,  # pylint: disable=too-many-branches
                 individual,
                 random_state: Optional[Union[random.RandomState, int]] = None):
        return super().__call__(individual=individual, random_state=random_state)


class MutationOneRandom(Mutation):
    def __call__(self,
                 individual,
                 random_state: Optional[Union[random.RandomState, int]] = None):

        random_state = random_state_parser(random_state)

        component = random_state.choice(individual.components)

        list_of_hyperparameters = list(component.hyperparameters.keys())

        if len(list_of_hyperparameters) <= 0:
            logging.warning("Impossible to apply the mutation, there are no hyperparameters available")
            return individual

        hyperparameter_name = random_state.choice(list_of_hyperparameters)
        hyperparameter_value = component.hyperparameters[hyperparameter_name]

        determined_value = _hyperparameter_generate_value(component=component, hyperparameter_name=hyperparameter_name, hyperparameter_value=hyperparameter_value, random_state=random_state)

        if determined_value is None:
            return individual

        component.hyperparameters[hyperparameter_name] = determined_value

        self.notify_statistics(
            individual=individual,
            component=component.name,
            determined={'value': determined_value})

        return individual


def _hyperparameter_generate_value(component, hyperparameter_name, hyperparameter_value, random_state):
    hyperparameter_type = get_hyperparameter_type(component=component.search_space_component_node, hyperparameter=hyperparameter_name)

    if hyperparameter_type != 'categorical':
        logging.error("We do not handle mutation for hyperparameter '%s' with the type '%s'", hyperparameter_name, hyperparameter_type)
        return None

    if is_integer(hyperparameter_value):
        lower_bound, upper_bound = hyperparameter_lower_upper_bound(component_node=component.search_space_component_node, hyperparameter_name=hyperparameter_name)

        if upper_bound - lower_bound < 2:
            logging.warning("The difference between the bounds (%f - %f) for hyperparameter '%s' are lower than 2, MutationOneRandom cannot be applied")
            return None

        determined_value = random_state.randint(lower_bound, upper_bound)

    elif is_float(hyperparameter_value):
        lower_bound, upper_bound = hyperparameter_lower_upper_bound(component_node=component.search_space_component_node, hyperparameter_name=hyperparameter_name)
        determined_value = random_state.uniform(lower_bound, upper_bound)
    else:
        if not keys_exists(component.search_space_component_node[SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS.value], hyperparameter_name):
            raise KeyError(f"The hyperparameter '{hyperparameter_name}' does not exist in {component.search_space_component_node}")

        hyperparameter_node = component.search_space_component_node[SEARCH_SPACE_KEY.COMPONENT_HYPERPARAMETERS.value][hyperparameter_name]
        determined_value = generate_hyperparameter_value(hyperparameter_node=hyperparameter_node, random_state=random_state)

    if hyperparameter_value_violiate_forbidden_clauses(hyperparameter_name=hyperparameter_name, hyperparameter_value=determined_value, forbiddens=component.forbiddens, **component.hyperparameters):
        logging.warning("Change the hyperparameter %s with the value %s violate a forbidden clause on the component %s", hyperparameter_name, hyperparameter_value, component.name)
        return None

    return determined_value


# NOTE insert does not work on root component
class MutationInsert(Mutation):
    @staticmethod
    def handle_individual(individual):
        if individual[0].forbiddens is not None and 'previous' in individual[0].forbiddens and 'preprocessors' in individual[0].forbiddens['previous']:
            return False

        #  if all availables algorithms are forbidden to insert at any place, we can't handle the individual
        #  for component in individual.components:

        return True

    def __call__(self,
                 individual,
                 random_state: Optional[Union[random.RandomState, int]] = None):
        random_state = random_state_parser(random_state)

        index = random_state.randint(0, len(individual))
        parent_components = [individual[i] for i in range(index, len(individual))]
        component = MLPipelineComponent.generate_component(search_space=individual.search_space, parent_components=parent_components, random_state=random_state)

        if component is None:
            return individual

        simulated_individual = copy.deepcopy(individual)
        simulated_individual.insert(index + 1, component)

        if not MLPipeline.is_valid_pipeline(simulated_individual):
            logging.warning('insert the component %s in the individual %s makes an invalid pipeline', component.name, individual)
            return individual

        individual.insert(index + 1, component)

        self.notify_statistics(
            individual=individual,
            component=component.name)

        return individual


class MutationDelete(Mutation):
    @staticmethod
    def handle_individual(individual):
        if len(individual) <= 1:
            logging.warning('individual %s is too small to remove a component', str(individual))
            return False
        return True

    def __call__(self,
                 individual,
                 random_state: Optional[Union[random.RandomState, int]] = None):
        random_state = random_state_parser(random_state)

        if not MutationDelete.handle_individual(individual):
            return individual

        index = random_state.randint(1, len(individual))

        simulated_individual = copy.deepcopy(individual)
        del simulated_individual[index]

        if not MLPipeline.is_valid_pipeline(simulated_individual):
            logging.warning('delete the component %s makes an invalid pipeline', individual[index].name)
            return individual

        component_name = individual[index].name
        del individual[index]

        self.notify_statistics(
            individual=individual,
            component=component_name)

        return individual


class MutationReplace(Mutation):
    def __call__(self,
                 individual,
                 random_state: Optional[Union[random.RandomState, int]] = None):
        random_state = random_state_parser(random_state)

        index = random_state.randint(0, len(individual))

        parent_components = None
        if index != 0:
            parent_components = [individual[i] for i in range(index, len(individual))]

        hyperparameter_index = random_state.randint(low=0, high=len(individual[index].hyperparameters) + 1)

        if hyperparameter_index == len(individual[index].hyperparameters):  # equivalent of changing a primitive node in TPOT
            component = MLPipelineComponent.generate_component(search_space=individual.search_space, parent_components=parent_components, random_state=random_state)

            if component is None:
                return individual

            simulated_individual = copy.deepcopy(individual)
            simulated_individual[index] = component

            if not MLPipeline.is_valid_pipeline(simulated_individual):
                logging.warning('replace the component %s by %s makes an invalid pipeline', individual[index], component)
                return individual

            individual[index] = component

            self.notify_statistics(
                individual=individual,
                component=component.name)
        else:  # equivalent of changing a terminal node in TPOT
            selected_key = list(individual[index].hyperparameters.keys())[hyperparameter_index]
            hyperparameter_value = individual[index].hyperparameters[selected_key]

            determined_value = _hyperparameter_generate_value(component=individual[index],
                                                              hyperparameter_name=selected_key,
                                                              hyperparameter_value=hyperparameter_value,
                                                              random_state=random_state)

            if determined_value is None:
                return individual

            simulated_individual = copy.deepcopy(individual)
            simulated_individual[index].hyperparameters[selected_key] = determined_value

            if not MLPipeline.is_valid_pipeline(simulated_individual):
                logging.warning('replace the hyperparameter %s with value %s by %s makes an invalid pipeline', selected_key, str(hyperparameter_value), str(determined_value))
                return individual

            individual[index].hyperparameters[selected_key] = determined_value

            self.notify_statistics(
                individual=individual,
                key=selected_key,
                value=determined_value)

        return individual


class MutationReplaceNodeOnly(Mutation):
    def __call__(self,
                 individual,
                 random_state: Optional[Union[random.RandomState, int]] = None):
        random_state = random_state_parser(random_state)

        index = random_state.randint(0, len(individual))

        parent_components = None
        if index != 0:
            parent_components = [individual[i] for i in range(index, len(individual))]

        component = MLPipelineComponent.generate_component(search_space=individual.search_space, parent_components=parent_components, random_state=random_state)

        if component is None:
            return individual

        simulated_individual = copy.deepcopy(individual)
        simulated_individual[index] = component

        if not MLPipeline.is_valid_pipeline(simulated_individual):
            logging.warning('replace the component %s by %s makes an invalid pipeline', individual[index], component)
            return individual

        individual[index] = component

        self.notify_statistics(
            individual=individual,
            component=component.name)

        return individual


class CrossoverOnePointExactly(Crossover):
    @staticmethod
    def handle_individual(individual_1, individual_2):
        if individual_1.hash() == individual_2.hash():
            return False
        return True

    def __call__(self,
                 individual_1,
                 individual_2,
                 random_state):
        random_state = random_state_parser(random_state)

        size = min(len(individual_1), len(individual_2))

        cxpoint = random_state.randint(0, size)

        original_individual_1 = copy.deepcopy(individual_1)
        original_individual_2 = copy.deepcopy(individual_2)

        simulated_individual_1 = copy.deepcopy(individual_1)
        simulated_individual_2 = copy.deepcopy(individual_2)

        simulated_individual_1[cxpoint] = original_individual_2[cxpoint]
        simulated_individual_2[cxpoint] = original_individual_1[cxpoint]

        if MLPipeline.is_valid_pipeline(simulated_individual_1):
            individual_1[cxpoint] = original_individual_2[cxpoint]

            self.notify_statistics(
                individual=individual_1,
                parameters=None,
                cxpoint=cxpoint)
        else:
            logging.warning('the CrossoverOnePointExactly (index: %d) makes an invalid pipeline for individual_1 %s', cxpoint, individual_1)

        if MLPipeline.is_valid_pipeline(simulated_individual_2):
            individual_2[cxpoint] = original_individual_1[cxpoint]

            self.notify_statistics(
                individual=individual_2,
                parameters=None,
                cxpoint=cxpoint)
        else:
            logging.warning('the CrossoverOnePointExactly (index: %d) makes an invalid pipeline for individual_2 %s', cxpoint, individual_2)

        return individual_1, individual_2


class CrossoverOnePoint(Crossover):
    @staticmethod
    def handle_individual(individual_1, individual_2):
        if max(len(individual_1), len(individual_2)) <= 1:
            return False
        return True

    def __call__(self,
                 individual_1,
                 individual_2,
                 random_state):
        random_state = random_state_parser(random_state)

        size = min(len(individual_1), len(individual_2))

        if size == 1:
            # exceptionally, if one of the candidate has only an estimator, i.e., size one,
            # and the the second individual has a size greater than 2, we permit
            # the subtree to go on the small individual
            if max(len(individual_1), len(individual_2)) >= 2:
                size += 1
            else:  # not interest to exchange both individual of size one
                return individual_1, individual_2

        cxpoint = random_state.randint(1, size)

        original_individual_1 = copy.deepcopy(individual_1)
        original_individual_2 = copy.deepcopy(individual_2)

        simulated_individual_1 = copy.deepcopy(individual_1)
        simulated_individual_2 = copy.deepcopy(individual_2)

        simulated_individual_1[cxpoint:] = original_individual_2[cxpoint:]
        simulated_individual_2[cxpoint:] = original_individual_1[cxpoint:]

        if MLPipeline.is_valid_pipeline(simulated_individual_1):
            individual_1[cxpoint:] = original_individual_2[cxpoint:]

            self.notify_statistics(
                individual=individual_1,
                parameters=None,
                cxpoint=cxpoint)
        else:
            logging.warning('the CrossoverOnePoint (index: %d) makes an invalid pipeline for individual_1 %s', cxpoint, individual_1)

        if MLPipeline.is_valid_pipeline(simulated_individual_2):
            individual_2[cxpoint:] = original_individual_1[cxpoint:]

            self.notify_statistics(
                individual=individual_2,
                parameters=None,
                cxpoint=cxpoint)
        else:
            logging.warning('the CrossoverOnePoint (index: %d) makes an invalid pipeline for individual_2 %s', cxpoint, individual_2)

        return individual_1, individual_2


class CrossoverOnePointAverage(Crossover):
    def __call__(self,  # pylint: disable=too-many-locals, too-many-branches
                 individual_1,
                 individual_2,
                 random_state):
        random_state = random_state_parser(random_state)

        common_components_individual_1 = defaultdict(set)
        common_components_individual_2 = defaultdict(set)

        # search common components
        for i1, c1 in enumerate(individual_1.components):
            for i2, c2 in enumerate(individual_2.components):
                if c1.name == c2.name:
                    common_components_individual_1[c1.name].add(i1)
                    common_components_individual_2[c2.name].add(i2)

        if len(common_components_individual_1) == 0:
            logging.warning('impossible to perform CrossoverOnePointAverage, there are no common components between individual_1 and individual_2')
            return individual_1, individual_2

        # we want to give the same chance between a component present once, and another one present multiple times
        selected_component = random_state.choice(list(common_components_individual_1.keys()))

        selected_index_individual_1 = random_state.choice(list(common_components_individual_1[selected_component]))
        selected_index_individual_2 = random_state.choice(list(common_components_individual_2[selected_component]))

        if len(individual_1[selected_index_individual_1].hyperparameters.keys()) == 0:
            logging.warning('impossible to perform CrossoverOnePointAverage, there are no hyperparameters for component %s', selected_component)
            return individual_1, individual_2

        available_hyperparameters = []

        # average can only be done on number HPs
        for hp_name, hp_value in individual_1[selected_index_individual_1].hyperparameters.items():
            if is_number(hp_value):
                available_hyperparameters.append(hp_name)

        if len(available_hyperparameters) == 0:
            logging.warning('impossible to perform CrossoverOnePointAverage, there are no hyperparameters of type number for component %s', selected_component)
            return individual_1, individual_2

        # randomly select an HP
        selected_hyperparameter = random_state.choice(available_hyperparameters)

        hp_value_individual_1, hp_value_individual_2 = individual_1[selected_index_individual_1].hyperparameters[selected_hyperparameter], individual_2[selected_index_individual_2].hyperparameters[selected_hyperparameter]

        # average the HP values
        hyperparameter_mean = (hp_value_individual_1 + hp_value_individual_2) / 2.

        if is_integer(hp_value_individual_1) or is_integer(hp_value_individual_2):
            hyperparameter_mean = round(hyperparameter_mean)

        # change the value between one of two pipelines
        if random_state.random() < 0.5:
            simulated_individual_1 = copy.deepcopy(individual_1)
            simulated_individual_1[selected_index_individual_1].hyperparameters[selected_hyperparameter] = hyperparameter_mean

            if MLPipeline.is_valid_pipeline(simulated_individual_1):
                individual_1[selected_index_individual_1].hyperparameters[selected_hyperparameter] = hyperparameter_mean

                self.notify_statistics(
                    individual=individual_1,
                    parameters=None)
        else:
            simulated_individual_2 = copy.deepcopy(individual_2)
            simulated_individual_2[selected_index_individual_2].hyperparameters[selected_hyperparameter] = hyperparameter_mean

            if MLPipeline.is_valid_pipeline(simulated_individual_2):
                individual_2[selected_index_individual_2].hyperparameters[selected_hyperparameter] = hyperparameter_mean

                self.notify_statistics(
                    individual=individual_1,
                    parameters=None)

        return individual_1, individual_2


class CrossoverTwoPoint(Crossover):
    '''
    @staticmethod
    def handle_individual(individual_1, individual_2):
        size = min(len(individual_1), len(individual_2))
        if size <= 3:
            return False
        return True
    '''

    def __call__(self,
                 individual_1,
                 individual_2,
                 random_state):
        random_state = random_state_parser(random_state)

        size = min(len(individual_1), len(individual_2))

        if size < 3:  # <=> one estimator and two preprocessors
            logging.warning('the CrossoverTwoPoint cannot be applied if a candidate has a size less than 3')
            return individual_1, individual_2

        cx_first_point = random_state.randint(1, size - 1)
        cx_second_point = random_state.randint(cx_first_point + 1, size)

        original_individual_1 = copy.deepcopy(individual_1)
        original_individual_2 = copy.deepcopy(individual_2)

        simulated_individual_1 = copy.deepcopy(individual_1)
        simulated_individual_2 = copy.deepcopy(individual_2)

        simulated_individual_1[cx_first_point:cx_second_point] = original_individual_2[cx_first_point:cx_second_point]

        simulated_individual_2[cx_first_point:cx_second_point] = original_individual_1[cx_first_point:cx_second_point]

        if MLPipeline.is_valid_pipeline(simulated_individual_1):
            individual_1[cx_first_point:cx_second_point] = original_individual_2[cx_first_point:cx_second_point]

            self.notify_statistics(
                individual=individual_1,
                parameters=None,
                cx_first_point=cx_first_point,
                cx_second_point=cx_second_point)
        else:
            logging.warning('the CrossoverTwoPoint (first_index: %d, second_index: %d) makes an invalid pipeline for individual_1 %s', cx_first_point, cx_second_point, individual_1)

        if MLPipeline.is_valid_pipeline(simulated_individual_2):
            individual_2[cx_first_point:cx_second_point] = original_individual_1[cx_first_point:cx_second_point]

            self.notify_statistics(
                individual=individual_2,
                parameters=None,
                cx_first_point=cx_first_point,
                cx_second_point=cx_second_point)
        else:
            logging.warning('the CrossoverTwoPoint (first_index: %d, second_index: %d) makes an invalid pipeline for individual_2 %s', cx_first_point, cx_second_point, individual_2)

        return individual_1, individual_2

# TODO mutation (méchanisme de correction, si forbidden, mettre l'hp à sa borne)
# TODO mutation that change only one component (randomly) or each values it can for each component
# TODO mutation gaussian that handles integer values
