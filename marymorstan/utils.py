# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

import logging

import numpy as np
from sklearn.datasets import make_classification, make_regression
from ruamel.yaml import YAML
from stopit import threading_timeoutable

from sktime.datasets import load_gunpoint


def keys_exists(element, *keys):
    if len(keys) == 0:
        raise AttributeError('keys_exists() expects at least two arguments, one given.')

    _element = element
    for key in keys:
        try:
            _element = _element[key]
        except KeyError:
            return False
        except TypeError:
            return False
    return True


def read_yaml(search_space_file=None) -> dict:
    yaml = YAML(typ='safe')

    try:
        with open(search_space_file, 'r', encoding="utf-8") as stream:
            mapper = yaml.load(stream)
    except Exception as e:
        logging.error("Trouble to find or read the search space file %s", search_space_file)
        raise e

    return mapper


def is_number(variable):
    if isinstance(variable, (int, np.integer, np.floating, float)):
        return True

    if isinstance(variable, (str, bool)):
        return False

    if np.issubdtype(variable, np.integer):
        return True

    if np.issubdtype(variable, np.bool_):
        return False

    logging.error("Impossible to determine if the type of the variable %s is a number %s", variable, type(variable))

    return False


def is_integer(variable):
    if isinstance(variable, (str, bool, float, np.bool_)):
        return False

    if isinstance(variable, (int, np.integer)):
        return True

    if np.issubdtype(variable, np.integer):
        return True

    logging.error("Impossible to determine if the type of the variable %s is integer %s", variable, type(variable))

    return False


def is_float(variable):
    if isinstance(variable, (np.floating, float)):
        return True

    if isinstance(variable, (int, np.integer, str, bool)) or np.issubdtype(variable, np.bool_):
        return False

    logging.error("Impossible to determine if the type of the variable %s is a float %s", variable, type(variable))

    return False


class SingletonSamples:
    class __SingletonSamples:
        def __init__(self):
            self.pretest_X = None
    instance = None

    def __init__(self):
        if not SingletonSamples.instance:
            SingletonSamples.instance = SingletonSamples.__SingletonSamples()

    def __getattr__(self, name):
        return getattr(self.instance, name)

    def get_binary_classification_samples(self):
        if self.instance.pretest_X is None:
            self.instance.pretest_X, self.instance.pretest_y = make_classification(n_samples=50, n_features=2, random_state=42)  # pylint: disable=attribute-defined-outside-init
        return self.instance.pretest_X, self.instance.pretest_y

    def get_multiclass_classification_samples(self):
        if self.instance.pretest_X is None:
            self.instance.pretest_X, self.instance.pretest_y = make_classification(n_samples=50, n_features=10, random_state=42)  # pylint: disable=attribute-defined-outside-init
        return self.instance.pretest_X, self.instance.pretest_y

    def get_regression_samples(self):
        if self.instance.pretest_X is None:
            self.instance.pretest_X, self.instance.pretest_y, _ = make_regression(n_samples=50, n_features=10, random_state=42)  # pylint: disable=attribute-defined-outside-init
        return self.instance.pretest_X, self.instance.pretest_y

    def get_timeseries_classification_samples(self):
        if self.instance.pretest_X is None:
            self.instance.pretest_X, self.instance.pretest_y = load_gunpoint(return_X_y=True)  # pylint: disable=attribute-defined-outside-init
        return self.instance.pretest_X, self.instance.pretest_y


@threading_timeoutable(default="timeout")
def time_limited_call(func, *args):
    func(*args)
