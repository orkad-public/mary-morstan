# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from typing import Optional, List, Dict, Callable, Union
import collections
import logging
import threading

from functools import partial

from numpy import random

from .individual import Individual
from .mutation import Mutations, Mutation
from .crossover import Crossovers, Crossover

from .statistics import Statistics
from .meta_data import EAMetaData
from .selection import select_random_with_exclude
from .utils import random_state_parser, randomly_pick_individual, balance_probabilities
from .operator_strategy import OPERATOR_DISTRIBUTION_STRATEGY

MAX_FORCE_LOOP_ITERATION = 50


class Variation(collections.abc.Callable):  # type: ignore[misc]
    def __init__(self,
                 ea_meta_data: EAMetaData,
                 mutation: Optional[Mutations] = None,
                 crossover: Optional[Crossovers] = None,
                 one_variation_per_individual: bool = False,
                 one_mutation_per_individual: bool = False,
                 one_crossover_per_individual: bool = False,
                 number_of_try_recall_variation: int = 1,
                 force_other_variations_if_fail: bool = False,
                 reset_fitness_on_changes: bool = True,
                 statistics: Optional[Statistics] = None,
                 random_state: Optional[random.RandomState] = None):
        self.ea_meta_data = ea_meta_data

        self.mutation = mutation
        self.crossover = crossover

        self.one_variation_per_individual = one_variation_per_individual
        self.one_mutation_per_individual = one_mutation_per_individual
        self.one_crossover_per_individual = one_crossover_per_individual
        self.number_of_try_recall_variation = number_of_try_recall_variation
        self.force_other_variations_if_fail = force_other_variations_if_fail
        self.reset_fitness_on_changes = reset_fitness_on_changes

        self.statistics = statistics
        self.random_state = random_state_parser(random_state)

    def __call__(self, population: List[Individual],  # pylint: disable=arguments-differ, too-many-locals, too-many-branches, too-many-statements
                 offspring_size: int = None,       # type: ignore[assignment]
                 current_iteration: int = None,    # type: ignore[assignment]
                 elapsed_time: Optional[int] = None,
                 total_time: Optional[int] = None,
                 global_stop_optimization_event: Optional[threading.Event] = None,
                 one_variation_per_individual: Optional[bool] = None,
                 one_mutation_per_individual: Optional[bool] = None,
                 one_crossover_per_individual: Optional[bool] = None,
                 number_of_try_recall_variation: Optional[int] = None,
                 force_other_variations_if_fail: Optional[bool] = None,
                 reset_fitness_on_changes: Optional[bool] = None):

        offspring = []

        if offspring_size is None:
            offspring_size = len(population)

        if one_variation_per_individual is None:
            one_variation_per_individual = self.one_variation_per_individual

        if one_mutation_per_individual is None:
            one_mutation_per_individual = self.one_mutation_per_individual

        if one_crossover_per_individual is None:
            one_crossover_per_individual = self.one_crossover_per_individual

        if number_of_try_recall_variation is None:
            number_of_try_recall_variation = self.number_of_try_recall_variation

        if force_other_variations_if_fail is None:
            force_other_variations_if_fail = self.force_other_variations_if_fail

        if reset_fitness_on_changes is None:
            reset_fitness_on_changes = self.reset_fitness_on_changes

        force_unique_individuals = self.ea_meta_data.force_unique_individuals
        hash_known_individuals = self.ea_meta_data.hash_known_individuals

        if self.crossover and len(self.crossover.crossovers) == 0:
            message = 'None crossovers have been declared, either specify at least one or do not declare it at all'
            logging.error(message)
            raise Exception(message)

        if self.mutation and len(self.mutation.mutations) == 0:
            message = 'None mutations have been declared, either specify at least one or do not declare it at all'
            logging.error(message)
            raise Exception(message)

        for _ in range(offspring_size):  # pylint: disable=too-many-nested-blocks
            individual, index_individual = randomly_pick_individual(population=population, random_state=self.random_state)
            individual = individual.clone()
            offspring.append(individual)

            crossover_has_been_applied = False
            mutation_has_been_applied = False

            random_number = self.random_state.random()

            new_unknown_individual = False

            # CROSSOVERS
            if self.crossover and random_number <= self.crossover.portion_of_population:
                # NOTE, contrarily to mutations, the order of this condition is not necesarily (for varOr algorithm in EA).
                # Indeed, in the variation, the crossover class is called first. However, we keep it in case for the future.
                if individual.meta_data.had_variation and one_variation_per_individual:
                    logging.debug("No crossover (only one variation per individual) for individual %s", individual)
                    continue

                individual_changed = False
                new_unknown_individual = False
                individual_hash_before_crossover = individual.hash()

                stop_forcing_loop = False
                forcing_loop_iteration = 0

                while (not stop_forcing_loop or (force_unique_individuals and not new_unknown_individual)) and forcing_loop_iteration < MAX_FORCE_LOOP_ITERATION:
                    if force_unique_individuals is False:
                        stop_forcing_loop = True

                    forcing_loop_iteration += 1

                    individual_2 = select_random_with_exclude(population=population, exclude=index_individual)

                    if not individual_2:
                        logging.error("Impossible to extract a second individual during crossover phase, continue to next individual")
                        continue

                    individual_2 = individual_2.clone()

                    # TODO extract variation_probabilities
                    probabilities = self._get_probabilities(self.crossover, elapsed_time, total_time)

                    selected_crossovers = self._get_selected_variations(variations=self.crossover.crossovers,
                                                                        variation_probabilities=probabilities,
                                                                        force_other_variations_if_fail=force_other_variations_if_fail,
                                                                        one_specific_variation_per_individual=one_crossover_per_individual,
                                                                        variation_handle_individual=partial(lambda variation_partial, individual_1, individual_2: variation_partial.func.handle_individual(individual_1, individual_2), individual_1=individual, individual_2=individual_2))

                    if len(selected_crossovers) == 0:
                        logging.warning('no crossovers able to handle the candidate %s', individual)
                        break

                    for crossover in selected_crossovers:
                        if crossover_has_been_applied and one_crossover_per_individual \
                           and (not force_unique_individuals or (force_unique_individuals and new_unknown_individual)):
                            logging.debug("Only one crossover per individual, continue to next individuals")
                            stop_forcing_loop = True
                            break

                        crossover_name = crossover.__name__

                        for i in range(number_of_try_recall_variation):
                            if global_stop_optimization_event is not None and global_stop_optimization_event.is_set():
                                return offspring

                            logging.debug('Try number %d for the crossover %s', i, crossover_name)

                            individual_2_hash_before_crossover = individual_2.hash()
                            logging.debug("Apply crossover %s for individual %s and %s (hash_1: %s - id_1: %s and hash_2: %s - id_2 %s)", crossover_name, individual, individual_2, individual_hash_before_crossover, individual.identifier, individual_2_hash_before_crossover, individual_2.identifier)

                            if self.statistics:
                                self.statistics.increment_variation_call(iteration=current_iteration, name=crossover_name)

                            individual, individual_2 = crossover(individual_1=individual, individual_2=individual_2)

                            individual_hash_after_crossover = individual.hash()
                            individual_changed = individual_hash_before_crossover != individual_hash_after_crossover

                            if individual_changed and individual.meta_data.had_variation is False:
                                # NOTE this can be used as an information (e.g., enforce re-evaluation, varAnd algorithm)
                                individual.meta_data.had_variation = True

                            if not individual_changed:
                                logging.warning("Failed to apply the crossover %s for individual %s (hash_1: %s - id_1: %s)", crossover_name, individual, individual_hash_before_crossover, individual.identifier)

                            # if (individual_changed or individual_2_changed) and not force_unique_individuals:
                            if individual_changed and not force_unique_individuals:
                                break

                            if force_unique_individuals:
                                if individual_changed:
                                    if individual_hash_after_crossover not in hash_known_individuals:
                                        logging.debug('New unknown individual has been created %s (hash: %s)', individual, individual_hash_after_crossover)
                                        hash_known_individuals.add(individual_hash_after_crossover)
                                        new_unknown_individual = True
                                    else:
                                        logging.debug('Individual %s (hash: %s) already known', individual, individual_hash_after_crossover)

                                if new_unknown_individual:
                                    stop_forcing_loop = True
                                    break

                        if individual_changed:
                            crossover_has_been_applied = True

                        logging.debug("Obtain crossovered individual %s hash_1: %s - id_1: %s)", individual, individual_hash_after_crossover, individual.identifier)

                        if individual.meta_data.had_variation is True and reset_fitness_on_changes:
                            del individual.fitness.values

                        if self.statistics and (individual_hash_before_crossover != individual_hash_after_crossover):
                            self.statistics.increment_variation_success_call(iteration=current_iteration, name=crossover_name)

            # MUTATIONS
            if self.mutation and (random_number <= self.mutation.portion_of_population or (force_unique_individuals and not new_unknown_individual)):
                # NOTE the position of this condition is important (in order to respect the varOr concept, i.e., an individual is either mutated or crossovered).
                # In the case of the individual had a crossovered, we pass to the next individual to give the chance to pass the probability of mutation condition.
                # e.g., if you had 90 percent of the population previously crossovered, with 10 percent of chance for the probability of mutation, then there will be very
                # few chance that a candidate pass the probability (i.e., p=0.1*0.1) since it has already been crossovered (if this condition is after the probability of mutation)
                # The fact that this condition preceed the probability of mutation, permits to solve this issue and to respect the varOr EA algorithm.
                if individual.meta_data.had_variation and one_variation_per_individual and (not force_unique_individuals or (force_unique_individuals and new_unknown_individual)):
                    logging.debug("No mutation (only one variation per individual) for individual %s", individual)
                    continue

                individual_changed = False
                new_unknown_individual = False
                individual_hash_before_mutation = individual.hash()

                stop_forcing_loop = False
                forcing_loop_iteration = 0

                # NOTE if we need unique individuals we repeat mutations until all individual are unique and unseen in previous generations
                while (not stop_forcing_loop or (force_unique_individuals and not new_unknown_individual)) and forcing_loop_iteration < MAX_FORCE_LOOP_ITERATION:

                    if force_unique_individuals is False:
                        stop_forcing_loop = True

                    forcing_loop_iteration += 1

                    probabilities = self._get_probabilities(self.mutation, elapsed_time, total_time)

                    selected_mutations = self._get_selected_variations(variations=self.mutation.mutations,
                                                                       variation_probabilities=probabilities,
                                                                       force_other_variations_if_fail=force_other_variations_if_fail,
                                                                       one_specific_variation_per_individual=one_mutation_per_individual,
                                                                       variation_handle_individual=partial(lambda variation_partial, individual: variation_partial.func.handle_individual(individual), individual=individual))

                    if len(selected_mutations) == 0:
                        logging.warning('no mutations able to handle the candidate %s', individual)
                        break

                    for mutation in selected_mutations:
                        if mutation_has_been_applied and one_mutation_per_individual \
                           and (not force_unique_individuals or (force_unique_individuals and new_unknown_individual)):
                            logging.debug("Only one mutation per individual, continue to next individuals")
                            stop_forcing_loop = True
                            break

                        mutation_name = mutation.__name__

                        logging.debug("Apply mutation %s for individual %s (hash: %s - id: %s)", mutation_name, individual, individual_hash_before_mutation, individual.identifier)

                        for try_number in range(number_of_try_recall_variation):
                            if global_stop_optimization_event is not None and global_stop_optimization_event.is_set():
                                return offspring

                            logging.debug('Try number %d for the mutation %s', try_number, mutation_name)

                            if self.statistics:
                                self.statistics.increment_variation_call(iteration=current_iteration, name=mutation_name)

                            individual = mutation(individual=individual)

                            individual_hash_after_mutation = individual.hash()
                            individual_changed = individual_hash_before_mutation != individual_hash_after_mutation

                            if individual_changed and individual.meta_data.had_variation is False:
                                # NOTE this can be used as an information (e.g., enforce re-evaluation, varAnd algorithm)
                                individual.meta_data.had_variation = True

                            if not individual_changed:
                                logging.warning("Failed to apply the mutation %s (try %d) for individual %s (hash: %s - id: %s)", mutation_name, try_number, individual, individual_hash_before_mutation, individual.identifier)
                                continue

                            if individual_changed and force_unique_individuals is False:
                                break

                            if individual_changed and force_unique_individuals is True:
                                if individual_hash_after_mutation not in hash_known_individuals:  # pylint: disable=no-else-break
                                    logging.debug('New unknown individual has been created %s (hash: %s)', individual, individual_hash_after_mutation)
                                    hash_known_individuals.add(individual_hash_after_mutation)
                                    stop_forcing_loop = True
                                    new_unknown_individual = True
                                    break
                                else:
                                    logging.debug('Individual %s (hash: %s) already known', individual, individual_hash_after_mutation)

                        if individual_changed:
                            logging.debug("Obtain mutated individual %s (hash: %s - id: %s)", individual, individual_hash_after_mutation, individual.identifier)
                            mutation_has_been_applied = True

                            if self.statistics:
                                self.statistics.increment_variation_success_call(iteration=current_iteration, name=mutation_name)

                        if individual.meta_data.had_variation is True and reset_fitness_on_changes:
                            del individual.fitness.values

        return offspring

    def _get_probabilities(self, variation, elapsed_time, total_time):
        distribution_strategy = variation.distribution_strategy

        if distribution_strategy.strategy() == OPERATOR_DISTRIBUTION_STRATEGY.TIME_BASED_DISTRIBUTION:
            return distribution_strategy.get_probabilities(elapsed_time, total_time)

        return distribution_strategy.get_probabilities()

    def _get_selected_variations(self,
                                 variations: List[Union[Mutation, Crossover]],
                                 variation_probabilities: Dict,
                                 force_other_variations_if_fail: bool,
                                 one_specific_variation_per_individual: bool,
                                 variation_handle_individual: Callable):

        available_variations_indexes = [i for i, variation in enumerate(variations) if variation_handle_individual(variation)]

        if len(available_variations_indexes) == 0:
            return []

        if force_other_variations_if_fail:
            self.random_state.shuffle(available_variations_indexes)
            return [variations[index] for index in available_variations_indexes]

        probabilities = [variation_probabilities[index] for index in available_variations_indexes]

        if len(probabilities) == 0:
            probabilities = [1. / len(available_variations_indexes)] * len(available_variations_indexes)

        # NOTE this situation could happen when variations are removed from the list because they are not able to handle a candidate
        # to solve this problem, we keep the initial proportion in order to have a sum equals to 1
        probabilities = balance_probabilities(probabilities)

        number_of_variations_selected = 1
        if not one_specific_variation_per_individual:
            number_of_variations_selected = sum(p > 0 for p in probabilities)

        selected_variations_indexes = self.random_state.choice(a=available_variations_indexes, size=number_of_variations_selected, p=probabilities)

        return [variations[index] for index in selected_variations_indexes]
