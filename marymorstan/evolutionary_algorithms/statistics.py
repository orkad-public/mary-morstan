# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from typing import List, Optional
from collections import defaultdict
import os.path
import logging
from copy import copy

import pandas as pd

from .individual import Individual
from .utils import diversity_ratio


class Statistics():
    def __init__(self, first_objective_maximize=True):
        self.individuals_ids_per_generation = {}
        self.fitnesses_per_candidate_selected = defaultdict(list)

        self.set_of_variations = set()
        self.variation_calls_per_generation = defaultdict(dict)
        self.variation_success_calls_per_generation = defaultdict(dict)
        self.best_individual_per_generation = {}

        self.elapsed_times = defaultdict(list)
        self.ratio_of_evaluations = []

        self.diversity_ratios = {}

        self.first_objective_maximize = first_objective_maximize

    @property
    def number_of_generations(self):
        return len(self.individuals_ids_per_generation)

    @property
    def list_of_variations(self):
        return list(self.set_of_variations)

    def increment_variation_call(self, iteration: Optional[int], name: str):
        self.set_of_variations.add(name)

        if name not in self.variation_calls_per_generation[iteration]:
            self.variation_calls_per_generation[iteration][name] = 0

        self.variation_calls_per_generation[iteration][name] += 1

    def increment_variation_success_call(self, iteration: Optional[int], name: str):
        if name not in self.variation_success_calls_per_generation[iteration]:
            self.variation_success_calls_per_generation[iteration][name] = 0

        self.variation_success_calls_per_generation[iteration][name] += 1

    def append_ratio_of_evaluations(self, ratio: float):
        self.ratio_of_evaluations.append(ratio)

    def append_elapsed_time(self, elapsed_time: float, iteration: int):
        self.elapsed_times[iteration].append(elapsed_time)

    def set_individuals(self, generation: int, individuals: List[Individual]):
        self.individuals_ids_per_generation[generation] = list(map(lambda x: x.identifier, individuals))

    def set_selected_individuals(self, generation: int, individuals: List[Individual]):
        self.fitnesses_per_candidate_selected[generation] = [individual.fitness.weighted_values for individual in individuals]

    def set_diversity(self, generation: int, individuals: List[Individual]):
        # FIXME use this as a test to verify when force_unique_individuals is true, optimize gives a diversity very close to 1
        self.diversity_ratios[generation] = diversity_ratio(individuals)

    def set_best_individual(self, individuals: List[Individual], generation: int):
        # if we pass a population, extract the best one among them
        if len(individuals) > 1:
            # NOTE best individual selected on the first objective only
            individuals = copy(individuals)
            individuals.sort(key=lambda individual: individual.fitness.weighted_values, reverse=self.first_objective_maximize)

        best_individual = individuals[0]

        if generation not in self.best_individual_per_generation:
            if generation <= 0:
                self.best_individual_per_generation[generation] = {str(best_individual): best_individual.fitness.weighted_values}
            else:
                if (generation - 1) not in self.best_individual_per_generation:
                    logging.warning("no best individual known at previous generation")
                    self.best_individual_per_generation[generation] = None
                    return

                best_individual_known = self.best_individual_per_generation[generation - 1]
                best_individual_known_fitness_weighted_values = list(best_individual_known.values())[0]

                if best_individual.fitness.weighted_values[0] > best_individual_known_fitness_weighted_values[0]:
                    logging.debug('Found a better individual (%s) during generation %d', str(best_individual), generation)
                    self.best_individual_per_generation[generation] = {str(best_individual): best_individual.fitness.weighted_values}
                else:
                    logging.debug('Set the best individual (%s) from the previous generation', str(best_individual_known))
                    self.best_individual_per_generation[generation] = best_individual_known
        else:
            best_individual_known = self.best_individual_per_generation[generation]

            if best_individual_known is None:
                logging.warning('no best individual previously known, the current one will be set as the best')
                self.best_individual_per_generation[generation] = {str(best_individual): best_individual.fitness.weighted_values}
                return

            best_individual_known_fitness_weighted_values = list(best_individual_known.values())[0]

            if best_individual.fitness.weighted_values[0] > best_individual_known_fitness_weighted_values[0]:
                logging.debug('Found a better individual (%s) during generation %d', str(best_individual), generation)
                self.best_individual_per_generation[generation] = {str(best_individual): best_individual.fitness.weighted_values}

    def _output_dataframe(self):
        dataframe = pd.DataFrame()

        # TODO optimize, include all the extracts under the same loop (explore pd.Series and pd.Dataframe)

        dataframe['generation'] = range(self.number_of_generations)
        dataframe['elapsed_time'] = pd.Series(self.elapsed_times).apply(sum)
        dataframe['fitnesses_per_candidate_selected'] = pd.Series(self.fitnesses_per_candidate_selected)
        dataframe['best_individual'] = pd.Series(self.best_individual_per_generation)
        dataframe['diversity_ratio'] = pd.Series(self.diversity_ratios)

        return dataframe

    def save(self, output_file):
        _, extension = os.path.splitext(output_file)

        df = self._output_dataframe()

        if extension == ".parquet":
            df.to_parquet(output_file, 'fastparquet', 'gzip')
        elif extension == ".json":
            df.to_json(output_file)
        else:
            raise Exception(f"Unsupported extension {extension} to save statistics")
