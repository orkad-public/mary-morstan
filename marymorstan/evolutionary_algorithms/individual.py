# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from __future__ import annotations

from collections.abc import Iterator, MutableSequence
from typing import Optional, List, Sequence

import copy

import numpy as np

from .fitness import Fitness
from .individual_statistics import IndividualStatistics


class Individual(Iterator, MutableSequence):
    def __init__(self, enable_statistics=False, fitness_weights=None):
        self.fitness = Fitness(weights=fitness_weights)
        self.meta_data = MetaData()

        self._statistics = None

        if enable_statistics:
            self._statistics = IndividualStatistics(individual=self)

    @property
    def statistics(self):
        return self._statistics

    @property
    def meta_data(self):
        if not self._meta_data:
            self.meta_data = MetaData()
        return self._meta_data

    @meta_data.setter
    def meta_data(self, meta_data: MetaData):
        self._meta_data = meta_data

    @meta_data.deleter
    def meta_data(self):
        self._meta_data = None

    @property
    def identifier(self):
        return hex(id(self))

    @property
    def attributes(self):
        raise NotImplementedError

    def __len__(self):
        raise NotImplementedError

    def __next__(self):
        raise NotImplementedError

    def __iter__(self):
        raise NotImplementedError

    def __getitem__(self, index):
        raise NotImplementedError

    def __setitem__(self, index, value):
        raise NotImplementedError

    def __delitem__(self, index):
        raise NotImplementedError

    def insert(self, index, value):
        raise NotImplementedError

    def __eq__(self, other):
        raise NotImplementedError

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        raise NotImplementedError

    def similar(self, other):
        raise NotImplementedError

    def hash(self):
        raise NotImplementedError

    def clone(self):
        return copy.deepcopy(self)

    # NOTE: potential operators for individual (https://docs.python.org/fr/3/library/operator.html)
    # could be used by EA's operators i.e., mutation and crossover


# NOTE inspired from https://stackoverflow.com/questions/4093029/how-to-inherit-and-extend-a-list-object-in-python
class SimpleIndividual(Individual):
    def __init__(self, attributes: Optional[List] = None, enable_statistics: bool = False, fitness_weights: Optional[Sequence] = None):
        super().__init__(enable_statistics=enable_statistics, fitness_weights=fitness_weights)

        self._inner_list = attributes

        if not attributes:
            self._inner_list = []

    @property
    def attributes(self):
        return self._inner_list

    def __len__(self):
        return len(self._inner_list)

    def __next__(self):
        return next(self._inner_list)

    def __iter__(self):
        return iter(self._inner_list)

    def __delitem__(self, index):
        self._inner_list.__delitem__(index)

    def insert(self, index, value):
        self._inner_list.insert(index, value)

    def __setitem__(self, index, value):
        self._inner_list.__setitem__(index, value)

    def __getitem__(self, index):
        return self._inner_list.__getitem__(index)

    def __str__(self):
        return str(self._inner_list)

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self._inner_list == other.attributes

    def similar(self, other, absolute_tolerance=1e-5):  # pylint: disable=arguments-differ
        return np.allclose(self._inner_list, other.attributes, atol=absolute_tolerance)

    def hash(self):
        if not self._inner_list:
            return None

        return hash(tuple(self._inner_list))


class MetaData():
    def __init__(self):
        self._had_variation = False
        self.current_iteration = 0

    @property
    def had_variation(self):
        return self._had_variation

    @had_variation.setter
    def had_variation(self, had_variation: bool):
        self._had_variation = had_variation

    @had_variation.deleter
    def had_variation(self):
        self._had_variation = False
