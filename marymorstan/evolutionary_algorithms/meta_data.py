# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

class EAMetaData():
    def __init__(self, force_unique_individuals: bool = False):
        self.force_unique_individuals = force_unique_individuals
        self._hash_known_individuals: set = set()

    @property
    def hash_known_individuals(self):
        return self._hash_known_individuals
