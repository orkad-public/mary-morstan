# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from typing import Optional, List
from collections.abc import Callable
import inspect
import logging

from functools import partial

from numpy import random

from .utils import random_state_parser, get_name
from .individual import Individual
from .operator_strategy import DistributionStrategy, StaticDistribution


class Crossovers():  # type: ignore[misc]
    def __init__(self,
                 portion_of_population: float = .1,
                 distribution_strategy: Optional[DistributionStrategy] = None,
                 random_state: Optional[random.RandomState] = None):
        self.portion_of_population = portion_of_population
        self._distribution_strategy = distribution_strategy

        self.random_state = random_state_parser(random_state)

    @property
    def distribution_strategy(self):
        if self._distribution_strategy is None:
            logging.warning('distribution was not specified, we assumed it is uniform')
            return StaticDistribution(len_operators=len(self.crossovers))
        return self._distribution_strategy

    @property
    def crossovers(self) -> List:
        raise NotImplementedError


class CustomizableCrossovers(Crossovers):
    def __init__(self,
                 portion_of_population: float = .1,
                 distribution_strategy: Optional[DistributionStrategy] = None,
                 random_state: Optional[random.RandomState] = None):
        super().__init__(portion_of_population=portion_of_population,
                         distribution_strategy=distribution_strategy,
                         random_state=random_state)

        self._crossovers: List = []

    def append(self, crossover, *args, **kwargs):
        partial_crossover = partial(crossover, *args, **kwargs)
        partial_crossover.__name__ = get_name(crossover)  # type: ignore[attr-defined]
        self._crossovers.append(partial_crossover)

    @property
    def crossovers(self):
        return self._crossovers


class SimpleCrossovers(Crossovers):
    def __init__(self,
                 portion_of_population: float = .1,
                 distribution_strategy: Optional[DistributionStrategy] = None,
                 random_state: Optional[random.RandomState] = None):
        super().__init__(portion_of_population=portion_of_population,
                         distribution_strategy=distribution_strategy,
                         random_state=random_state)

        self.crossover = CrossoverOnePoint()

    @property
    def crossovers(self):
        partial_crossover_onepoint = partial(self.crossover, random_state=self.random_state)
        partial_crossover_onepoint.__name__ = get_name(self.crossover)

        return [partial_crossover_onepoint]


class Crossover(Callable):  # type: ignore[misc]
    def __call__(self,  # pylint: disable=arguments-differ
                 individual_1: Individual,
                 individual_2: Individual):
        raise NotImplementedError

    def notify_statistics(self, individual, parameters=None, **kwargs):
        if not individual.statistics:
            return

        individual.statistics.append_variation(
            name=str(self),
            parameters=parameters,
            **kwargs)

    @staticmethod
    def handle_individual(individual_1: Individual, individual_2: Individual):  # pylint: disable=unused-argument
        return True

    # NOTE inspired from https://stackoverflow.com/questions/9058305/getting-attributes-of-a-class
    def __str__(self):
        attributes = inspect.getmembers(self, lambda a: not inspect.isroutine(a))
        str_attributes = ""
        for a in attributes:
            if not (a[0].startswith('__') and a[0].endswith('__')) and a[0] != '_abc_impl' and a[0] != 'independent_probability':
                str_attributes += "__" + str(a[0]) + "__" + str(a[1])
        return f"{self.__class__.__name__}{str_attributes}"


class CrossoverOnePoint(Crossover):
    def __call__(self,
                 individual_1: Individual,
                 individual_2: Individual,
                 random_state: Optional[random.RandomState] = None):
        random_state = random_state_parser(random_state)

        size = min(len(individual_1), len(individual_2))
        cxpoint = random_state.randint(1, size)
        individual_1[cxpoint:], individual_2[cxpoint:] = individual_2[cxpoint:], individual_1[cxpoint:]

        self.notify_statistics(
            individual=individual_1,
            parameters=None,
            cxpoint=cxpoint)

        self.notify_statistics(
            individual=individual_2,
            parameters=None,
            cxpoint=cxpoint)

        return individual_1, individual_2
