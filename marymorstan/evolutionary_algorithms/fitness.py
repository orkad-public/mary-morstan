# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from __future__ import annotations

from typing import Optional, Sequence

import logging

from operator import mul


class Fitness():
    def __init__(self, weights: Optional[Sequence] = None, values: Optional[Sequence] = None):
        self._weights = weights
        self._values = values

    @property
    def weights(self):
        if not self._weights and self.values:
            self._weights = [1.] * len(self.values)

        return self._weights

    @weights.setter
    def weights(self, weights: Optional[Sequence]):
        self._weights = weights

    @weights.deleter
    def weights(self):
        self._weights = None

    @property
    def values(self):
        return self._values

    @values.setter
    def values(self, values: Sequence):
        self._values = values

    @values.deleter
    def values(self):
        self._values = None

    @property
    def weighted_values(self):
        if self.values is None:
            logging.getLogger(__name__).error("There are no values, can't compute weighted_values")
            return None

        return tuple(map(mul, self.values, self.weights))

    @property
    def wvalues(self):
        return self.weighted_values

    def dominates(self, other: Fitness, objectives: slice = slice(None)):
        not_equal = False
        for weighted_values, other_weighted_values in zip(self.weighted_values[objectives], other.weighted_values[objectives]):
            if weighted_values > other_weighted_values:
                not_equal = True
            elif weighted_values < other_weighted_values:
                return False
        return not_equal

    def has_one_better_objective(self, other: Fitness, objectives: slice = slice(None)):
        for weighted_values, other_weighted_values in zip(self.weighted_values[objectives], other.weighted_values[objectives]):
            if weighted_values > other_weighted_values:
                return True
        return False

    @property
    def valid(self):
        return self.weighted_values is not None

    def __hash__(self):
        return hash(self.weighted_values)

    def __gt__(self, other):
        return not self.__le__(other)

    def __ge__(self, other):
        return not self.__lt__(other)

    def __le__(self, other):
        return self.weighted_values <= other.weighted_values

    def __lt__(self, other):
        return self.weighted_values < other.weighted_values

    def __eq__(self, other):
        return self.weighted_values == other.weighted_values

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return str(self.values if self.valid else tuple())
