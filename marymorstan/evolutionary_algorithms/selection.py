# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from typing import List, Optional
from operator import attrgetter
import logging

import random
import numpy
from deap.tools import emo, uniform_reference_points, selTournament

from .individual import Individual


class Selection():
    def __call__(self, population: List[Individual], current_iteration: int, *args, **kwargs):
        raise NotImplementedError

    def __str__(self):
        return self.__class__.__name__


class SelectBest(Selection):
    def __call__(self, population: List[Individual], current_iteration: int, *args, **kwargs):
        return select_k_best(population=population, k=1)


class SelectKBest(Selection):
    def __call__(self, population: List[Individual], current_iteration: int, k: int, *args, **kwargs):  # type: ignore[override]
        if callable(k):
            k = k(current_iteration)
        return select_k_best(population=population, k=k)


class SelectRandom(Selection):
    def __call__(self, population: List[Individual], current_iteration: int, *args, **kwargs):
        return select_k_random(population=population, k=1)


class SelectKRandom(Selection):
    def __call__(self, population: List[Individual], current_iteration: int, k: int, *args, **kwargs):  # type: ignore[override]
        if callable(k):
            k = k(current_iteration)
        return select_k_random(population=population, k=k)


class SelectTournament(Selection):
    def __call__(self, population: List[Individual], current_iteration: int, k: int, tournsize: Optional[int] = None, *args, **kwargs):  # type: ignore[override]
        if callable(k):
            k = k(current_iteration)
        return select_tournament(population=population, k=k, tournsize=tournsize)


class SelectNSGA2(Selection):
    def __call__(self, population: List[Individual], current_iteration: int, k: int, nd='standard', *args, **kwargs):  # type: ignore[override]
        if callable(k):
            k = k(current_iteration)
        return select_nsga_2(population=population, k=k, nd=nd)


class SelectNSGA3(Selection):
    def __call__(self, population: List[Individual], current_iteration: int, k: int, nd='log', *args, **kwargs):  # type: ignore[override]
        if callable(k):
            k = k(current_iteration)
        return select_nsga_3(population=population, k=k, nd=nd)


class SelectSPEA2(Selection):
    def __call__(self, population: List[Individual], current_iteration: int, k: int, *args, **kwargs):  # type: ignore[override]
        if callable(k):
            k = k(current_iteration)
        return select_spea_2(population=population, k=k)


def select_k_best(population: List[Individual], k: int) -> List[Individual]:
    return sorted(population, key=attrgetter('fitness'), reverse=True)[:k]


def select_k_random(population: List[Individual], k: int) -> List[Individual]:
    return [random.choice(population) for _ in range(k)]


def select_random_with_exclude(population: List[Individual], exclude: int):
    population_size = len(population)

    if population_size <= 1:
        return []

    while True:
        index_individual = numpy.random.choice(population_size)
        if index_individual != exclude:
            break

    return population[index_individual]


def select_tournament(population: List[Individual], k: int, tournsize: Optional[int]):
    # randomly select half of the population if tournsize is not specified
    if tournsize is None:
        if len(population) < 2:
            logging.warning('tournsize has been set-up to 1 since there is only one individual')
            tournsize = 1
        else:
            tournsize = int(round(.5 * len(population)))
    return selTournament(individuals=population, k=k, tournsize=tournsize)


def select_nsga_2(population: List[Individual], k: int, nd: str = 'standard') -> List[Individual]:
    return emo.selNSGA2(individuals=population, k=k, nd=nd)


def select_nsga_3(population: List[Individual], k: int, nd: str = 'log') -> List[Individual]:
    if k == 0:
        return []
    nobj = len(population[0].fitness.weights)
    ref_points = uniform_reference_points(nobj=nobj, p=4, scaling=None)
    return emo.selNSGA3(individuals=population, k=k, nd=nd, ref_points=ref_points)


def select_spea_2(population: List[Individual], k: int) -> List[Individual]:
    if k == 0:
        return []
    return emo.selSPEA2(individuals=population, k=k)
