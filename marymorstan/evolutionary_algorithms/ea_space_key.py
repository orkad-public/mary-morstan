# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from enum import Enum


class EA_SPACE_KEY(Enum):
    INITIALIZATION = "initialization"
    OPERATOR_MUTATION = "mutations"
    OPERATOR_CROSSOVER = "crossovers"
    OPERATOR_SELECTION = "selection"
    MUTATION_PROBABILITY = "mutation_probabilities"
    MUTATION_STRATEGY = "mutation_strategy"
    CROSSOVER_PROBABILITY = "crossover_probabilities"
    CROSSOVER_STRATEGY = "crossover_strategy"
