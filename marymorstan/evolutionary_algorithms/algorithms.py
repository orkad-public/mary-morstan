# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from enum import Enum


class EA_ALGORITHM(Enum):
    SIMPLE = "simple"
    MU_PLUS_LAMBDA = "mu+lambda"
    MU_COMMA_LAMBDA = "mu,lambda"
    MU_PLUS_LAMBDA_PLUS_KAPPA = "mu+lambda+kappa"


class EA_ALGORITHM_PARAMETERS():
    pass


class EAAlgorithmMuLambdaParameters(EA_ALGORITHM_PARAMETERS):
    def __init__(self, mu=None, lambda_=None):
        self._mu = mu
        self._lambda = lambda_

    @property
    def mu(self):
        return self._mu

    @property
    def lambda_(self):
        return self._lambda

    @lambda_.setter
    def lambda_(self, lambda_):
        self._lambda = lambda_


class EAAlgorithmMuPlusLambdaParameters(EAAlgorithmMuLambdaParameters):
    def __init__(self, mu=None, lambda_=None):
        super().__init__(mu=mu, lambda_=lambda_)


class EAAlgorithmMuCommaLambdaParameters(EAAlgorithmMuLambdaParameters):
    def __init__(self, mu=None, lambda_=None):
        super().__init__(mu=mu, lambda_=lambda_)


class EAAlgorithmMuPlusLambdaPlusKappaParameters(EAAlgorithmMuLambdaParameters):
    MAX_TRY = 10

    def __init__(self, mu=None, lambda_=None, kappa=None):
        super().__init__(mu=mu, lambda_=lambda_)
        self._kappa = kappa
        self._generate_method = None
        self._ea_meta_data = None

    @property
    def kappa(self):
        if self.lambda_ is not None:
            if self._kappa is None:
                return int(.1 * self.lambda_)
            if self._kappa > 0 and self._kappa <= 1.:
                kappa_ = int(self._kappa * self.lambda_)
                if kappa_ < 1:
                    raise Exception('kappa is less than 1')
                return kappa_
        return self._kappa

    def generate_kappa(self):
        offspring = []

        if self._generate_method is None:
            raise NotImplementedError

        if self._ea_meta_data is None:
            raise Exception('Missing ea meta data')

        # FIXME handle if force_unique_individuals, then ensure the generated individual does not exists
        for _ in range(self.kappa):
            generated_individual = None
            for _ in range(EAAlgorithmMuPlusLambdaPlusKappaParameters.MAX_TRY):
                generated_individual = self._generate_method()  # pylint: disable=not-callable

                if self._ea_meta_data.force_unique_individuals is False:
                    break

                if generated_individual.hash() not in self._ea_meta_data.hash_known_individuals:
                    break

            offspring.append(generated_individual)

        return offspring
