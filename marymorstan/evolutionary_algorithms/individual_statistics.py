# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from collections import defaultdict

import numpy as np
import pandas as pd


class IndividualStatistics():
    def __init__(self, individual):
        self.__individual = individual
        self.last_variation_at_generation = None

        self.variations = defaultdict(list)

    @property
    def current_generation(self):
        return self.__individual.meta_data.current_iteration

    def append_variation(self, name, parameters=None, **kwargs):
        infos = {'name': name, 'parameters': parameters, **kwargs}
        self.variations[self.current_generation].append(infos)
        self.last_variation_at_generation = self.current_generation

    def number_of_variations(self, generation=None):
        if not generation:
            return np.sum([len(v) for _, v in self.variations.items()])

        return len(self.variations[generation])

    def to_pandas(self):
        dataframe = pd.DataFrame()

        for generation, variations in self.variations.items():
            tmp_dataframe = pd.DataFrame.from_dict(variations, orient='columns')
            tmp_dataframe['variation_occurs_at_generation'] = generation
            tmp_dataframe['last_variation_at_generation'] = self.last_variation_at_generation
            tmp_dataframe['individual_id'] = self.__individual.identifier
            tmp_dataframe['individual_hash'] = self.__individual.hash()
            tmp_dataframe['individual'] = str(self.__individual)
            # dataframe = dataframe.append(tmp_dataframe)
            dataframe = pd.concat([dataframe, tmp_dataframe])

        return dataframe
