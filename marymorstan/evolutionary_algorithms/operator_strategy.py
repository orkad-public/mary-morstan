# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from __future__ import division

import logging
from enum import Enum

from .utils import extract_fractions


class InvalidDistribution(Exception):
    pass


class OPERATOR_DISTRIBUTION_STRATEGY(Enum):
    STATIC_DISTRIBUTION = "static_distribution"
    TIME_BASED_DISTRIBUTION = "time_based_distribution"


class DistributionStrategy:
    @staticmethod
    def strategy():
        raise NotImplementedError

    def get_probabilities(self, **kwargs):
        raise NotImplementedError

    @staticmethod
    def is_uniform_probabilities(probabilities):
        return probabilities is None or probabilities == 'uniform' or len(probabilities) == 0

    @staticmethod
    def build_uniform_probabilities(len_operators):
        if len_operators <= 0:
            return []
        return [1. / len_operators] * len_operators


class StaticDistribution(DistributionStrategy):
    @staticmethod
    def strategy():
        return OPERATOR_DISTRIBUTION_STRATEGY.STATIC_DISTRIBUTION

    def __init__(self, probabilities=None, len_operators=None):
        if probabilities is None and len_operators is None:
            raise InvalidDistribution('Please specify probabilities or len_operators')

        self.probabilities = probabilities

        if DistributionStrategy.is_uniform_probabilities(self.probabilities):
            self.probabilities = DistributionStrategy.build_uniform_probabilities(len_operators)

        self.probabilities = extract_fractions(self.probabilities)

    def get_probabilities(self):  # pylint: disable=arguments-differ
        return self.probabilities


class TimeBasedDistribution(DistributionStrategy):
    @staticmethod
    def strategy():
        return OPERATOR_DISTRIBUTION_STRATEGY.TIME_BASED_DISTRIBUTION

    def __init__(self, probabilities, len_operators):
        self.probabilities_per_tick = TimeBasedDistribution._extract_probabilities(probabilities, len_operators)

    @staticmethod
    def _extract_probabilities(probabilities, len_operators):
        if probabilities is None:
            raise InvalidDistribution("Please specify probabilities")

        if not isinstance(probabilities, list):
            raise InvalidDistribution("probabilities are not well formatted, it should be a list of dict")

        if len_operators is None:
            logging.warning("The number of operators is not specified yet, "
                            "we can't check if the probabilities are well defined")
        else:
            elapsed_time_sum_ratio = 0
            for e in probabilities:
                elapsed_time_sum_ratio += float(list(e.keys())[0])
                for p in e.values():
                    if not DistributionStrategy.is_uniform_probabilities(p) and len(p) != len_operators:
                        raise InvalidDistribution(f"The number of operators ({len_operators}) "
                                                  f"differs to the number of probabilities ({len(p)})")
            if elapsed_time_sum_ratio != 1:
                raise InvalidDistribution("The sum of the elapsed time ratios should be equal to 1")

        probabilities_per_tick = []

        for e in probabilities:
            time_ratio = float(list(e.keys())[0])
            probabilities = list(e.values())[0]

            if DistributionStrategy.is_uniform_probabilities(probabilities):
                probabilities = DistributionStrategy.build_uniform_probabilities(len_operators)
            else:
                probabilities = extract_fractions(probabilities)

            probabilities_per_tick.append((time_ratio, probabilities))

        return probabilities_per_tick

    def _get_tick_index(self, elapsed_time, total_time):
        if elapsed_time is None:
            message = 'Elapsed time cannot be negative or null'
            logging.error(message)
            raise Exception(message)

        if total_time is None or total_time <= 0:
            message = 'Total time cannot be negative or null'
            logging.error(message)
            raise Exception(message)

        if len(self.probabilities_per_tick) == 0:
            message = 'probabilities_per_tick is empty'
            logging.error(message)
            raise Exception(message)

        ratio = elapsed_time / total_time

        accumulated_time_ratio = 0

        index = 0

        for index, e in enumerate(self.probabilities_per_tick):
            time_ratio = e[0]
            accumulated_time_ratio += time_ratio
            if ratio <= accumulated_time_ratio:
                return index

        logging.warning('The elapsed time %d exceed the total time %d, '
                        'or the time based distribution has not been correctly set-up', elapsed_time, total_time)
        return index

    def get_probabilities(self, elapsed_time, total_time):  # pylint: disable=arguments-differ
        tick_index = self._get_tick_index(elapsed_time, total_time)
        return self.probabilities_per_tick[tick_index][1]
