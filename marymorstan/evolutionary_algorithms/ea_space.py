# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from collections.abc import Sequence
import importlib
import logging

from pkg_resources import resource_filename

from ..utils import read_yaml, keys_exists

from .ea_space_key import EA_SPACE_KEY
from .algorithms import EA_ALGORITHM, EAAlgorithmMuPlusLambdaParameters, EAAlgorithmMuCommaLambdaParameters, EAAlgorithmMuPlusLambdaPlusKappaParameters
from .operator_strategy import InvalidDistribution, StaticDistribution, TimeBasedDistribution, OPERATOR_DISTRIBUTION_STRATEGY


DEFAULT_PACKAGE = "marymorstan.evolutionary_algorithms"


class InvalidEASpace(Exception):
    pass


class EASpace(Sequence):  # pylint: disable=too-many-public-methods
    def __init__(self, evolutionary_algorithms_space_file=None, check_ea_space_file=True):
        if not evolutionary_algorithms_space_file:
            evolutionary_algorithms_space_file = EASpace.get_default_space_file()

        logging.debug("EA space defined from %s", evolutionary_algorithms_space_file)
        self.ea_space = read_yaml(evolutionary_algorithms_space_file)

        if check_ea_space_file:
            try:
                self.check_ea_space()
            except InvalidEASpace as e:
                logging.error("EA space is not well formatted: %s", e)
                raise e

    def set_parameters(self, parameters):
        if parameters is None:
            return
        for k, v in parameters.items():
            logging.debug('EA space, override parameter %s to %s', k, v)
            self.ea_space[k] = v

    @property
    def parameters(self):
        return self.ea_space

    def __len__(self):
        return len(self.ea_space)

    def __getitem__(self, index):
        return self.parameters[index]

    def __contains__(self, item):
        return item in self.parameters

    @staticmethod
    def get_default_space_file():
        return resource_filename(__name__, 'simple_ea_space.yml')

    def check_ea_space(self):
        if 'version' not in self.ea_space:
            raise InvalidEASpace('Please specify the version of the EA Space')

        for operator_type in ['mutation', 'crossover']:
            key_portion_of_population = f'{operator_type}_portion_of_population'
            if key_portion_of_population not in self.ea_space:
                message = f"{key_portion_of_population} is missing"
                logging.error(message)
                raise InvalidEASpace(message)

        if self.mutation_portion_of_population > 1 or self.mutation_portion_of_population < 0:
            raise InvalidEASpace("mutation_portion_of_population should be between 0 and 1")

        if self.crossover_portion_of_population > 1 or self.crossover_portion_of_population < 0:
            raise InvalidEASpace("crossover_portion_of_population should be between 0 and 1")

        total_portion_of_population = self.mutation_portion_of_population + self.crossover_portion_of_population

        if total_portion_of_population > 1 or total_portion_of_population < 0:
            raise InvalidEASpace("the sum of mutation_portion_of_population and crossover_portion_of_population should be between 0 and 1")

        message = "please define at least one crossover or one mutation"

        if not self._one_mutation_exists() and not self._one_crossover_exists():
            logging.error(message)
            raise InvalidEASpace(message)

        if not keys_exists(self.ea_space, EA_SPACE_KEY.OPERATOR_SELECTION.value) \
           or self.ea_space[EA_SPACE_KEY.OPERATOR_SELECTION.value] is None:
            message = f"{EA_SPACE_KEY.OPERATOR_SELECTION.value} is not defined"
            logging.error(message)
            raise InvalidEASpace(message)

    def _one_mutation_exists(self):
        return keys_exists(self.parameters, EA_SPACE_KEY.OPERATOR_MUTATION.value) \
            and not self.parameters[EA_SPACE_KEY.OPERATOR_MUTATION.value] is None

    def _one_crossover_exists(self):
        return keys_exists(self.parameters, EA_SPACE_KEY.OPERATOR_CROSSOVER.value) \
            and not self.parameters[EA_SPACE_KEY.OPERATOR_CROSSOVER.value] is None

    def mutation_instances(self):
        return self._operator_instances(operator_type='mutation')

    def crossover_instances(self):
        return self._operator_instances(operator_type='crossover')

    def mutation_distribution_strategy_instance(self):
        return self._operator_distribution_strategy_instance(operator_type='mutation')

    def crossover_distribution_strategy_instance(self):
        return self._operator_distribution_strategy_instance(operator_type='crossover')

    @property
    def mutation_strategy(self):
        return self._operator_strategy(operator_type='mutation')

    @property
    def crossover_strategy(self):
        return self._operator_strategy(operator_type='crossover')

    @property
    def mutation_portion_of_population(self):
        return self.parameters['mutation_portion_of_population']

    @property
    def crossover_portion_of_population(self):
        return self.parameters['crossover_portion_of_population']

    @property
    def ea_algorithm(self):
        if 'ea_algorithm' not in self.parameters:
            return None

        return EA_ALGORITHM(self.parameters['ea_algorithm'])

    def initialization_instance(self):
        if 'initialization' not in self.parameters:
            return None

        return self._instance(key=EA_SPACE_KEY.INITIALIZATION)

    @property
    def ea_algorithm_parameters(self):
        if 'ea_algorithm_parameters' not in self.parameters:
            return None

        if self.ea_algorithm is None:
            message = "ea_algorithm is not defined in the space"
            logging.error(message)
            raise InvalidEASpace(message)

        if self.ea_algorithm == EA_ALGORITHM.MU_PLUS_LAMBDA:
            parameters = self.parameters['ea_algorithm_parameters']
            return EAAlgorithmMuPlusLambdaParameters(mu=parameters['mu'], lambda_=parameters['lambda_'])

        if self.ea_algorithm == EA_ALGORITHM.MU_COMMA_LAMBDA:
            parameters = self.parameters['ea_algorithm_parameters']
            return EAAlgorithmMuCommaLambdaParameters(mu=parameters['mu'], lambda_=parameters['lambda_'])

        # FIXME handle generate method from ea space
        if self.ea_algorithm == EA_ALGORITHM.MU_PLUS_LAMBDA_PLUS_KAPPA:
            parameters = self.parameters['ea_algorithm_parameters']
            return EAAlgorithmMuPlusLambdaPlusKappaParameters(mu=parameters['mu'], lambda_=parameters['lambda_'], kappa=parameters['kappa'])

        return None

    @property
    def one_mutation_per_individual(self):
        if 'one_mutation_per_individual' in self.parameters:
            return self.parameters['one_mutation_per_individual']
        return False

    @property
    def one_crossover_per_individual(self):
        if 'one_crossover_per_individual' in self.parameters:
            return self.parameters['one_crossover_per_individual']
        return False

    @property
    def one_variation_per_individual(self):
        if 'one_variation_per_individual' in self.parameters:
            return self.parameters['one_variation_per_individual']
        return False

    @property
    def number_of_try_recall_variation(self):
        if 'number_of_try_recall_variation' in self.parameters:
            return self.parameters['number_of_try_recall_variation']
        return 1

    @property
    def force_unique_individuals(self):
        if 'force_unique_individuals' in self.parameters:
            return self.parameters['force_unique_individuals']
        return False

    @property
    def force_other_variations_if_fail(self):
        if 'force_other_variations_if_fail' in self.parameters:
            return self.parameters['force_other_variations_if_fail']
        return False

    def _operator_strategy(self, operator_type: str):
        if operator_type == 'mutation':
            strategy_operator_name = EA_SPACE_KEY.MUTATION_STRATEGY.value
        elif operator_type == 'crossover':
            strategy_operator_name = EA_SPACE_KEY.CROSSOVER_STRATEGY.value
        else:
            raise Exception(f"Unknown strategy_operator_name {operator_type}")

        if strategy_operator_name not in self.parameters:
            return OPERATOR_DISTRIBUTION_STRATEGY.STATIC_DISTRIBUTION

        return OPERATOR_DISTRIBUTION_STRATEGY(self.parameters[strategy_operator_name])

    def _operator_distribution_strategy_instance(self, operator_type: str):
        if operator_type == 'mutation':
            probability_operator_name = EA_SPACE_KEY.MUTATION_PROBABILITY.value
        elif operator_type == 'crossover':
            probability_operator_name = EA_SPACE_KEY.CROSSOVER_PROBABILITY.value
        else:
            raise Exception(f"Unknown probability_operator_name {operator_type}")

        strategy = self._operator_strategy(operator_type)
        probabilities = self.parameters[probability_operator_name] if (probability_operator_name
                                                                       in self.parameters) else None
        len_operators = len(self._operators(operator_type))

        if strategy == OPERATOR_DISTRIBUTION_STRATEGY.STATIC_DISTRIBUTION:
            try:
                return StaticDistribution(probabilities=probabilities, len_operators=len_operators)
            except InvalidDistribution as e:
                raise InvalidEASpace(e) from e

        if strategy == OPERATOR_DISTRIBUTION_STRATEGY.TIME_BASED_DISTRIBUTION:
            return TimeBasedDistribution(probabilities=probabilities, len_operators=len_operators)

        raise InvalidEASpace(f'Unknown distribution strategy {strategy}')

    def selection_instance(self):
        return self._instance(key=EA_SPACE_KEY.OPERATOR_SELECTION)

    def _instance(self, key):
        class_name, parameters = EASpace._parse_method(raw_method=self.parameters[key.value])

        module_name = ".".join(class_name.split('.')[:-1])
        module = importlib.import_module(module_name)

        method = getattr(module, class_name.split('.')[-1])

        return method, parameters

    @staticmethod
    def _instances_parse(operators: list):
        instances = []

        for operator in operators:
            class_name, kwargs_parameters = EASpace._parse_method(raw_method=operator)
            instance = EASpace._instanciate_from_ea_space(function_name=class_name, **kwargs_parameters)
            instances.append(instance)

        return instances

    @staticmethod
    def _parse_method(raw_method):
        class_name = raw_method
        kwargs_parameters: dict = {}

        # in case parameters are present as a dict
        if isinstance(raw_method, dict):
            class_name = list(raw_method.keys())[0]
            parameters = list(raw_method.values())[0]

            if isinstance(parameters, dict):
                kwargs_parameters.update(parameters)
            else:  # consider a list of dicts
                for parameter in parameters:
                    kwargs_parameters.update(parameter)
        elif '(' in raw_method and ')' in raw_method:  # parameters are present between parenthesis
            class_name = raw_method.split('(')[0]
            parameters = raw_method[raw_method.find("(") + 1:raw_method.find(")")]

            for parameter in parameters.split(','):
                key_value = parameter.split('=')
                kwargs_parameters.update({key_value[0].lstrip(): eval(key_value[1])})  # pylint: disable=eval-used

        return class_name, kwargs_parameters

    def _operators(self, operator_type: str):
        if operator_type == 'mutation':
            if EA_SPACE_KEY.OPERATOR_MUTATION.value not in self.parameters:
                return []
            return self.parameters[EA_SPACE_KEY.OPERATOR_MUTATION.value]

        if operator_type == 'crossover':
            if EA_SPACE_KEY.OPERATOR_CROSSOVER.value not in self.parameters:
                return []
            return self.parameters[EA_SPACE_KEY.OPERATOR_CROSSOVER.value]

        raise Exception(f"Unknown operator_type {operator_type}")

    def _operator_instances(self, operator_type: str):
        return EASpace._instances_parse(self._operators(operator_type))

    @staticmethod
    def _instanciate_from_ea_space(function_name: str, **kwargs):
        module_name = ".".join(function_name.split('.')[:-1])
        function_name = function_name.split('.')[-1]

        if not module_name:
            message = f"Impossible to determine the module for '{function_name}'"
            logging.error(message)
            raise InvalidEASpace(message)

        try:
            module = importlib.import_module(module_name)
            function = getattr(module, function_name)
        except Exception as e:
            message = f"Impossible to import the module '{function_name}' -> {e}"
            logging.error(message)
            raise InvalidEASpace(message) from e

        try:
            return function(**kwargs)
        except TypeError as e:
            message = f"Seems like one of these parameters : {kwargs}; do not exist in {function_name} -> {e}"
            logging.error(message)
            raise InvalidEASpace(message) from e
