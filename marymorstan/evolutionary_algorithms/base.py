# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from typing import Optional, List, Union
from enum import Enum
import time
import logging
import threading

from functools import partial

from numpy import random

from .meta_data import EAMetaData
from .algorithms import (EA_ALGORITHM, EA_ALGORITHM_PARAMETERS, EAAlgorithmMuPlusLambdaParameters,
                         EAAlgorithmMuCommaLambdaParameters, EAAlgorithmMuPlusLambdaPlusKappaParameters)
from .individual import Individual
from .mutation import SimpleMutations, CustomizableMutations
from .crossover import SimpleCrossovers, CustomizableCrossovers
from .variation import Variation
from .selection import select_k_best
from .statistics import Statistics
from .utils import random_state_parser, process_meta_data_reset_variation, \
    process_meta_data_current_iteration, process_ea_meta_data_init_known_individuals


class EA_METHODS(Enum):
    VARIATION = "variate"
    EVALUATION = "evaluate"
    SELECTION = "select"
    TERMINATION = "stop_criterion"


class EA():
    def __init__(self,
                 algorithm: EA_ALGORITHM = EA_ALGORITHM.MU_PLUS_LAMBDA,
                 algorithm_parameters: EA_ALGORITHM_PARAMETERS = None,   # type: ignore[assignment]
                 force_unique_individuals: bool = False,
                 force_revaluation: bool = False,
                 wall_time: Optional[int] = None,
                 statistics: Optional[Statistics] = None,
                 random_state: Optional[Union[random.RandomState, int]] = None):
        self.ea_meta_data = EAMetaData(force_unique_individuals=force_unique_individuals)
        self.force_revaluation = force_revaluation
        self.wall_time = wall_time
        self._statistics = statistics

        self._algorithm = algorithm
        self.algorithm_parameters = algorithm_parameters
        self._current_iteration = 0
        self._random_state = random_state_parser(random_state)

    @property
    def current_iteration(self):
        return self._current_iteration

    def increment_current_iteration(self):
        self._current_iteration += 1

    @property
    def algorithm_parameters(self):
        return self._algorithm_parameters

    @algorithm_parameters.setter
    def algorithm_parameters(self, algorithm_parameters):
        if self._algorithm == EA_ALGORITHM.MU_PLUS_LAMBDA and algorithm_parameters is None:
            self._algorithm_parameters = EAAlgorithmMuPlusLambdaParameters()
            return
        if self._algorithm == EA_ALGORITHM.MU_COMMA_LAMBDA and algorithm_parameters is None:
            self._algorithm_parameters = EAAlgorithmMuCommaLambdaParameters()
            return
        if self._algorithm == EA_ALGORITHM.MU_PLUS_LAMBDA_PLUS_KAPPA and algorithm_parameters is None:
            self._algorithm_parameters = EAAlgorithmMuPlusLambdaPlusKappaParameters()
            return
        self._algorithm_parameters = algorithm_parameters

    @property
    def algorithm(self):
        error_message = f'Please specify adequate parameters to {self._algorithm}'

        if self._algorithm == EA_ALGORITHM.MU_PLUS_LAMBDA and not isinstance(self._algorithm_parameters,
                                                                             EAAlgorithmMuPlusLambdaParameters):
            raise Exception(error_message)

        if self._algorithm == EA_ALGORITHM.MU_COMMA_LAMBDA and not isinstance(self._algorithm_parameters,
                                                                              EAAlgorithmMuCommaLambdaParameters):
            raise Exception(error_message)

        if (self._algorithm == EA_ALGORITHM.MU_PLUS_LAMBDA_PLUS_KAPPA and not isinstance(self._algorithm_parameters, EAAlgorithmMuPlusLambdaPlusKappaParameters)):
            raise Exception(error_message)

        return self._algorithm

    @algorithm.setter
    def algorithm(self, algorithm):
        if not isinstance(algorithm, EA_ALGORITHM):
            raise Exception(f"Invalid EA algorithm {algorithm}")
        self._algorithm = algorithm

    @property
    def statistics(self):
        return self._statistics

    @property
    def random_state(self):
        return self._random_state

    def variate(self,
                population: List[Individual],
                offspring_size: int,
                current_iteration: int,
                elapsed_time: Optional[int] = None,
                total_time: Optional[int] = None,
                global_stop_optimization_event: Optional[threading.Event] = None) -> List[Individual]:
        """
        Determine how to variate the population (i.e., crossover, mutations) in order to return the offspring
        """
        raise NotImplementedError

    def evaluate(self, population: List[Individual], current_iteration: int, *args, **kwargs):
        raise NotImplementedError

    def select(self, population: List[Individual], current_iteration: int, *args, **kwargs):
        raise NotImplementedError

    def stop_criterion(self, *args, **kwargs):
        raise NotImplementedError

    # NOTE avoid costly evaluation
    def population_to_evaluate(self, population: List[Individual]):
        # TODO should be optimized, in the case of SH we need to revaluate the whole population
        # because the sample size is changing, BUT not necessarily at each iteration.
        if self.force_revaluation is True:
            return population

        to_evaluate = []

        for individual in population:
            if individual.meta_data.had_variation is True or individual.fitness.values is None:
                to_evaluate.append(individual)
            else:
                logging.debug("Skip evaluation for individual (hash: %s - id: %s) (already evaluated)",
                              individual.hash(), individual.identifier)

        if self.statistics:
            ratio = len(to_evaluate) / float(len(population))
            self.statistics.append_ratio_of_evaluations(ratio)

        return to_evaluate

    def optimize(self, population: List[Individual], *args, **kwargs):  # pylint: disable=too-many-branches,too-many-statements
        current_population = population
        initial_time = time.time()

        global_stop_optimization_event = kwargs.get('global_stop_optimization_event', None)

        if self.algorithm in (EA_ALGORITHM.MU_PLUS_LAMBDA, EA_ALGORITHM.MU_COMMA_LAMBDA,
                              EA_ALGORITHM.MU_PLUS_LAMBDA_PLUS_KAPPA):
            if 'k' in kwargs and self.algorithm_parameters.mu is not None:
                # FIXME could make trouble with SH concept (when k is callable)
                logging.debug('Changing k: %d by mu: %d selection', kwargs['k'], self.algorithm_parameters.mu)
                kwargs['k'] = self.algorithm_parameters.mu

            if self.algorithm_parameters.lambda_ is None:
                self.algorithm_parameters.lambda_ = len(current_population)

        if self.statistics:
            self.statistics.set_individuals(individuals=current_population, generation=self.current_iteration)
            start_time = time.time()

        if self.ea_meta_data.force_unique_individuals is True:
            process_ea_meta_data_init_known_individuals(population=current_population,
                                                        hash_known_individuals=self.ea_meta_data.hash_known_individuals)

        logging.info("Evaluate initial population (current_iteration: %d)", self.current_iteration)
        self.evaluate(current_population, self.current_iteration)  # , global_stop_optimization_event

        if global_stop_optimization_event is not None and global_stop_optimization_event.is_set():
            population[:] = current_population
            return population

        if self.statistics:
            elapsed_time = time.time() - start_time
            self.statistics.set_selected_individuals(individuals=current_population, generation=self.current_iteration)
            self.statistics.append_elapsed_time(elapsed_time=elapsed_time, iteration=self.current_iteration)
            self.statistics.set_best_individual(individuals=current_population, generation=self.current_iteration)
            self.statistics.set_diversity(individuals=current_population, generation=self.current_iteration)

        logging.info("Start the loop EA process")
        while (not self.stop_criterion(current_iteration=self.current_iteration) and (global_stop_optimization_event is None or not global_stop_optimization_event.is_set())):
            self.increment_current_iteration()

            process_meta_data_current_iteration(current_population, self.current_iteration)

            if self.statistics:
                elapsed_time = 0
                start_time = time.time()

            if self.algorithm == EA_ALGORITHM.SIMPLE:
                offspring_size = len(current_population)
            elif self.algorithm in (EA_ALGORITHM.MU_PLUS_LAMBDA, EA_ALGORITHM.MU_COMMA_LAMBDA,
                                    EA_ALGORITHM.MU_PLUS_LAMBDA_PLUS_KAPPA):
                offspring_size = self.algorithm_parameters.lambda_

            total_elapsed_time = int(time.time() - initial_time)

            logging.info("Apply variations (current_iteration: %d)", self.current_iteration)
            offspring = self.variate(current_population,
                                     offspring_size,
                                     self.current_iteration,
                                     total_elapsed_time,
                                     self.wall_time,
                                     global_stop_optimization_event)

            if self.algorithm == EA_ALGORITHM.MU_PLUS_LAMBDA_PLUS_KAPPA:
                logging.info("Generate kappa individuals (current_iteration: %d)", self.current_iteration)
                offspring += self.algorithm_parameters.generate_kappa()

            logging.info("Evaluate new population (current_iteration: %d)", self.current_iteration)
            self.evaluate(self.population_to_evaluate(offspring), self.current_iteration)  # global_stop_optimization_event

            if global_stop_optimization_event is not None and global_stop_optimization_event.is_set():
                population[:] = current_population
                return population

            if self.algorithm in (EA_ALGORITHM.SIMPLE, EA_ALGORITHM.MU_COMMA_LAMBDA):
                current_population = offspring
            elif self.algorithm in (EA_ALGORITHM.MU_PLUS_LAMBDA, EA_ALGORITHM.MU_PLUS_LAMBDA_PLUS_KAPPA):
                current_population = current_population + offspring

            if self.statistics:
                elapsed_time += time.time() - start_time
                self.statistics.set_individuals(individuals=current_population, generation=self.current_iteration)
                start_time = time.time()

            # TODO currently mu + lambda, give the possiblity to not keep parent (i.e. current) population ?!
            #  and to keep all individual in memory
            logging.info("Selecting the next generation (current_iteration: %d, current population size: %d)",
                         self.current_iteration, len(population))
            current_population = self.select(current_population, self.current_iteration, *args, **kwargs)
            logging.info("Population size of selected individuals: %d", len(current_population))
            logging.debug("Selected individual ids: %s",
                          [individual.identifier for individual in current_population])

            if self.statistics:
                elapsed_time += time.time() - start_time
                # FIXME ? still include time with statistics inside variation/selection
                self.statistics.set_selected_individuals(individuals=current_population, generation=self.current_iteration)
                self.statistics.set_best_individual(individuals=current_population, generation=self.current_iteration)
                self.statistics.append_elapsed_time(elapsed_time=elapsed_time, iteration=self.current_iteration)
                self.statistics.set_diversity(individuals=current_population, generation=self.current_iteration)

            # NOTE temporary information to know if candidate had a variation or not
            process_meta_data_reset_variation(population=current_population)

            population[:] = current_population

        if global_stop_optimization_event is not None and not global_stop_optimization_event.is_set():
            global_stop_optimization_event.set()

        return population


class SimpleEA(EA):
    def __init__(self, generations,
                 evaluation_method,
                 mutation_probability=.9,
                 crossover_probability=.1,
                 force_unique_individuals: bool = False,
                 force_revaluation: bool = False,
                 wall_time: Optional[int] = None,
                 statistics: Optional[Statistics] = None,
                 random_state: Optional[Union[random.RandomState, int]] = None):
        # FIXME should be algorithm=EA_ALGORITHM.SIMPLE
        super().__init__(force_unique_individuals=force_unique_individuals,
                         force_revaluation=force_revaluation,
                         wall_time=wall_time,
                         statistics=statistics,
                         random_state=random_state)

        assert generations > 0
        assert crossover_probability + mutation_probability <= 1.

        self.generations = generations
        self._evaluation = evaluation_method

        mutation = SimpleMutations(portion_of_population=mutation_probability,
                                   random_state=self.random_state)

        crossover = SimpleCrossovers(portion_of_population=crossover_probability,
                                     random_state=self.random_state)

        self._variation = Variation(ea_meta_data=self.ea_meta_data,
                                    mutation=mutation,
                                    crossover=crossover,
                                    statistics=self.statistics,
                                    random_state=self.random_state)

    def variate(self,
                population: List[Individual],
                offspring_size: int,
                current_iteration: int,
                elapsed_time: Optional[int] = None,
                total_time: Optional[int] = None,
                global_stop_optimization_event: Optional[threading.Event] = None):
        return self._variation(population=population,
                               offspring_size=offspring_size,
                               current_iteration=current_iteration,
                               elapsed_time=elapsed_time,
                               total_time=total_time,
                               global_stop_optimization_event=global_stop_optimization_event,
                               one_mutation_per_individual=True,
                               one_crossover_per_individual=True,
                               number_of_try_recall_variation=1,
                               force_other_variations_if_fail=False)

    def evaluate(self,
                 population: List[Individual],
                 current_iteration: int,
                 *args, **kwargs):
        self._evaluation(population, current_iteration)

    def select(self, population: List[Individual], current_iteration: int, *args, **kwargs):
        return select_k_best(population=population, k=kwargs['k'])

    def stop_criterion(self, *args, **kwargs):
        if 'current_iteration' not in kwargs:
            raise Exception("Impossible to perform SimpleEA, current_iteration is missing")
        return kwargs['current_iteration'] >= self.generations


class CustomizableEA(EA):
    def __init__(self,
                 generations,
                 algorithm: EA_ALGORITHM = EA_ALGORITHM.MU_PLUS_LAMBDA,
                 algorithm_parameters: EA_ALGORITHM_PARAMETERS = None,     # type: ignore[assignment]
                 force_unique_individuals: bool = False,
                 wall_time: Optional[int] = None,
                 force_revaluation: bool = False,
                 statistics: Optional[Statistics] = None,
                 random_state: Optional[Union[random.RandomState, int]] = None):
        super().__init__(algorithm=algorithm,
                         algorithm_parameters=algorithm_parameters,
                         force_unique_individuals=force_unique_individuals,
                         force_revaluation=force_revaluation,
                         wall_time=wall_time,
                         statistics=statistics,
                         random_state=random_state)
        self.generations = generations

    def variate(self,
                population: List[Individual],
                offspring_size: int,
                current_iteration: int,
                elapsed_time: Optional[int] = None,
                total_time: Optional[int] = None,
                global_stop_optimization_event: Optional[threading.Event] = None):
        raise NotImplementedError

    def evaluate(self,
                 population: List[Individual],
                 current_iteration: int,
                 *args, **kwargs):
        raise NotImplementedError

    def select(self, population: List[Individual], current_iteration: int, *args, **kwargs):
        raise NotImplementedError

    def stop_criterion(self, *args, **kwargs):
        raise NotImplementedError

    def _load_from_file_mutations(self, ea_space):
        custom_mutation = CustomizableMutations(portion_of_population=ea_space.mutation_portion_of_population,
                                                distribution_strategy=ea_space.mutation_distribution_strategy_instance(),
                                                random_state=self.random_state)
        mutations = ea_space.mutation_instances()

        if len(mutations) == 0:
            return None

        for mutation in mutations:
            custom_mutation.append(mutation, random_state=custom_mutation.random_state)

        return custom_mutation

    def _load_from_file_crossovers(self, ea_space):
        custom_crossover = CustomizableCrossovers(portion_of_population=ea_space.crossover_portion_of_population,
                                                  distribution_strategy=ea_space.crossover_distribution_strategy_instance(),
                                                  random_state=self.random_state)
        crossovers = ea_space.crossover_instances()

        if len(crossovers) == 0:
            return None

        for crossover in crossovers:
            custom_crossover.append(crossover, random_state=custom_crossover.random_state)

        return custom_crossover

    def load_from_ea_space(self, ea_space):
        self.algorithm = EA_ALGORITHM.MU_PLUS_LAMBDA if ea_space.ea_algorithm is None else ea_space.ea_algorithm
        self.algorithm_parameters = ea_space.ea_algorithm_parameters

        self.ea_meta_data.force_unique_individuals = ea_space.force_unique_individuals

        custom_mutation = self._load_from_file_mutations(ea_space=ea_space)
        custom_crossover = self._load_from_file_crossovers(ea_space=ea_space)

        variation = Variation(ea_meta_data=self.ea_meta_data,
                              mutation=custom_mutation,
                              crossover=custom_crossover,
                              one_mutation_per_individual=ea_space.one_mutation_per_individual,
                              one_crossover_per_individual=ea_space.one_crossover_per_individual,
                              one_variation_per_individual=ea_space.one_variation_per_individual,
                              number_of_try_recall_variation=ea_space.number_of_try_recall_variation,
                              force_other_variations_if_fail=ea_space.force_other_variations_if_fail,
                              random_state=self.random_state,
                              statistics=self.statistics)

        self.register(EA_METHODS.VARIATION, variation)

        selection_method, parameters = ea_space.selection_instance()
        self.register(EA_METHODS.SELECTION, selection_method(), **parameters)
        return ea_space

    def register(self, method_name: EA_METHODS, function, *args, **kwargs):
        partial_function = partial(function, *args, **kwargs)
        partial_function.__name__ = method_name  # type: ignore[attr-defined]
        partial_function.__doc__ = function.__doc__

        if hasattr(function, "__dict__") and not isinstance(function, type):
            # Some functions don't have a dictionary, in these cases
            # simply don't copy it. Moreover, if the function is actually
            # a class, we do not want to copy the dictionary.
            partial_function.__dict__.update(function.__dict__.copy())

        setattr(self, method_name.value, partial_function)

    def optimize(self, population: List[Individual], *args, **kwargs):
        for method in EA_METHODS:
            if not hasattr(self, method.value):
                raise NotImplementedError(f"The method {method} has not been registered")

        return super().optimize(population, *args, **kwargs)
