# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from __future__ import division
from typing import Optional, List, Union
import inspect
import copy
from fractions import Fraction

from numpy import random

from .individual import Individual


def random_state_parser(random_state: Optional[Union[random.RandomState, int]] = None):
    if random_state is None:
        return random.RandomState()

    if isinstance(random_state, int):
        return random.RandomState(random_state)

    return random_state


def process_meta_data_current_iteration(population: List[Individual], current_iteration: int):
    for individual in population:
        individual.meta_data.current_iteration = current_iteration


def process_meta_data_reset_variation(population: List[Individual]):
    for individual in population:
        del individual.meta_data.had_variation


def process_ea_meta_data_init_known_individuals(population: List[Individual], hash_known_individuals: set):
    for individual in population:
        hash_known_individuals.add(individual.hash())


def get_name(variable):
    if inspect.isfunction(variable):
        return variable.__name__

    if hasattr(variable, '__str__'):
        return str(variable)

    raise Exception(f"Unable to find the name of the variable {variable}")


def copy_population(population: List[Individual], n: int = None,    # type: ignore[assignment]
                    random_state: Optional[Union[random.RandomState, int]] = None):
    if n is None or n == len(population):
        return copy.deepcopy(population)

    random_state = random_state_parser(random_state)

    if n > len(population):
        duplicate_population = []

        for _ in range(n // len(population)):
            duplicate_population += copy.deepcopy(population)

        return duplicate_population + copy_population(population, n=n % len(population))

    random_indexes = random_state.choice(range(len(population)), n, replace=False).tolist()

    return copy.deepcopy([population[i] for i in random_indexes])


def randomly_pick_individual(population: List[Individual],
                             random_state: Optional[Union[random.RandomState, int]] = None):
    random_state = random_state_parser(random_state)
    index = random_state.randint(0, len(population))
    return population[index], index


def diversity_ratio(population: List[Individual]):
    hashes = lambda p: [i.hash() for i in p]  # noqa: E731
    return float(len(set(hashes(population)))) / len(hashes(population))


def balance_probabilities(probabilities: List[float]):
    sum_probabilities = sum(probabilities)

    if sum_probabilities == 1:
        return probabilities

    leftover = 1 - sum_probabilities
    new_probabilities = list(map(lambda prob: prob + leftover * (prob * 1 / sum_probabilities), probabilities))

    return new_probabilities


def extract_fractions(list_of_str):
    fractions = []
    for v in list_of_str:
        fractions.append(float(Fraction(v)))
    return fractions
