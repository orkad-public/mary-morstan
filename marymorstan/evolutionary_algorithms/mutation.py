# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)

from __future__ import annotations

from typing import Union, Sequence, Optional, List, Iterator, Callable
import collections
import collections.abc
import inspect
import logging

from itertools import repeat
from functools import partial

from numpy import random

from .utils import random_state_parser, get_name
from .individual import Individual
from .operator_strategy import DistributionStrategy, StaticDistribution


class Mutations():  # type: ignore[misc] # NOTE see https://stackoverflow.com/questions/52652595/callable-is-invalid-base-class
    def __init__(self,
                 portion_of_population: float = .9,
                 distribution_strategy: Optional[DistributionStrategy] = None,
                 random_state: Optional[random.RandomState] = None):
        self.portion_of_population = portion_of_population
        self._distribution_strategy = distribution_strategy

        self.random_state = random_state_parser(random_state)

    @property
    def distribution_strategy(self):
        if self._distribution_strategy is None:
            logging.warning('distribution was not specified, we assumed it is uniform')
            return StaticDistribution(len_operators=len(self.mutations))
        return self._distribution_strategy

    @property
    def mutations(self) -> List:
        raise NotImplementedError


class CustomizableMutations(Mutations):
    def __init__(self,
                 portion_of_population: float = .9,
                 distribution_strategy: Optional[DistributionStrategy] = None,
                 random_state: Optional[random.RandomState] = None):
        super().__init__(portion_of_population=portion_of_population,
                         distribution_strategy=distribution_strategy,
                         random_state=random_state)

        self._mutations: List = []

    def append(self, mutation: Union[Callable, Mutation], *args, **kwargs):
        mutation_name = get_name(mutation)

        partial_mutation = partial(mutation, *args, **kwargs)
        partial_mutation.__name__ = mutation_name  # type: ignore[attr-defined]
        self._mutations.append(partial_mutation)

    @property
    def mutations(self):
        return self._mutations


class SimpleMutations(Mutations):
    def __init__(self,
                 portion_of_population: float = .9,
                 random_state: Optional[random.RandomState] = None):
        super().__init__(portion_of_population=portion_of_population,
                         distribution_strategy=None,
                         random_state=random_state)

        self.mutation_gaussian = MutationGaussian()
        self.mutation_uniform_integer = MutationUniformInteger()

    @property
    def mutations(self):
        partial_mutation_gaussian = partial(self.mutation_gaussian, random_state=self.random_state)
        partial_mutation_gaussian.__name__ = get_name(self.mutation_gaussian)

        partial_mutation_uniform_integer = partial(self.mutation_uniform_integer, random_state=self.random_state)
        partial_mutation_uniform_integer.__name__ = get_name(self.mutation_uniform_integer)

        return [partial_mutation_gaussian, partial_mutation_uniform_integer]


class Mutation(collections.abc.Callable):  # type: ignore[misc]
    def __call__(self, individual: Individual, random_state: Optional[Union[random.RandomState, int]] = None):  # pylint: disable=arguments-differ
        raise NotImplementedError

    def notify_statistics(self, individual, parameters=None, **kwargs):
        if not individual.statistics:
            return

        individual.statistics.append_variation(
            name=str(self),
            parameters=parameters,
            **kwargs)

    @staticmethod
    def handle_individual(individual: Individual):  # pylint: disable=unused-argument
        return True

    # NOTE inspired from https://stackoverflow.com/questions/9058305/getting-attributes-of-a-class
    def __str__(self):
        attributes = inspect.getmembers(self, lambda a: not inspect.isroutine(a))
        str_attributes = ""
        for a in attributes:
            if not (a[0].startswith('__') and a[0].endswith('__')) and a[0] != '_abc_impl' and a[0] != 'independent_probability':
                str_attributes += "__" + str(a[0]) + "__" + str(a[1])
        return f"{self.__class__.__name__}{str_attributes}"


class MutationGaussian(Mutation):
    def __init__(self,
                 mu: Union[float, Iterator, Sequence] = 0.,
                 sigma: Union[float, Iterator, Sequence] = 1.,
                 independent_probability: float = 1.):
        self.mu = mu
        self.sigma = sigma
        self.independent_probability = independent_probability

    def __call__(self,
                 individual: Individual,
                 random_state: Optional[Union[random.RandomState, int]] = None):

        random_state = random_state_parser(random_state)
        size = len(individual)

        mu = self.mu
        sigma = self.sigma

        if not isinstance(mu, collections.abc.Sequence):
            mu = repeat(mu, size)
        elif len(mu) < size:
            raise IndexError(f"mu must be at least the size of individual: {len(mu)} < {size}")

        if not isinstance(sigma, collections.abc.Sequence):
            sigma = repeat(sigma, size)
        elif len(sigma) < size:
            raise IndexError(f"sigma must be at least the size of individual: {len(sigma)} < {size}")

        for i, m, s in zip(range(size), mu, sigma):
            if random_state.random() < self.independent_probability:
                individual[i] += random_state.normal(m, s)
                self.notify_statistics(
                    individual=individual,
                    parameters={'mu': m, 'sigma': s},
                    attribute=i)

        return individual


class MutationUniformInteger(Mutation):
    def __init__(self,
                 lower_bound: Union[float, Iterator, Sequence] = 0.,
                 upper_bound: Union[float, Iterator, Sequence] = 2.,
                 independent_probability: float = 1.):
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
        self.independent_probability = independent_probability

    def __call__(self,
                 individual: Individual,
                 random_state: Optional[Union[random.RandomState, int]] = None):

        random_state = random_state_parser(random_state)
        size = len(individual)

        lower_bound = self.lower_bound
        upper_bound = self.upper_bound

        if not isinstance(lower_bound, collections.abc.Sequence):
            lower_bound = repeat(lower_bound, size)
        elif len(lower_bound) < size:
            raise IndexError(f"lower_bound must be at least the size of individual: {len(lower_bound)} < {size}")

        if not isinstance(upper_bound, collections.abc.Sequence):
            upper_bound = repeat(upper_bound, size)
        elif len(upper_bound) < size:
            raise IndexError(f"upper_bound must be at least the size of individual: {len(upper_bound)} < {size}")

        for i, xl, xu in zip(range(size), lower_bound, upper_bound):
            if random_state.random() < self.independent_probability:
                individual[i] += random_state.randint(xl, xu)
                self.notify_statistics(
                    individual=individual,
                    parameters={'lower_bound': xl, 'upper_bound': xu},
                    attribute=i)

        return individual


# TODO mutation that change order of attributes, using np.random.shuffle
# TODO add a mutation as a function to keep an example with no class
