# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)
import logging
from enum import Enum

from .evolutionary_algorithms.utils import random_state_parser


class INITIALIZATION_STRATEGY(Enum):
    RANDOM = "random"
    LHS = "lhs"


class Initialization:
    @staticmethod
    def strategy():
        raise NotImplementedError

    def get_estimator(self):
        raise NotImplementedError

    def get_preprocessor(self, component_index):
        raise NotImplementedError

    @property
    def default_hyperparameters(self):
        return self._default_hyperparameters

    # FIXME get default_hyperparameters from ea_space
    def __init__(self, min_size, max_size=None, default_hyperparameters=False):
        if max_size is None:
            max_size = min_size

        if min_size > max_size:
            logging.warning("min_size(%d) > max_size(%d), so we changed max_size = min_size", min_size, max_size)
            max_size = min_size

        self.min_size = min_size
        self.max_size = max_size

        self._default_hyperparameters = default_hyperparameters


class RandomInitialization(Initialization):
    @staticmethod
    def strategy():
        return INITIALIZATION_STRATEGY.RANDOM

    def get_estimator(self):
        return None

    def get_preprocessor(self, component_index):
        return None

    def __init__(self, min_size, max_size=None, default_hyperparameters=False):
        super().__init__(min_size=min_size, max_size=max_size, default_hyperparameters=default_hyperparameters)


class LHSInitialization(Initialization):
    @staticmethod
    def strategy():
        return INITIALIZATION_STRATEGY.LHS

    def __init__(self, min_size, population_size, search_space, random_state=None, max_size=None,
                 default_hyperparameters=False):
        super().__init__(min_size=min_size, max_size=max_size, default_hyperparameters=default_hyperparameters)

        self._index_rotors = [0] * self.max_size

        self.population_size = population_size
        self.search_space = search_space
        self.random_state = random_state_parser(random_state)

        self.pipeline_sizes = []

        # generate the pipeline sizes in advances, so we can see if LHS fits in it
        for _ in range(population_size):
            pipeline_size = self.random_state.randint(low=self.min_size, high=self.max_size + 1)
            self.pipeline_sizes.append(pipeline_size)

        self._max_size_present = max(self.pipeline_sizes)

        self._degraded_index = self.__estimate_degraded_index()

        # check if the population is large enough to try each element (algorithm + preprocessing methods) at least once
        if self._degraded_index is not None:
            logging.warning('LHS initialization potentially inefficient, works in degraded mode '
                            '-> increase the population size and/or the max size of the pipeline,'
                            ' and/or reduce the search space')

    def pop_pipeline_size(self):
        if len(self.pipeline_sizes) == 0:
            raise Exception('No more pipeline size available in the stack of the LHS initialization')
        return self.pipeline_sizes.pop(0)

    def get_estimator(self):
        if self._degraded_index == 0:
            logging.warning('LHS works in degraded mode, random estimator is selected')
            return self.random_state.choice(self.search_space.list_of_estimators)

        index = self._index_rotors[0]
        estimator = self.search_space.list_of_estimators[index]
        self._index_rotors[0] = (self._index_rotors[0] + 1) % len(self.search_space.list_of_estimators)

        if self._index_rotors[0] == 0:
            self._increment_rotors(index_rotor=1)

        return estimator

    def get_preprocessor(self, component_index):
        if component_index < 1:
            raise Exception('component_index < 1, please use get_estimator if it is 0')

        if self._degraded_index is not None and component_index >= self._degraded_index:
            logging.warning('LHS works in degraded mode,'
                            'random preprocessor is selected (degraded index: %d)', self._degraded_index)
            return self.random_state.choice(self.search_space.list_of_preprocessors)

        index = self._index_rotors[component_index]
        preprocessor = self.search_space.list_of_preprocessors[index]
        return preprocessor

    def _increment_rotors(self, index_rotor):
        if index_rotor >= len(self._index_rotors):
            return

        self._index_rotors[index_rotor] = ((self._index_rotors[index_rotor] + 1) %
                                           len(self.search_space.list_of_preprocessors))

        if self._index_rotors[index_rotor] == 0:
            self._increment_rotors(index_rotor=index_rotor + 1)

    def __estimate_degraded_index(self):
        sum_sizes = sum(self.pipeline_sizes)

        if self.population_size < len(self.search_space.list_of_estimators):
            return 0

        for size in range(self.min_size, self._max_size_present):
            if sum_sizes < self.search_space.algorithms_combinatorial_size(pipeline_size=size + 1):
                return size

        return None
