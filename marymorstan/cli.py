# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)
"""parse different available arguments for mary-morstan executable"""
import argparse
import json

from marymorstan.evaluation import EVALUATION_STRATEGY, SUMMARIZE_METHOD
from marymorstan.objectives import OBJECTIVE
from marymorstan.problems import PROBLEM_TYPE


def get_cli_args():
    parser = argparse.ArgumentParser(description='Mary Morstan')

    parser.add_argument(
        '--generations',
        action='store',
        dest='generations',
        default=100,
        type=int
    )

    parser.add_argument(
        '--population-size',
        action='store',
        dest='population_size',
        default=100,
        type=int
    )

    # TODO remove, now in EA space
    parser.add_argument(
        '--init-pipeline-size-min',
        action='store',
        dest='min_pipeline_size',
        default=3,
        type=int
    )

    # TODO remove, now in EA Space
    parser.add_argument(
        '--init-pipeline-size-max',
        action='store',
        dest='max_pipeline_size',
        default=3,
        type=int
    )

    # FIXME enable by default, could impact integration tests
    # TODO put that in EA space
    parser.add_argument(
        '--allow-fit-to-valid-pipeline',
        action='store_true',
        dest='allow_fit_to_valid_pipeline',
        default=False,
        help="Mechanism similar to TPOT (equivalent to decorator _pre_test) where each pipeline is fitted with a small generated samples in order to be considered as valid."
    )

    # TODO put that in EA space
    parser.add_argument(
        '--max-number-of-fits',
        action='store',
        dest='max_number_of_fits',
        default=5,
        type=int,
        help="Only works if --allow-fit-to-valid-pipeline is specified"
    )

    parser.add_argument(
        '--wall-time-in-seconds',
        action='store',
        dest='wall_time_optimization_seconds',
        type=int,
        default=None
    )

    # TODO put that in EA space
    parser.add_argument(
        '--budget-per-fit-to-valid-pipeline-seconds',
        action='store',
        dest='budget_per_fit_to_valid_pipeline_seconds',
        default=2,
        type=float,
        help="Only works if --allow-fit-to-valid-pipeline is specified"
    )

    parser.add_argument(
        '--evaluation-strategy',
        action='store',
        dest='evaluation_strategy',
        default=EVALUATION_STRATEGY.HOLDOUT,
        type=EVALUATION_STRATEGY,
        choices=list(EVALUATION_STRATEGY),
    )

    parser.add_argument(
        '--evaluation-summarize-method',
        action='store',
        dest='evaluation_summarize_method',
        default=SUMMARIZE_METHOD.MEAN,
        type=SUMMARIZE_METHOD,
        choices=list(SUMMARIZE_METHOD),
    )

    parser.add_argument(
        '--evaluation-test-size',
        action='store',
        dest='evaluation_test_size',
        default=.1,
        type=float,
        help="Used by stratified split strategies"
    )

    parser.add_argument(
        '--evaluation-n-splits',
        action='store',
        dest='evaluation_n_splits',
        default=5,
        type=int,
        help="Used by stratified split strategies"
    )

    parser.add_argument(
        '--problem-type',
        action='store',
        dest='problem_type',
        default=PROBLEM_TYPE.SUPERVISED_CLASSIFICATION_MULTICLASS,
        type=PROBLEM_TYPE,
        choices=list(PROBLEM_TYPE),
        help="Important to be specified for Time Series problem, it will disable the shuffle of the dataset"
    )

    parser.add_argument(
        '--objectives',
        action='store',
        dest='objectives',
        nargs='+',
        default=None,
        type=OBJECTIVE.argparse,
        choices=list(OBJECTIVE)
    )

    parser.add_argument(
        '--seed',
        action='store',
        dest='seed',
        default=None,
        type=int
    )

    parser.add_argument(
        '--dataset',
        action='store',
        dest='dataset',
        default="iris",
        type=str
    )

    parser.add_argument(
        '--dataset-preprocessing',
        action='store',
        dest='dataset_preprocessing',
        default="datasets.iris_dataset_preprocessing",
        type=str
    )

    parser.add_argument(
        '--dataset-fill-nan',
        action='store',
        dest='dataset_fill_nan',
        default=None
    )

    parser.add_argument(
        '--dataset-drop-columns-with-unique-value',
        action='store_true',
        dest='dataset_drop_columns_with_unique_value',
        default=False
    )

    parser.add_argument(
        '--test-size-ratio',
        action='store',
        dest='test_size_ratio',
        default=.25,
        type=float
    )

    parser.add_argument(
        '--enable-statistics',
        action='store_true',
        dest='enable_statistics',
        default=False
    )

    parser.add_argument(
        '--store-statistics',
        action='store',
        dest='statistics_file',
        default='statistics.parquet',
        type=str,
        help="Support .json and .parquet (require fastparquet library)"
    )

    parser.add_argument(
        '--log-level',
        action='store',
        dest='log_level',
        default='INFO',
        type=str
    )

    # parser.add_argument(
    #     '--dataset-mapper-file',
    #     action='store',
    #     dest='dataset_mapper_file',
    #     default=None,
    #     type=str
    # )

    parser.add_argument(
        '--search-space',
        action='store',
        dest='search_space_file',
        default=None,
        type=str
    )

    parser.add_argument(
        '--evolutionary-algorithms-space',
        action='store',
        dest='ea_space_file',
        default=None,
        type=str
    )

    parser.add_argument(
        '--evolutionary-algorithms-parameters',
        action='store',
        dest='ea_parameters',
        default=None,
        type=json.loads,
        help="Pass a dictionary with parameters in case you want customize in a different way than through the ea space file."
    )

    parser.add_argument(
        '--budget-per-candidate-seconds',
        action='store',
        dest='budget_per_candidate_seconds',
        default=300,
        type=float
    )

    parser.add_argument(
        '--number-of-jobs',
        action='store',
        dest='number_of_jobs',
        default=1,
        type=int,
        help="-1 to use all CPUs, 1 to not use parallelism"
    )

    parser.add_argument(
        '--number-of-pipeline-failed-allowed',
        action='store',
        dest='number_of_pipelines_failed_allowed',
        default=0,
        type=int,
        help="-1 to disable"
    )

    parser.add_argument(
        '--successive-halving',
        action='store_true',
        dest='successive_halving',
        default=False
    )

    parser.add_argument(
        '--successive-halving-minimum-population-size',
        dest='successive_halving_minimum_population_size',
        default=1.,
        type=float
    )

    parser.add_argument(
        '--successive-halving-initial-budget',
        dest='successive_halving_initial_budget',
        default=.1,
        type=float
    )

    parser.add_argument(
        '--successive-halving-maximum-budget',
        dest='successive_halving_maximum_budget',
        default=1.,
        type=float
    )

    parser.add_argument(
        '--print-best-pipeline-only',
        action='store_true',
        dest='print_best_pipeline_only',
        default=False
    )

    parser.add_argument(
        '--test-best-pipeline',
        action='store_true',
        dest='test_best_pipeline',
        default=False,
        help="The best pipeline issued from the optimization will be trained with the whole training set and tested with the whole test set."
    )

    return parser.parse_args()
