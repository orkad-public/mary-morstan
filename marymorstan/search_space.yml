version: 2

groups:
  estimators:
    - GaussianNB
    - BernoulliNB
    - MultinomialNB
    - DecisionTreeClassifier
    - ExtraTreesClassifier
    - RandomForestClassifier
    - GradientBoostingClassifier
    - KNeighborsClassifier
    - LogisticRegression
  preprocessors:
    - Binarizer
    - FastICA
    - Normalizer
    - StandardScaler
    - MaxAbsScaler
    - MinMaxScaler

algorithms:
  GaussianNB:
    backend: "sklearn"
    class_identifier: "naive_bayes.GaussianNB"
    forbiddens:
      previous:
        - FastICA
  BernoulliNB:
    backend: "sklearn"
    class_identifier: "naive_bayes.BernoulliNB"
    forbiddens:
      hyperparameters:
        - alpha <= 0
    hyperparameters:
      alpha:
        type: "categorical"
        choices: [0.001, 0.01, 0.1, 1.0, 10.0, 100.0]
        default: 1.
  MultinomialNB:
    backend: "sklearn"
    class_identifier: "naive_bayes.MultinomialNB"
    hyperparameters:
      alpha:
        type: "categorical"
        choices: [1e-3, 1e-2, 1e-1, 1., 10., 100.]
        default: 1.
      fit_prior:
        type: "categorical"
        choices: [True, False]
        default: True
    forbiddens:
      previous:
        - Normalizer
        - StandardScaler
        - MaxAbsScaler
        - FastICA
      hyperparameters:
        - alpha <= 0
  DecisionTreeClassifier:
    backend: "sklearn"
    class_identifier: "tree.DecisionTreeClassifier"
    hyperparameters:
      criterion:
        type: "categorical"
        choices: ["gini", "entropy"]
        default: "gini"
      max_depth:
        type: "categorical"
        range: (1, 11)
        default: None
      min_samples_split:
        type: "categorical"
        range: (2, 21)
        default: 2
      min_samples_leaf:
        type: "categorical"
        range: (1, 21)
        default: 1
  ExtraTreesClassifier:
    backend: "sklearn"
    class_identifier: "ensemble.ExtraTreesClassifier"
    forbiddens:
      hyperparameters:
        - max_features > 1. or max_features < 0
    hyperparameters:
      n_estimators:
        type: "categorical"
        choices: [100]
        default: 100
      criterion:
        type: "categorical"
        choices: ["gini", "entropy"]
        default: "gini"
      max_features:
        type: "categorical"
        arange: (0.05, 1.01, 0.05)
        default: "auto"
      min_samples_split:
        type: "categorical"
        range: (2, 21)
        default: 2
      min_samples_leaf:
        type: "categorical"
        range: (1, 21)
        default: 1
      bootstrap:
        type: "categorical"
        choices: [True, False]
        default: False
  RandomForestClassifier:
    backend: "sklearn"
    class_identifier: "ensemble.RandomForestClassifier"
    forbiddens:
      hyperparameters:
        - max_features > 1. or max_features < 0
    hyperparameters:
      n_estimators:
        type: "categorical"
        choices: [100]
        default: 100
      criterion:
        type: "categorical"
        choices: ["gini", "entropy"]
        default: "gini"
      max_features:
        type: "categorical"
        arange: (0.05, 1.01, 0.05)
        default: "auto"
      min_samples_split:
        type: "categorical"
        range: (2, 21)
        default: 2
      min_samples_leaf:
        type: "categorical"
        range: (1, 21)
        default: 1
      bootstrap:
        type: "categorical"
        choices: [True, False]
        default: False
  GradientBoostingClassifier:
    backend: "sklearn"
    class_identifier: "ensemble.GradientBoostingClassifier"
    forbiddens:
      hyperparameters:
        - learning_rate < 0
        - max_features > 1. or max_features < 0
        - subsample <= 0 or subsample > 1
    hyperparameters:
      n_estimators:
        type: "categorical"
        choices: [100]
        default: 100
      learning_rate:
        type: "categorical"
        choices: [1e-3, 1e-2, 1e-1, 0.5, 1.]
        default: .1
      max_depth:
        type: "categorical"
        range: (1, 11)
        default: 3
      min_samples_split:
        type: "categorical"
        range: (2, 21)
        default: 2
      min_samples_leaf:
        type: "categorical"
        range: (1, 21)
        default: 1
      subsample:
        type: "categorical"
        arange: (0.05, 1.01, 0.05)
        default: 1.
      max_features:
        type: "categorical"
        arange: (0.05, 1.01, 0.05)
        default: None
  KNeighborsClassifier:
    backend: "sklearn"
    class_identifier: "neighbors.KNeighborsClassifier"
    hyperparameters:
      n_neighbors:
        type: "categorical"
        range: (1, 101)
        default: 5
      weights:
        type: "categorical"
        choices: ["uniform", "distance"]
        default: "uniform"
      p:
        type: "categorical"
        choices: [1, 2]
        default: 2
  LogisticRegression:
    backend: "sklearn"
    class_identifier: "linear_model.LogisticRegression"
    limits:
      number_of_try_hyperparameters: 50
    forbiddens:
      hyperparameters:
        - dual == True and penalty == 'l1'
        - C < 0
        - solver == 'lbfgs' and (penalty == 'l1' or dual == True)
    hyperparameters:
      penalty:
        type: "categorical"
        choices: ["l1", "l2"]
        default: "l2"
      C:
        type: "categorical"
        choices: [1e-4, 1e-3, 1e-2, 1e-1, 0.5, 1., 5., 10., 15., 20., 25.]
        default: 1.
      dual:
        type: "categorical"
        choices: [True, False]
        default: False
      solver:
        type: "categorical"
        choices: ['lbfgs']
        default: 'lbfgs'
  Binarizer:
    backend: "sklearn"
    class_identifier: "preprocessing.Binarizer"
    hyperparameters:
      threshold:
        type: "categorical"
        arange: (0., 1.01, 0.05)
        default: 0.
  FastICA:
    backend: "sklearn"
    class_identifier: "decomposition.FastICA"
    forbiddens:
      previous:
        - Binarizer  # TODO not obvious case, found thanks to benchmark and manual try, could be found smartly
        - FastICA
    hyperparameters:
      tol:
        type: "categorical"
        arange: (0., 1.01, 0.05)
        default: .0001
  Normalizer:
    backend: "sklearn"
    class_identifier: "preprocessing.Normalizer"
    hyperparameters:
      norm:
        type: "categorical"
        choices: ["l1", "l2", "max"]
        default: "l1"
  StandardScaler:
    backend: "sklearn"
    class_identifier: "preprocessing.StandardScaler"
  MaxAbsScaler:
    backend: "sklearn"
    class_identifier: "preprocessing.MaxAbsScaler"
  MinMaxScaler:
    backend: "sklearn"
    class_identifier: "preprocessing.MinMaxScaler"
