# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)
import logging
from enum import Enum
import warnings
import time
from joblib import parallel_backend

import numpy as np
import pandas as pd
from stopit import TimeoutException, ThreadingTimeout

from sklearn.model_selection import train_test_split
from sklearn.model_selection._validation import _fit_and_score, _score
from sklearn.base import clone
from sklearn.model_selection import StratifiedKFold, StratifiedShuffleSplit, TimeSeriesSplit, KFold, ShuffleSplit

from .pipeline import compile_population
from .objectives import OBJECTIVE


class InvalidStrategy(Exception):
    pass


# Note see https://scikit-learn.org/stable/auto_examples/model_selection/plot_cv_indices.html#sphx-glr-auto-examples-model-selection-plot-cv-indices-py
class EVALUATION_STRATEGY(Enum):
    HOLDOUT = "holdout"
    K_FOLD = "k_fold"
    REPEAT_K_FOLD = "repeat_k_fold"
    SHUFFLE_SPLIT = "shuffle_split"
    STRATIFIED_K_FOLD = "stratified_k_fold"
    STRATIFIED_SHUFFLE_SPLIT = "stratified_shuffle_split"
    TIME_SERIES_K_SPLIT = "time_series_k_split"

    def __str__(self):
        return str(self.value)


class SUMMARIZE_METHOD(Enum):
    MEAN = "mean"
    MEDIAN = "median"


class Evaluation():
    def __init__(self, X_train, y_train,  # pylint: disable=too-many-locals
                 time_limit_seconds,
                 number_of_jobs,
                 objectives,
                 evaluation_strategy=EVALUATION_STRATEGY.HOLDOUT,
                 summarize_method=SUMMARIZE_METHOD.MEAN,
                 n_splits=5,
                 test_size=.1,
                 number_of_failed_allowed=0,
                 successive_halving=None,
                 statistics=None,
                 random_state=None,
                 global_stop_optimization_event=None):
        self._X_train = X_train
        self._y_train = y_train

        self.time_limit_seconds = time_limit_seconds
        self.number_of_jobs = number_of_jobs
        self.objectives = objectives

        self.evaluation_strategy = evaluation_strategy
        self.summarize_method = summarize_method
        self.n_splits = n_splits
        self.test_size = test_size
        self.number_of_failed_allowed = number_of_failed_allowed
        self.successive_halving = successive_halving

        self.statistics = statistics

        self.random_state = random_state

        self.global_stop_optimization_event = global_stop_optimization_event

        self._fail_fit_counter = 0
        self._current_iteration = 0

    @property
    def X_train(self):
        if self.successive_halving is not None:
            current_sh_budget = self.successive_halving.budget(current_iteration=self._current_iteration)
            return self.random_state.permutation(self._X_train)[:current_sh_budget]
        return self._X_train

    @property
    def y_train(self):
        if self.successive_halving is not None:
            current_sh_budget = self.successive_halving.budget(current_iteration=self._current_iteration)
            return self.random_state.permutation(self._y_train)[:current_sh_budget]
        return self._y_train

    @staticmethod
    def _summarize_scores_per_objective(scores, columns=None, method=SUMMARIZE_METHOD.MEAN):
        summarize_method = Evaluation._get_summarize_method(method=method)
        # NOTE the parameter 'columns' ensure the order of the objectives
        df = pd.DataFrame(scores, columns=columns)
        return df.apply(summarize_method, axis=0).tolist()

    @staticmethod
    def _get_summarize_method(method):
        if method == SUMMARIZE_METHOD.MEAN:
            summarize_method = np.nanmean
        elif method == SUMMARIZE_METHOD.MEDIAN:
            summarize_method = np.nanmedian
        else:
            raise Exception("Summarize method not handled")

        return summarize_method

    def _holdout(self, pipeline):
        X_train, X_test, y_train, y_test = train_test_split(self.X_train, self.y_train, test_size=self.test_size, random_state=self.random_state)
        fitted_pipeline = pipeline.fit(X_train, y_train)
        scores = list(_score(fitted_pipeline, X_test, y_test, self.objectives.scorers).values())
        return scores

    def __fold_split(self, fold_method, pipeline):
        validation_scores = []

        for train_index, test_index in fold_method.split(self.X_train, self.y_train):
            returns = _fit_and_score(estimator=clone(pipeline),  # pylint: disable=unbalanced-tuple-unpacking
                                     X=self.X_train,
                                     y=self.y_train,
                                     scorer=self.objectives.scorers,
                                     train=train_index,
                                     test=test_index,
                                     verbose=0,
                                     parameters=None,
                                     fit_params=None,
                                     return_estimator=False,
                                     error_score='raise')

            validation_scores.append(returns['test_scores'])

        return Evaluation._summarize_scores_per_objective(scores=validation_scores, columns=self.objectives.scorers.keys(), method=self.summarize_method)

    def _k_fold(self, pipeline, shuffle=False):
        kwargs = {}
        if shuffle:
            kwargs = {'random_state': self.random_state}
        kf = KFold(n_splits=self.n_splits, shuffle=shuffle, **kwargs)
        return self.__fold_split(kf, pipeline)

    def _repeat_k_fold(self, pipeline, n_repeat=5):
        summarize_method = Evaluation._get_summarize_method(method=self.summarize_method)
        results = []

        for _ in range(n_repeat):
            results.append(self._k_fold(pipeline, shuffle=True))

        return summarize_method(results, axis=0)

    def _shuffle_split(self, pipeline):
        sp = ShuffleSplit(n_splits=self.n_splits, test_size=self.test_size, random_state=self.random_state)
        return self.__fold_split(sp, pipeline)

    def _stratified_shuffle_split(self, pipeline):
        sss = StratifiedShuffleSplit(n_splits=self.n_splits, test_size=self.test_size, random_state=self.random_state)
        return self.__fold_split(sss, pipeline)

    def _stratified_k_fold(self, pipeline, shuffle=False):
        kwargs = {}
        if shuffle:
            kwargs = {'random_state': self.random_state}
        skf = StratifiedKFold(n_splits=self.n_splits, shuffle=shuffle, **kwargs)
        return self.__fold_split(skf, pipeline)

    def _timeseries_k_split(self, pipeline):
        tscv = TimeSeriesSplit(n_splits=self.n_splits)
        return self.__fold_split(tscv, pipeline)

    @staticmethod
    def check_is_valid_strategy(strategy):
        for known_strategy in EVALUATION_STRATEGY:
            if strategy == known_strategy:
                return strategy

        raise InvalidStrategy(f"The '{strategy}' strategy is unknown")

    def _evaluation(self, compiled_pipeline, evaluation_strategy, **kwargs):  # pylint: disable=inconsistent-return-statements,too-many-return-statements
        if evaluation_strategy == EVALUATION_STRATEGY.HOLDOUT:
            return self._holdout(pipeline=compiled_pipeline, **kwargs)

        if evaluation_strategy == EVALUATION_STRATEGY.K_FOLD:
            return self._k_fold(pipeline=compiled_pipeline, **kwargs)

        if evaluation_strategy == EVALUATION_STRATEGY.REPEAT_K_FOLD:
            return self._repeat_k_fold(pipeline=compiled_pipeline, **kwargs)

        if evaluation_strategy == EVALUATION_STRATEGY.SHUFFLE_SPLIT:
            return self._shuffle_split(pipeline=compiled_pipeline, **kwargs)

        if evaluation_strategy == EVALUATION_STRATEGY.STRATIFIED_SHUFFLE_SPLIT:
            return self._stratified_shuffle_split(pipeline=compiled_pipeline, **kwargs)

        if evaluation_strategy == EVALUATION_STRATEGY.STRATIFIED_K_FOLD:
            return self._stratified_k_fold(pipeline=compiled_pipeline, **kwargs)

        if evaluation_strategy == EVALUATION_STRATEGY.TIME_SERIES_K_SPLIT:
            return self._timeseries_k_split(pipeline=compiled_pipeline, **kwargs)

    def evaluate(self, pipelines, current_iteration=0, evaluation_strategy=None, **kwargs):  # pylint: disable=too-many-branches,too-many-locals,too-many-statements
        if evaluation_strategy is None:
            evaluation_strategy = self.evaluation_strategy

        Evaluation.check_is_valid_strategy(evaluation_strategy)

        self._current_iteration = current_iteration

        logging.info("Will compile and evaluate %d pipelines for iteration %d", len(pipelines), current_iteration)
        compiled_pipelines = compile_population(pipelines)

        for compiled_pipeline, pipeline in zip(compiled_pipelines, pipelines):
            if self.global_stop_optimization_event is not None and self.global_stop_optimization_event.is_set():
                break

            validation_score = self.objectives.scorer_default_values

            pipeline.meta_data.evaluation_values.update(dict(zip(self.objectives.scorer_keys, validation_score)))

            if self.objectives.min_pipeline_size_present:
                pipeline.meta_data.evaluation_values[OBJECTIVE.MIN_PIPELINE_SIZE] = pipeline.complexity()

            pipeline.fitness.values = self.objectives.ordered_values_for_objectives(pipeline.meta_data.evaluation_values)

            try:
                logging.debug("Train and evaluate pipeline %s (hash: %s - id: %s) ...", pipeline, pipeline.hash(), pipeline.identifier)

                if self.objectives.min_training_time_present:
                    start_training_time = time.time()

                with ThreadingTimeout(self.time_limit_seconds, swallow_exc=False), parallel_backend('threading', n_jobs=self.number_of_jobs), warnings.catch_warnings():
                    warnings.filterwarnings("ignore")
                    validation_score = self._evaluation(compiled_pipeline, evaluation_strategy, **kwargs)

                if self.objectives.min_training_time_present:
                    training_time = time.time() - start_training_time
                    pipeline.meta_data.evaluation_values[OBJECTIVE.MIN_TRAINING_TIME] = training_time

                pipeline.meta_data.evaluation_values.update(dict(zip(self.objectives.scorer_keys, validation_score)))
                pipeline.fitness.values = self.objectives.ordered_values_for_objectives(pipeline.meta_data.evaluation_values)
                logging.debug("... %s obtained validation_score corresponding to the objectives %s", validation_score, list(self.objectives.scorers.keys()))

                if self.statistics:
                    self.statistics.set_best_individual(individuals=[pipeline], generation=current_iteration)
            except TimeoutException as e:
                logging.warning("It took too much time to evaluate the pipeline "
                                "%s (hash: %s - id: %s), skip evaluation, score will be null; message: %s", pipeline, pipeline.hash(), pipeline.identifier, e)
                pipeline.fitness.values = self.objectives.ordered_values_for_objectives(pipeline.meta_data.evaluation_values)

                if self.statistics:
                    self.statistics.append_failing_pipeline(generation=pipeline.meta_data.current_iteration, pipeline=pipeline)
            except Exception as e:  # pylint: disable=broad-except
                message = (f"Impossible to evaluate the pipeline "
                           f"(hash: {pipeline.hash()} - id: {pipeline.identifier}) {pipeline} -> {e}")
                logging.error(message)
                self._fail_fit_counter += 1

                if self._fail_fit_counter > self.number_of_failed_allowed and self.number_of_failed_allowed != -1:
                    logging.error('The limit of pipeline allowed to fail of has been reached, raise last exception')

                    # if global_stop_optimization_event is not None and not global_stop_optimization_event.is_set():
                    #    global_stop_optimization_event.set()

                    raise e

                pipeline.fitness.values = self.objectives.ordered_values_for_objectives(pipeline.meta_data.evaluation_values)

                if self.statistics:
                    self.statistics.append_failing_pipeline(generation=pipeline.meta_data.current_iteration, pipeline=pipeline)
            finally:
                del pipeline.meta_data.evaluation_values
