# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)
from .evolutionary_algorithms import EA, SimpleEA, CustomizableEA  # noqa: F401
