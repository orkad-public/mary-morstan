# copyright: mary-morstan developers (see Authors file), GPL v3 License (see LICENSE file)
from marymorstan.search_space import SearchSpace
from marymorstan.pipeline import MLPipeline, MLPipelineComponent, MLPipelineComponentType


class MLPipelineCompiler():
    def __init__(self, search_space=None):
        if not search_space:
            search_space = SearchSpace()
        self.search_space = search_space

    @staticmethod
    def extract_subcomponents(raw_subcomponents, search_space_component_node):
        subcomponents = {}

        for key, hyperparameters in raw_subcomponents.items():
            if key.startswith('subcomponent:'):
                component_type = MLPipelineComponentType(key.split(':')[1])
                component_name = key.split(':')[2]
                subcomponents[component_name] = MLPipelineComponent(name=component_name,
                                                                    component_type=component_type,
                                                                    hyperparameters=hyperparameters,
                                                                    search_space_component_node=search_space_component_node['hyperparameters'][component_name],
                                                                    subcomponents={})

        return subcomponents

    @staticmethod
    def extract_hyperparameters(raw_component):
        raw_subcomponents = {}
        hyperparameters = {}

        raw_hyperparameters = eval(raw_component.split('(')[1][:-1])  # pylint: disable=eval-used

        for key, value in raw_hyperparameters.items():
            if key.startswith('subcomponent:'):
                raw_subcomponents[key] = value
            else:
                hyperparameters[key] = value

        return hyperparameters, raw_subcomponents

    def from_raw(self, raw_pipeline):
        if isinstance(raw_pipeline, str):
            raw_pipeline = eval(raw_pipeline)  # pylint: disable=eval-used

        if not isinstance(raw_pipeline, list):
            raise ValueError(f'Do not handle a pipeline of type {type(raw_pipeline)}')

        if len(raw_pipeline) == 0:
            return None

        pipeline = MLPipeline(generate=False, search_space=self.search_space)

        raw_pipeline.reverse()

        raw_component = raw_pipeline.pop()
        component_name = raw_component.split('(')[0]
        search_space_component_node = self.search_space.estimators[component_name]
        component_hyperparameters, raw_subcomponents = MLPipelineCompiler.extract_hyperparameters(raw_component)
        component_subcomponents = MLPipelineCompiler.extract_subcomponents(raw_subcomponents, search_space_component_node)
        component = MLPipelineComponent(name=component_name,
                                        component_type=MLPipelineComponentType.ESTIMATOR,
                                        hyperparameters=component_hyperparameters,
                                        search_space_component_node=search_space_component_node,
                                        subcomponents=component_subcomponents)
        pipeline.append(component)

        while len(raw_pipeline) > 0:
            raw_component = raw_pipeline.pop()
            component_name = raw_component.split('(')[0]
            search_space_component_node = self.search_space.preprocessors[component_name]
            component_hyperparameters, raw_subcomponents = MLPipelineCompiler.extract_hyperparameters(raw_component)
            component_subcomponents = MLPipelineCompiler.extract_subcomponents(raw_subcomponents, search_space_component_node)
            component = MLPipelineComponent(name=component_name,
                                            component_type=MLPipelineComponentType.PREPROCESSOR,
                                            hyperparameters=component_hyperparameters,
                                            search_space_component_node=search_space_component_node,
                                            subcomponents=component_subcomponents)
            pipeline.append(component)

        return pipeline

    def compile(self, raw_pipeline):
        return self.from_raw(raw_pipeline).compile()
