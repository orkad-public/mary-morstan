ENV_VERSION?=latest
SPECIFIC_TEST?=tests/
SPECIFIC_BENCHMARK?=benchmarks/

DEBUG ?= 0

PYTEST_DEBUG=""

ifeq ($(DEBUG),1)
	PYTEST_DEBUG="--log-cli-level=-1"
endif

PDB ?= 0


tests: check-code unit-tests integration-tests
		

check-code:
	./misc/check-code.sh

unit-tests:
	./misc/unit-tests.sh $(PYTEST_DEBUG) $(SPECIFIC_TEST) $(PDB)

integration-tests:
	./misc/integration-tests.sh

benchmark-tests:
	./misc/benchmark-tests.sh $(PYTEST_DEBUG) $(SPECIFIC_BENCHMARK)

